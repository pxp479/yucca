#include <yucca/input/read_input.hpp>
#include <yucca/scf/scf.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/structure/load_geometry.hpp>
#include <yucca/util/libint_interface.hpp>

#include "catch_amalgamated.hpp"
#include "yucca_test.h"

using namespace std;
using namespace yucca;
using namespace Catch;

static string examples = YUCCA_EXAMPLES_DIR;
static string molecules = YUCCA_MOLECULES_DIR;

TEST_CASE("CIS: H2O (def2-SVP)", "[CIS]")
{
  libint2::initialize();

  InputNode input = yucca::read_input(examples + "cis_h2o_def2-svp.yml");
  auto scfinput = input.get_node("scf");

  auto molfile = scfinput.get<string>("molecule");
  scfinput.put<string>("molecule", molecules + molfile);

  auto [geometry, wfn] = load_geometry(scfinput);

  auto hf = get_method(scfinput, geometry);
  hf->compute();

  REQUIRE(hf->energy() == Approx(-75.9613370199).epsilon(1e-7));

  wfn = hf->wavefunction();
  auto cisinput = input.get_node("cis");
  auto cis = get_method(cisinput, wfn);
  cis->compute();
  REQUIRE(cis->energy() == Approx(0.34612258).epsilon(1e-5));

  libint2::finalize();
}

TEST_CASE("TDDFT-ris: H2O (def2-SVP)", "[CIS]")
{
  libint2::initialize();

  InputNode input = yucca::read_input(examples + "tddft-ris-h2o-svp.yml");
  auto scfinput = input.get_node("scf");

  auto molfile = scfinput.get<string>("molecule");
  scfinput.put<string>("molecule", molecules + molfile);

  auto [geometry, wfn] = load_geometry(scfinput);

  auto hf = get_method(scfinput, geometry);
  hf->compute();

  REQUIRE(hf->energy() == Approx(-76.27558507).epsilon(1e-7));

  wfn = hf->wavefunction();
  auto cisinput = input.get_node("ris");

  tie(geometry, wfn) = load_geometry(cisinput, geometry, wfn);
  auto cis = get_method(cisinput, wfn);
  cis->compute();
  REQUIRE(cis->energy() == Approx(0.28669570).epsilon(1e-5));

  libint2::finalize();
}

TEST_CASE("TDDFT-rispd: H2O (def2-SVP)", "[CIS]")
{
  libint2::initialize();

  InputNode input = yucca::read_input(examples + "tddft-rispd-h2o-svp.yml");
  auto scfinput = input.get_node("scf");

  auto molfile = scfinput.get<string>("molecule");
  scfinput.put<string>("molecule", molecules + molfile);

  auto [geometry, wfn] = load_geometry(scfinput);

  auto hf = get_method(scfinput, geometry);
  hf->compute();

  REQUIRE(hf->energy() == Approx(-76.2740044626).epsilon(1e-7));

  wfn = hf->wavefunction();
  auto cisinput = input.get_node("ris");

  tie(geometry, wfn) = load_geometry(cisinput, geometry, wfn);
  auto cis = get_method(cisinput, wfn);
  cis->compute();
  REQUIRE(cis->energy() == Approx(0.2863877064).epsilon(1e-5));

  libint2::finalize();
}
