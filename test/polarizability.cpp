#include <yucca/input/read_input.hpp>
#include <yucca/scf/scf.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/structure/load_geometry.hpp>
#include <yucca/util/libint_interface.hpp>

#include "catch_amalgamated.hpp"
#include "yucca_test.h"

using namespace std;
using namespace yucca;
using namespace Catch;

static string examples = YUCCA_EXAMPLES_DIR;
static string molecules = YUCCA_MOLECULES_DIR;

TEST_CASE("polarizability: H2O (def2-SVP)", "[polarizability]")
{
  libint2::initialize();

  InputNode input = yucca::read_input(examples + "hf-polar-df_h2o_def2-svp.yml");
  auto scfinput = input.get_node("scf");

  auto molfile = scfinput.get<string>("molecule");
  scfinput.put<string>("molecule", molecules + molfile);
  auto [geometry, wfn] = load_geometry(scfinput);

  auto hf = get_method(scfinput, geometry);
  hf->compute();

  REQUIRE(hf->energy() == Approx(-75.9612813706).epsilon(1e-7));

  wfn = hf->wavefunction();
  auto polinput = input.get_node("polarizability");
  auto pol = get_method(polinput, wfn);
  pol->compute();

  // polarizability trace / 3
  CHECK(pol->energy() == Approx(4.800194).epsilon(1e-5));

  auto alpha = pol->energies();
  CHECK(alpha[0] == Approx(6.572662).epsilon(1e-5));
  CHECK(alpha[4] == Approx(2.983922).epsilon(1e-5));
  CHECK(alpha[8] == Approx(4.843998).epsilon(1e-5));

  libint2::finalize();
}

TEST_CASE("polarizability with df: H2O (def2-SVP)", "[polarizability]")
{
  libint2::initialize();

  InputNode input = yucca::read_input(examples + "hf-polar_h2o_def2-svp.yml");
  auto scfinput = input.get_node("scf");

  auto molfile = scfinput.get<string>("molecule");
  Molecule molecule(molecules + molfile);
  auto basis = make_shared<BasisSet>(scfinput.get<string>("basis"), ".");
  auto dfbasis = scfinput.contains("df-basis") ?
                     make_shared<BasisSet>(scfinput.get<string>("df-basis"), ".") :
                     shared_ptr<BasisSet>();
  auto geometry = make_shared<Geometry>(molecule, basis, dfbasis);

  auto hf = get_method(scfinput, geometry);
  hf->compute();

  REQUIRE(hf->energy() == Approx(-75.96133702).epsilon(1e-7));

  auto wfn = hf->wavefunction();
  auto polinput = input.get_node("polarizability");
  auto pol = get_method(polinput, wfn);
  pol->compute();

  // polarizability trace / 3
  CHECK(pol->energy() == Approx(4.800499).epsilon(1e-5));

  auto alpha = pol->energies();
  CHECK(alpha[0] == Approx(6.57296377).epsilon(1e-5));
  CHECK(alpha[4] == Approx(2.98440599).epsilon(1e-5));
  CHECK(alpha[8] == Approx(4.84412862).epsilon(1e-5));

  libint2::finalize();
}
