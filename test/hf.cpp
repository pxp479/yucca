#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor_dot.hpp>

#include <yucca/dft/integrate_potential.hpp>
#include <yucca/dft/molecular_grid.hpp>
#include <yucca/input/read_input.hpp>
#include <yucca/scf/scf.hpp>
#include <yucca/structure/compute_densities.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/structure/load_geometry.hpp>
#include <yucca/util/libint_interface.hpp>
#include <yucca/wfn/single_reference.hpp>

#include "catch_amalgamated.hpp"
#include "yucca_test.h"

using namespace std;
using namespace yucca;
using namespace Catch;

static string examples = YUCCA_EXAMPLES_DIR;
static string molecules = YUCCA_MOLECULES_DIR;

shared_ptr<yucca::Method_> test_method(string inp)
{
  InputNode input = yucca::read_input(examples + inp);
  auto scfinput = input.get_node("scf");

  auto molfile = scfinput.get<string>("molecule");
  scfinput.put<string>("molecule", molecules + molfile);

  auto [geometry, wfn] = load_geometry(scfinput);

  auto method = get_method(scfinput, geometry);

  return method;
}

TEST_CASE("Hartree-Fock: H2O (def2-SVP)", "[SCF]")
{
  libint2::initialize();

  SECTION("Water integral direct")
  {
    auto method = test_method("hf_h2o_def2-svp.yml");
    method->compute();

    CHECK(method->energy() == Approx(-75.9613370199).epsilon(1e-7));

    auto gradient = method->gradient();
    CHECK(gradient(0, 0) == Approx(0.0).margin(1e-12));
    CHECK(gradient(1, 0) == Approx(0.0).margin(1e-12));
    CHECK(gradient(2, 0) == Approx(-0.001190318).epsilon(1e-3));

    CHECK(gradient(0, 1) == Approx(0.0003255595).epsilon(1e-4));
    CHECK(gradient(1, 1) == Approx(0.0).margin(1e-12));
    CHECK(gradient(2, 1) == Approx(0.000595159).epsilon(1e-3));

    CHECK(gradient(0, 2) == Approx(-0.0003255595).epsilon(1e-4));
    CHECK(gradient(1, 2) == Approx(0.0).margin(1e-12));
    CHECK(gradient(2, 2) == Approx(0.000595159).epsilon(1e-3));
  }

  SECTION("Water RI")
  {
    auto method = test_method("hf-rijk_h2o_def2-svp.yml");
    method->compute();

    CHECK(method->energy() == Approx(-75.9612813705).epsilon(1e-7));

    auto gradient = method->gradient();
    CHECK(gradient(0, 0) == Approx(0.0).margin(1e-12));
    CHECK(gradient(1, 0) == Approx(0.0).margin(1e-12));
    CHECK(gradient(2, 0) == Approx(-0.0012043114).epsilon(1e-3));

    CHECK(gradient(0, 1) == Approx(0.0003228886).epsilon(1e-4));
    CHECK(gradient(1, 1) == Approx(0.0).margin(1e-12));
    CHECK(gradient(2, 1) == Approx(0.0006021557).epsilon(1e-3));

    CHECK(gradient(0, 2) == Approx(-0.0003228886).epsilon(1e-4));
    CHECK(gradient(1, 2) == Approx(0.0).margin(1e-12));
    CHECK(gradient(2, 2) == Approx(0.0006021557).epsilon(1e-3));

    shared_ptr<Wfn> wfn = method->wavefunction();
    shared_ptr<SingleReference> scfwfn = dynamic_pointer_cast<SingleReference>(wfn);
    REQUIRE(scfwfn);

    auto ccocc = scfwfn->coeffs()->slice(0, scfwfn->nocc());
    auto rho = qleve::gemm("n", "t", 2.0, ccocc, ccocc);

    InputNode finp;
    finp.put("grid level", 3ll);
    finp.put("prune grid", false);
    MolecularGrid mgrid(finp, wfn->geometry()->atoms());

    auto densities =
        compute_densities_gradients(*scfwfn->geometry()->orbital_basis(), rho, mgrid.points());
    const double integrated_density = qleve::dot_product(densities.pluck(0), mgrid.weights());
    const double integrated_dx = qleve::dot_product(densities.pluck(1), mgrid.weights());
    const double integrated_dy = qleve::dot_product(densities.pluck(2), mgrid.weights());
    const double integrated_dz = qleve::dot_product(densities.pluck(3), mgrid.weights());

    CHECK(densities.extent(0) == 18760);
    REQUIRE(densities.extent(1) == 4);

    CHECK(integrated_density == Approx(10.0001804695));
    CHECK(integrated_dx == Approx(0.0).margin(1e-4));
    CHECK(integrated_dy == Approx(0.0).margin(1e-4));
    CHECK(integrated_dz == Approx(0.0).margin(1e-4));
  }

  SECTION("Water Triplet UHF")
  {
    auto method = test_method("uhf_h2o_svp.yml");
    method->compute();

    CHECK(method->energy() == Approx(-75.70798674).epsilon(1e-7));
  }

  SECTION("Water Triplet UHF")
  {
    auto method = test_method("uhf_rijk_h2o_svp.yml");
    method->compute();

    CHECK(method->energy() == Approx(-75.70793506).epsilon(1e-7));
  }

  SECTION("Water LDA")
  {
    auto method = test_method("lda-rij_h2o_def2-svp.yml");
    method->compute();

    CHECK(method->energy() == Approx(-75.7750800013).epsilon(1e-7));
  }

  SECTION("Water PBE")
  {
    auto method = test_method("pbe-rij_h2o_def2-svp.yml");
    method->compute();

    CHECK(method->energy() == Approx(-76.25127185).epsilon(1e-7));
  }

  SECTION("Water PBE0")
  {
    auto method = test_method("pbe0-rij_h2o_def2-svp.yml");
    method->compute();

    CHECK(method->energy() == Approx(-76.2758539193).epsilon(1e-7));
  }

  libint2::finalize();
}
