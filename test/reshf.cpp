#include <yucca/input/read_input.hpp>
#include <yucca/resmf/reshf.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/util/libint_interface.hpp>

#include "catch_amalgamated.hpp"
#include "yucca_test.h"

using namespace std;
using namespace yucca;
using namespace Catch;

static string examples = YUCCA_EXAMPLES_DIR;
static string molecules = YUCCA_MOLECULES_DIR;

shared_ptr<yucca::Method_> test_reshf(string inp)
{
  InputNode input = yucca::read_input(examples + inp);
  auto scfinput = input.get_node("scf");

  auto molfile = scfinput.get<string>("molecule");
  Molecule molecule(molecules + molfile);
  auto basis = make_shared<BasisSet>(scfinput.get<string>("basis"), ".");
  auto dfbasis = scfinput.contains("df-basis") ?
                     make_shared<BasisSet>(scfinput.get<string>("df-basis"), ".") :
                     shared_ptr<BasisSet>();
  auto geometry = make_shared<Geometry>(molecule, basis, dfbasis);

  auto hf = get_method(scfinput, geometry);
  hf->compute();

  auto cisinput = input.get_node("cis");
  auto hf_wfn = hf->wavefunction();
  auto cis = get_method(cisinput, hf_wfn);
  cis->compute();

  auto cis_wfn = cis->wavefunction();
  auto reshfinput = input.get_node("reshf");
  auto reshf = get_method(reshfinput, cis_wfn);

  return reshf;
}

TEST_CASE("Resonating Hartree-Fock: H2O (def2-SVP)", "[ResHF]")
{
  libint2::initialize();

  SECTION("Testing full functionality of ResHF initial guess code")
  {
    auto method = test_reshf("reshf-allguess-transform_h2o_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    REQUIRE(reshf->nSD() == 7);
  }

  SECTION("Reconstructing HF from ResHF")
  {
    auto method = test_reshf("reshf-hf-reconst_h2o_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    REQUIRE(reshf->energy() == Approx(-75.9612813705).epsilon(1e-7));
  }

  SECTION("Reconstructing CIS from ResHF - no NTO transform")
  {
    auto method = test_reshf("reshf-cis-reconst-notransform_h2o_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    auto energies = reshf->Ek();
    REQUIRE(energies->at(1) == Approx(-84.92231242).epsilon(1e-5));
    REQUIRE(energies->at(2) == Approx(-84.85606382).epsilon(1e-5));
  }

  SECTION("Reconstructing CIS from ResHF - with NTO transform")
  {
    auto method = test_reshf("reshf-cis-reconst-transform_h2o_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    auto energies = reshf->Ek();
    REQUIRE(energies->at(1) == Approx(-84.92231242).epsilon(1e-5));
    REQUIRE(energies->at(2) == Approx(-84.85606382).epsilon(1e-5));
  }

  SECTION("Testing ResHF SCF Convergence with Matrix Adjugate Implementation")
  {
    auto method = test_reshf("reshf-matadj-conv-scf_h2o_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    auto energies = reshf->Ek();
    REQUIRE(energies->at(0) == Approx(-85.26875221).epsilon(1e-5));
    REQUIRE(energies->at(1) == Approx(-84.98752448).epsilon(1e-5));
    REQUIRE(energies->at(2) == Approx(-84.46058148).epsilon(1e-5));

    double orb_gradient = reshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }

  SECTION("Testing ResHF SCF Convergence with non-Matrix Adjugate Implementation")
  {
    auto method = test_reshf("reshf-nomatadj-conv-scf_h2o_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    auto energies = reshf->Ek();
    REQUIRE(energies->at(0) == Approx(-85.26875221).epsilon(1e-5));
    REQUIRE(energies->at(1) == Approx(-84.98752448).epsilon(1e-5));
    REQUIRE(energies->at(2) == Approx(-84.46058148).epsilon(1e-5));

    double orb_gradient = reshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }

  SECTION("Testing ResHF FDNR Convergence with Matrix Adjugate Implementation")
  {
    auto method = test_reshf("reshf-matadj-conv-fdnr_h2o_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    auto energies = reshf->Ek();
    REQUIRE(energies->at(0) == Approx(-85.26875221).epsilon(1e-5));
    REQUIRE(energies->at(1) == Approx(-84.98752448).epsilon(1e-5));
    REQUIRE(energies->at(2) == Approx(-84.46058148).epsilon(1e-5));

    double orb_gradient = reshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }

  SECTION("Testing ResHF FDNR Convergence with non-Matrix Adjugate Implementation")
  {
    auto method = test_reshf("reshf-nomatadj-conv-fdnr_h2o_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    auto energies = reshf->Ek();
    REQUIRE(energies->at(0) == Approx(-85.26875221).epsilon(1e-5));
    REQUIRE(energies->at(1) == Approx(-84.98752448).epsilon(1e-5));
    REQUIRE(energies->at(2) == Approx(-84.46058148).epsilon(1e-5));

    double orb_gradient = reshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }

  libint2::finalize();
}

TEST_CASE("Resonating Hartree-Fock: H2 (def2-SVP)", "[ResHF]")
{
  libint2::initialize();
  SECTION("Reconstructing two_det_CI from ResHF - with orbital rotations")
  {
    auto method = test_reshf("reshf-2ci-reconst_h2-1.1bond_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    REQUIRE(reshf->energy() == Approx(-1.09287975).epsilon(1e-7));
  }

  libint2::finalize();
}

TEST_CASE("Resonating Hartree-Fock: CH2CH2 (def2-SVP)", "[ResHF]")
{
  libint2::initialize();

  SECTION("Testing ResHF SCF Convergence with Matrix Adjugate Implementation")
  {
    auto method = test_reshf("reshf-matadj-conv-scf_ethene_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    auto energies = reshf->Ek();
    REQUIRE(energies->at(0) == Approx(-111.32941541).epsilon(1e-5));
    REQUIRE(energies->at(1) == Approx(-110.98140912).epsilon(1e-5));

    double orb_gradient = reshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }

  SECTION("Testing ResHF SCF Convergence with non-Matrix Adjugate Implementation")
  {
    auto method = test_reshf("reshf-nomatadj-conv-scf_ethene_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    auto energies = reshf->Ek();
    REQUIRE(energies->at(0) == Approx(-111.32941541).epsilon(1e-5));
    REQUIRE(energies->at(1) == Approx(-110.98140912).epsilon(1e-5));

    double orb_gradient = reshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }

  SECTION("Testing ResHF FDNR Convergence with Matrix Adjugate Implementation")
  {
    auto method = test_reshf("reshf-matadj-conv-fdnr_ethene_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    auto energies = reshf->Ek();
    REQUIRE(energies->at(0) == Approx(-111.32941541).epsilon(1e-5));
    REQUIRE(energies->at(1) == Approx(-110.98140912).epsilon(1e-5));

    double orb_gradient = reshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }

  SECTION("Testing ResHF FDNR Convergence with non-Matrix Adjugate Implementation")
  {
    auto method = test_reshf("reshf-nomatadj-conv-fdnr_ethene_def2-svp.yml");
    method->compute();

    auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

    if (!reshf) {
      throw runtime_error("Dynamic cast of base method to ResHF method failed!");
    }

    auto energies = reshf->Ek();
    REQUIRE(energies->at(0) == Approx(-111.32941541).epsilon(1e-5));
    REQUIRE(energies->at(1) == Approx(-110.98140912).epsilon(1e-5));

    double orb_gradient = reshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }

  libint2::finalize();
}
