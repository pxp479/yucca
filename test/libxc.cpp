#include <random>
#include <yucca_test.h>

#include <fmt/core.h>

#include <yucca/dft/libxc_interface.hpp>

#include "catch_amalgamated.hpp"

using namespace std;
using namespace yucca;
using namespace qleve;
using namespace Catch;

TEST_CASE("LibXC", "[libxc]")
{
  SECTION("Version")
  {
    const string version = libxc::get_version();
    CHECK(version[0] >= 5);
  }

  SECTION("Reference")
  {
    const string xcref = libxc::get_libxc_reference();
    CHECK(xcref != "");
  }

  SECTION("Functional ID")
  {
    CHECK(libxc::get_functional_id("lda_x") == 1);
    CHECK(libxc::get_functional_id("lda_c_vwn") == 7);

    CHECK(libxc::get_functional_id("gga_c_pbe") == 130);
    CHECK(libxc::get_functional_id("xc_gga_x_pbe") == 101);

    CHECK(libxc::get_functional_id("xc_mgga_x_tpss") == 202);
    CHECK(libxc::get_functional_id("xc_mgga_c_tpss") == 231);
  }

  SECTION("Functional Name")
  {
    CHECK(libxc::get_functional_name(1) == "lda_x");
    CHECK(libxc::get_functional_name(7) == "lda_c_vwn");

    CHECK(libxc::get_functional_name(130) == "gga_c_pbe");
    CHECK(libxc::get_functional_name(101) == "gga_x_pbe");

    CHECK(libxc::get_functional_name(202) == "mgga_x_tpss");
    CHECK(libxc::get_functional_name(231) == "mgga_c_tpss");
  }

  SECTION("Functional Family")
  {
    CHECK(libxc::get_functional_family(1) == "lda");
    CHECK(libxc::get_functional_family(7) == "lda");

    CHECK(libxc::get_functional_family(130) == "gga");
    CHECK(libxc::get_functional_family(101) == "gga");

    CHECK(libxc::get_functional_family(202) == "mgga");
    CHECK(libxc::get_functional_family(231) == "mgga");
  }

  SECTION("XCFunc PBEc")
  {
    libxc::XCFunc xcfunc("gga_c_pbe", 1);

    CHECK(xcfunc.id() == 130);
    CHECK(xcfunc.name() == "gga_c_pbe");
    CHECK(xcfunc.family() == "gga");

    CHECK(xcfunc.has_exc());
    CHECK(xcfunc.has_vxc());
    CHECK(xcfunc.has_fxc());

    auto refs = xcfunc.references();
    CHECK(refs.size() == 2);
  }

  SECTION("XCFunc B3LYP")
  {
    libxc::XCFunc xcfunc("hyb_gga_xc_b3lyp", 1);

    CHECK(xcfunc.id() == 402);
    CHECK(xcfunc.name() == "hyb_gga_xc_b3lyp");
    CHECK(xcfunc.family() == "gga");

    CHECK(xcfunc.exchange_factor() == 0.2);

    const auto [omega, alpha, beta] = xcfunc.rsh_factors();
    CHECK(omega == 0.0);
    CHECK(alpha == 0.2);
    CHECK(beta == 0.0);

    CHECK(xcfunc.has_exc());
    CHECK(xcfunc.has_vxc());
    CHECK(xcfunc.has_fxc());
  }

  SECTION("XCFunc CAM-B3LYP")
  {
    libxc::XCFunc xcfunc("hyb_gga_xc_cam_b3lyp", 1);

    CHECK(xcfunc.id() == 433);
    CHECK(xcfunc.name() == "hyb_gga_xc_cam_b3lyp");
    CHECK(xcfunc.family() == "gga");

    CHECK(xcfunc.exchange_factor() == 0.65);

    const auto [omega, alpha, beta] = xcfunc.rsh_factors();
    CHECK(omega == 0.33);
    CHECK(alpha == 0.65);
    CHECK(beta == -0.46);

    CHECK(xcfunc.has_exc());
    CHECK(xcfunc.has_vxc());
    CHECK(xcfunc.has_fxc());
  }
}

TEST_CASE("Libxc Exc", "[libxc]")
{
  const size_t np = 5;

  Tensor<1> rho(np);
  rho(0) = 0.1;
  rho(1) = 0.2;
  rho(2) = 0.3;
  rho(3) = 0.4;
  rho(4) = 0.5;

  Tensor<1> sigma(np);
  sigma(0) = 0.2;
  sigma(1) = 0.3;
  sigma(2) = 0.4;
  sigma(3) = 0.5;
  sigma(4) = 0.6;

  Tensor<1> lapl(sigma);
  Tensor<1> tau(sigma);

  Tensor<1> exc(np);

  SECTION("LDA")
  {
    libxc::XCFunc xcfunc("lda_x");

    xcfunc.exc_lda(rho, exc);

    CHECK(exc(0) == Approx(-0.342809));
    CHECK(exc(1) == Approx(-0.431912));
    CHECK(exc(2) == Approx(-0.494416));
    CHECK(exc(3) == Approx(-0.544175));
    CHECK(exc(4) == Approx(-0.586194));
  }

  SECTION("GGA")
  {
    libxc::XCFunc xcfunc("gga_x_pbe");

    xcfunc.exc_gga(rho, sigma, exc);

    CHECK(exc(0) == Approx(-0.45259752));
    CHECK(exc(1) == Approx(-0.47887787));
    CHECK(exc(2) == Approx(-0.52067422));
    CHECK(exc(3) == Approx(-0.56142780));
    CHECK(exc(4) == Approx(-0.59866125));
  }

  SECTION("MGGA")
  {
    libxc::XCFunc xcfunc("mgga_x_tpss");

    xcfunc.exc_mgga(rho, sigma, lapl, tau, exc);

    CHECK(exc(0) == Approx(-0.41786824));
    CHECK(exc(1) == Approx(-0.47736655));
    CHECK(exc(2) == Approx(-0.52352317));
    CHECK(exc(3) == Approx(-0.56432823));
    CHECK(exc(4) == Approx(-0.60170908));
  }
}

TEST_CASE("Libxc Vxc", "[libxc]")
{
  const size_t np = 5;

  Tensor<1> rho(np);
  rho(0) = 0.1;
  rho(1) = 0.2;
  rho(2) = 0.3;
  rho(3) = 0.4;
  rho(4) = 0.5;

  Tensor<1> sigma(np);
  sigma(0) = 0.2;
  sigma(1) = 0.3;
  sigma(2) = 0.4;
  sigma(3) = 0.5;
  sigma(4) = 0.6;

  Tensor<1> lapl(sigma);
  Tensor<1> tau(sigma);

  Tensor<1> vrho = rho.zeros_like();
  Tensor<1> vsigma = sigma.zeros_like();
  Tensor<1> vlapl = lapl.zeros_like();
  Tensor<1> vtau = tau.zeros_like();

  SECTION("LDA")
  {
    libxc::XCFunc xcfunc("lda_x");

    xcfunc.vxc_lda(rho, vrho);

    CHECK(vrho(0) == Approx(-0.45707815));
    CHECK(vrho(1) == Approx(-0.57588238));
    CHECK(vrho(2) == Approx(-0.65922077));
    CHECK(vrho(3) == Approx(-0.72556634));
    CHECK(vrho(4) == Approx(-0.78159264));
  }

  SECTION("GGA")
  {
    libxc::XCFunc xcfunc("gga_x_pbe");

    xcfunc.vxc_gga(rho, sigma, vrho, vsigma);

    CHECK(vrho(0) == Approx(-0.42731424));
    CHECK(vrho(1) == Approx(-0.53019988));
    CHECK(vrho(2) == Approx(-0.62883481));
    CHECK(vrho(3) == Approx(-0.70437656));
    CHECK(vrho(4) == Approx(-0.76584967));
  }

  SECTION("MGGA")
  {
    libxc::XCFunc xcfunc("mgga_x_tpss");

    xcfunc.vxc_mgga(rho, sigma, lapl, tau, vrho, vsigma, vlapl, vtau);

    CHECK(vrho(0) == Approx(-0.41632108));
    CHECK(vrho(1) == Approx(-0.57105071));
    CHECK(vrho(2) == Approx(-0.64668841));
    CHECK(vrho(3) == Approx(-0.71780131));
    CHECK(vrho(4) == Approx(-0.77999031));
  }
}

TEST_CASE("Libxc Exc+Vxc", "[libxc]")
{
  const size_t np = 5;

  Tensor<1> rho(np);
  rho(0) = 0.1;
  rho(1) = 0.2;
  rho(2) = 0.3;
  rho(3) = 0.4;
  rho(4) = 0.5;

  Tensor<1> sigma(np);
  sigma(0) = 0.2;
  sigma(1) = 0.3;
  sigma(2) = 0.4;
  sigma(3) = 0.5;
  sigma(4) = 0.6;

  Tensor<1> lapl(sigma);
  Tensor<1> tau(sigma);

  Tensor<1> exc = rho.zeros_like();

  Tensor<1> vrho = rho.zeros_like();
  Tensor<1> vsigma = sigma.zeros_like();
  Tensor<1> vlapl = lapl.zeros_like();
  Tensor<1> vtau = tau.zeros_like();

  SECTION("LDA")
  {
    libxc::XCFunc xcfunc("lda_x");

    xcfunc.exc_vxc_lda(rho, exc, vrho);

    CHECK(exc(0) == Approx(-0.342809));
    CHECK(exc(1) == Approx(-0.431912));
    CHECK(exc(2) == Approx(-0.494416));
    CHECK(exc(3) == Approx(-0.544175));
    CHECK(exc(4) == Approx(-0.586194));

    CHECK(vrho(0) == Approx(-0.45707815));
    CHECK(vrho(1) == Approx(-0.57588238));
    CHECK(vrho(2) == Approx(-0.65922077));
    CHECK(vrho(3) == Approx(-0.72556634));
    CHECK(vrho(4) == Approx(-0.78159264));
  }

  SECTION("GGA")
  {
    libxc::XCFunc xcfunc("gga_x_pbe");

    xcfunc.exc_vxc_gga(rho, sigma, exc, vrho, vsigma);

    CHECK(exc(0) == Approx(-0.45259752));
    CHECK(exc(1) == Approx(-0.47887787));
    CHECK(exc(2) == Approx(-0.52067422));
    CHECK(exc(3) == Approx(-0.56142780));
    CHECK(exc(4) == Approx(-0.59866125));

    CHECK(vrho(0) == Approx(-0.42731424));
    CHECK(vrho(1) == Approx(-0.53019988));
    CHECK(vrho(2) == Approx(-0.62883481));
    CHECK(vrho(3) == Approx(-0.70437656));
    CHECK(vrho(4) == Approx(-0.76584967));
  }

  SECTION("MGGA")
  {
    libxc::XCFunc xcfunc("mgga_x_tpss");

    xcfunc.exc_vxc_mgga(rho, sigma, lapl, tau, exc, vrho, vsigma, vlapl, vtau);

    CHECK(exc(0) == Approx(-0.41786824));
    CHECK(exc(1) == Approx(-0.47736655));
    CHECK(exc(2) == Approx(-0.52352317));
    CHECK(exc(3) == Approx(-0.56432823));
    CHECK(exc(4) == Approx(-0.60170908));

    CHECK(vrho(0) == Approx(-0.41632108));
    CHECK(vrho(1) == Approx(-0.57105071));
    CHECK(vrho(2) == Approx(-0.64668841));
    CHECK(vrho(3) == Approx(-0.71780131));
    CHECK(vrho(4) == Approx(-0.77999031));
  }
}

TEST_CASE("Libxc simplified", "[libxc]")
{
  const size_t np = 5;

  Tensor<2> rho(np, 4);
  rho(0, 0) = 0.1;
  rho(1, 0) = 0.2;
  rho(2, 0) = 0.3;
  rho(3, 0) = 0.4;
  rho(4, 0) = 0.5;
  rho(0, 1) = 0.2;
  rho(1, 1) = 0.3;
  rho(2, 1) = 0.4;
  rho(3, 1) = 0.5;
  rho(4, 1) = 0.6;
  rho.pluck(2) = rho.pluck(1);
  rho.pluck(3) = rho.pluck(1);

  Tensor<2> vrho(np, 5);

  SECTION("GGA")
  {
    libxc::XCFunc xcfunc("gga_x_pbe");

    xcfunc.exc_vxc(rho, vrho);

    auto exc = vrho.pluck(0);
    CHECK(exc(0) == Approx(-0.45259752));
    CHECK(exc(1) == Approx(-0.47887787));
    CHECK(exc(2) == Approx(-0.52067422));
    CHECK(exc(3) == Approx(-0.56142780));
    CHECK(exc(4) == Approx(-0.59866125));

    CHECK(vrho(0, 1) == Approx(-0.42731424));
    CHECK(vrho(1, 1) == Approx(-0.53019988));
    CHECK(vrho(2, 1) == Approx(-0.62883481));
    CHECK(vrho(3, 1) == Approx(-0.70437656));
    CHECK(vrho(4, 1) == Approx(-0.76584967));
  }
}
