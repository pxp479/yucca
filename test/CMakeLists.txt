# copy files from examples and test into testing directory for convenience
set(YUCCA_EXAMPLES_DIR "${CMAKE_SOURCE_DIR}/examples/")
set(YUCCA_MOLECULES_DIR "${CMAKE_SOURCE_DIR}/molecules/")

configure_file(${CMAKE_SOURCE_DIR}/test/yucca_test.h.in ${CMAKE_CURRENT_BINARY_DIR}/yucca_test.h)

set(TEST_SOURCES
  algo.cpp
  input.cpp
  hf.cpp
  cis.cpp
  polarizability.cpp
  reshf.cpp
  grids.cpp
  gaussians.cpp
  libxc.cpp
  dft.cpp
  catch_amalgamated.cpp
  ureshf.cpp
  )

if(NOT TARGET Catch)
  set(CATCH_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
  add_library(Catch INTERFACE)
  target_include_directories(Catch INTERFACE ${CATCH_INCLUDE_DIR})
endif()

# pull definition of YUCCA_LIBS and LIBS from yucca directory
get_directory_property(YUCCA_LIBS DIRECTORY ${CMAKE_SOURCE_DIR}/yucca DEFINITION YUCCA_LIBS)

add_executable(YuccaTest ${TEST_SOURCES} main.cpp)
target_link_libraries(YuccaTest PUBLIC ${YUCCA_LIBS} qleve Libint2::int2 Libint2::cxx ${FILESYSTEM} cxxopts Libxc::xc)
target_include_directories(YuccaTest PUBLIC ${CMAKE_CURRENT_BINARY_DIR})
if(NOT DISABLE_COVERAGE)
  target_code_coverage(YuccaTest)
endif()

include(Catch)
catch_discover_tests(YuccaTest)
