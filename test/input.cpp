#include <iostream>
#include <random>

#include <qleve/tensor.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/input/read_input.hpp>

#include "catch_amalgamated.hpp"
#include "yucca_test.h"

using namespace std;
using namespace yucca;
using namespace qleve;
using namespace Catch;

TEST_CASE("Input Node", "[input]")
{

  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);

  SECTION("typestrings")
  {
    InputNode node;
    node.put("string", "string");
    node.put("integer", 1);
    node.put("float", 1.0);
    node.put("boolean", true);
    node.put("vector", vector<int>{});
    node.put("map", map<string, int>{});

    CHECK(node.get_node("string").has_type() == "string");
    CHECK(node.get_node("integer").has_type() == "integer");
    CHECK(node.get_node("float").has_type() == "float");
    CHECK(node.get_node("boolean").has_type() == "boolean");
    CHECK(node.get_node("vector").has_type() == "vector");
    CHECK(node.get_node("map").has_type() == "map");

    CHECK_THROWS(node.get<ptrdiff_t>("string"));
    CHECK_THROWS(node.get<double>("string"));
    CHECK_THROWS(node.get<bool>("string"));

    CHECK_THROWS(node.get<string>("integer"));
    CHECK_THROWS(node.get<bool>("integer"));

    CHECK_THROWS(node.get<string>("float"));
    CHECK_THROWS(node.get<ptrdiff_t>("float"));
    CHECK_THROWS(node.get<bool>("float"));

    CHECK_THROWS(node.get<string>("boolean"));
    CHECK_THROWS(node.get<ptrdiff_t>("boolean"));
    CHECK_THROWS(node.get<double>("boolean"));
  }

  SECTION("put Tensor<1>")
  {
    Tensor<1> t(5);
    for (size_t i = 0; i < t.size(); ++i) {
      t.data(i) = distribution(generator);
    }

    InputNode node;
    node.put("test", t);

    auto nn = node.get_node("test");
    auto shape = nn.get_vector<ptrdiff_t>("extent");
    CHECK(shape.at(0) == 5);

    auto dat = nn.get_vector<double>("data");
    CHECK(dat[2] == t.data(2));
  }

  SECTION("put Tensor<2>")
  {
    Tensor<2> t(2, 2);
    for (size_t i = 0; i < t.size(); ++i) {
      t.data(i) = distribution(generator);
    }

    InputNode node;
    node.put("test", t);

    auto nn = node.get_node("test");
    auto shape = nn.get_vector<ptrdiff_t>("extent");
    CHECK(shape.at(0) == 2);
    CHECK(shape.at(1) == 2);
    CHECK(shape.size() == 2);

    auto dat = nn.get_vector<double>("data");
    CHECK(dat[2] == t.data(2));
  }

  SECTION("get Tensor<1>")
  {
    auto node = read_input_stream("test:\n  extent: [5]\n  data: [1.0, 2.0, 3.0, 4.0, 5.0]\n");

    auto t = node.get_tensor<double, 1>("test");

    CHECK(t(2) == 3.0);

    CHECK_THROWS(node.get_tensor<double, 2>("test"));
  }

  SECTION("get Tensor<2>")
  {
    auto node = read_input_stream("test:\n  extent: [2, 2]\n  data: [1.0, 2.0, 3.0, 4.0]\n");

    auto t = node.get_tensor<double, 2>("test");

    CHECK(t(0, 1) == 3.0);

    CHECK_THROWS(node.get_tensor<double, 3>("test"));

    // wrong number of data entries
    auto n2 = read_input_stream("test:\n  extent: [2, 2]\n  data: [1.0, 2.0, 3.0, 4.0, 5.0]\n");
    CHECK_THROWS(n2.get_tensor<double, 2>("test"));
  }

  SECTION("Tensor<2> round trip")
  {
    Tensor<2> t(2, 2);
    for (size_t i = 0; i < t.size(); ++i) {
      t.data(i) = distribution(generator);
    }

    InputNode node;
    node.put("test", t);

    auto node2 = read_input_stream(node.to_string());
    auto t2 = node2.get_tensor<double, 2>("test");

    CHECK(t(0, 0) == t2(0, 0));
    CHECK(t(1, 0) == t2(1, 0));
    CHECK(t(0, 1) == t2(0, 1));
    CHECK(t(1, 1) == t2(1, 1));
  }
}
