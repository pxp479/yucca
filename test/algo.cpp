#include <random>

#include <qleve/diagonalize.hpp>
#include <qleve/solve_herm_lse.hpp>
#include <qleve/tensor_contract.hpp>

#include <yucca/algo/bfgs.hpp>
#include <yucca/algo/davidson.hpp>
#include <yucca/algo/krylov_lse.hpp>
#include <yucca/algo/orthogonalize.hpp>

#include "catch_amalgamated.hpp"
#include "yucca_test.h"

using namespace std;
using namespace yucca;
using namespace Catch;

TEST_CASE("Algorithm Unit Tests", "[ALGO]")
{

  qleve::Tensor<2> C(4, 4);

  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);
  for (size_t i = 0; i < C.size(); ++i) {
    C.data(i) = distribution(generator);
  }

  SECTION("orthogonalization Tensor<2> based on provided metric")
  {
    qleve::Tensor<2> S(4, 4);
    qleve::gemm("t", "n", 1.0, C, C, 0.0, S);

    auto X = orthogonalize_metric(S);
    auto orthoC = C.zeros_like();
    qleve::gemm("n", "n", 1.0, C, X, 0.0, orthoC);

    auto orthoS = S.zeros_like();
    qleve::gemm("t", "n", 1.0, orthoC, orthoC, 0.0, orthoS);

    CHECK(orthoS(0, 0) == Approx(1.0).epsilon(1e-8));
    CHECK(orthoS(0, 1) == Approx(0.0).margin(1e-8));
  }

  SECTION("orthogonalization TensorView<2> based on provided metric")
  {
    qleve::Tensor<2> S(4, 4);
    qleve::gemm("t", "n", 1.0, C, C, 0.0, S);

    auto Sview = S.slice(0, 4);
    auto X = orthogonalize_metric(Sview);
    auto orthoC = C.zeros_like();
    qleve::gemm("n", "n", 1.0, C, X, 0.0, orthoC);

    auto orthoS = S.zeros_like();
    qleve::gemm("t", "n", 1.0, orthoC, orthoC, 0.0, orthoS);

    CHECK(orthoS(0, 0) == Approx(1.0).epsilon(1e-8));
    CHECK(orthoS(0, 1) == Approx(0.0).margin(1e-8));
  }

  SECTION("Orthogonalize vectors")
  {
    ptrdiff_t northo = orthogonalize_vectors(C);
    auto orthoC = C.slice(0, northo);

    qleve::Tensor<2> S = orthoC.zeros_like();
    qleve::gemm("t", "n", 1.0, orthoC, orthoC, 0.0, S);

    CHECK(S(0, 0) == Approx(1.0).epsilon(1e-8));
    CHECK(S(0, 1) == Approx(0.0).margin(1e-8));
  }

  SECTION("Diagonalize a matrix with Davidson")
  {
    std::normal_distribution<> d{0, 0.2};

    // generate random matrix
    const size_t N = 10;
    qleve::Tensor<2> H(N, N);
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < i; ++j) {
        H(j, i) = d(generator);
        H(i, j) = H(j, i);
      }
      H(i, i) = i * i;
    }

    qleve::Tensor<2> vecs(H);
    auto eigs = qleve::linalg::diagonalize(vecs);

    auto diag = std::make_shared<qleve::Tensor<1>>(N);
    for (size_t i = 0; i < N; ++i) {
      diag->at(i) = H(i, i);
    }

    Davidson<double> david(1, diag, N);
    auto [d_eig, d_vecs] = david.diagonalize(
        [&H](const qleve::ConstTensorView<2> v, qleve::TensorView<2> w) {
          qleve::contract(1.0, H, "pq", v, "qr", 0.0, w, "pr");
        },
        N, 1.0e-6);

    REQUIRE(d_eig(0) == Approx(eigs(0)));
  }

  SECTION("Solve LSE with Krylov algorithm")
  {
    std::normal_distribution<> d{0, 0.2};

    const ptrdiff_t nfreq = 2;

    // generate random matrix
    const size_t N = 10;
    qleve::Tensor<2> H(N, N);
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < i; ++j) {
        H(j, i) = d(generator);
        H(i, j) = H(j, i);
      }
      H(i, i) = (i + 1) * (i + 1);
    }
    // generate random RHS
    qleve::Tensor<2> R(N, nfreq);
    for (size_t i = 0; i < N; ++i) {
      R(i, 0) = d(generator);
      R(i, 1) = d(generator);
    }
    // freq = 0
    qleve::Tensor<1> freq(nfreq);

    // extract diag
    auto diag = std::make_shared<qleve::Tensor<1>>(N);
    for (size_t i = 0; i < N; ++i) {
      diag->at(i) = H(i, i);
    }

    KrylovLSE<double> krylov(R, freq, diag, N);
    auto vecs = krylov.solve(
        [&H](const qleve::ConstTensorView<2> v, qleve::TensorView<2> w) {
          qleve::contract(1.0, H, "pq", v, "qr", 0.0, w, "pr");
        },
        2 * N, 1.0e-5);

    auto x = qleve::linalg::solve_herm_lse(H, R);

    CHECK(x(0, 0) == Approx(vecs(0, 0)));
    CHECK(x(1, 0) == Approx(vecs(1, 0)));
  }

  SECTION("BFGS")
  {
    auto compute_energy_and_grad = [](const qleve::ConstTensorView<1>& x, qleve::TensorView<1> g) {
      const double x1 = x(0);
      const double x2 = x(1);

      const double val = exp(x1 - 1.0) + exp(1 - x2) + (x1 - x2) * (x1 - x2);

      g(0) = exp(x1 - 1.0) + 2.0 * (x1 - x2);
      g(1) = -exp(1.0 - x2) - 2.0 * (x1 - x2);

      return val;
    };

    qleve::Tensor<1> h0(2);
    h0 = 2.7159034298;

    qleve::Tensor<1> x(2);
    const int maxiter = 10;

    bfgs_minimize<double>(compute_energy_and_grad, x, h0, maxiter);

    REQUIRE(x(0) == Approx(0.79611689));
    REQUIRE(x(1) == Approx(1.20389118));
  }
}
