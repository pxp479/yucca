#!/usr/bin/env python

from __future__ import print_function

import argparse
import subprocess
import sys

if sys.version_info < (3, 5):
    sys.exit("Python 3.5 or later is required for this configure script.")

def get_version(cc):
    command = { "clang" : "clang", "gnu" : "gcc" }[cc]
    comm_out = subprocess.run([command, "--version"], capture_output=True)
    if "clang" in cc:
        long_vers = comm_out.stdout.split()[2].decode('utf-8').split('.')
        vers = long_vers[0]
    elif "gnu" in cc:
        long_vers = comm_out.stdout.splitlines()[0].split()[-1].decode('utf-8').split('.')
        vers = long_vers[0]
    else:
        raise Exception("Unrecognized compiler option")
    return vers

def cmake_setup(options, dry_run=False):
    comps = {
            "C" : options["CC"],
            "CXX" : options["CXX"]
            }
    envs = " ".join(["-DCMAKE_{0}_COMPILER={1}".format(x, comps[x]) for x in comps])
    build = "-DCMAKE_BUILD_TYPE={0}".format(options["build_type"].capitalize())
    if options["commands"]:
        build += " -DCMAKE_EXPORT_COMPILE_COMMANDS=1"

    command = "cmake {src} {build} {envs}".format(src=options["source_dir"], build=build, envs=envs).split()
    if not dry_run:
        subprocess.run(command)
    else:
        print(" ".join(command))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="configure.py",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="Configure directory to build yucca",
            epilog="Example:\n"
                   "  mkdir build\n"
                   "  ../configure.py CC=clang CXX=clang++\n"
                   "  make")

    parser.add_argument("source_dir", type=str)
    parser.add_argument("-t", "--type", choices=("release", "debug"), default="debug")
    parser.add_argument("--compiler", choices=["clang", "gnu"], default="gnu")
    parser.add_argument("--compiler_version", type=str, default="")
    parser.add_argument("--no-commands", dest='commands', action='store_false', help="export compiler options on build")
    parser.add_argument("--libcxx", type=str, default="")
    parser.add_argument("--cc", type=str, default="")
    parser.add_argument("--cxx", type=str, default="")
    parser.add_argument("--fc", type=str, default="")
    parser.add_argument("--dry", action='store_true', help='specifies a dry run where commands are printed, not run')

    args = parser.parse_args()

    comp = args.compiler

    options = {
        "compiler" : args.compiler,
        "source_dir" : args.source_dir,
        "build_type" : args.type
        }

    dcc, dcxx = { "clang" : ("clang", "clang++"),
            "gnu" : ("gcc", "g++") }[comp]
    dlibcxx = { "clang" : "libc++",
            "gnu" : "libstdc++" }[comp]
    dfc = "gfortran"

    cc = args.cc if args.cc else dcc
    cxx = args.cxx if args.cxx else dcxx
    fc = args.fc if args.fc else dfc
    libcxx = args.libcxx if args.libcxx else dlibcxx
    compiler_version = args.compiler_version if args.compiler_version else get_version(comp)

    options.update( {
            "compiler_version" : compiler_version,
            "libcxx" : libcxx,
            "CC" : cc,
            "CXX" : cxx,
            "FC" : fc,
            "commands" : args.commands
            })

    cmake_setup(options, dry_run=args.dry)
