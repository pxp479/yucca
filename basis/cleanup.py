#!/usr/bin/env python

from __future__ import print_function

from collections import OrderedDict

import argparse
import yaml

angular_map = { "s" : 0, "p" : 1, "d" : 2, "f" : 3, "g" : 4, "h" : 5, "i" : 6 }

symbols = ["q",
           "h", "he", "li", "be", "b", "c", "n", "o", "f", "ne", "na", "mg", "al",
           "si", "p", "s", "cl", "ar", "k", "ca", "sc", "ti", "v", "cr", "mn", "fe",
           "co", "ni", "cu", "zn", "ga", "ge", "as", "se", "br", "kr", "rb", "sr",
           "y", "zr", "nb", "mo", "tc", "ru", "rh", "pd", "ag", "cd", "in", "sn",
           "sb", "te", "i", "xe", "cs", "ba", "la", "ce", "pr", "nd", "pm", "sm",
           "eu", "gd", "tb", "dy", "ho", "er", "tm", "yb", "lu", "hf", "ta", "w",
           "re", "os", "ir", "pt", "au", "hg", "tl", "pb", "bi", "po", "at", "rn",
           "fr", "ra", "ac", "th", "pa", "u", "np", "pu", "am", "cm", "bk", "cf",
           "es", "fm", "md", "no", "lr", "rf", "db", "sg", "bh", "hs", "mt", "ds",
           "rg", "cn", "uut", "fl", "uup", "lv", "uus", "uuo"]

ap = argparse.ArgumentParser("sort and clean the basis file")

ap.add_argument("dirty", help="dirty basis file")

args = ap.parse_args()

data = yaml.safe_load(open(args.dirty))

out = {}

for e in symbols:
    if e in data:
        shells = data[e]
        for sh in shells:
            if sh["angular"] in angular_map:
                sh["angular"] = angular_map[sh["angular"]]
        out[e] = shells

        print(yaml.dump({ e : shells}, indent=2, explicit_start=False, default_flow_style=False))
