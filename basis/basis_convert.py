#!/usr/bin/env python

"""
Converts a GAMESS-UK basis file into a yaml file used by yucca
GAMESS-UK is used purely because it is easy to parse
"""

from __future__ import print_function

import argparse
import re
import yaml

shellstart_re = re.compile(r"^\s*([spdfghijkl])\s+([a-zA-Z]+)\s*$", re.IGNORECASE)
contraction_re = re.compile(r"^\s*([-+.0-9eEdD]+)\s+([-.0-9][-+.0-9eEdD]+)")

angular_map = { "s" : 0, "p" : 1, "d" : 2, "f" : 3, "g" : 4, "h" : 5, "i" : 6, "j" : 7}

symbols = ["q",
           "h", "he", "li", "be", "b", "c", "n", "o", "f", "ne", "na", "mg", "al",
           "si", "p", "s", "cl", "ar", "k", "ca", "sc", "ti", "v", "cr", "mn", "fe",
           "co", "ni", "cu", "zn", "ga", "ge", "as", "se", "br", "kr", "rb", "sr",
           "y", "zr", "nb", "mo", "tc", "ru", "rh", "pd", "ag", "cd", "in", "sn",
           "sb", "te", "i", "xe", "cs", "ba", "la", "ce", "pr", "nd", "pm", "sm",
           "eu", "gd", "tb", "dy", "ho", "er", "tm", "yb", "lu", "hf", "ta", "w",
           "re", "os", "ir", "pt", "au", "hg", "tl", "pb", "bi", "po", "at", "rn",
           "fr", "ra", "ac", "th", "pa", "u", "np", "pu", "am", "cm", "bk", "cf",
           "es", "fm", "md", "no", "lr", "rf", "db", "sg", "bh", "hs", "mt", "ds",
           "rg", "cn", "uut", "fl", "uup", "lv", "uus", "uuo"]


def to_float(fstr):
    fstr = fstr.replace("d", "e").replace("D", "E")
    return float(fstr)

def read_gamess_basis(gamess_basis_file):
    out = {}
    with open(gamess_basis_file) as f:
        line = f.readline()
        while True:
            m = shellstart_re.search(line)
            while not m: # advance to the start of the next shell block
                line = f.readline()
                if line == "": break
                m = shellstart_re.search(line)

            if line == "": break # end of file

            shell_ang = int(angular_map[m.group(1).lower()])
            element = m.group(2).lower()

            shell = { "angular" : shell_ang, "exponents" : [], "coeffs" : [] }

            line = f.readline()
            m = contraction_re.search(line)
            while m:
                shell["coeffs"].append(to_float(m.group(1)))
                shell["exponents"].append(to_float(m.group(2)))

                line = f.readline()
                m = contraction_re.search(line)

            if element not in out:
                out[element] = []

            out[element].append(shell)
    return out

if __name__ == "__main__":
    ap = argparse.ArgumentParser("Convert a GAMESS-UK basis file to yaml")

    ap.add_argument("basis", help="GAMESS-UK basis file")

    args = ap.parse_args()

    basis = read_gamess_basis(args.basis)

    for e in symbols:
        if e in basis:
            print(
                yaml.dump({ e : basis[e] },
                    indent=2,
                    explicit_start=False,
                    default_flow_style=False
                    )
                )
