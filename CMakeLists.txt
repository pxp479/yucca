cmake_minimum_required(VERSION 3.11)

project(Yucca
  VERSION 0.1
  LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED YES)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/cmake/ ${CMAKE_SOURCE_DIR}/qleve/cmake)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native")

find_package(OpenMP REQUIRED)
if(NOT OpenMP_CXX_FOUND)
  message(FATAL_ERROR "OpenMP required to compile yucca")
endif()

find_package(TBB REQUIRED)

find_package(Libint2 REQUIRED PATHS ${LIBINT_ROOT}/lib/cmake/libint2)
message(STATUS "Found libint2 (${LIBINT2_VERSION}) at: ${Libint2_DIR}")

find_package(Filesystem)
if (Filesystem_FOUND)
  set(FILESYSTEM std::filesystem)
  add_compile_definitions(FILESYSTEM_HEADER=<${CXX_FILESYSTEM_HEADER}>)
  add_compile_definitions(FILESYSTEM_NAMESPACE=${CXX_FILESYSTEM_NAMESPACE})

  # Boost still needed by libint2
  find_package(Boost REQUIRED)
else()
  find_package(Boost REQUIRED COMPONENTS filesystem)

  add_compile_definitions(FILESYSTEM_HEADER=<boost/filesystem.hpp>)
  add_compile_definitions(FILESYSTEM_NAMESPACE=boost::filesystem)
  set(FILESYSTEM Boost::filesystem)
endif()

# check for the existence of extern projects included as submodules
find_package(Git QUIET)
if(GIT_FOUND AND EXISTS "${PROJECT_SOURCE_DIR}/.git")
  # Update submodules as needed
  option(GIT_SUBMODULE "Check submodules during build" ON)
  if(GIT_SUBMODULE)
    message(STATUS "Submodule update")
    execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
                    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                    RESULT_VARIABLE GIT_SUBMOD_RESULT)
    if(NOT GIT_SUBMOD_RESULT EQUAL "0")
        message(FATAL_ERROR "git submodule update --init failed with ${GIT_SUBMOD_RESULT}, please checkout submodules")
    endif()
  endif()
endif()

find_package(Eigen3 REQUIRED)

find_package(Libxc)
if (NOT Libxc_FOUND)
  message(FATAL_ERROR "Libxc version 5 or 6 required to compile yucca")
else()
  message(STATUS "Found libxc (${Libxc_VERSION}) at: ${Libxc_DIR}")
  if (Libxc_VERSION VERSION_LESS "5.0.0")
    message(FATAL_ERROR "Libxc version 5 or 6 required to compile yucca")
  elseif (Libxc_VERSION VERSION_GREATER_EQUAL "7.0.0")
    message(FATAL_ERROR "Libxc version 5 or 6 required to compile yucca")
  endif()
endif()

# skip qleve tests by default
set(QLEVE_DISABLE_TESTS ON)

set(DISABLE_COVERAGE ON)
if(NOT DISABLE_COVERAGE)
  include(CodeCoverage)
  add_code_coverage()
endif()

include_directories(${CMAKE_SOURCE_DIR})

# handle externals first
add_subdirectory(external)

if(NOT EXISTS "${PROJECT_SOURCE_DIR}/qleve/CMakeLists.txt")
  message(FATAL_ERROR "qleve submodule was not found! GIT_SUBMODULE was turned off or failed. Please update submodules and try again.")
endif()
add_subdirectory(qleve)

add_subdirectory(yucca)

if (NOT YUCCA_DISABLE_TESTS)
  enable_testing()
  add_subdirectory(test)
endif()
