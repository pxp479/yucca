// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/physical/constants.hpp
///
/// Collection of physical constants
#pragma once

/// physical constants pulled from CODATA Internationally recommended values 2014
/// https://physics.nist.gov/cuu/Constants/index.html
/// (equivalently: https://physics.nist.gov/cuu/Constants/Table/allascii.txt)

namespace physical
{

template <typename T>
constexpr T pi = (T)3.14159265358979323846264338327950288419\
71693993751058209749445923078164062862089986280348253421170679821480865132823066\
47093844609550582231725359408128481117450284102701938521105559644622948954930381\
964428810975665933446128475648233786783165271201909145648566923460;

static const double speed_of_light_m_s = 299792458;
static const double amu2kg = 1.660539040e-27;
static const double electronmass_kg = 9.10938356e-31;
static const double amu_au = amu2kg / electronmass_kg;

static const double bohr2ang = 0.52917721067;
static const double ang2bohr = 1.0 / bohr2ang;

static const double Hartree2eV = 27.21138602;
static const double eV2Hartree = 1.0 / Hartree2eV;

static const double Hartree2Hz = 6.579683920502e15;

static const double Hartree2nm = 1e9 * speed_of_light_m_s / Hartree2Hz;

} // namespace physical
