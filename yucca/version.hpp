// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/version.hpp
///
/// \brief Version information
#ifndef YUCCA_VERSION_HPP
#define YUCCA_VERSION_HPP

#include <string>

namespace yucca::version
{
const std::string to_string();
unsigned int major();
unsigned int minor();
unsigned int patch();
const std::string flag();
unsigned int distance();
const std::string short_hash();
const std::string full_hash();
bool isdirty();
const std::string branch();

const std::string description();
} // namespace yucca::version

#endif // YUCCA_VERSION_HPP
