// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/density_functional.hpp
///
/// Wrapper for computing density functionals
#pragma once

#include <memory>
#include <vector>

#include <qleve/tensor.hpp>

struct xc_func_type; // fwd declare libxc

namespace yucca::libxc
{

std::string get_version();
std::string get_libxc_reference();

int get_functional_id(const std::string xcname);
std::string get_functional_family(const int func_id);
std::string get_functional_name(const int func_id);

class XCFunc {
 protected:
  xc_func_type* xc_;

  int functional_id_;
  int nspin_;

  using rho_t = qleve::ConstTensorView<1>;
  using exc_t = qleve::TensorView<1>;

  using rhos_t = qleve::ConstTensorView<2>;
  using vrhos_t = qleve::TensorView<2>;

 public:
  XCFunc(const std::string libxcname, const int nspin = 1);
  ~XCFunc();

  int id() const { return functional_id_; }
  std::string family() const { return get_functional_family(functional_id_); }
  std::string name() const { return get_functional_name(functional_id_); }
  std::vector<std::string> references() const;
  std::vector<std::string> dois() const;
  std::string doi() const;

  bool has_exc() const;
  bool has_vxc() const;
  bool has_fxc() const;
  bool has_gxc() const;

  int needs_derivatives_up_to() const;

  void set_spin(const int nspin);

  std::string hybrid_type() const;
  double exchange_factor() const;
  std::array<double, 3> rsh_factors() const;

  void exc_lda(const rho_t& rho, exc_t exc);
  void exc_gga(const rho_t& rho, const rho_t& sigma, exc_t exc);
  void exc_mgga(const rho_t& rho, const rho_t& sigma, const rho_t& lapl, const rho_t& tau,
                exc_t exc);
  void exc(const rhos_t& rho, vrhos_t vrho);

  void vxc_lda(const rho_t& rho, exc_t vrho);
  void vxc_gga(const rho_t& rho, const rho_t& sigma, exc_t vrho, exc_t vsigma);
  void vxc_mgga(const rho_t& rho, const rho_t& sigma, const rho_t& lapl, const rho_t& tau,
                exc_t vrho, exc_t vsigma, exc_t vlapl, exc_t vtau);
  void vxc(const rhos_t& rho, vrhos_t vrho);

  void exc_vxc_lda(const rho_t& rho, exc_t exc, exc_t vrho);
  void exc_vxc_gga(const rho_t& rho, const rho_t& sigma, exc_t exc, exc_t vrho, exc_t vsigma);
  void exc_vxc_mgga(const rho_t& rho, const rho_t& sigma, const rho_t& lapl, const rho_t& tau,
                    exc_t exc, exc_t vrho, exc_t vsigma, exc_t vlapl, exc_t vtau);
  void exc_vxc(const rhos_t& rho, vrhos_t vrho);
};

} // namespace yucca::libxc
