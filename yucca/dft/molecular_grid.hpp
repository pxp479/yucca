// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/molecular_grid.hpp
///
/// Atom-centered grids for DFT integration
#pragma once

#include <memory>

#include <qleve/tensor.hpp>

#include <yucca/dft/partitioning.hpp>
#include <yucca/input/input_node.hpp>

class Atom; // #include <yucca/structure/molecule.hpp>

namespace yucca
{

class MolecularGrid {
 protected:
  std::unique_ptr<qleve::Tensor<2>> points_;
  std::unique_ptr<qleve::Tensor<1>> weights_;

  int grid_level_;
  bool prune_grids_;

  std::vector<ptrdiff_t> atom_offsets_;
  std::vector<ptrdiff_t> atom_sizes_;

  std::shared_ptr<BeckePartition_> partitions_;

 public:
  MolecularGrid(const yucca::InputNode& input, const std::vector<Atom>& atoms);

  qleve::Tensor<2>& points() { return *points_; }
  const qleve::Tensor<2>& points() const { return *points_; }
  qleve::Tensor<1>& weights() { return *weights_; }
  const qleve::Tensor<1>& weights() const { return *weights_; }
};


} // namespace yucca
