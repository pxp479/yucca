// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/integrate_functional.hpp
///
/// Integrate a DensityFunctional object outputs an SAO basis

#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/tensor_dot.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

#include <yucca/dft/density_functional.hpp>
#include <yucca/dft/integrate_functional.hpp>
#include <yucca/dft/molecular_grid.hpp>
#include <yucca/structure/evaluate_cao.hpp>
#include <yucca/util/distribute_tasks.hpp>

using namespace yucca;
using namespace std;
using namespace qleve;

tuple<double, Tensor<2>> yucca::dft::integrate_exc_vxc(
    const Basis& basis, const MolecularGrid& grid, const DensityFunctional& df,
    const ConstTensorView<2> rho, TensorView<2> density, const int batchsize, const double thresh)
{
  auto& bshells = basis.basisshells();
  const size_t nshells = bshells.size();
  const ptrdiff_t np = grid.weights().size();
  const ptrdiff_t ncao = basis.ncao();
  const double logthresh = std::log(thresh);

  // shell quantities
  const ptrdiff_t max_l = basis.max_angular_momentum();
  const ptrdiff_t max_cart = (max_l + 1) * (max_l + 2) / 2;

  // densities needed
  const int max_drho_needed = df.needs_derivatives_up_to();
  const int ndens_ops = (max_drho_needed + 1) * (max_drho_needed + 1);
  const int nxc_ops = df.nxc_operations();
  const int nxc_out = nxc_ops + 1; // exc + vxc (has same output ingredients as input)

  assert(density.extent(0) == np && density.extent(1) == nxc_ops);

  // transform rho to cao
  auto rho_cao = solid_harmonics::transform_to_cao(basis, rho);
  for (ptrdiff_t jcao = 0; jcao < ncao; ++jcao) {
    for (ptrdiff_t icao = 0; icao < jcao; ++icao) {
      rho_cao(icao, jcao) += rho_cao(jcao, icao);
      rho_cao(icao, jcao) *= 2.0;
    }
    rho_cao(jcao, jcao) *= 2.0;
  }

  qleve::Tensor<1> maxrho(ncao);
  for (ptrdiff_t icao = 0; icao < ncao; ++icao) {
    double mx = 0.0;
    for (ptrdiff_t jcao = 0; jcao <= icao; ++jcao) {
      if (const double r = std::abs(rho_cao(jcao, icao)); r > mx) {
        mx = r;
      }
    }
    maxrho(icao) = mx;
  }
  maxrho = qleve::log(maxrho);

  // collect integrals in CAO basis
  double Exc = 0.0;
  qleve::Tensor<2> vcao(ncao, ncao);

  qleve::Tensor<1> densbuf(batchsize * ndens_ops * ncao);
  qleve::Tensor<1> rhoxcbuf(batchsize * nxc_ops);
  qleve::Tensor<1> drhobuf(batchsize * ndens_ops);
  qleve::Tensor<1> exc_vxcbuf(batchsize * nxc_out);
  qleve::Tensor<1> contractbuf(batchsize * ndens_ops);

  // shell norms for bounding contributions
  qleve::Tensor<1> batchnorms(ncao);

  unique_ptr<double[]> aow_buf;
  if (ndens_ops == 4) {
    aow_buf = make_unique<double[]>(batchsize * ncao);
  }

  StaticDistributeTasks tasks(np, batchsize);

  auto compute_batch = [&](const ptrdiff_t batch) {
    const auto [start, stop, tsize] = tasks.batch(batch);
    auto _xyz = grid.points().const_slice(start, stop);
    auto _weights = grid.weights().const_slice(start, stop);

    TensorView<3> _dbuf(densbuf.data(), tsize, ndens_ops, ncao);
    TensorView<2> _rhoxc(rhoxcbuf.data(), tsize, nxc_ops);
    TensorView<2> _drho(drhobuf.data(), tsize, ndens_ops);
    TensorView<2> _exc_vxc(exc_vxcbuf.data(), tsize, nxc_out);
    TensorView<2> _cbuf(contractbuf.data(), tsize, ndens_ops);

    _drho = 0.0;
    _exc_vxc = 0.0;
    _dbuf = 0.0;

    // compute chi_mu(xi) for all mu and xi
    const double shellthresh = thresh;
    for (ptrdiff_t ish = 0; ish < nshells; ++ish) {
      const auto [ioff, iend, icart] = bshells[ish].bounds_cao();

      auto dd = _dbuf.slice(ioff, iend);
      evaluate_shell_at_points(max_drho_needed, bshells[ish], _xyz, dd);
    }

    qleve::contract(1.0, _dbuf, "xdi", _dbuf, "xdi", 0.0, batchnorms, "i", /*single*/ true);
    batchnorms = 0.5 * qleve::log(batchnorms);

    // create density, density derivatives -> _drho
    for (ptrdiff_t jcao = 0; jcao < ncao; ++jcao) {
      const double bj = batchnorms(jcao) + maxrho(jcao);
      if (bj < logthresh) {
        continue;
      }
      _cbuf = 0.0;
      for (ptrdiff_t icao = 0; icao <= jcao; ++icao) {
        const double bi = batchnorms(icao) + maxrho(icao);
        if (bi + bj < logthresh) {
          continue;
        }
        _cbuf += rho_cao(icao, jcao) * _dbuf.pluck(icao);
      }

      auto chi_j = _dbuf.pluck(0, jcao);
      auto chi_rho_j = _cbuf.pluck(0);

      // density is always needed
      _drho.pluck(0) += chi_j * chi_rho_j;

      if (max_drho_needed >= 1) { // generalized gradient
        // grad rho . grad rho
        for (int i = 0; i < 3; ++i) {
          auto dchi_j = _dbuf.pluck(i + 1, jcao);
          auto dchi_rho_j = _cbuf.pluck(i + 1);
          _drho.pluck(i + 1) += chi_j * dchi_rho_j + dchi_j * chi_rho_j;
        }
      }
      if (max_drho_needed >= 2) { // laplacian and KE density
        assert(false && "mGGA not yet implemented");
      }
    }

    // assemble density, generalized gradient, etc. -> _rhoxc
    _rhoxc.pluck(0) = _drho.pluck(0);
    if (max_drho_needed >= 1) {
      auto sigma = _rhoxc.pluck(1);
      auto drhodx = _drho.pluck(1);
      auto drhody = _drho.pluck(2);
      auto drhodz = _drho.pluck(3);

      sigma = drhodx * drhodx + drhody * drhody + drhodz * drhodz;
    }
    if (max_drho_needed >= 2) {
      assert(false && "mGGA not yet implemented");
    }

    // copy _rhoxc to density as output
    density.subtensor({start, 0ll}, {stop, density.extent(1)}) = _rhoxc;

    const double dnorm = _rhoxc.norm();
    if (dnorm < thresh) {
      return;
    }

    // evaluate density functional
    df.exc_vxc(_rhoxc, _exc_vxc);

    // exc *= weights
    auto exc_density = _exc_vxc.pluck(0);
    exc_density *= _weights;

    // integrate Exc
    Exc += qleve::dot_product(_rhoxc.pluck(0), exc_density);

    // integrate Vxc
    auto vrho = _exc_vxc.pluck(1);
    if (nxc_ops == 1) {
      TensorView<1> _v_r(contractbuf.data(), tsize);
      _v_r = vrho * _weights;
      auto chi = _dbuf.reshape(tsize, ncao);

      // factor of 0.5 because it gets explicitly symmetrized at the end
      qleve::weighted_gemm("t", "n", 0.5, chi, _v_r, chi, 1.0, vcao, /*single*/ true);
    } else if (nxc_ops == 2) {
      auto vsigma = _exc_vxc.pluck(2);

      TensorView<2> wv(contractbuf.data(), tsize, 4);
      wv.pluck(0) = 0.5 * _weights * vrho;
      wv.pluck(1) = 2.0 * _drho.pluck(1) * _weights * vsigma;
      wv.pluck(2) = 2.0 * _drho.pluck(2) * _weights * vsigma;
      wv.pluck(3) = 2.0 * _drho.pluck(3) * _weights * vsigma;

      TensorView<2> aow(aow_buf.get(), tsize, ncao);
      qleve::contract(1.0, _dbuf, "xdi", wv, "xd", 0.0, aow, "xi", /*single*/ true);
      qleve::dgemm_("t", "n", ncao, ncao, tsize, 1.0, aow.data(), aow.stride(1), _dbuf.data(),
                    _dbuf.stride(2), 1.0, vcao.data(), vcao.stride(1));
    } else {
      assert(false && "mGGA not yet implemented");
    }
  };

  for (ptrdiff_t ibatch = 0; ibatch < tasks.nbatch(); ++ibatch) {
    compute_batch(ibatch);
  }

  vcao += vcao.transpose();

  return {Exc, solid_harmonics::transform_to_sao(basis, vcao)};
}
