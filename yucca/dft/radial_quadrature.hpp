// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/radial_quadrature.hpp
///
/// Radial quadrature routines for atom-centered grids
#pragma once

#include <qleve/tensor.hpp>

namespace yucca
{

enum class RadialQuad
{
  TA_M4 // Treutler-Ahlrichs M4
};

void radial_quadrature(const RadialQuad& quad, const int& npoints, const double& alpha,
                       const double& xi, qleve::TensorView<1> points, qleve::TensorView<1> weights);

std::tuple<qleve::Tensor<1>, qleve::Tensor<1>> radial_quadrature(const RadialQuad& quad,
                                                                 const int& npoints,
                                                                 const double& alpha,
                                                                 const double& xi);

/// radial quadrature from Treutler-Ahlrichs T2 M4
void radial_quadrature_m4(const int& npoints, const double& alpha, const double& xi,
                          qleve::TensorView<1> points, qleve::TensorView<1> weights);

std::tuple<qleve::Tensor<1>, qleve::Tensor<1>> radial_quadrature_m4(const int& npoints,
                                                                    const double& alpha,
                                                                    const double& xi);

} // namespace yucca
