// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/molecular_grid.cpp
///
/// Atom-centered grids for DFT integration

#include <fmt/core.h>

#include <yucca/dft/atomic_grid.hpp>
#include <yucca/dft/molecular_grid.hpp>
#include <yucca/dft/standard_grids.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/structure/molecule.hpp>

using namespace yucca;
using namespace std;
using namespace qleve;

MolecularGrid::MolecularGrid(const yucca::InputNode& input, const std::vector<Atom>& atoms)
{
  grid_level_ = input.get_if_map<ptrdiff_t>("grid level", 3);
  grid_level_ = input.get_if_map<ptrdiff_t>("grid", grid_level_);

  prune_grids_ = input.get_if_map<bool>("prune grid", true);

  fmt::print("Molecular Integration Grid\n");
  fmt::print("{:=^80s}\n", "");
  fmt::print("grid level: {:d}\n", grid_level_);
  if (prune_grids_) {
    fmt::print("grid pruning on\n");
  }

  partitions_ = create_partitions(input, atoms);

  // first figure out grid sizes
  vector<vector<int>> radial_sizes;
  radial_sizes.reserve(atoms.size());

  ptrdiff_t npoints = 0;
  for (const auto& atom : atoms) {
    atom_offsets_.push_back(npoints);

    const int row = atom.row();
    auto sizes = get_standard_grid_sizes(row, grid_level_, prune_grids_);
    const ptrdiff_t atombatch = accumulate(sizes.begin(), sizes.end(), 0ll);
    atom_sizes_.push_back(atombatch);
    npoints += atombatch;
    radial_sizes.push_back(sizes);

    fmt::print("Atom {:d}\n", radial_sizes.size());
    fmt::print("  - {:d} radial points\n", sizes.size());
    fmt::print("  - {:d} angular points\n", sizes.back());
  }

  points_ = make_unique<Tensor<2>>(3, npoints);
  weights_ = make_unique<Tensor<1>>(npoints);

  for (ptrdiff_t iat = 0; iat < atoms.size(); ++iat) {
    const auto ioff = atom_offsets_.at(iat);
    const auto isize = atom_sizes_.at(iat);
    const int charge = atoms.at(iat).charge();
    const auto center = atoms.at(iat).position();

    auto atompoints = points_->slice(ioff, ioff + isize);
    auto atomweights = weights_->slice(ioff, ioff + isize);

    const double alpha = 1.0;
    const double xi = get_treutler_ahlrichs_xi(charge);

    // atomgrid
    shelled_radialangular_grid(center, radial_sizes.at(iat), RadialQuad::TA_M4, alpha, xi,
                               atompoints, atomweights);

    // reweight using Becke partitioning
    for (ptrdiff_t ip = 0; ip < isize; ++ip) {
      const double x = atompoints(0, ip);
      const double y = atompoints(1, ip);
      const double z = atompoints(2, ip);

      const double pi = partitions_->weight(iat, x, y, z);
      atomweights(ip) *= pi;
    }
  }

  fmt::print("\n");
}
