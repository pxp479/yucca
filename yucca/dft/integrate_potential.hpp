// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/integrate_potential.hpp
///
/// Integrate a potential and output an AO basis
#pragma once

#include <qleve/tensor.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

#include <yucca/dft/molecular_grid.hpp>
#include <yucca/structure/evaluate_cao.hpp>
#include <yucca/util/distribute_tasks.hpp>

namespace yucca::dft
{

template <class Func>
qleve::Tensor<2> integrate_potential(const Basis& basis, const MolecularGrid& grid,
                                     const qleve::ConstTensorView<2> densities, Func func)
{
  const double thresh = 1e-12; // this should be an input parameter
  const ptrdiff_t batchsize = 100;
  assert(grid.points().extent(1) == densities.extent(0));

  auto& bshells = basis.basisshells();
  const size_t nshells = bshells.size();
  const ptrdiff_t np = densities.extent(0);
  const ptrdiff_t ncao = basis.ncao();

  const ptrdiff_t max_l = basis.max_angular_momentum();
  const ptrdiff_t max_cart = (max_l + 1) * (max_l + 2) / 2;

  // collect integrals in CAO basis
  qleve::Tensor<2> vcao(ncao, ncao);
  qleve::Tensor<1> densbuf(batchsize * ncao);
  qleve::Tensor<1> f_r(batchsize);

  // shell norms for bounding contributions
  qleve::Tensor<1> batchnorms(ncao);

  StaticDistributeTasks tasks(np, batchsize);

  auto compute_batch = [&](const ptrdiff_t batch) {
    const auto [start, stop, tsize] = tasks.batch(batch);
    auto _xyz = grid.points().const_slice(start, stop);
    auto _weights = grid.weights().const_slice(start, stop);
    auto _rho = densities.const_pluck(0).const_slice(start, stop);
    auto _fr = f_r.slice(0, tsize);

    qleve::TensorView<2> dbuf(densbuf.data(), tsize, ncao);
    dbuf = 0.0;

    // compute chi_mu(xi) for all mu and xi
    const double shellthresh = thresh;
    for (ptrdiff_t ish = 0; ish < nshells; ++ish) {
      const auto [ioff, iend, icart] = bshells[ish].bounds_cao();

      auto dd = dbuf.slice(ioff, iend).reshape(tsize, 1, icart);
      evaluate_shell_at_points<0>(bshells[ish], _xyz, dd);
    }

    // for (ptrdiff_t icao = 0; icao < ncao; ++icao) {
    //  batchnorms(icao) = sqrt(dot_product(tsize, &densbuf(0, icao), &densbuf(0, icao)));
    //}

    // this is probably inefficient. still need to do some batching and screening
    // compute f(r_i) * w_i on batch of points
    for (ptrdiff_t ip = 0; ip < tsize; ++ip) {
      const double x = _xyz(0, ip);
      const double y = _xyz(1, ip);
      const double z = _xyz(2, ip);

      qleve::Tensor<2> r(densities.const_subtensor({start + ip, 0}, {start + ip + 1, 1}));
      f_r(ip) = func(x, y, z, r.reshape(r.size())) * _weights(ip);
    }

    qleve::weighted_gemm("t", "n", 1.0, dbuf, f_r, dbuf, 1.0, vcao, /*single*/ true);
  };

  for (ptrdiff_t ibatch = 0; ibatch < tasks.nbatch(); ++ibatch) {
    compute_batch(ibatch);
  }

  return solid_harmonics::transform_to_sao(basis, vcao);
}

} // namespace yucca::dft
