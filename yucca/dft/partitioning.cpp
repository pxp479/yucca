// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/partitioning.cpp
///
/// Becke partition functions

#include <yucca/dft/partitioning.hpp>

using namespace yucca;
using namespace std;
using namespace qleve;

BeckeADFPartition::BeckeADFPartition(const yucca::InputNode& input, const vector<Atom>& atoms) :
    nregions_(atoms.size())
{
  centers_ = make_unique<Tensor<2>>(3, nregions_);
  etas_ = make_unique<Tensor<1>>(nregions_);

  for (ptrdiff_t i = 0; i < nregions_; ++i) {
    const auto& atom = atoms[i];
    const auto [x, y, z] = atom.position();
    centers_->at(0, i) = x;
    centers_->at(1, i) = y;
    centers_->at(2, i) = z;

    etas_->at(i) = (atom.charge() == 1) ? 0.3 : 1.0;
  }
}

double BeckeADFPartition::nonnormed_partition(const ptrdiff_t& region, const double& x,
                                              const double& y, const double& z)
{
  const double xi = x - centers_->at(0, region);
  const double yi = y - centers_->at(1, region);
  const double zi = z - centers_->at(2, region);
  const double ri = std::sqrt(xi * xi + yi * yi + zi * zi);

  const double pu = (ri == 0) ? 1.0 : etas_->at(region) * std::exp(-2.0 * ri) / (ri * ri * ri);
  return pu;
}

double BeckeADFPartition::weight(const ptrdiff_t& region, const double& x, const double& y,
                                 const double& z)
{
  double pu = 0.0;
  double ptot = 0.0;
  for (ptrdiff_t i = 0; i < nregions_; ++i) {
    const double pi = this->nonnormed_partition(i, x, y, z);
    ptot += pi;
    if (i == region)
      pu = pi;
  }

  return pu / ptot;
}

shared_ptr<BeckePartition_> yucca::create_partitions(const yucca::InputNode& input,
                                                     const std::vector<Atom>& atoms)
{
  // out of laziness for now, this will only make the BeckeADFPartition function
  // but we keep the structure here so it can be amended in the future
  shared_ptr<BeckePartition_> out;

  out = static_pointer_cast<BeckePartition_>(make_shared<BeckeADFPartition>(input, atoms));

  return out;
}

