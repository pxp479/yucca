// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/libxc_interface.cpp
///
/// Wrapper for using libxc

#include <map>
#include <string>
#include <xc.h>

#include <fmt/core.h>
#include <fmt/format.h>

#include <yucca/dft/libxc_interface.hpp>

using namespace yucca;
using namespace std;

int libxc::get_functional_id(const std::string libxcname)
{
  return xc_functional_get_number(libxcname.c_str());
}

string libxc::get_functional_family(const int func_id)
{
  int family_id;
  int number_in_family;
  const int success = xc_family_from_id(func_id, &family_id, &number_in_family);

  string out;
  switch (family_id) {
#if XC_MAJOR_VERSION == 5 || XC_MAJOR_VERSION == 6
    case XC_FAMILY_HYB_LDA:
#endif
    case XC_FAMILY_LDA:
      out = "lda";
      break;
#if XC_MAJOR_VERSION == 5 || XC_MAJOR_VERSION == 6
    case XC_FAMILY_HYB_GGA:
#endif
    case XC_FAMILY_GGA:
      out = "gga";
      break;
#if XC_MAJOR_VERSION == 5 || XC_MAJOR_VERSION == 6
    case XC_FAMILY_HYB_MGGA:
#endif
    case XC_FAMILY_MGGA:
      out = "mgga";
      break;
    default:
      out = "unknown";
      break;
  }
  return out;
}

string libxc::get_functional_name(const int func_id)
{
  char* name = xc_functional_get_name(func_id);
  string out(name);
  free(name);
  return out;
}

string libxc::get_version()
{
  int vmajor, vminor, vpatch;
  xc_version(&vmajor, &vminor, &vpatch);
  return fmt::format("{}.{}.{}", vmajor, vminor, vpatch);
}

string libxc::get_libxc_reference() { return xc_reference(); }

libxc::XCFunc::XCFunc(const std::string libxcname, const int nspin) : nspin_(nspin)
{
  assert(nspin_ == 1 || nspin_ == 2);
  functional_id_ = get_functional_id(libxcname);
  xc_ = xc_func_alloc();
  const int xcspin = nspin_ == 1 ? XC_UNPOLARIZED : XC_POLARIZED;
  xc_func_init(xc_, functional_id_, xcspin);
}

libxc::XCFunc::~XCFunc()
{
  xc_func_end(xc_);
  xc_func_free(xc_);
}

bool libxc::XCFunc::has_exc() const { return xc_->info->flags | XC_FLAGS_HAVE_EXC; }

bool libxc::XCFunc::has_vxc() const { return xc_->info->flags | XC_FLAGS_HAVE_VXC; }

bool libxc::XCFunc::has_fxc() const { return xc_->info->flags | XC_FLAGS_HAVE_FXC; }

bool libxc::XCFunc::has_gxc() const
{
  // libxc calls third derivatives kxc
  // turbomole world calls third derivatives gxc
  return xc_->info->flags | XC_FLAGS_HAVE_KXC;
}

int libxc::XCFunc::needs_derivatives_up_to() const
{
  if (const string fam = this->family(); fam == "lda") {
    return 0;
  } else if (fam == "gga") {
    return 1;
  } else if (fam == "mgga") {
    return 2;
  } else {
    assert(false);
    return 999;
  }
}

void libxc::XCFunc::set_spin(const int nspin)
{
  assert(nspin == 1 || nspin == 2);
  if (nspin != nspin_) {
    const int xcspin = nspin == 1 ? XC_UNPOLARIZED : XC_POLARIZED;
    nspin_ = nspin;
    xc_func_end(xc_);
    xc_func_init(xc_, functional_id_, xcspin);
  }
}

string libxc::XCFunc::hybrid_type() const
{
#if XC_MAJOR_VERSION == 7
  // clang-format off
  const static map<int, string> hyb_types{
      {XC_HYB_SEMILOCAL,     "semilocal"},
      {XC_HYB_HYBRID,        "hybrid"},
      {XC_HYB_CAM,           "cam"},
      {XC_HYB_CAMY,          "camy"},
      {XC_HYB_CAMG,          "camg"},
      {XC_HYB_DOUBLE_HYBRID, "double"},
      {XC_HYB_MIXTURE,       "mixture"}
  };
  // clang-format on

  const int hyb_type = xc_hyb_type(xc_);
  assert(hyb_type >= 0);

  return hyb_types.at(hyb_type);
#elif XC_MAJOR_VERSION == 5 || XC_MAJOR_VERSION == 6
  // clang-format off
  const static map<int, string> hyb_types{
      {XC_FAMILY_UNKNOWN,     "unknown"},
      {XC_FAMILY_LDA,         "semilocal"},
      {XC_FAMILY_GGA,         "semilocal"},
      {XC_FAMILY_MGGA,        "semilocal"},
      {XC_FAMILY_LCA,         "semilocal"},
      {XC_FAMILY_OEP,         "oep"},
      {XC_FAMILY_HYB_GGA,     "hybrid"},
      {XC_FAMILY_HYB_MGGA,    "hybrid"},
      {XC_FAMILY_HYB_LDA,     "hybrid"}
  };
  // clang-format on

  int family_id;
  int number_in_family;
  const int success = xc_family_from_id(this->functional_id_, &family_id, &number_in_family);
  assert(family_id >= 0);

  return hyb_types.at(family_id);
#else
  assert(false);
  return "unknown";
#endif
}

double libxc::XCFunc::exchange_factor() const
{
  if (const string hybtype = this->hybrid_type(); hybtype == "semilocal") {
    return 0.0;
  } else if (hybtype == "cam") {
    const auto [omega, alpha, beta] = rsh_factors();
    return alpha;
  } else {
    return xc_hyb_exx_coef(xc_);
  }
}

array<double, 3> libxc::XCFunc::rsh_factors() const
{
  double omega, alpha, beta;
  xc_hyb_cam_coef(xc_, &omega, &alpha, &beta);
  return {omega, alpha, beta};
}

vector<string> libxc::XCFunc::references() const
{
  vector<string> out;
  for (int i = 0; xc_->info->refs[i] != NULL; ++i) {
    out.emplace_back(xc_->info->refs[i]->ref);
  }
  return out;
}

vector<string> libxc::XCFunc::dois() const
{
  vector<string> out;
  for (int i = 0; xc_->info->refs[i] != NULL; ++i) {
    out.emplace_back(xc_->info->refs[i]->doi);
  }
  return out;
}

string libxc::XCFunc::doi() const { return fmt::format("{}", fmt::join(dois(), ", ")); }

// functions to evalue Exc
void libxc::XCFunc::exc_lda(const rho_t& rho, exc_t exc)
{
  const size_t np = static_cast<size_t>(exc.size());
  assert(rho.size() == np * nspin_);
  xc_lda_exc(xc_, np, rho.data(), exc.data());
}
void libxc::XCFunc::exc_gga(const rho_t& rho, const rho_t& sigma, exc_t exc)
{
  const size_t np = static_cast<size_t>(exc.size());
  assert(rho.size() == np * nspin_);
  assert(sigma.size() == (nspin_ == 2 ? 3 : 1) * np);
  xc_gga_exc(xc_, np, rho.data(), sigma.data(), exc.data());
}
void libxc::XCFunc::exc_mgga(const rho_t& rho, const rho_t& sigma, const rho_t& lapl,
                             const rho_t& tau, exc_t exc)
{
  const size_t np = static_cast<size_t>(exc.size());
  assert(rho.size() == np * nspin_);
  assert(sigma.size() == (nspin_ == 2 ? 3 : 1) * np);
  assert(lapl.size() == nspin_ * np && tau.size() == nspin_ * np);
  xc_mgga_exc(xc_, np, rho.data(), sigma.data(), lapl.data(), tau.data(), exc.data());
}

// functions to evalue Vxc
void libxc::XCFunc::vxc_lda(const rho_t& rho, exc_t vrho)
{
  const size_t np = static_cast<size_t>(rho.size() / nspin_);
  assert(rho.size() == np * nspin_ && vrho.size() == np * nspin_);
  xc_lda_vxc(xc_, np, rho.data(), vrho.data());
}
void libxc::XCFunc::vxc_gga(const rho_t& rho, const rho_t& sigma, exc_t vrho, exc_t vsigma)
{
  const size_t np = static_cast<size_t>(rho.size() / nspin_);
  assert(rho.size() == np * nspin_ && vrho.size() == np * nspin_);
  assert(sigma.size() == (nspin_ == 2 ? 3 : 1) * np && vsigma.size() == (nspin_ == 2 ? 3 : 1) * np);
  xc_gga_vxc(xc_, np, rho.data(), sigma.data(), vrho.data(), vsigma.data());
}
void libxc::XCFunc::vxc_mgga(const rho_t& rho, const rho_t& sigma, const rho_t& lapl,
                             const rho_t& tau, exc_t vrho, exc_t vsigma, exc_t vlapl, exc_t vtau)
{
  const size_t np = static_cast<size_t>(rho.size() / nspin_);
  assert(rho.size() == np * nspin_ && vrho.size() == np * nspin_);
  assert(sigma.size() == (nspin_ == 2 ? 3 : 1) * np && vsigma.size() == (nspin_ == 2 ? 3 : 1) * np);
  assert(lapl.size() == nspin_ * np && tau.size() == nspin_ * np && vlapl.size() == nspin_ * np
         && vtau.size() == nspin_ * np);
  xc_mgga_vxc(xc_, np, rho.data(), sigma.data(), lapl.data(), tau.data(), vrho.data(),
              vsigma.data(), vlapl.data(), vsigma.data());
}

// functions to evalue Exc & Vxc
void libxc::XCFunc::exc_vxc_lda(const rho_t& rho, exc_t exc, exc_t vrho)
{
  const size_t np = exc.size();
  assert(rho.size() == np * nspin_ && vrho.size() == np * nspin_);
  xc_lda_exc_vxc(xc_, np, rho.data(), exc.data(), vrho.data());
}
void libxc::XCFunc::exc_vxc_gga(const rho_t& rho, const rho_t& sigma, exc_t exc, exc_t vrho,
                                exc_t vsigma)
{
  const size_t np = exc.size();
  assert(rho.size() == np * nspin_ && vrho.size() == np * nspin_);
  assert(sigma.size() == (nspin_ == 2 ? 3 : 1) * np && vsigma.size() == (nspin_ == 2 ? 3 : 1) * np);
  xc_gga_exc_vxc(xc_, np, rho.data(), sigma.data(), exc.data(), vrho.data(), vsigma.data());
}
void libxc::XCFunc::exc_vxc_mgga(const rho_t& rho, const rho_t& sigma, const rho_t& lapl,
                                 const rho_t& tau, exc_t exc, exc_t vrho, exc_t vsigma, exc_t vlapl,
                                 exc_t vtau)
{
  const size_t np = exc.size();
  assert(rho.size() == np * nspin_ && vrho.size() == np * nspin_);
  assert(sigma.size() == (nspin_ == 2 ? 3 : 1) * np && vsigma.size() == (nspin_ == 2 ? 3 : 1) * np);
  assert(lapl.size() == nspin_ * np && tau.size() == nspin_ * np && vlapl.size() == nspin_ * np
         && vtau.size() == nspin_ * np);
  xc_mgga_exc_vxc(xc_, np, rho.data(), sigma.data(), lapl.data(), tau.data(), exc.data(),
                  vrho.data(), vsigma.data(), vlapl.data(), vsigma.data());
}

void libxc::XCFunc::exc(const qleve::ConstTensorView<2>& rhos, qleve::TensorView<2> vrhos)
{
  if (nspin_ == 1) {
    if (const string fam = this->family(); fam == "lda") {
      auto rho = rhos.const_pluck(0);
      auto exc = vrhos.pluck(0);
      exc_lda(rho, exc);
    } else if (fam == "gga") {
      auto rho = rhos.const_pluck(0);
      auto sigma = rhos.const_pluck(1);
      auto exc = vrhos.pluck(0);
      exc_gga(rho, sigma, exc);
    } else if (fam == "mgga") {
      auto rho = rhos.const_pluck(0);
      auto sigma = rhos.const_pluck(1);
      auto lapl = rhos.const_pluck(2);
      auto tau = rhos.const_pluck(3);
      auto exc = vrhos.pluck(0);
      exc_mgga(rho, sigma, lapl, tau, exc);
    } else {
      assert(false);
    }
  } else {
    assert("spin polarized not yet implemented");
  }
}

void libxc::XCFunc::vxc(const qleve::ConstTensorView<2>& rhos, qleve::TensorView<2> vrhos)
{
  if (nspin_ == 1) {
    if (const string fam = this->family(); fam == "lda") {
      auto rho = rhos.const_pluck(0);
      auto vrho = vrhos.pluck(0);

      vxc_lda(rho, vrho);
    } else if (fam == "gga") {
      auto rho = rhos.const_pluck(0);
      auto sigma = rhos.const_pluck(1);
      auto vrho = vrhos.pluck(0);
      auto vsigma = vrhos.pluck(1);

      vxc_gga(rho, sigma, vrho, vsigma);
    } else if (fam == "mgga") {
      auto rho = rhos.const_pluck(0);
      auto sigma = rhos.const_pluck(1);
      auto lapl = rhos.const_pluck(2);
      auto tau = rhos.const_pluck(3);

      auto vrho = vrhos.pluck(0);
      auto vsigma = vrhos.pluck(1);
      auto vlapl = vrhos.pluck(2);
      auto vtau = vrhos.pluck(3);

      vxc_mgga(rho, sigma, lapl, tau, vrho, vsigma, vlapl, vtau);
    } else {
      assert(false);
    }
  } else {
    assert("spin polarized not yet implemented");
  }
}

void libxc::XCFunc::exc_vxc(const qleve::ConstTensorView<2>& rhos, qleve::TensorView<2> vrhos)
{
  if (nspin_ == 1) {
    if (const string fam = this->family(); fam == "lda") {
      auto rho = rhos.const_pluck(0);
      auto exc = vrhos.pluck(0);
      auto vrho = vrhos.pluck(1);

      exc_vxc_lda(rho, exc, vrho);
    } else if (fam == "gga") {
      auto rho = rhos.const_pluck(0);
      auto sigma = rhos.const_pluck(1);
      auto exc = vrhos.pluck(0);
      auto vrho = vrhos.pluck(1);
      auto vsigma = vrhos.pluck(2);

      exc_vxc_gga(rho, sigma, exc, vrho, vsigma);
    } else if (fam == "mgga") {
      auto rho = rhos.const_pluck(0);
      auto sigma = rhos.const_pluck(1);
      auto lapl = rhos.const_pluck(2);
      auto tau = rhos.const_pluck(3);

      auto exc = vrhos.pluck(0);
      auto vrho = vrhos.pluck(1);
      auto vsigma = vrhos.pluck(2);
      auto vlapl = vrhos.pluck(3);
      auto vtau = vrhos.pluck(4);

      exc_vxc_mgga(rho, sigma, lapl, tau, exc, vrho, vsigma, vlapl, vtau);
    } else {
      assert(false);
    }
  } else {
    assert("spin polarized not yet implemented");
  }
}
