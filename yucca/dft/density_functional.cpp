// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/density_functional.cpp
///
/// Wrapper for computing density functionals

#include <map>
#include <memory>
#include <string>

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <yucca/dft/density_functional.hpp>
#include <yucca/input/input_node.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

static map<string, vector<pair<string, double>>> common_functionals{
    {"vwn", {{"xc_lda_x", 1.0}, {"xc_lda_c_vwn", 1.0}}},
    {"pbe", {{"xc_gga_x_pbe", 1.0}, {"xc_gga_c_pbe", 1.0}}},
    {"pbe0", {{"hyb_gga_xc_pbeh", 1.0}}},
    {"blyp", {{"xc_gga_x_bp86", 1.0}, {"xc_gga_c_lyp", 1.0}}},
    {"b3lyp", {{"hyb_gga_xc_b3lyp", 1.0}}}};

yucca::DensityFunctional::DensityFunctional(const InputNode input, const int nspin) : nspin_(nspin)
{
  name_ = "";
  if (input.is_string()) {
    name_ = input.get<string>();
  } else if (input.is_map() && input.contains("functional")) {
    name_ = input.get<string>("functional");
  } else {
    throw runtime_error("No functional specified!");
  }

  if (auto iter = common_functionals.find(name_); iter != common_functionals.end()) {
    const auto func_list = iter->second;
    for (const auto [xcname, xcfactor] : func_list) {
      auto xcfunc = make_shared<libxc::XCFunc>(xcname, nspin);
      xc_funcs_.emplace_back(xcfunc, xcfactor);
    }
  } else {
    fmt::print("Functional \"{}\" not found. The allowed functionals are:\n", name_);
    for (const auto& func : common_functionals) {
      fmt::print(" - {}\n", func.first);
    }
    throw runtime_error("Functional not found!");
  }
}

void yucca::DensityFunctional::print_header() const
{
  fmt::print("DFT\n");
  fmt::print("{:=^80s}\n", "");
  fmt::print("  functional: {}\n", this->name());
  for (const auto [xc, fac] : xc_funcs_) {
    fmt::print("  - {:3f} x {:s} (doi: {:s})\n", fac, xc->name(), xc->doi());
  }
  fmt::print("  exchange factor: {:3f}\n", this->exchange_factor());

  fmt::print("\n");
  fmt::print("Density Functionals provided by Libxc v{:s}:\n", libxc::get_version());
  fmt::print("{:s}\n", libxc::get_libxc_reference());
  fmt::print("\n");
}

int yucca::DensityFunctional::needs_derivatives_up_to() const
{
  vector<int> orders(xc_funcs_.size());
  transform(xc_funcs_.begin(), xc_funcs_.end(), orders.begin(),
            [](const auto xc) { return xc.first->needs_derivatives_up_to(); });
  return *max_element(orders.begin(), orders.end());
}

int yucca::DensityFunctional::nxc_operations() const
{
  const auto n = this->needs_derivatives_up_to();
  const static vector<int> nxc{1, 2, 4};
  return nxc.at(n);
}

double yucca::DensityFunctional::exchange_factor() const
{
  double out = 0.0;
  for (const auto& [xc, factor] : xc_funcs_) {
    out += xc->exchange_factor() * factor;
  }
  return out;
}

void DensityFunctional::exc(const qleve::ConstTensorView<2>& densities,
                            qleve::TensorView<1> Exc) const
{
  if (xc_funcs_.size() == 0) {
    return;
  } else {
    auto exc_buf = Exc.reshape(Exc.size(), 1).zeros_like();
    for (const auto [xc, factor] : xc_funcs_) {
      xc->exc(densities, exc_buf);
      Exc += factor * exc_buf.pluck(0);
    }
  }
}

void DensityFunctional::vxc(const qleve::ConstTensorView<2>& densities,
                            qleve::TensorView<2> vdensities) const
{
  if (xc_funcs_.size() == 0) {
    return;
  } else {
    auto vbuf = vdensities.zeros_like();
    for (const auto [xc, factor] : xc_funcs_) {
      xc->vxc(densities, vbuf);
      vdensities += factor * vbuf;
    }
  }
}

void DensityFunctional::exc_vxc(const qleve::ConstTensorView<2>& densities,
                                qleve::TensorView<2> vdensities) const
{
  if (xc_funcs_.size() == 0) {
    return;
  } else {
    auto vbuf = vdensities.zeros_like();
    for (const auto [xc, factor] : xc_funcs_) {
      xc->exc_vxc(densities, vbuf);
      vdensities += factor * vbuf;
    }
  }
}

