// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/standard_grids.cpp
///
/// "Standard" grid definitions, like the Ahlrichs grids

#include <yucca/dft/standard_grids.hpp>
#include <yucca/physical/constants.hpp>

using namespace yucca;
using namespace std;

int get_standard_angular_size(const int ptrow, const int grid_level)
{
  // clang-format off
  const static vector<vector<int>> grids =
  // grid level   1    2    3    4    5    6    7    8     9    ptable row
            { {  50, 110, 194, 302, 434, 590,   770,  974, 1202 }, // 1
              { 110, 194, 302, 434, 974, 1202, 1454, 1454, 1454 }, // >2
            };
  // clang-format on

  const int pt = ptrow > 1 ? 1 : 0;
  return grids[pt][grid_level - 1];
}

int get_standard_radial_size(const int ptrow, const int grid_level)
{
  // clang-format off
  //                         grid level  1   2   3   4   5   6   7   8    9    ptable row
  const static vector<vector<int>> grids = { { 15, 20, 25, 30, 40, 50, 60,  75, 100 },   // 1
                                      { 20, 25, 30, 35, 45, 55, 65,  80, 100 },   // 2
                                      { 25, 20, 35, 40, 50, 60, 70,  85, 100 },   // 3
                                      { 30, 35, 40, 45, 55, 65, 75,  90, 100 },   // 4
                                      { 35, 40, 45, 50, 60, 70, 80,  95, 100 },   // 5
                                      { 40, 45, 50, 55, 65, 75, 85, 100, 100 },   // 6
                                      { 40, 45, 50, 55, 65, 75, 85, 100, 100 } }; // 7
  // clang-format on
  const int pt = std::min(ptrow, 7);
  return grids[pt - 1][grid_level - 1];
}

vector<int> yucca::get_standard_grid_sizes(const int row, const int grid_level, const bool prune)
{
  const int nr = get_standard_radial_size(row, grid_level);
  const int nw = get_standard_angular_size(row, grid_level);
  vector<int> out(nr, nw);
  if (prune) {
    for (int ir = 0; ir < nr / 2; ++ir) {
      out[ir] = (ir < (nr / 3)) ? 14 : 50;
    }
  }
  return out;
}

static vector<double> xi_vals = {0.8, 0.9, 1.8, 1.4, 1.3, 1.1, 0.9, 0.9, 0.9, 0.9, 1.4, 1.3,
                                 1.3, 1.2, 1.1, 1.0, 1.0, 1.0, 1.5, 1.4, 1.3, 1.2, 1.2, 1.2,
                                 1.2, 1.2, 1.2, 1.1, 1.1, 1.1, 1.1, 1.0, 0.9, 0.9, 0.9, 0.9};

double yucca::get_treutler_ahlrichs_xi(const int element_charge)
{
  if (element_charge - 1 < xi_vals.size()) {
    return xi_vals[element_charge - 1];
  } else {
    return 1.0;
  }
}
