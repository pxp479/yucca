// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/lebedev_laikov.hpp
///
/// Atom-centered grids for DFT integration
#pragma once

#include <qleve/tensor.hpp>

namespace yucca
{

void lebedev_angular_grid(const int& npoints, const double& radius,
                          const std::array<double, 3>& center, qleve::TensorView<1> x,
                          qleve::TensorView<1> y, qleve::TensorView<1> z,
                          qleve::TensorView<1> weights);

std::tuple<qleve::Tensor<2>, qleve::Tensor<1>> lebedev_angular_grid(
    const int& npoints, const double& radius,
    const std::array<double, 3>& center = {0.0, 0.0, 0.0});

} // namespace yucca
