// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/radial_quadrature.cpp
///
/// Radial quadrature routines for atom-centered grids

#include <yucca/dft/radial_quadrature.hpp>
#include <yucca/physical/constants.hpp>

using namespace yucca;


/// generic radial quadrature interface
void yucca::radial_quadrature(const RadialQuad& quadrature, const int& npoints, const double& alpha,
                              const double& xi, qleve::TensorView<1> points,
                              qleve::TensorView<1> weights)
{
  switch (quadrature) {
    case RadialQuad::TA_M4:
      radial_quadrature_m4(npoints, alpha, xi, points, weights);
      break;
  }
}


/// generic radial quadrature interface
std::tuple<qleve::Tensor<1>, qleve::Tensor<1>> yucca::radial_quadrature(
    const RadialQuad& quadrature, const int& npoints, const double& alpha, const double& xi)
{
  qleve::Tensor<1> points(npoints);
  qleve::Tensor<1> weights(npoints);
  radial_quadrature(quadrature, npoints, alpha, xi, points, weights);
  return std::make_tuple(points, weights);
}


/// radial quadrature from Treutler-Ahlrichs T2 M4
void yucca::radial_quadrature_m4(const int& npoints, const double& alpha, const double& xi,
                                 qleve::TensorView<1> points, qleve::TensorView<1> weights)
{
  const double pi = physical::pi<double>;

  assert(points.size() == npoints);
  assert(weights.size() == npoints);

  qleve::Tensor<1> ii(npoints);
  std::iota(ii.data(), ii.data() + ii.size(), 1ll);

  const double np1 = npoints + 1.0;
  qleve::Tensor<1> xx(qleve::cos(pi * ii / np1));

  const double ln2 = std::log(2.0);

  points = (xi / ln2) * qleve::pow(1.0 + xx, alpha) * qleve::log(2.0 / (1.0 - xx));

  const double fac = xi * xi * xi * pi / np1 / (ln2 * ln2 * ln2);
  weights = fac * qleve::pow(1.0 + xx, 3.0 * alpha)
            * (qleve::sqrt((1.0 + xx) / (1.0 - xx)) * qleve::pow(qleve::log(0.5 * (1.0 - xx)), 2.)
               - alpha * qleve::sqrt((1.0 - xx) / (1.0 + xx))
                     * qleve::pow(qleve::log(0.5 * (1.0 - xx)), 3.));
}

/// radial quadrature from Treutler-Ahlrichs T2 M4
std::tuple<qleve::Tensor<1>, qleve::Tensor<1>> yucca::radial_quadrature_m4(const int& npoints,
                                                                           const double& alpha,
                                                                           const double& xi)
{
  qleve::Tensor<1> points(npoints);
  qleve::Tensor<1> weights(npoints);
  yucca::radial_quadrature_m4(npoints, alpha, xi, points, weights);
  return std::make_tuple(points, weights);
}
