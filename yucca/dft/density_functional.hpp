// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/density_functional.hpp
///
/// Wrapper for computing density functionals
#pragma once

#include <memory>

#include <yucca/dft/libxc_interface.hpp>
#include <yucca/input/input_node.hpp>

namespace yucca
{

class DensityFunctional {
 public:
  using xc_ptr = std::shared_ptr<libxc::XCFunc>;

 protected:
  std::vector<std::pair<xc_ptr, double>> xc_funcs_;
  ptrdiff_t nspin_;
  std::string name_;

 public:
  DensityFunctional(const InputNode input, const int nspin = 1);

  std::string name() const { return name_; }

  void print_header() const;

  int needs_derivatives_up_to() const;
  int nxc_operations() const;
  double exchange_factor() const;

  void exc(const qleve::ConstTensorView<2>& densities, qleve::TensorView<1> Exc) const;
  void vxc(const qleve::ConstTensorView<2>& densities, qleve::TensorView<2> vdensities) const;
  void exc_vxc(const qleve::ConstTensorView<2>& densities, qleve::TensorView<2> vdensities) const;
};


} // namespace yucca

