// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/main.cpp
///
/// \brief Main executable of yucca

#define TBB_PREVIEW_GLOBAL_CONTROL 1

#include <any>
#include <iostream>
#include <memory>
#include <thread>

#include <cxxopts.hpp>
#include <fmt/core.h>
#include <tbb/global_control.h>
#include <tbb/tbb.h>
#include <yaml-cpp/yaml.h>

#include <yucca/input/input_node.hpp>
#include <yucca/input/read_input.hpp>
#include <yucca/resmf/reshf.hpp>
#include <yucca/resmf/ureshf.hpp>
#include <yucca/structure/basisset.hpp>
#include <yucca/structure/density_fitting.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/structure/load_geometry.hpp>
#include <yucca/structure/molecule.hpp>
#include <yucca/util/libint_interface.hpp>
#include <yucca/util/mdi_interface.hpp>
#include <yucca/util/method.hpp>
#include <yucca/version.hpp>
#include <yucca/wfn/load_wfn.hpp>
#include <yucca/wfn/wfn.hpp>

using namespace std;

int main(int argc, char* argv[])
{
  // scan command-line arguments for -mdi <options>
  // if present, they'll be parsed out, "normal" yucca will run,
  // and then downstream MDI functions will be activated
  // if absent, downstream MDI functions will just throw some warning messages
  MDI_Init(&argc, &argv);

  string input;

  // default is to read from thread::hardware_concurrency
  int nthreads = std::thread::hardware_concurrency();
  // override with environment variable
  const char* cnthreads = getenv("TBB_NUM_THREADS");
  if (cnthreads) {
    nthreads = max(1, atoi(cnthreads));
  }

  try {
    cxxopts::Options options("yucca", "Command line options for program yucca");
    options.positional_help("<input>").show_positional_help();

    // clang-format off
    options.add_options()
      ("i,input", "input .yaml file", cxxopts::value<string>(input))
      ( "n,ncpu", "number of cpus to use", cxxopts::value<int>(nthreads)->default_value(fmt::format("{:d}", nthreads)))
      ( "h,help", "Print help");
    // clang-format on

    options.parse_positional({"input"});

    auto result = options.parse(argc, argv);

    bool print_help = result.count("help") > 0;

    if (auto inputcount = result.count("input"); inputcount == 0) {
      fmt::print("input file required!\n");
      print_help = true;
    } else if (inputcount > 1) {
      fmt::print("only one input file currently accepted!\n");
      print_help = true;
    }

    if (print_help) {
      fmt::print("{}\n", options.help());
      return 0;
    }
  } catch (const cxxopts::exceptions::exception& e) {
    fmt::print("error parsing options: {}\n", e.what());
    return 1;
  }

  fmt::print("Using {:d} TBB threads\n", nthreads);
  tbb::global_control control(tbb::global_control::max_allowed_parallelism, nthreads);

  fmt::print("yucca {:s}\n", yucca::version::description());
  fmt::print("{:-^40s}\n\n", "");

  fmt::print("reading from \"{}\"\n", input);
  fmt::print("{:-^40s}\n", "");
  yucca::InputNode config = yucca::read_input(input);
  config.print(cout);
  fmt::print("{:-^40s}\n\n", "");

  if (!config.contains("stages") && !config.is_map()) {
    fmt::print("Please specify the order of calculations with a stages key,"
               " or include only one map in \"{}\".\n",
               input);
    return 1;
  }

  vector<const yucca::InputNode*> stages;
  vector<string> stage_names;
  if (config.contains("stages")) {
    stage_names = config.get_vector<string>("stages");
    for (auto& st : stage_names) {
      stages.push_back(&config.get_node(st));
    }
  } else {
    stages.push_back(&config);
    stage_names = {"root"};
  }

  libint2::initialize();

  shared_ptr<yucca::Geometry> geometry;
  shared_ptr<yucca::Wfn> wfn;
  shared_ptr<yucca::Method_> method;

  size_t istage = 0;
  for (auto stage : stages) {
    const yucca::InputNode& cs = *stage;
    if (cs.contains("title")) {
      fmt::print("Starting: {}\n", cs.get<string>("title"));
    }
    fmt::print("Stage: {}\n", stage_names.at(istage));

    tie(geometry, wfn) = load_geometry(cs, geometry, wfn);

    if (cs.contains("load")) {
      const string ld = cs.get<string>("load");
      fmt::print("Loading previous data from {}\n", ld);
      wfn = load_wfn_from_file(geometry, ld);
    }

    // now do a calculation? include a bagel::Reference-like object?
    if (wfn) {
      method = yucca::get_method(cs, wfn);
    } else {
      method = yucca::get_method(cs, geometry);
    }
    method->compute();
    wfn = method->wavefunction();

    if (cs.contains("save")) {
      const string sv = cs.get<string>("save");
      fmt::print("Saving reference state to file {}\n", sv);
      wfn->save(sv);
    }

    if (cs.get<bool>("debug", false)) {
      fmt::print("Debug mode selected\n");
      const string method_name = cs.get<string>("method");

      if (method_name == "ureshf") {
        const double h = cs.get<double>("h", 0.0001);
        yucca::fd_fock_ureshf(method, geometry, h);

      } else if (method_name == "reshf") {
        const double h = cs.get<double>("h", 0.0001);
        yucca::fd_fock_rreshf(method, geometry, h);

      } else {
        throw runtime_error("Debugging not implemented for this method!");
      }
    } // end debugging routines

    istage++;
  } // move to next stage

  // register all supported MDI nodes and commands
  MDI_Register_node("@DEFAULT");
  MDI_Register_command("@DEFAULT", "EXIT");
  MDI_Register_command("@DEFAULT", "<ENERGY");
  MDI_Register_command("@DEFAULT", "<NATOMS");
  MDI_Register_command("@DEFAULT", ">COORDS");

  // check for incoming MDI commands
  MDI_Comm mdi_comm = MDI_COMM_NULL;
  MDI_Accept_communicator(&mdi_comm);

  // if -mdi <options> was provided, execute requested MDI commands
  if (mdi_comm != MDI_COMM_NULL) {
    const yucca::InputNode& last_stage = *(stages.back());
    yucca::mdi::run_mdi("@DEFAULT", mdi_comm, last_stage, method, geometry);
  }

  fmt::print("Yucca is done!\n");

  libint2::finalize();
}
