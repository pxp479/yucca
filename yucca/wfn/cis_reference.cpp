// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/single_reference.cpp
///
/// Wavefunction describing a CIS reference wavefunction

#include <fstream>

#include <yucca/input/input_node.hpp>
#include <yucca/wfn/cis_reference.hpp>

using namespace yucca;
using namespace std;

void yucca::CISReference::save(const std::string& filename)
{
  InputNode outnode;

  this->save_geometry(outnode);

  outnode.put("type", "cis reference");
  outnode.put("nmo", nmo_);
  outnode.put("nocc", nocc_);
  outnode.put("nvir", nvir_);

  outnode.put("orbitals", *coeffs_);
  outnode.put("eigenvectors", *eigenstates_);

  std::ofstream outstream(filename);
  outnode.print(outstream);
}

shared_ptr<Wfn> yucca::CISReference::update_impl(shared_ptr<Geometry> geometry) const
{
  return make_shared<CISReference>(geometry, coeffs_, eigenstates_, nocc_, nvir_);
}
