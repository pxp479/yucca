// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/load_wfn.cpp
///
/// Load Wfn object from file, string, or InputNode

#include <yucca/input/input_node.hpp>
#include <yucca/input/read_input.hpp>
#include <yucca/wfn/cis_reference.hpp>
#include <yucca/wfn/load_wfn.hpp>
#include <yucca/wfn/reshf_reference.hpp>
#include <yucca/wfn/single_reference.hpp>
#include <yucca/wfn/ureshf_reference.hpp>
#include <yucca/wfn/wfn.hpp>

using namespace yucca;
using namespace qleve;
using namespace std;

shared_ptr<Wfn> yucca::load_wfn(const shared_ptr<yucca::Geometry> geo, const InputNode& node)
{
  const string wfn_type = node.get<string>("type");
  if (wfn_type == "single reference") {
    auto cc = make_shared<Tensor<2>>(node.get_tensor<double, 2>("orbitals"));
    auto fock = make_shared<Tensor<2>>(node.get_tensor<double, 2>("fock"));
    const ptrdiff_t nocc = node.get<ptrdiff_t>("nocc");
    const ptrdiff_t nvir = node.get<ptrdiff_t>("nvir");
    const double energy = node.get<double>("energy");

    return make_shared<SingleReference>(geo, cc, fock, nocc, nvir, energy);
  } else if (wfn_type == "cis reference") {
    auto cc = make_shared<Tensor<2>>(node.get_tensor<double, 2>("orbitals"));
    auto eigenvectors = make_shared<Tensor<2>>(node.get_tensor<double, 2>("eigenvectors"));

    const ptrdiff_t nocc = node.get<ptrdiff_t>("nocc");
    const ptrdiff_t nvir = node.get<ptrdiff_t>("nvir");

    return make_shared<CISReference>(geo, cc, eigenvectors, nocc, nvir);
  } else if (wfn_type == "reshf reference") {
    const ptrdiff_t nelec = node.get<ptrdiff_t>("nelec");
    const ptrdiff_t nocc = node.get<ptrdiff_t>("nocc");
    const ptrdiff_t nvir = node.get<ptrdiff_t>("nvir");
    const ptrdiff_t nmo = node.get<ptrdiff_t>("nmo");

    auto cc = make_shared<Tensor<3>>(node.get_tensor<double, 3>("orbital coeffs"));
    auto sdcc = make_shared<Tensor<2>>(node.get_tensor<double, 2>("sd coeffs"));
    auto fock = make_shared<Tensor<3>>(node.get_tensor<double, 3>("fock"));

    shared_ptr<Tensor<2>> trans =
        node.contains("transform") ?
            make_shared<Tensor<2>>(node.get_tensor<double, 2>("transform")) :
            nullptr;

    vector<pair<ptrdiff_t, ptrdiff_t>> constrained_pairs;
    if (node.contains("constrained pairs")) {
      const InputNode& cpnode = node.get_node("constrained pairs");
      const ptrdiff_t ncp = cpnode.size();
      for (ptrdiff_t i = 0; i < ncp; ++i) {
        const InputNode& cp = cpnode.get_node(i);
        const auto pq = cp.as_vector<ptrdiff_t>();
        if (pq.size() != 2) {
          throw runtime_error("Constraints can only be applied between two determinants");
        }
        constrained_pairs.emplace_back(pq[0], pq[1]);
      }
    }

    vector<double> weight = node.get_vector<double>("weight");
    const double energy = node.get<double>("energy");

    return make_shared<ResHFReference>(geo, cc, sdcc, trans, constrained_pairs, fock, weight, nelec,
                                       nocc, nvir, energy);
  } else if (wfn_type == "ureshf reference") {

    std::array<ptrdiff_t, 2> nelec;
    nelec[0] = node.get<ptrdiff_t>("nelec alpha");
    nelec[1] = node.get<ptrdiff_t>("nelec beta");

    const ptrdiff_t nmo = node.get<ptrdiff_t>("nmo");

    const double E = node.get<double>("energy SA");
    vector<double> weight = node.get_vector<double>("weight");

    auto scfco = make_shared<Tensor<2>>(node.get_tensor<double, 2>("scf coeffs"));
    auto sdco = make_shared<Tensor<4>>(node.get_tensor<double, 4>("sdet coeffs"));
    auto wfnco = make_shared<Tensor<2>>(node.get_tensor<double, 2>("wfn coeffs"));
    auto fock = make_shared<Tensor<4>>(node.get_tensor<double, 4>("fock"));

    return make_shared<uResHFReference>(geo, scfco, sdco, wfnco, fock, weight, nelec, nmo, E);
  } else {
    throw runtime_error("Unrecognized wavefunction type being loaded.");
  }
  return nullptr;
}

shared_ptr<Wfn> yucca::load_wfn_from_file(const shared_ptr<yucca::Geometry> geo,
                                          const string& filename)
{
  InputNode node = read_input(filename);
  return load_wfn(geo, node);
}

shared_ptr<Wfn> yucca::load_wfn_from_string(const shared_ptr<yucca::Geometry> geo,
                                            const string& yaml)
{
  InputNode node = read_input_stream(yaml);
  return load_wfn(geo, node);
}
