// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/reshf_reference.cpp
///
/// Wavefunction describing a linear combination of Slater Determinants

#include <fstream>

#include <qleve/tensor.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/wfn/reshf_reference.hpp>

using namespace yucca;
using namespace qleve;
using namespace std;

void yucca::ResHFReference::save(const std::string& filename)
{
  InputNode outnode;

  this->save_geometry(outnode);

  outnode.put("type", "reshf reference");
  outnode.put("nelec", nelec_);
  outnode.put("nocc", nocc_);
  outnode.put("nvir", nvir_);
  outnode.put("nmo", nmo_);

  outnode.put("orbital coeffs", *orbital_coeffs_);
  outnode.put("sd coeffs", *sd_coeffs_);
  outnode.put("fock", *fock_);
  if (transform_) {
    outnode.put("transform", *transform_);
  }
  if (!constrained_pairs_.empty()) {
    InputNode cp;
    for (const auto& [i, j] : constrained_pairs_) {
      cp.push_back(InputNode(vector<ptrdiff_t>{i, j}));
    }
    outnode.put("constrained pairs", cp);
  }
  outnode.put("weight", weight_);
  outnode.put("energy", energy_);

  ofstream outstream(filename);
  outnode.print(outstream);
}

shared_ptr<Wfn> yucca::ResHFReference::update_impl(shared_ptr<Geometry> geometry) const
{
  return make_shared<ResHFReference>(geometry, orbital_coeffs_, sd_coeffs_, transform_,
                                     constrained_pairs_, fock_, weight_, nelec_, nocc_, nvir_,
                                     energy_);
}
