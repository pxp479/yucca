// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/load_wfn.hpp
///
/// Load Wfn object from file
#pragma once

namespace yucca
{

class Wfn;      // #include <yucca/wfn/wfn.hpp>
class Geometry; // #include <yucca/structure/geometry.hpp>

std::shared_ptr<Wfn> load_wfn(const std::shared_ptr<Geometry> geometry, const InputNode& input);
std::shared_ptr<Wfn> load_wfn_from_file(const std::shared_ptr<Geometry> geometry,
                                        const std::string& filename);
std::shared_ptr<Wfn> load_wfn_from_string(const std::shared_ptr<Geometry> geometry,
                                          const std::string& yaml);

} // namespace yucca
