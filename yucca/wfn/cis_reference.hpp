// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/single_reference.hpp
///
/// Wavefunction describing a single reference wavefunction

#pragma once

#include <yucca/wfn/wfn.hpp>

namespace yucca
{

class CISReference : public Wfn {
 protected:
  const std::shared_ptr<qleve::Tensor<2>> coeffs_;
  const std::shared_ptr<qleve::Tensor<2>> eigenstates_;

  ptrdiff_t nmo_;
  ptrdiff_t nocc_;
  ptrdiff_t nvir_;

 public:
  CISReference(const std::shared_ptr<yucca::Geometry> geom,
               const std::shared_ptr<qleve::Tensor<2>> cc,
               const std::shared_ptr<qleve::Tensor<2>> eigenstates, const ptrdiff_t nocc,
               const ptrdiff_t nvir) :
      Wfn(geom),
      coeffs_(cc),
      eigenstates_(eigenstates),
      nmo_(cc->extent(1)),
      nocc_(nocc),
      nvir_(nvir)
  {}

  std::shared_ptr<qleve::Tensor<2>> coeffs() const { return coeffs_; }
  std::shared_ptr<qleve::Tensor<2>> eigenstates() const { return eigenstates_; }

  ptrdiff_t nmo() const { return nmo_; }
  ptrdiff_t nocc() const { return nocc_; }
  ptrdiff_t nvir() const { return nvir_; }

  virtual void save(const std::string& filename) override;

 private:
  virtual std::shared_ptr<Wfn> update_impl(std::shared_ptr<Geometry> geometry) const override;
};

} // namespace yucca
