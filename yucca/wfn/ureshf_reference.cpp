// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/ureshf_reference.cpp
///
/// Wavefunction describing a linear combination of Slater Determinants

#include <fstream>

#include <qleve/tensor.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/wfn/ureshf_reference.hpp>

using namespace yucca;
using namespace qleve;
using namespace std;

void yucca::uResHFReference::save(const std::string& filename)
{
  InputNode outnode;

  this->save_geometry(outnode);

  outnode.put("type", "ureshf reference");
  outnode.put("nelec alpha", nelec_[0]);
  outnode.put("nelec beta", nelec_[1]);
  outnode.put("nmo", nmo_);

  outnode.put("scf coeffs", *scf_coeffs_);
  outnode.put("sdet coeffs", *sd_coeffs_);
  outnode.put("wfn coeffs", *wfn_coeffs_);
  outnode.put("fock", *fock_);
  outnode.put("weight", weight_);
  outnode.put("energy SA", energySA_);

  ofstream outstream(filename);
  outnode.print(outstream);
}

shared_ptr<Wfn> yucca::uResHFReference::update_impl(shared_ptr<Geometry> geometry) const
{
  return make_shared<uResHFReference>(geometry, scf_coeffs_, sd_coeffs_, wfn_coeffs_, fock_,
                                      weight_, nelec_, nmo_, energySA_);
}
