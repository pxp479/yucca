// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/ureshf_reference.hpp
///
/// Wavefunction describing a linear combination of Slater Determinants

#pragma once

#include <yucca/wfn/wfn.hpp>

namespace yucca
{

class uResHFReference : public Wfn {
 protected:
  std::shared_ptr<qleve::Tensor<2>> scf_coeffs_;
  std::shared_ptr<qleve::Tensor<4>> sd_coeffs_;
  std::shared_ptr<qleve::Tensor<2>> wfn_coeffs_;
  std::shared_ptr<qleve::Tensor<4>> fock_;

  std::vector<double> weight_;

  std::array<ptrdiff_t, 2> nelec_;
  ptrdiff_t nmo_;

  double energySA_;

 public:
  uResHFReference(const std::shared_ptr<yucca::Geometry>& geom,
                  const std::shared_ptr<qleve::Tensor<2>>& scf_coeffs,
                  const std::shared_ptr<qleve::Tensor<4>>& sdets,
                  const std::shared_ptr<qleve::Tensor<2>>& wfn_coeffs,
                  const std::shared_ptr<qleve::Tensor<4>>& fock, const std::vector<double>& weight,
                  const std::array<ptrdiff_t, 2>& nelec, const ptrdiff_t nmo, const double ESA) :
      Wfn(geom),
      scf_coeffs_(scf_coeffs),
      sd_coeffs_(sdets),
      wfn_coeffs_(wfn_coeffs),
      fock_(fock),
      weight_(weight),
      nelec_(nelec),
      nmo_(nmo),
      energySA_(ESA)
  {}

  std::shared_ptr<qleve::Tensor<2>> scf() const { return scf_coeffs_; }
  std::shared_ptr<qleve::Tensor<4>> sdets() const { return sd_coeffs_; }
  std::shared_ptr<qleve::Tensor<2>> wfn_coeffs() const { return wfn_coeffs_; }
  const std::vector<double>& weight() const { return weight_; }
  std::shared_ptr<qleve::Tensor<4>> fock() const { return fock_; }

  std::array<ptrdiff_t, 2> nelec() const { return nelec_; }
  ptrdiff_t nmo() const { return nmo_; }

  virtual void save(const std::string& filename) override;

 private:
  virtual std::shared_ptr<Wfn> update_impl(std::shared_ptr<Geometry> geometry) const override;
};

} // namespace yucca
