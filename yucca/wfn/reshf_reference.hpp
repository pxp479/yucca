// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/reshf_reference.hpp
///
/// Wavefunction describing a linear combination of Slater Determinants

#pragma once

#include <yucca/wfn/wfn.hpp>

namespace yucca
{

class ResHFReference : public Wfn {
 protected:
  std::shared_ptr<qleve::Tensor<3>> orbital_coeffs_;
  std::shared_ptr<qleve::Tensor<2>> sd_coeffs_;
  std::shared_ptr<qleve::Tensor<2>> transform_;
  std::vector<std::pair<ptrdiff_t, ptrdiff_t>> constrained_pairs_;
  std::shared_ptr<qleve::Tensor<3>> fock_;

  std::vector<double> weight_;

  ptrdiff_t nelec_;
  ptrdiff_t nocc_;
  ptrdiff_t nvir_;
  ptrdiff_t nmo_;

  double energy_;

 public:
  ResHFReference(const std::shared_ptr<yucca::Geometry>& geom,
                 const std::shared_ptr<qleve::Tensor<3>>& orbital_coeffs,
                 const std::shared_ptr<qleve::Tensor<2>>& sd_coeffs,
                 const std::shared_ptr<qleve::Tensor<2>>& trans,
                 const std::vector<std::pair<ptrdiff_t, ptrdiff_t>>& constrained_pairs,
                 const std::shared_ptr<qleve::Tensor<3>>& fock, const std::vector<double>& weight,
                 const ptrdiff_t nelec, const ptrdiff_t nocc, const ptrdiff_t nvir,
                 const double energy) :
      Wfn(geom),
      orbital_coeffs_(orbital_coeffs),
      sd_coeffs_(sd_coeffs),
      transform_(trans),
      constrained_pairs_(constrained_pairs),
      fock_(fock),
      weight_(weight),
      nelec_(nelec),
      nocc_(nocc),
      nvir_(nvir),
      nmo_(nocc + nvir),
      energy_(energy)
  {}

  std::shared_ptr<qleve::Tensor<3>> orbital_coeffs() const { return orbital_coeffs_; }
  std::shared_ptr<qleve::Tensor<2>> sd_coeffs() const { return sd_coeffs_; }
  std::shared_ptr<qleve::Tensor<2>> transform() const { return transform_; }
  const std::vector<std::pair<ptrdiff_t, ptrdiff_t>>& constrained_pairs() const
  {
    return constrained_pairs_;
  }
  const std::vector<double>& weight() const { return weight_; }
  std::shared_ptr<qleve::Tensor<3>> fock() const { return fock_; }

  ptrdiff_t nelec() const { return nelec_; }
  ptrdiff_t nocc() const { return nocc_; }
  ptrdiff_t nvir() const { return nvir_; }
  ptrdiff_t nmo() const { return nmo_; }

  virtual void save(const std::string& filename) override;

 private:
  virtual std::shared_ptr<Wfn> update_impl(std::shared_ptr<Geometry> geometry) const override;
};

} // namespace yucca
