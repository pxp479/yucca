// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/density_fitting.cpp
///
/// Compute objects to collect density fitting
#include <fmt/core.h>
#include <tbb/enumerable_thread_specific.h>
#include <tbb/tbb.h>

#include <qleve/cholesky.hpp>
#include <qleve/tensor.hpp>

#include <yucca/structure/density_fitting.hpp>
#include <yucca/util/libint_interface.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

DensityFitting::DensityFitting(const shared_ptr<Basis>& bas, const shared_ptr<Basis>& jkbas) :
    orbital_basis_(bas), fitting_basis_(jkbas)
{
  fmt::print("Computing 2c and 3c ERIs for density fitting\n");
  // (uv|P)
  fill_2e3c();

  // (P|Q)
  fill_2e2c();

  metric_cholesky_ = make_unique<Tensor<2>>(*metric_);
  linalg::cholesky(*metric_cholesky_);

  B_ = std::make_unique<qleve::Tensor<3>>(*Coulomb3C_);
  linalg::cholesky_solve("r", "l", "t", "n", *metric_cholesky_,
                         B_->reshape(B_->extent(0) * B_->extent(1), B_->extent(2)));
}

qleve::Tensor<3> yucca::DensityFitting::compute_B_Jinv()
{
  B_Jinv_ = make_unique<Tensor<3>>(*B_);
  linalg::cholesky_solve(
      "r", "l", "n", "n", *metric_cholesky_,
      B_Jinv_->reshape(B_Jinv_->extent(0) * B_Jinv_->extent(1), B_Jinv_->extent(2)));
  return *B_Jinv_;
}

void yucca::DensityFitting::fill_2e3c()
{
  const ptrdiff_t nao = orbital_basis_->nbasis();
  const ptrdiff_t nfit = fitting_basis_->nbasis();

  Coulomb3C_ = make_unique<Tensor<3>>(nao, nao, nfit);

  const auto max_prim = max(orbital_basis_->max_nprim(), fitting_basis_->max_nprim());
  const auto max_l =
      max(orbital_basis_->max_angular_momentum(), fitting_basis_->max_angular_momentum());

  // consider doing this in the wrapper, but it's just more convenient here for now
  libint2::Engine engine(libint2::Operator::coulomb, max_prim, max_l, 0);
  engine.set(libint2::BraKet::xs_xx);
  const auto& buf = engine.results();

  const ptrdiff_t nosh = orbital_basis_->basisshells().size();
  const ptrdiff_t nopairs = nosh * (nosh + 1) / 2;
  const ptrdiff_t nxsh = fitting_basis_->basisshells().size();

  const auto orbshells = libint::get_shells(*orbital_basis_);
  const auto& borbshells = orbital_basis_->basisshells();
  const auto fitshells = libint::get_shells(*fitting_basis_);
  const auto& bfitshells = fitting_basis_->basisshells();

  tbb::enumerable_thread_specific<libint2::Engine> engines([&]() {
    libint2::Engine out(libint2::Operator::coulomb, max_prim, max_l, 0);
    out.set(libint2::BraKet::xs_xx);
    return out;
  });


  tbb::parallel_for(tbb::blocked_range2d<size_t>(0, nxsh, 0, nosh),
                    [&](const tbb::blocked_range2d<size_t>& r) {
                      auto engine = engines.local();
                      const auto& buf = engine.results();
                      for (ptrdiff_t ixsh = r.rows().begin(); ixsh < r.rows().end(); ++ixsh) {
                        const auto& xsh = fitshells[ixsh];
                        const auto& bxsh = bfitshells[ixsh];
                        const ptrdiff_t xoff = bxsh.offset();
                        const ptrdiff_t xsize = bxsh.size();

                        for (ptrdiff_t ish = r.cols().begin(); ish < r.cols().end(); ++ish) {
                          const auto& osh_i = orbshells[ish];
                          const auto& bsh_i = borbshells[ish];
                          const ptrdiff_t ioff = bsh_i.offset();
                          const ptrdiff_t isize = bsh_i.size();
                          for (ptrdiff_t jsh = 0; jsh <= ish; ++jsh) {
                            const auto& osh_j = orbshells[jsh];
                            const auto& bsh_j = borbshells[jsh];
                            const ptrdiff_t joff = bsh_j.offset();
                            const ptrdiff_t jsize = bsh_j.size();

                            engine.compute2<libint2::Operator::coulomb, libint2::BraKet::xs_xx, 0>(
                                xsh, libint2::Shell::unit(), osh_i, osh_j);

                            const double* vxij = buf[0];

                            if (vxij == nullptr) {
                              continue;
                            }

                            for (ptrdiff_t x = xoff, xij = 0; x < xoff + xsize; ++x) {
                              for (ptrdiff_t i = ioff; i < ioff + isize; ++i) {
                                for (ptrdiff_t j = joff; j < joff + jsize; ++j, ++xij) {
                                  (*Coulomb3C_)(j, i, x) = vxij[xij];
                                  if (ish != jsh) {
                                    (*Coulomb3C_)(i, j, x) = vxij[xij];
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    });
}

void yucca::DensityFitting::fill_2e2c()
{
  const ptrdiff_t nfit = fitting_basis_->nbasis();

  metric_ = make_unique<Tensor<2>>(nfit, nfit);

  const auto max_prim = fitting_basis_->max_nprim();
  const auto max_l = fitting_basis_->max_angular_momentum();

  // consider doing this in the wrapper, but it's just more convenient here for now
  libint2::Engine engine(libint2::Operator::coulomb, max_prim, max_l, 0);
  engine.set(libint2::BraKet::xs_xs);
  const auto& buf = engine.results();

  const ptrdiff_t nxsh = fitting_basis_->basisshells().size();

  const auto fitshells = libint::get_shells(*fitting_basis_);
  const auto& bfitshells = fitting_basis_->basisshells();

  for (ptrdiff_t ish = 0; ish < nxsh; ++ish) {
    const auto& ixshell = fitshells[ish];
    const auto& ibxsh = bfitshells[ish];
    const ptrdiff_t ioff = ibxsh.offset();
    const ptrdiff_t isize = ibxsh.size();

    for (ptrdiff_t jsh = 0; jsh <= ish; ++jsh) {
      const auto& jxshell = fitshells[jsh];
      const auto& jbxsh = bfitshells[jsh];
      const ptrdiff_t joff = jbxsh.offset();
      const ptrdiff_t jsize = jbxsh.size();

      engine.compute2<libint2::Operator::coulomb, libint2::BraKet::xs_xs, 0>(
          ixshell, libint2::Shell::unit(), jxshell, libint2::Shell::unit());

      const double* vxij = buf[0];

      if (vxij == nullptr) {
        continue;
      }

      for (ptrdiff_t i = ioff, xij = 0; i < ioff + isize; ++i) {
        for (ptrdiff_t j = joff; j < joff + jsize; ++j, ++xij) {
          (*metric_)(j, i) = vxij[xij];
          if (ish != jsh) {
            (*metric_)(i, j) = vxij[xij];
          }
        }
      }
    }
  }
}
