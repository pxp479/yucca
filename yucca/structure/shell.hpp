// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/shell.hpp
///
/// Shell data
#pragma once

#include <array>
#include <iomanip>
#include <string>
#include <vector>

namespace yucca
{

/// Stores position and identity of a single atom
class Atom {
 protected:
  std::array<double, 3> position_;
  double charge_;
  double mass_;
  std::string symbol_;

 public:
  Atom(const std::array<double, 3>& xyz, const double atomiccharge, const double atomicmass,
       const std::string& symbol);

  std::array<double, 3>& position() { return position_; }
  const std::array<double, 3>& position() const { return position_; }

  const double& charge() const { return charge_; }
  const double& mass() const { return mass_; }
  const std::string& symbol() const { return symbol_; }
};

/// Stores positions and identities of atoms in the molecule
class Molecule {
 protected:
  std::vector<Atom> atoms_;

 public:
  Molecule(const std::string& molfile);

  std::vector<yucca::Atom>& atoms() { return atoms_; }
  const std::vector<yucca::Atom>& atoms() const { return atoms_; }

  template <class Output>
  friend Output& operator<<(Output& o, const Molecule& m)
  {
    o << m.atoms().size() << std::endl << std::endl;
    for (auto& a : m.atoms()) {
      o << std::setw(3) << a.symbol() << std::setw(16) << std::setprecision(8) << a.position()[0]
        << std::setw(16) << std::setprecision(8) << a.position()[1] << std::setw(16)
        << std::setprecision(8) << a.position()[2] << std::endl;
    }
    return o;
  }

  double nuclear_repulsion() const;

 private:
  void read_xyz(const std::string& molfile);
};

} // namespace yucca
