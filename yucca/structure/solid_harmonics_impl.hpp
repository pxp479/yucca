// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file structure/solid_harmonics_impl.hpp
///
/// Build transformation matrices for solid harmonics

#pragma once

#include <vector>

namespace yucca::solid_harmonics
{

static constexpr int max_angular_momentum = 10;
static const std::vector<char> shell_labels = { 's','p','d','f','g','h','i','k','m','n','o','q','r','t' };

void fill_sph_to_cart_transform(const int l, double* data, const ptrdiff_t lda);
void fill_cart_to_sph_transform(const int l, double* data, const ptrdiff_t lda);
void fill_sph_to_cart_s_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_s_transform(double* a, const ptrdiff_t lda);
void fill_sph_to_cart_p_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_p_transform(double* a, const ptrdiff_t lda);
void fill_sph_to_cart_d_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_d_transform(double* a, const ptrdiff_t lda);
void fill_sph_to_cart_f_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_f_transform(double* a, const ptrdiff_t lda);
void fill_sph_to_cart_g_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_g_transform(double* a, const ptrdiff_t lda);
void fill_sph_to_cart_h_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_h_transform(double* a, const ptrdiff_t lda);
void fill_sph_to_cart_i_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_i_transform(double* a, const ptrdiff_t lda);
void fill_sph_to_cart_k_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_k_transform(double* a, const ptrdiff_t lda);
void fill_sph_to_cart_m_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_m_transform(double* a, const ptrdiff_t lda);
void fill_sph_to_cart_n_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_n_transform(double* a, const ptrdiff_t lda);
void fill_sph_to_cart_o_transform(double* a, const ptrdiff_t lda);
void fill_cart_to_sph_o_transform(double* a, const ptrdiff_t lda);

} // namespace yucca::solid_harmonics
