// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/evaluate_cao.cpp
///
/// compute densities on points classes

#include <yucca/structure/evaluate_cao.hpp>

using namespace yucca;

void yucca::evaluate_shell_at_points(const int max_order, const BasisShell& basisshell,
                                     const qleve::ConstTensorView<2>& xyz,
                                     qleve::TensorView<3> values)
{
  assert(max_order >= 0 && max_order <= 2);
  if (max_order == 0) {
    evaluate_shell_at_points<0>(basisshell, xyz, values);
  } else if (max_order == 1) {
    evaluate_shell_at_points<1>(basisshell, xyz, values);
  } else if (max_order == 2) {
    evaluate_shell_at_points<2>(basisshell, xyz, values);
  } else {
    assert(false);
  }
}
