// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/load_geometry.hpp
///
/// Read from input file to load molecular structure, basis sets, etc.
#pragma once

#include <memory>
#include <tuple>

#include <yucca/input/input_node.hpp>

namespace yucca
{
class Geometry; // #include <yucca/structure/geometry.hpp>
class Wfn;      // #include <yucca/wfn/wfn.hpp>
} // namespace yucca

namespace yucca
{

std::tuple<std::shared_ptr<Geometry>, std::shared_ptr<Wfn>> load_geometry(
    const InputNode& input, std::shared_ptr<Geometry> geometry = nullptr,
    std::shared_ptr<Wfn> wfn = nullptr);

} // namespace yucca
