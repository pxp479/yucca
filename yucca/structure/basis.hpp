// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/geometry.hpp
///
/// Geometry classes
#pragma once

#include <yucca/structure/basisset.hpp>
#include <yucca/structure/molecule.hpp>

namespace yucca
{

/// Single shell of basis functions
class BasisShell {
 protected:
  std::array<double, 3> position_;
  ptrdiff_t size_;       //< number of functions in shell
  ptrdiff_t offset_;     //< starting number of this shell in the full basis
  ptrdiff_t offset_cao_; //< starting number of this shell in the CAO basis
  ptrdiff_t atom_index_; //< atom number this shell sits on
  int angular_momentum_; //< angular momentum of the shell
  bool spherical_;       //< spherical or cartesian AOs

  std::vector<double> exponents_;
  std::vector<double> coeffs_;

 public:
  BasisShell(const std::array<double, 3>& xyz, const std::vector<double>& xp,
             const std::vector<double>& coef, const std::ptrdiff_t off, const ptrdiff_t cao_off,
             const std::ptrdiff_t atom, const int l, const bool sph) :
      coeffs_(coef),
      size_(sph ? 2 * l + 1 : (l + 1) * (l + 2) / 2),
      offset_(off),
      offset_cao_(cao_off),
      atom_index_(atom),
      angular_momentum_(l),
      spherical_(sph),
      position_(xyz),
      exponents_(xp)
  {
    normalize_coeffs();
  }

  const std::array<double, 3>& position() const { return position_; }
  const std::vector<double>& exponents() const { return exponents_; }
  const std::vector<double>& coeffs() const { return coeffs_; }
  ptrdiff_t offset() const { return offset_; }
  ptrdiff_t offset_cao() const { return offset_cao_; }
  ptrdiff_t atom_index() const { return atom_index_; }
  int angular_momentum() const { return angular_momentum_; }
  bool spherical() const { return spherical_; }

  const int size() const { return size_; }
  const int size_cao() const
  {
    const int l = angular_momentum_;
    return (l + 1) * (l + 2) / 2;
  }

  /// useful shortcut so looks can use structured bindings: auto [istart, iend, isize] =
  /// bsh.bounds();
  const std::array<ptrdiff_t, 3> bounds() const
  {
    return std::array<ptrdiff_t, 3>{offset(), offset() + size(), size()};
  }
  const std::array<ptrdiff_t, 3> bounds_cao() const
  {
    const ptrdiff_t ncart = size_cao();
    return std::array<ptrdiff_t, 3>{offset_cao_, offset_cao_ + ncart, ncart};
  }

 private:
  void normalize_coeffs();
};

/// Collection of shells sitting on a single center
class AtomShells {
 protected:
  std::array<double, 3> position_;
  std::vector<BasisShell> shells_;
  ptrdiff_t offset_;
  ptrdiff_t offset_cao_;
  ptrdiff_t size_;
  ptrdiff_t size_cao_;
  ptrdiff_t atom_index_;
  std::string symbol_;
  int charge_;
  double mass_;
  bool spherical_;

 public:
  AtomShells(const std::array<double, 3>& xyz, const std::vector<BasisShell>& shells,
             const ptrdiff_t offset, const ptrdiff_t offset_cao, const std::string& symbol,
             const int charge, const double mass) :
      position_(xyz),
      shells_(shells),
      offset_(offset),
      offset_cao_(offset_cao),
      size_(std::accumulate(shells.begin(), shells.end(), 0ull,
                            [](ptrdiff_t a, const BasisShell& sh) { return a + sh.size(); })),
      size_cao_(
          std::accumulate(shells.begin(), shells.end(), 0ull,
                          [](ptrdiff_t a, const BasisShell& sh) { return a + sh.size_cao(); })),
      atom_index_(shells.empty() ? 0 : shells.front().atom_index()),
      symbol_(symbol),
      charge_(charge),
      mass_(mass),
      spherical_(std::all_of(shells.begin(), shells.end(),
                             [](const BasisShell& sh) { return sh.spherical(); }))
  {}

  const std::array<double, 3>& position() const { return position_; }
  const std::vector<BasisShell>& shells() const { return shells_; }
  int charge() const { return charge_; }
  double mass() const { return mass_; }
  bool spherical() const { return spherical_; }

  const ptrdiff_t offset() const { return offset_; }
  const ptrdiff_t offset_cao() const { return offset_cao_; }
  const ptrdiff_t size() const { return size_; }
  const ptrdiff_t size_cao() const { return size_cao_; }
  const std::array<ptrdiff_t, 3> bounds() const
  {
    return std::array<ptrdiff_t, 3>{offset(), offset() + size(), size()};
  }
  const std::array<ptrdiff_t, 3> bounds_cao() const
  {
    return std::array<ptrdiff_t, 3>{offset_cao(), offset_cao() + size_cao(), size_cao()};
  }
  ptrdiff_t atom_index() const { return atom_index_; }

  const std::string& symbol() const { return symbol_; }
};

class Basis {
 protected:
  std::vector<BasisShell> basisshells_;
  std::vector<AtomShells> atomshells_;
  std::shared_ptr<BasisSet> basisset_;
  ptrdiff_t nbasis_;
  ptrdiff_t ncao_;
  bool spherical_;
  int max_angular_momentum_;
  int max_nprim_;

 public:
  Basis(const std::vector<Atom>& atoms, const std::shared_ptr<BasisSet>& basisset,
        const bool spherical = true);
  Basis(const std::vector<AtomShells>& atomshells_, const std::shared_ptr<BasisSet>& basisset);

  Basis update(const qleve::ConstTensorView<2> coords) const;

  const std::string& basisnick() const { return basisset_->nick(); }
  const std::vector<AtomShells>& atomshells() const { return atomshells_; }
  const std::vector<BasisShell>& basisshells() const { return basisshells_; }
  const std::shared_ptr<BasisSet>& basisset() const { return basisset_; }
  std::shared_ptr<BasisSet> basisset() { return basisset_; }

  const std::ptrdiff_t nbasis() const { return nbasis_; }
  const std::ptrdiff_t ncao() const { return ncao_; }
  int max_angular_momentum() const { return max_angular_momentum_; }
  int max_nprim() const { return max_nprim_; }
  ptrdiff_t max_shell_size() const
  {
    return std::max_element(
               basisshells_.begin(), basisshells_.end(),
               [](const BasisShell& a, const BasisShell& b) { return a.size() < b.size(); })
        ->size();
  }
  bool spherical() const { return spherical_; }

  void print(std::ostream& os = std::cout) const;
};

} // namespace yucca
