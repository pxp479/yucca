// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/basis.cpp
///
/// A brief description of the contents of this file
#include <iostream>
#include <set>

#include <yucca/physical/constants.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/structure/basisset.hpp>

using namespace std;
using namespace yucca;

void BasisShell::normalize_coeffs()
{
  const int ndepth = exponents_.size();
  const double pi = physical::pi<double>;
  vector<double> normed_coeff;
  const int l = angular_momentum();
  const double n = std::tgamma(2 * l + 1) / std::tgamma(l + 1) / pow(4, l) * pow(pi, 1.5);
  for (int i = 0; i < ndepth; ++i) {
    const double x = n / pow(exponents_[i] * 2.0, l + 1.5);
    normed_coeff.push_back(coeffs_[i] / sqrt(x));
  }

  double dexp = 0.0;
  for (int ixp = 0; ixp < ndepth; ++ixp) {
    for (int jxp = 0; jxp < ndepth; ++jxp) {
      dexp +=
          normed_coeff[ixp] * normed_coeff[jxp] / pow(exponents_[ixp] + exponents_[jxp], l + 1.5);
    }
  }

  const double scal = 1.0 / sqrt(dexp * n);
  for (int ixp = 0; ixp < ndepth; ++ixp) {
    coeffs_[ixp] *= scal;
  }
}

Basis::Basis(const std::vector<Atom>& atoms, const shared_ptr<BasisSet>& basisset,
             const bool spherical) :
    basisset_(basisset), spherical_(spherical)
{
  ptrdiff_t ioff = 0;
  ptrdiff_t cartoff = 0;
  ptrdiff_t iatom = 0;
  for (const auto& atom : atoms) {
    const string& symbol = atom.symbol();
    const auto& basisshell = basisset->atom_set(symbol);

    vector<BasisShell> atomshell;
    const ptrdiff_t atomoffset = ioff;
    const ptrdiff_t atomcartoff = cartoff;
    for (const auto& b : basisshell) {
      basisshells_.emplace_back(atom.position(), b.exponents(), b.coeffs(), ioff, cartoff, iatom,
                                b.angmom(), this->spherical_);
      atomshell.push_back(basisshells_.back());
      ioff += basisshells_.back().size();
      cartoff += basisshells_.back().size_cao();
    }
    atomshells_.emplace_back(atom.position(), atomshell, atomoffset, atomcartoff, atom.symbol(),
                             atom.charge(), atom.mass());
    ++iatom;
  }
  nbasis_ = ioff;
  ncao_ = cartoff;
  max_angular_momentum_ = max_element(basisshells_.begin(), basisshells_.end(),
                                      [](const BasisShell& a, const BasisShell& b) {
                                        return a.angular_momentum() < b.angular_momentum();
                                      })
                              ->angular_momentum();
  max_nprim_ = max_element(basisshells_.begin(), basisshells_.end(),
                           [](const BasisShell& a, const BasisShell& b) {
                             return a.coeffs().size() < b.coeffs().size();
                           })
                   ->coeffs()
                   .size();

  print();
}

yucca::Basis::Basis(const vector<AtomShells>& ashells, const shared_ptr<BasisSet>& basisset) :
    basisset_(basisset)
{
  ptrdiff_t ioff = 0;
  ptrdiff_t cartoff = 0;
  ptrdiff_t iatom = 0;
  for (const auto& ash : ashells) {
    const ptrdiff_t atomoff = ioff;
    const ptrdiff_t atomcartoff = cartoff;

    vector<BasisShell> bsh;
    for (const auto& b : ash.shells()) {
      bsh.emplace_back(b.position(), b.exponents(), b.coeffs(), ioff, cartoff, iatom,
                       b.angular_momentum(), b.spherical());
      ioff += b.size();
      cartoff += b.size_cao();
    }
    atomshells_.emplace_back(ash.position(), bsh, atomoff, atomcartoff, ash.symbol(), ash.charge(),
                             ash.mass());
    basisshells_.insert(basisshells_.end(), bsh.begin(), bsh.end());

    iatom += 1;
  }
  nbasis_ = ioff;
  ncao_ = cartoff;

  max_angular_momentum_ = max_element(basisshells_.begin(), basisshells_.end(),
                                      [](const BasisShell& a, const BasisShell& b) {
                                        return a.angular_momentum() < b.angular_momentum();
                                      })
                              ->angular_momentum();
  max_nprim_ = max_element(basisshells_.begin(), basisshells_.end(),
                           [](const BasisShell& a, const BasisShell& b) {
                             return a.coeffs().size() < b.coeffs().size();
                           })
                   ->coeffs()
                   .size();
  spherical_ = all_of(basisshells_.begin(), basisshells_.end(),
                      [](const BasisShell& sh) { return sh.spherical(); });

  if (bool any_spherical = any_of(basisshells_.begin(), basisshells_.end(),
                                  [](const BasisShell& sh) { return sh.spherical(); });
      any_spherical != spherical_) {
    throw runtime_error("Basis cannot mix spherical and cartesian basis functions");
  }

  print();
}

Basis Basis::update(const qleve::ConstTensorView<2> coords) const
{
  const ptrdiff_t natoms = atomshells_.size();
  assert(3 == coords.extent(0) && natoms == coords.extent(1));

  vector<AtomShells> atomshells;
  atomshells.reserve(natoms);
  for (ptrdiff_t iatom = 0; iatom < natoms; ++iatom) {
    array<double, 3> xyz;
    xyz[0] = coords(0, iatom);
    xyz[1] = coords(1, iatom);
    xyz[2] = coords(2, iatom);

    const AtomShells& ash = atomshells_[iatom];
    vector<BasisShell> bsh;
    for (auto& b : ash.shells()) {
      bsh.emplace_back(xyz, b.exponents(), b.coeffs(), b.offset(), b.offset_cao(), b.atom_index(),
                       b.angular_momentum(), b.spherical());
    }
    atomshells.emplace_back(xyz, bsh, ash.offset(), ash.offset_cao(), ash.symbol(), ash.charge(),
                            ash.mass());
  }

  return Basis(atomshells, basisset_);
}

void yucca::Basis::print(std::ostream& os) const
{
  if (!basisset_) {
    return;
  }
  // get collection of unique atoms
  set<string> atoms;
  for (const auto& ash : atomshells_) {
    atoms.insert(ash.symbol());
  }
  // convert to vector
  vector<string> atomvec(atoms.begin(), atoms.end());
  basisset_->print(os, atomvec);
}
