// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file structure/solid_harmonics.hpp
///
/// Build transformation matrices for solid harmonics
#pragma once

#include <memory>
#include <vector>

#include <qleve/tensor.hpp>

#include <yucca/structure/solid_harmonics_impl.hpp>

namespace yucca
{

class SolidHarmonics {
 protected:
  std::unique_ptr<qleve::Tensor<1>> c2s_storage_;
  std::unique_ptr<qleve::Tensor<1>> s2c_storage_;

  std::vector<std::shared_ptr<qleve::ConstTensorView<2>>> c2s_transforms_;
  std::vector<std::shared_ptr<qleve::ConstTensorView<2>>> s2c_transforms_;

 public:
  SolidHarmonics();

  qleve::ConstTensorView<2>& cartesian_to_spherical(const int l) { return *c2s_transforms_.at(l); }
  qleve::ConstTensorView<2>& spherical_to_cartesian(const int l) { return *s2c_transforms_.at(l); }

  qleve::Tensor<2> cartesian_to_spherical(const std::vector<int>& ll);
  qleve::Tensor<2> spherical_to_cartesian(const std::vector<int>& ll);
};

class Basis;

namespace solid_harmonics
{

std::vector<std::array<int, 3>> cca_cartesian_ordering(const int l);

qleve::Tensor<2> transform_to_cao(const Basis& basis, const qleve::ConstTensorView<2>& A);
qleve::Tensor<2> transform_to_sao(const Basis& basis, const qleve::ConstTensorView<2>& A);

} // namespace solid_harmonics

} // namespace yucca
