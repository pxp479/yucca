#!/usr/bin/env python3

from sympy import *

import sys

import argparse

ANG_LABELS = "spdfghikmnoqrt"
MAX_L = 10+1

"""
Notes: The IJQC described things a little goofy. Their expressions
are more accurately spherical to cartesian transformations because
it is writing (real) spherical harmonics in terms of cartesian
gaussians.
"""

def sph_to_cart(l, m, x, y, z):
    j = (x + y - abs(m))
    if j % 2 != 0:
        return 0
    j = j/S(2)

    if x + y + z != l:
        return 0

    NN = factorial(2*x)*factorial(2*y)*factorial(2*z)*factorial(l)*factorial(l-abs(m)) / (
            factorial(2*l)*factorial(x)*factorial(y)*factorial(z)*factorial(l+abs(m)) )
    N = sqrt(NN) / ( S(2)**l * factorial(l) )

    A = S(0)
    for i in range((l-abs(m))//2 + 1):
        a = binomial(l,i) * binomial(i,j) * (-1)**i * factorial(2*l - 2*i) / (
                factorial(l - abs(m) - 2 * i) )
        B = S(0)
        for k in range(j + 1):
            B += binomial(j, k) * binomial(abs(m), x - 2*k) * S(-1)**(sign(m)*(abs(m)-x+2*S(k))/S(2))
        A += a * B

    return N * A

def real_sph_to_cart(l, m, x, y, z):
    mm = abs(m)
    if m > 0:
        cc = (sph_to_cart(l,mm,x,y,z) + sph_to_cart(l,-mm,x,y,z))/sqrt(S(2))
    elif m < 0:
        cc = (sph_to_cart(l,mm,x,y,z) - sph_to_cart(l,-mm,x,y,z))/sqrt(S(-2))
    else:
        cc = sph_to_cart(l,mm,x,y,z)
    return simplify(cc)

def cart_overlap(x1,y1,z1,x2,y2,z2):
    xx = x1+x2
    yy = y1+y2
    zz = z1+z2
    if (xx % 2) != 0 or (yy % 2) != 0 or (zz % 2) != 0:
        return S(0)

    ax = factorial(xx) / factorial(xx//2)
    ay = factorial(yy) / factorial(yy//2)
    az = factorial(zz) / factorial(zz//2)
    A = ax * ay * az

    bx = factorial(2*x1) * factorial(2*x2) / (factorial(x1) * factorial(x2))
    by = factorial(2*y1) * factorial(2*y2) / (factorial(y1) * factorial(y2))
    bz = factorial(2*z1) * factorial(2*z2) / (factorial(z1) * factorial(z2))
    B = sqrt(bx * by * bz)

    return A/B

def quick_overlap(xyz1, xyz2):
    x1 = xyz1.count("x")
    y1 = xyz1.count("y")
    z1 = xyz1.count("z")
    x2 = xyz2.count("x")
    y2 = xyz2.count("y")
    z2 = xyz2.count("z")
    return cart_overlap(x1,y1,z1,x2,y2,z2)

def cart_overlap_matrix(cartlist):
    out = []
    for i in cartlist:
        orow = []
        for j in cartlist:
            orow.append(quick_overlap(i,j))
        out.append(orow)
    return Matrix(out)

def cart_to_sph(l,m,x1,y1,z1):
    out = S(0)
    for x2 in range(l+1):
        for y2 in range(l+1):
            for z2 in range(l+1):
                if x2+y2+z2 == l:
                    out += cart_overlap(x1,y1,z1,x2,y2,z2) * conjugate(
                            sph_to_cart(l,m,x2,y2,z2))
    return out

def cart_to_real_sph(l,m,x1,y1,z1):
    out = S(0)
    for x2 in range(l+1):
        for y2 in range(l+1):
            for z2 in range(l+1):
                if x2+y2+z2 == l:
                    out += cart_overlap(x1,y1,z1,x2,y2,z2) * conjugate(
                            real_sph_to_cart(l,m,x2,y2,z2))
    return out

def gen_cart_to_real_sph_transform(l, sphorder, cartorder):
    """Outputs the matrix transform from cartesian AOs to real spherical harmonics"""
    xyzs = [ (xyz.count("x"), xyz.count("y"), xyz.count("z")) for xyz in cartorder ]

    transform = []
    for m in sphorder:
        transform.append([cart_to_real_sph(l, m, x, y, z) for x, y, z in xyzs])

    return transform

def gen_real_sph_to_cart_transform(l, sphorder, cartorder):
    """Outputs the matrix transform from real spherical harmonics to cartesian AOs"""
    transform = []
    for i, xyz in enumerate(cartorder):
        x = xyz.count("x")
        y = xyz.count("y")
        z = xyz.count("z")

        transform.append( [ real_sph_to_cart(l, m, x, y, z) for m in sphorder ] )

    return transform

molden_cart_s_order = [ "" ]
molden_cart_p_order = "x y z".split()
molden_cart_d_order = "xx yy zz xy xz yz".split()
molden_cart_f_order = "xxx yyy zzz xyy xxy xxz xzz yzz yyz xyz".split()
molden_cart_g_order = "xxxx yyyy zzzz xxxy xxxz yyyx yyyz zzzx zzzy xxyy xxzz yyzz xxyz yyxz zzxy".split()

molden_cart_order = [ molden_cart_s_order, molden_cart_p_order,
        molden_cart_d_order, molden_cart_f_order, molden_cart_g_order ]

def gen_molden_sph_order(l):
    out = [0]
    for m in range(1,l+1):
        out.append(m)
        out.append(-m)
    return out

molden_sph_order = [ gen_molden_sph_order(l) for l in range(12) ]

def gen_cca_cart_order(l):
    out = []
    for lx in range(l, -1, -1):
        for ly in range(l-lx, -1, -1):
            lz = l - lx - ly
            out.append("x"*lx + "y"*ly + "z"*lz)
    return out

cca_cart_order = [ gen_cca_cart_order(l) for l in range(12) ]

def gen_cca_sph_order(l):
    out = []
    for m in range(-l,l+1):
        out.append(m)
    return out

cca_sph_order = [ gen_cca_sph_order(l) for l in range(12) ]

def yucca_dispatch(direction, fhpp=sys.stdout, fcpp=sys.stdout):
    print("void fill_{:s}_transform(const int l, double* data, const ptrdiff_t lda);".format(direction), file=fhpp)
    print("void yucca::solid_harmonics::fill_{:s}_transform(const int l, double* data, const ptrdiff_t lda)".format(direction), file=fcpp)
    print("{", file=fcpp)
    print("  if (l == 0) {", file=fcpp)
    print("    fill_{:s}_s_transform(data, lda);".format(direction), file=fcpp)
    for l in range(1, MAX_L):
        print("  }} else if (l == {:d}) {{".format(l), file=fcpp)
        print("    fill_{:s}_{:s}_transform(data, lda);".format(direction, ANG_LABELS[l]), file=fcpp)
    print("  } else {", file=fcpp)
    print("    assert(false);", file=fcpp)
    print("  }", file=fcpp)
    print("}", file=fcpp)
    print(file=fcpp)

def yucca_function(l, transform, direction, fhpp=sys.stdout, fcpp=sys.stdout):
    lsymb = ANG_LABELS[l]
    nfrom = len(transform[0])
    nto = len(transform)
    # gen header first. header has namespace declared already
    print("void fill_{:s}_{:s}_transform(double* a, const ptrdiff_t lda);".format(direction, lsymb), file=fhpp)

    print("void yucca::solid_harmonics::fill_{:s}_{:s}_transform(double* a, const ptrdiff_t lda)".format(direction, lsymb), file=fcpp)
    print("{", file=fcpp)
    print("  assert(lda >= {:d});".format(nto), file=fcpp)
    print(file=fcpp)

    for ifrom in range(nfrom):
        for ito in range(nto):
            x = transform[ito][ifrom]
            if x != 0.0:
                print("  a[{:d} + lda*{:d}] = {:s};".format(ito, ifrom, cxxcode(x)), file=fcpp)

    print("}", file=fcpp)
    print(file=fcpp)

def main():
    parser = argparse.ArgumentParser("sph_to_cart",
            description="Computes transformation coefficients between normalized " +
            "real spherical AOs and normalized cartesian AOs (CAOs). Formulas for the " +
            "coefficients are taken from Schlegel, Frisch IJQC 1995 v. 54 p. 83. " +
            "Calculations are done with sympy so the results are exact expressions, " +
            "i.e., have no precision loss"
            )

    parser.add_argument("--ordering", choices=["molden", "cca"], default="cca", help="ordering scheme for transform matrices")
    parser.add_argument("--no-yucca", dest="yucca", action="store_false", help="print as yucca function")
    parser.add_argument("--latex", action="store_true", help="print pretty latex transforms")

    args = parser.parse_args()

    sph_order = { "molden" : molden_sph_order, "cca" : cca_sph_order }[args.ordering]
    cart_order = { "molden" : molden_cart_order, "cca" : cca_cart_order }[args.ordering]

    if args.latex:
        print("Real Spherical to Cartesian")
        g = Function("v")
        for l in range(MAX_L):
            for m in range(-l,l+1):
                term = None
                for xyz in cart_order[l]:
                    x = xyz.count("x")
                    y = xyz.count("y")
                    z = xyz.count("z")
                    cc = real_sph_to_cart(l, m, x, y, z)

                    if cc != 0:
                        if term is None:
                            term = cc * g(S(x), S(y), S(z))
                        else:
                            term += cc * g(S(x), S(y), S(z))
                print("v({0},{1}) = {2}".format(l, m, latex(term)))
            print()

        print("Cartesian to Real Spherical")
        for l in range(MAX_L):
            for cart in cart_order[l]:
                # looking for coefficients for v(x,y,z)
                x = cart.count("x")
                y = cart.count("y")
                z = cart.count("z")

                term = None
                for m in range(-l,l+1):
                    cc = cart_to_real_sph(l,m,x,y,z)

                    if cc != 0:
                        if term is None:
                            term = cc * g(S(l), S(m))
                        else:
                            term += cc * g(S(l), S(m))
                print("v({0},{1},{2}) = {3}".format(x, y, z, latex(term)))
            print()

    if True: # debugging
        l = 2
        olap = cart_overlap_matrix(cart_order[l])
        c2s = Matrix(gen_cart_to_real_sph_transform(l, sph_order[l], cart_order[l]))
        s2c = Matrix(gen_real_sph_to_cart_transform(l, sph_order[l], cart_order[l]))
        print("Spherical order: ", sph_order[l])
        print("Cartesian order: ", cart_order[l])
        print("s2c2s:")
        pprint(c2s * s2c)
        print("c2s2c")
        pprint(s2c * c2s)

    if args.yucca:
        with open("solid_harmonics_impl.hpp", "w") as fhpp, open("solid_harmonics_impl.cpp", "w") as fcpp:
            for f in [ fhpp, fcpp]:
                print('// This Source Code Form is subject to the terms of the Mozilla Public', file=f)
                print('// License, v. 2.0. If a copy of the MPL was not distributed with this', file=f)
                print('// file, You can obtain one at http://mozilla.org/MPL/2.0/.', file=f)
                print('// -------------------------------------------------------------------', file=f)
            print(r'/// \file structure/solid_harmonics_impl.hpp', file=fhpp)
            print(r'/// \file structure/solid_harmonics_impl.cpp', file=fcpp)
            for f in [ fhpp, fcpp]:
                print(r'///', file=f)
                print(r'/// Build transformation matrices for solid harmonics', file=f)
                print(file=f)

            print(r'#pragma once', file=fhpp)
            print(file=fhpp)
            print(r'#include <vector>', file=fhpp)
            print(file=fhpp)
            print(r'namespace yucca::solid_harmonics', file=fhpp)
            print(r'{', file=fhpp)
            print(file=fhpp)
            print('static constexpr int max_angular_momentum = {:d};'.format(MAX_L-1), file=fhpp)
            print('static const std::vector<char> shell_labels = {{ {:s} }};'.format(
                ",".join(["'{:s}'".format(x) for x in ANG_LABELS])), file=fhpp)
            print(file=fhpp)

            print(r'#include <cassert>', file=fcpp)
            print(r'#include <cmath>', file=fcpp)
            print(r'#include <cstddef>', file=fcpp)
            print(r'#include <yucca/structure/solid_harmonics_impl.hpp>', file=fcpp)
            print(file=fcpp)

            yucca_dispatch("sph_to_cart", fhpp, fcpp)
            yucca_dispatch("cart_to_sph", fhpp, fcpp)

            for l in range(MAX_L):
                print("generating l={:d} transforms".format(l))
                s2c = gen_real_sph_to_cart_transform(l, sph_order[l], cart_order[l])
                yucca_function(l, s2c, "sph_to_cart", fhpp, fcpp)
                c2s = gen_cart_to_real_sph_transform(l, sph_order[l], cart_order[l])
                yucca_function(l, c2s, "cart_to_sph", fhpp, fcpp)

            print(file=fhpp)
            print(r'} // namespace yucca::solid_harmonics', file=fhpp)

if __name__ == "__main__":
    main()
