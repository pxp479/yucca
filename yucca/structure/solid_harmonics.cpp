// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file structure/solid_harmonics.hpp
///
/// Build transformation matrices for solid harmonics

#include <memory>

#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor.hpp>

#include <yucca/structure/basis.hpp>
#include <yucca/structure/solid_harmonics.hpp>
#include <yucca/structure/solid_harmonics_impl.hpp>

using namespace qleve;
using namespace std;
using namespace yucca;

yucca::SolidHarmonics::SolidHarmonics()
{
  const int max_l = solid_harmonics::max_angular_momentum;
  ptrdiff_t nmap = 0;
  for (int l = 0; l <= max_l; ++l) {
    nmap += (l + 1) * (l + 2) / 2 * (2 * l + 1);
  }

  c2s_storage_ = make_unique<Tensor<1>>(nmap);
  s2c_storage_ = make_unique<Tensor<1>>(nmap);

  ptrdiff_t offset = 0;
  for (int l = 0; l <= max_l; ++l) {
    const ptrdiff_t ncart = (l + 1) * (l + 2) / 2;
    const ptrdiff_t nsph = 2 * l + 1;

    double* c2s = c2s_storage_->data() + offset;
    double* s2c = s2c_storage_->data() + offset;

    solid_harmonics::fill_cart_to_sph_transform(l, c2s, nsph);
    solid_harmonics::fill_sph_to_cart_transform(l, s2c, ncart);

    c2s_transforms_.emplace_back(make_shared<ConstTensorView<2>>(c2s, nsph, ncart));
    s2c_transforms_.emplace_back(make_shared<ConstTensorView<2>>(s2c, ncart, nsph));

    offset += ncart * nsph;
  }

  assert(offset == nmap);
}

Tensor<2> SolidHarmonics::spherical_to_cartesian(const vector<int>& ll)
{
  const ptrdiff_t totalcart = accumulate(ll.begin(), ll.end(), 0ll, [](const auto t, const auto l) {
    return t + (l + 1) * (l + 2) / 2;
  });
  const ptrdiff_t totalsph = accumulate(ll.begin(), ll.end(), 0ll,
                                        [](const auto t, const int l) { return t + (2 * l + 1); });

  Tensor<2> out(totalcart, totalsph);
  ptrdiff_t cartoff = 0;
  ptrdiff_t sphoff = 0;
  for (int l : ll) {
    const ptrdiff_t cartsize = (l + 1) * (l + 2) / 2;
    const ptrdiff_t sphsize = (2 * l + 1);
    out.subtensor({cartoff, sphoff}, {cartoff + cartsize, sphoff + sphsize}) =
        this->spherical_to_cartesian(l);
    cartoff += cartsize;
    sphoff += sphsize;
  }
  return out;
}

Tensor<2> SolidHarmonics::cartesian_to_spherical(const vector<int>& ll)
{
  const ptrdiff_t totalcart = accumulate(ll.begin(), ll.end(), 0ll, [](const auto t, const auto l) {
    return t + (l + 1) * (l + 2) / 2;
  });
  const ptrdiff_t totalsph = accumulate(ll.begin(), ll.end(), 0ll,
                                        [](const auto t, const int l) { return t + (2 * l + 1); });

  Tensor<2> out(totalsph, totalcart);
  ptrdiff_t cartoff = 0;
  ptrdiff_t sphoff = 0;
  for (int l : ll) {
    const ptrdiff_t cartsize = (l + 1) * (l + 2) / 2;
    const ptrdiff_t sphsize = (2 * l + 1);
    out.subtensor({sphoff, cartoff}, {sphoff + sphsize, cartoff + cartsize}) =
        this->cartesian_to_spherical(l);
    cartoff += cartsize;
    sphoff += sphsize;
  }
  return out;
}

vector<array<int, 3>> solid_harmonics::cca_cartesian_ordering(const int angular_momentum)
{
  const int nl = (angular_momentum + 1) * (angular_momentum + 2) / 2;
  vector<array<int, 3>> out(nl);

  for (int i = angular_momentum, ijk = 0; i >= 0; i--) {
    for (int j = angular_momentum - i; j >= 0; j--, ++ijk) {
      const int k = angular_momentum - i - j;
      out[ijk] = {i, j, k};
    }
  }
  return out;
}

// this is a pretty wasteful implementation for now, but the key is that it can be improved
Tensor<2> yucca::solid_harmonics::transform_to_cao(const Basis& basis, const ConstTensorView<2>& A)
{
  // transform to CAO basis
  vector<int> angshells(basis.basisshells().size());
  transform(basis.basisshells().begin(), basis.basisshells().end(), angshells.begin(),
            [](const BasisShell& bsh) { return bsh.angular_momentum(); });

  SolidHarmonics sh;
  auto s2c = sh.spherical_to_cartesian(angshells);
  auto crho = gemm("n", "n", 1.0, s2c, A);
  auto rho_cao = gemm("n", "t", 1.0, crho, s2c);
  return rho_cao;
}

// this is a pretty wasteful implementation for now, but the key is that it can be improved
Tensor<2> yucca::solid_harmonics::transform_to_sao(const Basis& basis, const ConstTensorView<2>& A)
{
  // transform to SAO basis
  vector<int> angshells(basis.basisshells().size());
  transform(basis.basisshells().begin(), basis.basisshells().end(), angshells.begin(),
            [](const BasisShell& bsh) { return bsh.angular_momentum(); });

  SolidHarmonics sh;
  auto c2s = sh.spherical_to_cartesian(angshells);
  auto crho = gemm("t", "n", 1.0, c2s, A);
  auto rho_sao = gemm("n", "n", 1.0, crho, c2s);
  return rho_sao;
}
