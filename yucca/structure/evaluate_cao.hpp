// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/evaluate_cao.hpp
///
/// compute densities on points classes
#pragma once

#include <qleve/tensor.hpp>

#include <yucca/physical/constants.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/structure/solid_harmonics.hpp>
#include <yucca/util/factorial.hpp>

namespace yucca
{

void evaluate_shell_at_points(const int max_order, const BasisShell& basisshell,
                              const qleve::ConstTensorView<2>& xyz, qleve::TensorView<3> values);

template <int max_order>
void evaluate_shell_at_points(const BasisShell& basisshell, const qleve::ConstTensorView<2>& xyz,
                              qleve::TensorView<3> values)
{
  static_assert(max_order >= 0, "max_order must be non-negative");
  const double thresh = 1e-8;
  // declare some useful stack memory
  std::array<double, solid_harmonics::max_angular_momentum + 1> Lnorm;
  std::array<double, solid_harmonics::max_angular_momentum + 1 + max_order> powxx;
  std::array<double, solid_harmonics::max_angular_momentum + 1 + max_order> powyy;
  std::array<double, solid_harmonics::max_angular_momentum + 1 + max_order> powzz;
  std::array<double, 16> expnorms;

  // values(l, p, x)
  const auto [X, Y, Z] = basisshell.position();
  const std::vector<double>& expon = basisshell.exponents();
  const int nexp = expon.size();
  const std::vector<double>& coef = basisshell.coeffs();
  const int l = basisshell.angular_momentum();
  const int maxl = l + 1;
  const int maxpow = l + 1 + max_order;
  const int ncart = basisshell.size_cao();
  const ptrdiff_t np = xyz.extent(1);

  assert(values.extent(0) == np && values.extent(1) == ((max_order + 1) * (max_order + 1))
         && values.extent(2) == ncart);

  // std::vector<double> expnorms(expon); // part of normalization constant coming from exponent
  for (int ii = 0; ii < nexp; ++ii) {
    const double alpha = expon[ii];
    const double x = pow(physical::pi<double>, 1.5) / pow(2.0 * alpha, l + 1.5);
    expnorms[ii] = coef[ii] / sqrt(x);
  }

  for (int il = 0; il < maxl; ++il) {
    const double lfac = angular_norm(il);
    Lnorm[il] = 1.0 / std::sqrt(lfac);
  }

  const double mn_exp = expon.back();
  const double logt = -std::log(thresh);
  const double maxr = logt / mn_exp;

  const auto ijks = solid_harmonics::cca_cartesian_ordering(l);

  for (ptrdiff_t ip = 0; ip < np; ++ip) {
    const double xx = xyz(0, ip) - X;
    const double yy = xyz(1, ip) - Y;
    const double zz = xyz(2, ip) - Z;

    const double rr = xx * xx + yy * yy + zz * zz;
    if (rr > maxr) {
      continue;
    }
    double rad = 0.0;
    double drad = 0.0;
    for (int iexp = 0; iexp < nexp; ++iexp) {
      const double exprr = expnorms[iexp] * std::exp(-expon[iexp] * rr);
      rad += exprr;
      if constexpr (max_order >= 1) {
        drad += 2.0 * expon[iexp] * exprr;
      }
    }

    for (int il = 0; il < maxpow; ++il) {
      powxx[il] = std::pow(xx, il);
      powyy[il] = std::pow(yy, il);
      powzz[il] = std::pow(zz, il);
    }

    for (int icart = 0; icart < ncart; ++icart) {
      const auto [i, j, k] = ijks[icart];
      const double angnorm = Lnorm[i] * Lnorm[j] * Lnorm[k];
      const double gxyz = angnorm * powxx[i] * powyy[j] * powzz[k] * rad;
      values(ip, 0, icart) = gxyz;

      if constexpr (max_order >= 1) {
        // d/dx
        const double Lyz = angnorm * powyy[j] * powzz[k];
        const double dgdx = (i > 0 ? i * powxx[i - 1] * rad : 0) - powxx[i + 1] * drad;
        values(ip, 1, icart) = dgdx * Lyz;

        // d/dy
        const double Lxz = angnorm * powxx[i] * powzz[k];
        const double dgdy = (j > 0 ? j * powyy[j - 1] * rad : 0) - powyy[j + 1] * drad;
        values(ip, 2, icart) = dgdy * Lxz;

        // d/dy
        const double Lxy = angnorm * powxx[i] * powyy[j];
        const double dgdz = (k > 0 ? k * powzz[k - 1] * rad : 0) - powzz[k + 1] * drad;
        values(ip, 3, icart) = dgdz * Lxy;
      }
    }
  }
}

} // namespace yucca
