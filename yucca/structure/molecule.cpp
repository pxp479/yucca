// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/molecule.cpp
///
/// A brief description of the contents of this file
#include <algorithm>
#include <cctype>
#include <cmath>
#include <fstream>
#include <iostream>
#include <numeric>
#include <sstream>

#include <fmt/core.h>
#include <fmt/ostream.h>

#include <yucca/input/input_node.hpp>
#include <yucca/physical/constants.hpp>
#include <yucca/structure/molecule.hpp>
#include <yucca/util/periodictable.hpp>

using namespace std;
using namespace yucca;

Atom::Atom(const array<double, 3>& xyz, const double atomiccharge, const double atomicmass,
           const string& symbol) :
    position_(xyz), charge_(atomiccharge), mass_(atomicmass), symbol_(symbol)
{}

Molecule::Molecule(const InputNode& input)
{
  const string& molfile = input.get<string>("molecule");
  filename_ = molfile;
  read_molecule(molfile);

  if (auto ext = input.get_node_if("external field"); ext) {
    efield_ = ext->as_vector<double>();
    if (efield_.size() != 3) {
      throw std::runtime_error("Expecting electric with 3 components");
    }
    fmt::print("Applying external electric field: ({:12.6f}, {:12.6f}, {:12.6f})\n", efield_.at(0),
               efield_.at(1), efield_.at(2));
  }

  this->print_xyz();

  fmt::print("Nuclear repulsion energy: {:22.12f}\n", this->nuclear_repulsion());
}

Molecule::Molecule(const string& molfile) : filename_(molfile)
{
  read_molecule(molfile);

  this->print_xyz();

  fmt::print("Nuclear repulsion energy: {:22.12f}\n", this->nuclear_repulsion());
}

void Molecule::read_molecule(const string& molfile)
{
  fmt::print("Reading molecule from: {}\n", molfile);

  // check extension
  if (auto idx = molfile.rfind('.'); idx != string::npos) {
    if (string extension = molfile.substr(idx + 1); extension == string("xyz")) {
      read_xyz(molfile);
    } else if (extension == string("molden")) {
      throw runtime_error("Molden import not implemented");
    } else {
      fmt::print("Unrecognized extension on molecule file. Assuming .xyz\n");
      read_xyz(molfile);
    }
  } else {
    fmt::print("Found no extension on molecule file. Assuming .xyz\n");
    read_xyz(molfile);
  }
}

void Molecule::read_xyz(const string& molfile)
{
  PeriodicTable pt;
  string line;

  ifstream input(molfile);
  if (!input.good())
    throw std::runtime_error("File not found");

  getline(input, line);

  // first line should have natoms
  const int natoms = stoi(line);

  // second line is a title (ignored)
  getline(input, line);

  // rest of the lines contain atomic symbols and positions
  for (int iatom = 0; iatom < natoms; ++iatom) {
    getline(input, line);
    stringstream ss(line);
    string symbol;
    double x, y, z;
    ss >> symbol >> x >> y >> z;

    // xyz is in Angstrom, so need to convert to bohr
    x *= physical::ang2bohr;
    y *= physical::ang2bohr;
    z *= physical::ang2bohr;

    // retrieve element
    transform(symbol.begin(), symbol.end(), symbol.begin(), ::tolower);
    const Element& element = pt.element(symbol);

    atoms_.emplace_back(array<double, 3>{x, y, z}, element.charge, element.meanmass,
                        element.symbol);
  }
}

void Molecule::print_xyz() const { this->print_xyz(std::cout); }

qleve::Tensor<2> Molecule::get_coord_tensor() const
{
  qleve::Tensor<2> out(3, natoms());

  for (ptrdiff_t iatom = 0; iatom < natoms(); ++iatom) {
    out(0, iatom) = atoms_[iatom].position()[0];
    out(1, iatom) = atoms_[iatom].position()[1];
    out(2, iatom) = atoms_[iatom].position()[2];
  }

  return out;
}

double Molecule::nuclear_repulsion() const
{
  double out = 0.0;

  for (auto ia = atoms_.begin(); ia != atoms_.end(); ++ia) {
    for (auto ja = atoms_.begin(); ja != ia; ++ja) {
      const double dx = ia->position()[0] - ja->position()[0];
      const double dy = ia->position()[1] - ja->position()[1];
      const double dz = ia->position()[2] - ja->position()[2];
      const double r = sqrt(dx * dx + dy * dy + dz * dz);
      out += ia->charge() * ja->charge() / r;
    }
  }

  return out;
}

size_t Molecule::total_nuclear_charge() const
{
  return accumulate(atoms().begin(), atoms().end(), 0ull,
                    [](const size_t q, const yucca::Atom& a) { return q + a.charge(); });
}

double Molecule::total_mass() const
{
  return accumulate(atoms().begin(), atoms().end(), 0ull,
                    [](const double m, const yucca::Atom& a) { return m + a.mass(); });
}

qleve::Tensor<1> Molecule::center_of_mass() const
{
  const double mass = total_mass();
  qleve::Tensor<1> out(3);
  for (const auto& at : atoms_) {
    const double weight = at.mass() / mass;
    out(0) += at.position()[0] * weight;
    out(1) += at.position()[1] * weight;
    out(2) += at.position()[2] * weight;
  }
  return out;
}

qleve::Tensor<1> Molecule::nuclear_multipole(const int order, std::array<double, 3> origin) const
{
  assert(order > 0 && order <= 2);
  int nops;
  if (order == 1) {
    nops = 4;
  } else if (order == 2) {
    nops = 10;
  } else if (order == 3) {
    nops = 20;
  }

  qleve::Tensor<1> out(nops);

  // dipole moments
  const auto [x0, y0, z0] = origin;
  for (const auto& at : atoms_) {
    const double q = at.charge();
    const auto [x, y, z] = at.position();
    out(1) += (x - x0) * q;
    out(2) += (y - y0) * q;
    out(3) += (z - z0) * q;
  }

  if (order < 2) {
    return out;
  }

  // quadrupole moments
  for (const auto& atA : atoms_) {
    const double qA = atA.charge();
    const auto [xA, yA, zA] = atA.position();
    for (const auto& atB : atoms_) {
      const double qB = atB.charge();
      const auto [xB, yB, zB] = atB.position();

      out(4) += (xA - x0) * (xB - x0) * qA;
      out(5) += (xA - x0) * (yB - y0) * qA;
      out(6) += (xA - x0) * (zB - z0) * qA;
      out(7) += (yA - y0) * (yB - y0) * qA;
      out(8) += (yA - y0) * (zB - z0) * qA;
      out(9) += (zA - z0) * (zB - z0) * qA;
    }
  }

  if (order < 3) {
    return out;
  }

  // hexapole moments would go here

  return out;
}
