// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/molecule.hpp
///
/// Molecule classes
#pragma once

#include <array>
#include <iomanip>
#include <string>
#include <vector>

#include <fmt/core.h>

#include <qleve/tensor.hpp>

namespace yucca
{

// fwd declare
class InputNode; // #include <yucca/input/input_node.hpp>

/// Stores position and identity of a single atom
class Atom {
 protected:
  std::array<double, 3> position_;
  double charge_;
  double mass_;
  std::string symbol_;

 public:
  Atom(const std::array<double, 3>& xyz, const double atomiccharge, const double atomicmass,
       const std::string& symbol);

  std::array<double, 3>& position() { return position_; }
  const std::array<double, 3>& position() const { return position_; }

  const double& charge() const { return charge_; }
  const double& mass() const { return mass_; }
  const std::string& symbol() const { return symbol_; }

  int row() const
  {
    const int nproton = static_cast<int>(charge() + 0.5);
    const int row = 1 + (nproton > 2) + (nproton > 10) + (nproton > 18) + (nproton > 36)
                    + (nproton > 54) + (nproton > 86);
    return row;
  }
};

/// Stores positions and identities of atoms in the molecule
class Molecule {
 protected:
  std::vector<Atom> atoms_;

  std::string filename_;

  std::vector<double> efield_;

 public:
  Molecule() = default;
  Molecule(const Molecule& mol) = default;
  Molecule(const InputNode& input);
  Molecule(const std::string& molfile);
  Molecule(const std::vector<Atom>& atoms) : atoms_(atoms), filename_("") {}

  std::vector<yucca::Atom>& atoms() { return atoms_; }
  const std::vector<yucca::Atom>& atoms() const { return atoms_; }
  ptrdiff_t natoms() const { return atoms_.size(); }
  std::vector<double>& electric_field() { return efield_; }
  const std::vector<double>& electric_field() const { return efield_; }

  qleve::Tensor<2> get_coord_tensor() const;

  template <class Output>
  friend Output& operator<<(Output& o, const Molecule& m)
  {
    o << m.atoms().size() << std::endl << std::endl;
    for (const auto& a : m.atoms()) {
      o << std::setw(3) << a.symbol() << std::setw(16) << std::setprecision(8) << a.position()[0]
        << std::setw(16) << std::setprecision(8) << a.position()[1] << std::setw(16)
        << std::setprecision(8) << a.position()[2] << std::endl;
    }
    return o;
  }

  template <class Output>
  void print_xyz(Output&& o) const
  {
    fmt::print(o, "{:d}\n\n", atoms_.size());
    for (const auto& a : this->atoms()) {
      fmt::print(o, "{:3s} {:16.8f} {:16.8f} {:16.8f}\n", a.symbol(), a.position()[0],
                 a.position()[1], a.position()[2]);
    }
  }

  void print_xyz() const;

  double nuclear_repulsion() const;
  size_t total_nuclear_charge() const;
  double total_mass() const;
  qleve::Tensor<1> center_of_mass() const;

  qleve::Tensor<1> nuclear_multipole(const int order,
                                     const std::array<double, 3> = {0.0, 0.0, 0.0}) const;

 private:
  void read_molecule(const std::string& molfile);
  void read_xyz(const std::string& molfile);
};

} // namespace yucca
