// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/compute_densities.cpp
///
/// compute densities on points classes
///
/// Normalizations and transformation coefficients are from
/// Schlegel, Frisch, IJQC 54, 83-87 (1995)

#include <algorithm>
#include <cmath>

#include <qleve/matrix_multiply.hpp>

#include <yucca/physical/constants.hpp>
#include <yucca/structure/compute_densities.hpp>
#include <yucca/structure/evaluate_cao.hpp>
#include <yucca/structure/solid_harmonics.hpp>
#include <yucca/util/distribute_tasks.hpp>

using namespace yucca;
using namespace std;
using namespace qleve;

namespace
{

template <int max_order>
void compute_densities_impl(const Basis& basis, const ConstTensorView<2>& rho,
                            const ConstTensorView<2>& xyz, TensorView<2> densities,
                            const ptrdiff_t batchsize)
{
  const double thresh = 1e-12; // this should be an input parameter
  static_assert(max_order >= 0, "max_order must be non-negative");
  assert(basis.nbasis() == rho.extent(0) && rho.extent(0) == rho.extent(1));
  assert(xyz.extent(0) == 3 && xyz.extent(1) == densities.extent(0));
  assert(densities.extent(1) == (max_order + 1) * (max_order + 1));

  auto& bshells = basis.basisshells();
  const size_t nshells = bshells.size();

  const ptrdiff_t np = xyz.extent(1);
  const ptrdiff_t nops = (max_order + 1) * (max_order + 1);

  const ptrdiff_t max_l = basis.max_angular_momentum();
  const ptrdiff_t max_cart = (max_l + 1) * (max_l + 2) / 2;

  // transform to CAO basis
  auto rho_cao = solid_harmonics::transform_to_cao(basis, rho);
  const ptrdiff_t ncao = basis.ncao();
  // add off-diagonal elements
  for (ptrdiff_t jcao = 0; jcao < ncao; ++jcao) {
    for (ptrdiff_t icao = 0; icao < jcao; ++icao) {
      rho_cao(icao, jcao) += rho_cao(jcao, icao);
    }
  }

  Tensor<1> densbuf(batchsize * nops * ncao);
  Tensor<2> contract_buf(batchsize, nops);

  // shell norms for bounding contributions
  Tensor<1> column_norms(ncao);
  for (ptrdiff_t icao = 0; icao < ncao; ++icao) {
    column_norms(icao) = rho_cao.pluck(icao).norm();
  }
  Tensor<2> batchnorms(ncao, nops);

  StaticDistributeTasks tasks(np, batchsize);

  auto compute_batch = [&](const ptrdiff_t batch) {
    const auto [start, stop, tsize] = tasks.batch(batch);
    auto _xyz = xyz.const_slice(start, stop);
    TensorView<3> dbuf(densbuf.data(), tsize, nops, ncao);
    dbuf = 0.0;

    // compute chi_mu(xi) for all mu and xi
    const double shellthresh = thresh;
    for (ptrdiff_t ish = 0; ish < nshells; ++ish) {
      const auto [ioff, iend, icart] = bshells[ish].bounds_cao();

      auto dd = dbuf.slice(ioff, iend);
      evaluate_shell_at_points<max_order>(bshells[ish], _xyz, dd);
    }


    for (ptrdiff_t icao = 0; icao < ncao; ++icao) {
      for (ptrdiff_t iop = 0; iop < nops; ++iop) {
        batchnorms(icao, iop) = sqrt(dot_product(tsize, &dbuf(0, iop, icao), &dbuf(0, iop, icao)));
      }
    }

    for (ptrdiff_t jcao = 0; jcao < ncao; ++jcao) {
      if (batchnorms(jcao, 0) * column_norms(jcao) < thresh)
        continue;
      contract_buf = 0.0;
      for (ptrdiff_t icao = 0; icao <= jcao; ++icao) {
        if (batchnorms(icao, 0) * abs(rho_cao(icao, jcao)) < thresh)
          continue;

        contract_buf += rho_cao(icao, jcao) * dbuf.pluck(icao);
      }

      auto vals = dbuf.pluck(0, jcao).slice(0, tsize);
      auto cdens = contract_buf.pluck(0).slice(0, tsize);

      auto dens = densities.pluck(0).slice(start, stop);
      dens += vals * cdens;

      if constexpr (max_order >= 1) {
        // need to think a little more about what happens to derivatives here.
        for (int i = 0; i < 3; ++i) {
          auto dvals = dbuf.pluck(i + 1, jcao).slice(0, tsize);
          auto dcdens = contract_buf.pluck(i + 1).slice(0, tsize);
          auto ddens = densities.pluck(i + 1).slice(start, stop);

          ddens += vals * dcdens + dvals * cdens;
        }
      }
    }
  };

  for (ptrdiff_t ibatch = 0; ibatch < tasks.nbatch(); ++ibatch) {
    compute_batch(ibatch);
  }
}

} // namespace

/// Compute densities on a set of points
Tensor<2> yucca::compute_densities(const Basis& basis, const ConstTensorView<2>& rho,
                                   const ConstTensorView<2>& xyz)
{
  Tensor<2> densities(xyz.extent(1), 1);
  compute_densities_impl<0>(basis, rho, xyz, densities, 100);
  return densities;
}

/// Compute densities and generalized gradients on a set of points
Tensor<2> yucca::compute_densities_gradients(const Basis& basis, const ConstTensorView<2>& rho,
                                             const ConstTensorView<2>& xyz)
{
  Tensor<2> densities(xyz.extent(1), 4);
  compute_densities_impl<1>(basis, rho, xyz, densities, 100);
  return densities;
}
