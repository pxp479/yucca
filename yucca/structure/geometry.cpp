// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/geometry.cpp
///
/// A brief description of the contents of this file
#include <iostream>

#include <yucca/structure/basis.hpp>
#include <yucca/structure/basisset.hpp>
#include <yucca/structure/density_fitting.hpp>
#include <yucca/structure/geometry.hpp>

using namespace std;
using namespace yucca;

Geometry::Geometry(const Molecule& mol, const shared_ptr<BasisSet>& bas,
                   const shared_ptr<BasisSet>& jkbas, const shared_ptr<BasisSet>& jbas,
                   const shared_ptr<BasisSet>& kbas) :
    Molecule(mol),
    orbital_basis_(make_shared<Basis>(mol.atoms(), bas)),
    fitting_basis_(jkbas ? make_shared<Basis>(mol.atoms(), jkbas) : shared_ptr<Basis>()),
    j_fitting_basis_(jbas ? make_shared<Basis>(mol.atoms(), jbas) : shared_ptr<Basis>()),
    k_fitting_basis_(kbas ? make_shared<Basis>(mol.atoms(), kbas) : shared_ptr<Basis>())
{
  if (fitting_basis_) {
    density_fitting_ = make_shared<DensityFitting>(orbital_basis_, fitting_basis_);
  }
  if (j_fitting_basis_) {
    j_density_fitting_ = make_shared<DensityFitting>(orbital_basis_, j_fitting_basis_);
  }
  if (k_fitting_basis_) {
    k_density_fitting_ = make_shared<DensityFitting>(orbital_basis_, k_fitting_basis_);
  }
}

Geometry::Geometry(const Molecule& mol, const shared_ptr<Basis>& bas,
                   const shared_ptr<Basis>& jkbas, const shared_ptr<Basis>& jbas,
                   const shared_ptr<Basis>& kbas) :
    Molecule(mol),
    orbital_basis_(bas),
    fitting_basis_(jkbas),
    j_fitting_basis_(jbas),
    k_fitting_basis_(kbas)
{
  density_fitting_ = fitting_basis_ ? make_shared<DensityFitting>(orbital_basis_, fitting_basis_) :
                                      shared_ptr<DensityFitting>();
  j_density_fitting_ = j_fitting_basis_ ?
                           make_shared<DensityFitting>(orbital_basis_, j_fitting_basis_) :
                           shared_ptr<DensityFitting>();
  k_density_fitting_ = k_fitting_basis_ ?
                           make_shared<DensityFitting>(orbital_basis_, k_fitting_basis_) :
                           shared_ptr<DensityFitting>();
}

Geometry Geometry::update(const qleve::ConstTensorView<2> coords) const
{
  const ptrdiff_t natoms = atoms_.size();
  assert(3 == coords.extent(0) && natoms == coords.extent(1));

  vector<Atom> atoms;
  atoms.reserve(natoms);
  for (ptrdiff_t iatom = 0; iatom < natoms; ++iatom) {
    array<double, 3> xyz;
    xyz[0] = coords(0, iatom);
    xyz[1] = coords(1, iatom);
    xyz[2] = coords(2, iatom);

    const Atom& a = atoms_[iatom];
    atoms.emplace_back(xyz, a.charge(), a.mass(), a.symbol());
  }

  Molecule mol(atoms);
  shared_ptr<Basis> bas =
      orbital_basis_ ? make_shared<Basis>(orbital_basis_->update(coords)) : shared_ptr<Basis>();
  shared_ptr<Basis> jkbas =
      fitting_basis_ ? make_shared<Basis>(fitting_basis_->update(coords)) : shared_ptr<Basis>();

  shared_ptr<Basis> jbas =
      j_fitting_basis_ ? make_shared<Basis>(j_fitting_basis_->update(coords)) : shared_ptr<Basis>();
  shared_ptr<Basis> kbas =
      k_fitting_basis_ ? make_shared<Basis>(k_fitting_basis_->update(coords)) : shared_ptr<Basis>();

  return Geometry(mol, bas, jkbas, jbas, kbas);
}
