// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/basisset.cpp
///
/// A brief description of the contents of this file

#include FILESYSTEM_HEADER

#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <yaml-cpp/yaml.h>

#include <yucca/structure/basisset.hpp>
#include <yucca/structure/solid_harmonics_impl.hpp>
#include <yucca/util/default_paths.hpp>

using namespace std;
using namespace yucca;

namespace fs = FILESYSTEM_NAMESPACE;

string BasisSet::find_library(const string& nick, const vector<string>& libraries) const
{
  const string nickyaml = nick + string(".yaml");
  for (const auto& d : libraries) {
    fs::path p(d);

    auto libpath = p / nickyaml;
    if (fs::exists(libpath)) {
      return libpath.string();
    }
  }

  fmt::print("Basis set {} not found in any of the following paths!\n", nick);
  for (const auto& d : libraries) {
    fmt::print("{}\n", d);
  }
  throw runtime_error("Basis set could not be found!");
}

BasisSet::BasisSet(const string& nick, const string& localdir) : nick_(nick)
{
  vector<string> libraries = default_paths();
  for (auto& d : libraries) {
    d = d + "/basis";
  }
  // check localdir first
  libraries.insert(libraries.begin(), localdir);

  auto basisfile = find_library(nick, libraries);

  fmt::print("Reading basis from file {}\n", basisfile);

  YAML::Node basis = YAML::LoadFile(basisfile);

  for (auto iel = basis.begin(); iel != basis.end(); ++iel) {
    const string element = iel->first.as<string>();

    vector<ShellSet> shells;
    for (YAML::Node shell : iel->second) {
      const int angmom = shell["angular"].as<int>();
      const vector<double> exponents = shell["exponents"].as<vector<double>>();
      const vector<double> coeffs = shell["coeffs"].as<vector<double>>();

      shells.emplace_back(exponents, coeffs, angmom);
    }

    basis_map_.emplace(element, shells);
  }
}

void BasisSet::print(std::ostream& os, const std::vector<std::string>& elements) const
{
  size_t total_nprim = 0;
  size_t total_ncontr = 0;
  size_t total_nbf = 0;
  fmt::print(os, "  {:3s} {:40s} {:>8s} {:>8s} {:>8s}\n", "el", "pattern", "nprim", "ncontr",
             "nbf");
  fmt::print(os, "{:-^74s}\n", "");
  for (const auto& element : elements) {
    const auto& shells = basis_map_.at(element);

    constexpr int max_l = solid_harmonics::max_angular_momentum;
    std::vector<ptrdiff_t> nprim_per_shell(max_l + 1);
    std::vector<ptrdiff_t> ncont_per_shell(max_l + 1);

    for (const auto& s : shells) {
      nprim_per_shell[s.angmom()] += s.coeffs().size();
      ncont_per_shell[s.angmom()] += 1;
    }

    const ptrdiff_t nprim = std::accumulate(nprim_per_shell.begin(), nprim_per_shell.end(), 0);
    const ptrdiff_t ncontr = std::accumulate(ncont_per_shell.begin(), ncont_per_shell.end(), 0);

    ptrdiff_t nbf = 0;
    for (int l = 0; l <= max_l; ++l) {
      nbf += ncont_per_shell[l] * (2 * l + 1);
    }

    auto contraction_pattern = fmt::memory_buffer();

    fmt::format_to(std::back_inserter(contraction_pattern), "[");
    for (int l = 0; l <= max_l; ++l) {
      if (ncont_per_shell[l] == 0) {
        continue;
      }
      char lchar = solid_harmonics::shell_labels[l];
      fmt::format_to(std::back_inserter(contraction_pattern), "{:d}{:c}", nprim_per_shell[l],
                     lchar);
    }
    fmt::format_to(std::back_inserter(contraction_pattern), "]->(");
    for (int l = 0; l <= max_l; ++l) {
      if (ncont_per_shell[l] == 0) {
        continue;
      }
      char lchar = solid_harmonics::shell_labels[l];
      fmt::format_to(std::back_inserter(contraction_pattern), "{:d}{:c}", ncont_per_shell[l],
                     lchar);
    }
    fmt::format_to(std::back_inserter(contraction_pattern), ")");
    const auto contraction_string = fmt::to_string(contraction_pattern);

    fmt::print(os, "  {:3s} {:40s} {:>8d} {:>8d} {:>8d}\n", element, contraction_string, nprim,
               ncontr, nbf);
  }
  fmt::print(os, "{:-^74s}\n\n", "");
}
