// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/grad/gradients.cpp
///
/// gradient routines
#include <iostream>

#include <fmt/core.h>

#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/tensor.hpp>

#include <yucca/grad/gradients.hpp>
#include <yucca/structure/geometry.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

void yucca::gradient::nuclear_repulsion(const shared_ptr<Geometry>& geometry,
                                        qleve::TensorView<2> ddx)
{
  const ptrdiff_t natoms = geometry->natoms();
  for (ptrdiff_t iatom = 0; iatom < natoms; ++iatom) {
    const auto& a_atom = geometry->atoms()[iatom];
    const double Za = a_atom.charge();
    const auto& [xa, ya, za] = a_atom.position();

    for (ptrdiff_t jatom = 0; jatom < natoms; ++jatom) {
      if (iatom == jatom) {
        continue;
      }
      const auto& b_atom = geometry->atoms()[jatom];
      const double Zb = b_atom.charge();
      const auto& [xb, yb, zb] = b_atom.position();

      const double xab = xa - xb;
      const double yab = ya - yb;
      const double zab = za - zb;

      const double rab3_2_inv = -Za * Zb / std::pow(xab * xab + yab * yab + zab * zab, 1.5);

      ddx(0, iatom) += xab * rab3_2_inv;
      ddx(1, iatom) += yab * rab3_2_inv;
      ddx(2, iatom) += zab * rab3_2_inv;
    }
  }
}

void yucca::gradient::hcore(const shared_ptr<Geometry>& geometry,
                            const qleve::ConstTensorView<2>& P, const qleve::ConstTensorView<2>& W,
                            qleve::TensorView<2> ddx)
{
  contract_1e(geometry, P, libint2::Operator::kinetic, ddx);
  contract_1e(geometry, P, libint2::Operator::nuclear, ddx, geometry->natoms());
  contract_1e(geometry, W, libint2::Operator::overlap, ddx);
}

void yucca::gradient::contract_1e(const shared_ptr<Geometry>& geometry,
                                  const qleve::ConstTensorView<2> X, const libint2::Operator& op,
                                  qleve::TensorView<2> ddx, const ptrdiff_t& nparams)
{
  auto engine = libint::get_engine(op, 1, *geometry->orbital_basis());
  if (op == libint2::Operator::nuclear) {
    engine.set_params(libint::get_point_charges(geometry->atoms()));
  }

  const auto& buf = engine.results();

  auto shells = libint::get_shells(*geometry->orbital_basis());
  auto& bshells = geometry->orbital_basis()->basisshells();
  const ptrdiff_t nshells = shells.size();

  const ptrdiff_t max_shell =
      std::max_element(
          shells.begin(), shells.end(),
          [](const libint2::Shell& x, const libint2::Shell& y) { return x.size() < y.size(); })
          ->size();

  qleve::Tensor<2> Xtmp(max_shell, max_shell);
  qleve::Tensor<1> local_ddx(3 * (2 + nparams));

  for (ptrdiff_t jsh = 0; jsh < nshells; ++jsh) {
    const ptrdiff_t joff = bshells[jsh].offset();
    const ptrdiff_t jsize = shells[jsh].size();
    const ptrdiff_t jatom = bshells[jsh].atom_index();
    for (ptrdiff_t ish = 0; ish <= jsh; ++ish) {
      const ptrdiff_t ioff = bshells[ish].offset();
      const ptrdiff_t isize = shells[ish].size();
      const ptrdiff_t iatom = bshells[ish].atom_index();

      const ptrdiff_t nij = jsize * isize;

      qleve::TensorView<2> xblock(Xtmp.data(), jsize, isize);
      for (ptrdiff_t i = 0; i < isize; ++i) {
        for (ptrdiff_t j = 0; j < jsize; ++j) {
          xblock(j, i) = X(joff + j, ioff + i);
          if (ish != jsh) {
            xblock(j, i) += X(ioff + i, joff + j);
          }
        }
      }

      engine.compute(shells[ish], shells[jsh]);

      if (buf[0] == nullptr) {
        continue;
      }

      for (int i = 0; i < 3 * (2 + nparams); ++i) {
        local_ddx(i) = qleve::dot_product(nij, buf[i], xblock.data());
      }

      { // if synchronization is necessary, could do it here
        double* ddix = &ddx(0, iatom);
        ddix[0] += local_ddx(0);
        ddix[1] += local_ddx(1);
        ddix[2] += local_ddx(2);

        double* ddjx = &ddx(0, jatom);
        ddjx[0] += local_ddx(3);
        ddjx[1] += local_ddx(4);
        ddjx[2] += local_ddx(5);

        double* ddx_ptr = ddx.data();
        for (ptrdiff_t iparam = 0; iparam < 3 * nparams; ++iparam) {
          ddx_ptr[iparam] += local_ddx(6 + iparam);
        }
      }
    }
  }
}

void yucca::gradient::print(const shared_ptr<Geometry>& geometry, const ConstTensorView<2>& ddx)
{
  const ptrdiff_t natoms = geometry->natoms();

  fmt::print("{:>8s}   {:>18s} {:>18s} {:>18s}\n", "element", "dE/dx (H/bohr)", "dE/dy (H/bohr)",
             "dE/dz (H/bohr)");
  fmt::print("{:-^70s}\n", "");
  for (ptrdiff_t iat = 0; iat < natoms; ++iat) {
    auto x = ddx(0, iat);
    auto y = ddx(1, iat);
    auto z = ddx(2, iat);
    auto atomsymbol = geometry->atoms()[iat].symbol();

    fmt::print("{:>8s}   {:>18.8e} {:>18.8e} {:>18.8e}\n", atomsymbol, x, y, z);
  }
  fmt::print("\n");
  double norm = ddx.norm();
  double max =
      *std::max_element(ddx.data(), ddx.data() + ddx.size(),
                        [](const double x, const double y) { return std::abs(x) < std::abs(y); });

  fmt::print("Norm: {:18.8e}\n", norm);
  fmt::print("Max : {:18.8e}\n", max);
}
