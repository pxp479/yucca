// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/grad/contract_2e_impl.hpp
///
/// Driver class for handling gradients
#pragma once

#include <memory>

#include <fmt/core.h>
#include <tbb/enumerable_thread_specific.h>
#include <tbb/tbb.h>

#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor.hpp>

#include <yucca/structure/geometry.hpp>
#include <yucca/util/libint_interface.hpp>

namespace yucca::gradient
{

template <typename Func>
void contract_2e_2c_impl(const std::shared_ptr<Geometry>& geometry, const libint2::Operator& op,
                         Func func, qleve::TensorView_<double, 2> ddx)
{
  assert(geometry->fitting_basis());

  auto shells = libint::get_shells(*geometry->fitting_basis());
  auto& bshells = geometry->fitting_basis()->basisshells();
  const ptrdiff_t nshells = shells.size();

  const ptrdiff_t max_shell = geometry->fitting_basis()->max_shell_size();

  tbb::enumerable_thread_specific<libint2::Engine> engine_pool([&]() {
    auto out = libint::get_engine(op, 1, *geometry->fitting_basis());
    out.set(libint2::BraKet::xs_xs);
    return out;
  });

  // temporary storage for effective densities
  tbb::enumerable_thread_specific<qleve::Tensor<2>> Gamma2_pool(max_shell, max_shell);

  constexpr int nd = 6;

  // thread-local ddx
  tbb::enumerable_thread_specific<qleve::Tensor<2>> thread_ddx_pool(ddx.shape());

  auto kernel = [&](const tbb::blocked_range<size_t>& r) {
    auto& engine = engine_pool.local();
    const auto& buf = engine.results();

    std::array<double, nd> shell_ddx;
    qleve::Tensor<2>& thread_ddx = thread_ddx_pool.local();

    qleve::Tensor<2>& Gamma2 = Gamma2_pool.local();
    double* gamma_ptr = Gamma2.data();

    for (ptrdiff_t jsh = r.begin(); jsh < r.end(); ++jsh) {
      const ptrdiff_t joff = bshells[jsh].offset();
      const ptrdiff_t jsize = shells[jsh].size();
      const ptrdiff_t jatom = bshells[jsh].atom_index();
      for (ptrdiff_t ish = 0; ish <= jsh; ++ish) {
        const ptrdiff_t ioff = bshells[ish].offset();
        const ptrdiff_t isize = shells[ish].size();
        const ptrdiff_t iatom = bshells[ish].atom_index();

        if (iatom == jatom) {
          continue;
        }

        const ptrdiff_t nij = jsize * isize;

        const double ijdeg = (ish == jsh) ? 1.0 : 2.0;

        assert(nij <= Gamma2.size());

        engine.compute2<libint2::Operator::coulomb, libint2::BraKet::xs_xs, 1>(
            shells[ish], libint2::Shell::unit(), shells[jsh], libint2::Shell::unit());

        if (buf[0] == nullptr) {
          continue;
        }

        func(ish, ioff, isize, jsh, joff, jsize, gamma_ptr);

        for (int d = 0; d < nd; ++d) {
          shell_ddx[d] = ijdeg * qleve::dot_product(nij, buf[d], gamma_ptr);
        }

        thread_ddx(0, iatom) += shell_ddx[0];
        thread_ddx(1, iatom) += shell_ddx[1];
        thread_ddx(2, iatom) += shell_ddx[2];

        thread_ddx(0, jatom) += shell_ddx[3];
        thread_ddx(1, jatom) += shell_ddx[4];
        thread_ddx(2, jatom) += shell_ddx[5];
      }
    }
  };

  tbb::parallel_for(tbb::blocked_range<size_t>(0, nshells), kernel);

  // reduce thread-local ddxs
  thread_ddx_pool.combine_each([&](const qleve::Tensor<2>& thread_ddx) { ddx += thread_ddx; });
}


} // namespace yucca::gradient
