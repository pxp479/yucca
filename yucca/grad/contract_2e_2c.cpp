// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/grad/contract_2e_2c.cpp
///
/// Driver class for handling gradients of the 2e 2-center integrals
/// encountered using density fitting
#include <memory>

#include <qleve/tensor.hpp>

#include <yucca/grad/gradients.hpp>
#include <yucca/structure/geometry.hpp>

using namespace std;
using namespace yucca;

void yucca::gradient::contract_2e_2c(const shared_ptr<Geometry>& geometry,
                                     const libint2::Operator& op,
                                     const qleve::ConstTensorView<2>& Gamma2c,
                                     qleve::TensorView<2> ddx)
{
  assert(geometry->fitting_basis());

  auto gamma_func = [&](const ptrdiff_t& ish, const ptrdiff_t& ioff, const ptrdiff_t& isize,
                        const ptrdiff_t& jsh, const ptrdiff_t& joff, const ptrdiff_t& jsize,
                        double* gamma_ptr) {
    qleve::TensorView<2> gamma(gamma_ptr, jsize, isize);

    for (ptrdiff_t j = 0; j < jsize; ++j) {
      for (ptrdiff_t i = 0; i < isize; ++i) {
        gamma(j, i) = Gamma2c(i + ioff, j + joff);
      }
    }

    return gamma_ptr;
  };

  gradient::contract_2e_2c_impl(geometry, op, gamma_func, ddx);
}

