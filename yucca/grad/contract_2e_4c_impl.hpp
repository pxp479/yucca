// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/grad/contract_2e_4c_impl.hpp
///
/// Driver class for handling gradients
#pragma once

#include <memory>

#include <fmt/core.h>

#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor.hpp>

#include <yucca/structure/geometry.hpp>
#include <yucca/util/libint_interface.hpp>

namespace yucca::gradient
{

template <typename Func>
void contract_2e_4c_impl(const std::shared_ptr<Geometry>& geometry, const libint2::Operator& op,
                         Func func, qleve::TensorView_<double, 2> ddx)
{
  auto engine = libint::get_engine(op, 1, *geometry->orbital_basis());

  const auto& buf = engine.results();

  auto shells = libint::get_shells(*geometry->orbital_basis());
  auto& bshells = geometry->orbital_basis()->basisshells();
  const ptrdiff_t nshells = shells.size();

  const ptrdiff_t max_shell =
      std::max_element(
          shells.begin(), shells.end(),
          [](const libint2::Shell& x, const libint2::Shell& y) { return x.size() < y.size(); })
          ->size();

  qleve::Tensor<4> Gamma_tmp(max_shell, max_shell, max_shell, max_shell);
  qleve::Tensor<1> local_ddx(3 * 4);

  double* gamma_ptr = Gamma_tmp.data();

  for (ptrdiff_t jsh = 0; jsh < nshells; ++jsh) {
    const ptrdiff_t joff = bshells[jsh].offset();
    const ptrdiff_t jsize = shells[jsh].size();
    const ptrdiff_t jatom = bshells[jsh].atom_index();
    for (ptrdiff_t ish = 0; ish <= jsh; ++ish) {
      const ptrdiff_t ioff = bshells[ish].offset();
      const ptrdiff_t isize = shells[ish].size();
      const ptrdiff_t iatom = bshells[ish].atom_index();

      const ptrdiff_t nij = jsize * isize;
      const ptrdiff_t ijsh = jsh * (jsh + 1) / 2 + ish;

      for (ptrdiff_t lsh = 0; lsh <= jsh; ++lsh) {
        const ptrdiff_t loff = bshells[lsh].offset();
        const ptrdiff_t lsize = shells[lsh].size();
        const ptrdiff_t latom = bshells[lsh].atom_index();
        for (ptrdiff_t ksh = 0; ksh <= lsh && ((lsh * (lsh + 1) / 2 + ksh) <= ijsh); ++ksh) {
          const ptrdiff_t koff = bshells[ksh].offset();
          const ptrdiff_t ksize = shells[ksh].size();
          const ptrdiff_t katom = bshells[ksh].atom_index();

          const ptrdiff_t nkl = ksize * lsize;
          const ptrdiff_t klsh = lsh * (lsh + 1) / 2 + ksh;

          const ptrdiff_t nijkl = nij * nkl;

          assert(nijkl <= Gamma_tmp.size());

          engine.compute2<libint2::Operator::coulomb, libint2::BraKet::xx_xx, 1>(
              shells[ish], shells[jsh], shells[ksh], shells[lsh]);

          if (buf[0] == nullptr) {
            continue;
          }

          func(ish, ioff, isize, jsh, joff, jsize, ksh, koff, ksize, lsh, loff, lsize, gamma_ptr);

          for (int d = 0; d < 12; ++d) {
            local_ddx(d) = qleve::dot_product(nijkl, buf[d], gamma_ptr);
          }

          { // if synchronization is necessary, could do it here
            ddx(0, iatom) += local_ddx(0);
            ddx(1, iatom) += local_ddx(1);
            ddx(2, iatom) += local_ddx(2);

            ddx(0, jatom) += local_ddx(3);
            ddx(1, jatom) += local_ddx(4);
            ddx(2, jatom) += local_ddx(5);

            ddx(0, katom) += local_ddx(6);
            ddx(1, katom) += local_ddx(7);
            ddx(2, katom) += local_ddx(8);

            ddx(0, latom) += local_ddx(9);
            ddx(1, latom) += local_ddx(10);
            ddx(2, latom) += local_ddx(11);
          }
        }
      }
    }
  }
}


} // namespace yucca::gradient
