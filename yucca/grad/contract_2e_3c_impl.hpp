// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/grad/contract_2e_impl.hpp
///
/// Driver class for handling gradients
#pragma once

#include <memory>
#include <mutex>

#include <fmt/core.h>
#include <tbb/enumerable_thread_specific.h>
#include <tbb/tbb.h>

#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor.hpp>

#include <yucca/structure/geometry.hpp>
#include <yucca/util/libint_interface.hpp>

namespace yucca::gradient
{

template <typename Func>
void contract_2e_3c_impl(const std::shared_ptr<Geometry>& geometry, const libint2::Operator& op,
                         Func func, qleve::TensorView_<double, 2> ddx)
{
  assert(geometry->fitting_basis());

  const auto orbital = geometry->orbital_basis();
  const auto fitting = geometry->fitting_basis();

  const ptrdiff_t nao = orbital->nbasis();
  const ptrdiff_t nfit = fitting->nbasis();

  const std::size_t max_prim = std::max(orbital->max_nprim(), fitting->max_nprim()) + 4;
  const int max_l = std::max(orbital->max_angular_momentum(), fitting->max_angular_momentum());
  const ptrdiff_t nosh = orbital->basisshells().size();
  const ptrdiff_t nopairs = nosh * (nosh + 1) / 2;
  const ptrdiff_t nxsh = fitting->basisshells().size();

  const auto orbshells = libint::get_shells(*orbital);
  const auto& borbshells = orbital->basisshells();
  const auto fitshells = libint::get_shells(*fitting);
  const auto& bfitshells = fitting->basisshells();

  tbb::enumerable_thread_specific<libint2::Engine> engine_pool([=]() {
    libint2::Engine out(libint2::Operator::coulomb, max_prim, max_l, 1);
    out.set(libint2::BraKet::xs_xx);
    return out;
  });

  const ptrdiff_t max_orb_shell = orbital->max_shell_size();
  const ptrdiff_t max_fit_shell = fitting->max_shell_size();
  const ptrdiff_t max_shell = std::max(max_orb_shell, max_fit_shell);

  constexpr int nd = 9;
  tbb::enumerable_thread_specific<qleve::Tensor<3>> Gamma3_pool(max_fit_shell, max_orb_shell,
                                                                max_orb_shell);
  tbb::enumerable_thread_specific<qleve::Tensor<2>> thread_ddx_pool(ddx.shape());

  auto kernel = [&](const tbb::blocked_range2d<size_t>& r) {
    auto& engine = engine_pool.local();
    const auto& buf = engine.results();

    std::array<double, nd> shell_ddx;
    qleve::Tensor<2>& thread_ddx = thread_ddx_pool.local();

    qleve::Tensor<3>& Gamma3 = Gamma3_pool.local();
    double* gamma_ptr = Gamma3.data();

    for (ptrdiff_t ixsh = r.rows().begin(); ixsh < r.rows().end(); ++ixsh) {
      const auto& xsh = fitshells[ixsh];
      const auto& bxsh = bfitshells[ixsh];
      const ptrdiff_t xoff = bxsh.offset();
      const ptrdiff_t xsize = bxsh.size();
      const ptrdiff_t xatom = bfitshells[ixsh].atom_index();

      for (ptrdiff_t ish = r.cols().begin(); ish < r.cols().end(); ++ish) {
        const auto& osh_i = orbshells[ish];
        const auto& bsh_i = borbshells[ish];
        const ptrdiff_t ioff = bsh_i.offset();
        const ptrdiff_t isize = bsh_i.size();
        const ptrdiff_t iatom = borbshells[ish].atom_index();
        for (ptrdiff_t jsh = 0; jsh <= ish; ++jsh) {
          const auto& osh_j = orbshells[jsh];
          const auto& bsh_j = borbshells[jsh];
          const ptrdiff_t joff = bsh_j.offset();
          const ptrdiff_t jsize = bsh_j.size();
          const ptrdiff_t jatom = borbshells[jsh].atom_index();

          if (xatom == iatom && xatom == jatom) {
            continue;
          }

          const double ijdeg = (ish == jsh) ? 1.0 : 2.0;

          const ptrdiff_t nxij = xsize * isize * jsize;
          assert(nxij <= Gamma3.size());

          engine.compute2<libint2::Operator::coulomb, libint2::BraKet::xs_xx, 1>(
              xsh, libint2::Shell::unit(), osh_i, osh_j);

          const double* vxij = buf[0];

          if (vxij == nullptr) {
            continue;
          }

          // load shell data into gamma_ptr
          func(jsh, joff, jsize, ish, ioff, isize, ixsh, xoff, xsize, gamma_ptr);

          for (int d = 0; d < nd; ++d) {
            shell_ddx[d] = ijdeg * qleve::dot_product(nxij, buf[d], gamma_ptr);
          }

          thread_ddx(0, xatom) += shell_ddx[0];
          thread_ddx(1, xatom) += shell_ddx[1];
          thread_ddx(2, xatom) += shell_ddx[2];

          thread_ddx(0, iatom) += shell_ddx[3];
          thread_ddx(1, iatom) += shell_ddx[4];
          thread_ddx(2, iatom) += shell_ddx[5];

          thread_ddx(0, jatom) += shell_ddx[6];
          thread_ddx(1, jatom) += shell_ddx[7];
          thread_ddx(2, jatom) += shell_ddx[8];
        }
      }
    }
  };

  tbb::parallel_for(tbb::blocked_range2d<size_t>(0, nxsh, 0, nosh), kernel);

  // reduce thread-local ddxs
  thread_ddx_pool.combine_each([&](const qleve::Tensor<2>& thread_ddx) { ddx += thread_ddx; });
}

} // namespace yucca::gradient
