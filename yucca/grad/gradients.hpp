// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/grad/gradients.hpp
///
/// Driver class for handling gradients
#pragma once

#include <memory>

#include <fmt/core.h>

#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor.hpp>

#include <yucca/util/libint_interface.hpp>

namespace yucca
{

// use forward declarations here to prevent compilation cascades
class Geometry; // #include <yucca/structure/geometry.hpp>
class Wfn;      // #include <yucca/wfn/wfn.hpp>

namespace gradient
{

void nuclear_repulsion(const std::shared_ptr<Geometry>& geom, qleve::TensorView<2> ddx);

void hcore(const std::shared_ptr<Geometry>& geom, const qleve::ConstTensorView<2>& P,
           const qleve::ConstTensorView<2>& W, qleve::TensorView<2> ddx);

void contract_1e(const std::shared_ptr<Geometry>& geom, const qleve::ConstTensorView<2> X,
                 const libint2::Operator& op, qleve::TensorView<2> ddx,
                 const ptrdiff_t& nparams = 0);

/// And it should fill in the contents of Gamma with a shell block of the 2RDM (or whatever else
/// is used for contracting)
template <typename Func>
void contract_2e_4c_impl(const std::shared_ptr<Geometry>& geometry, const libint2::Operator& op,
                         Func func, qleve::TensorView<2> ddx);
/// 2e contraction with lazy filling of the 2RDM
template <typename Func>
void contract_2e_2c_impl(const std::shared_ptr<Geometry>& geometry, const libint2::Operator& op,
                         Func func, qleve::TensorView<2> ddx);
template <typename Func>
void contract_2e_3c_impl(const std::shared_ptr<Geometry>& geometry, const libint2::Operator& op,
                         Func func, qleve::TensorView<2> ddx);

/// 2e contraction with precomputed 2c and 3c 2RDM contributions
void contract_2e_2c(const std::shared_ptr<Geometry>& geometry, const libint2::Operator& op,
                    const qleve::ConstTensorView<2>& Gamma2c, qleve::TensorView<2> ddx);

void contract_2e_3c(const std::shared_ptr<Geometry>& geometry, const libint2::Operator& op,
                    const qleve::ConstTensorView<3>& Gamma3c, qleve::TensorView<2> ddx);

void print(const std::shared_ptr<Geometry>& geom, const qleve::ConstTensorView<2>& ddx);

} // namespace gradient

} // namespace yucca

#include <yucca/grad/contract_2e_2c_impl.hpp>
#include <yucca/grad/contract_2e_3c_impl.hpp>
#include <yucca/grad/contract_2e_4c_impl.hpp>
