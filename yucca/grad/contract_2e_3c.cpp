// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/grad/contract_2e_3c.cpp
///
/// Driver class for handling gradients of the 2e 3-center integrals
/// encountered using density fitting
#include <memory>

#include <qleve/tensor.hpp>

// #include <yucca/grad/contract_2e_3c_impl.hpp>
#include <yucca/grad/gradients.hpp>
#include <yucca/structure/geometry.hpp>

using namespace std;
using namespace yucca;

void yucca::gradient::contract_2e_3c(const shared_ptr<Geometry>& geometry,
                                     const libint2::Operator& op,
                                     const qleve::ConstTensorView<3>& Gamma3c,
                                     qleve::TensorView<2> ddx)
{
  assert(geometry->fitting_basis());

  auto gamma_func = [&](const ptrdiff_t& ish, const ptrdiff_t& ioff, const ptrdiff_t& isize,
                        const ptrdiff_t& jsh, const ptrdiff_t& joff, const ptrdiff_t& jsize,
                        const ptrdiff_t& ksh, const ptrdiff_t& koff, const ptrdiff_t& ksize,
                        double* gamma_ptr) {
    qleve::TensorView<3> gamma(gamma_ptr, isize, jsize, ksize);
    gamma = Gamma3c.const_subtensor({ioff, joff, koff}, {ioff + isize, joff + jsize, koff + ksize});

    return gamma_ptr;
  };

  gradient::contract_2e_3c_impl(geometry, op, gamma_func, ddx);
}

