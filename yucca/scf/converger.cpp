// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/converger.cpp
///
/// Implementation for convergence acceleration algorithms, like DIIS
#include <iostream>
#include <sstream>
#include <string>

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <qleve/diagonalize.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/unitary_exp.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/scf/converger.hpp>

using namespace yucca;
using namespace qleve;
using namespace std;

SCFConverger::SCFConverger(const InputNode& input, const std::string& key) :
    key_(key), mode_(OptMode::diagonalize)
{
  string conv_key = input.get_if_value<string>("convergence", "default");
  auto conv = input.get_node_if("convergence");
  if (conv && conv->is_map()) {
    conv_key = conv->get<string>("thresholds", conv_key);
  }

  diis_max_size_ = 5;
  diis_min_size_ = 1;
  diis_start_ = 2;
  level_shift_ = 0.0;
  orbital_select_ = "aufbau";

  use_second_order_ = input.get<bool>("second order", false);
  sec_order_switch_thresh_ = 1.0e-3;
  sec_order_start_ = 5;

  if (conv_key == "loose") {
    energy_thresh_ = 1.0e-6;
    grad_thresh_ = 1.0e-4;
    density_thresh_ = 1.0e-6;
  } else if (conv_key == "default") {
    energy_thresh_ = 1.0e-8;
    grad_thresh_ = 1.0e-6;
    density_thresh_ = 1.0e-8;
  } else if (conv_key == "tight") {
    energy_thresh_ = 1.0e-10;
    grad_thresh_ = 1.0e-7;
    density_thresh_ = 1.0e-10;
  } else if (conv_key == "ultratight") {
    energy_thresh_ = 1.0e-12;
    grad_thresh_ = 1.0e-9;
    density_thresh_ = 1.0e-12;
  } else {
    throw runtime_error("Unknown convergence option: use loose, default or tight");
  }

  if (auto conv = input.get_node_if("convergence"); conv && conv->is_map()) {
    energy_thresh_ = conv->get<double>("energy", energy_thresh_);
    grad_thresh_ = conv->get<double>("grad", grad_thresh_);
    density_thresh_ = conv->get<double>("density", density_thresh_);
    diis_max_size_ = conv->get<ptrdiff_t>("diis max", diis_max_size_);
    diis_min_size_ = conv->get<ptrdiff_t>("diis min", diis_min_size_);
    diis_start_ = conv->get<ptrdiff_t>("diis start", diis_start_);
    level_shift_ = conv->get<double>("level shift", level_shift_);
    orbital_select_ = conv->get<string>("orbital select", orbital_select_);

    sec_order_switch_thresh_ = conv->get<double>("second order switch", sec_order_switch_thresh_);
    sec_order_start_ = conv->get<ptrdiff_t>("second order start", sec_order_start_);
    if (conv->contains("second order switch") || conv->contains("second order start")) {
      use_second_order_ = true;
    }
  }

  // transformations
  transform(orbital_select_.begin(), orbital_select_.end(), orbital_select_.begin(), ::tolower);

  // sanity checks
  if (!(orbital_select_ == "aufbau" || orbital_select_ == "mom")) {
    throw runtime_error("Unrecognized orbital selection option: must be aufbau or mom");
  }

  iter_ = 0;

  // report options chosen
  fmt::print("Convergence Options:\n");
  fmt::print("  Thresholds:\n");
  fmt::print("    - energy change: {:6.2g}\n", energy_thresh_);
  // fmt::print("    - density change: {:6.2f}\n", density_thresh_);
  fmt::print("    - orbital gradient: {:6.2g}\n", grad_thresh_);

  fmt::print("\n");
  fmt::print("Convergence Acceleration:\n");
  fmt::print("  DIIS:\n");
  fmt::print("    max size: {:d}\n", diis_max_size_);
  fmt::print("    min size: {:d}\n", diis_min_size_);
  fmt::print("    start: {:d}\n", diis_start_);
  if (level_shift_ != 0.0)
    fmt::print("    level shift: {:6.2g} Hartree\n", level_shift_);
  if (orbital_select_ == "mom")
    fmt::print("    using maximum-overlap-method for orbital selection\n");

  if (use_second_order_) {
    fmt::print("Second order optimization turned on. Switching to second order when:\n");
    fmt::print("  - gradient norm < {:12.6f}\n", sec_order_switch_thresh_);
    fmt::print("  - minimum steps: {:d}\n", sec_order_start_);
  }
}

void SCFConverger::push_impl(const ConstArrayView& fock, const ConstArrayView& err,
                             const double energy)
{
  assert(err.size() == fock.size());

  if (!diis_) {
    diis_ = make_shared<DIIS<double>>(fock.size(), diis_max_size_, diis_min_size_);
  }
  if (iter_ < diis_start_) {
    diis_->clear();
  }
  diis_->push(fock, err);

  iter_ += 1;

  energies_.push_back(energy);
  errnrm_.push_back(err.norm());
}

void SCFConverger::next_impl(ArrayView next_coeff, ArrayView next_densmat) const {}

void SCFConverger::MOM_impl(const ptrdiff_t nocc, const qleve::ConstTensorView<2>& U,
                            qleve::TensorView<2> oldcoeff, qleve::TensorView<2> newcoeff)
{
  const ptrdiff_t nmo = oldcoeff.extent(1);
  assert(nmo == newcoeff.extent(1) && oldcoeff.extent(0) == newcoeff.extent(0));
  qleve::Tensor<1> olap(nmo);
  for (ptrdiff_t i = 0; i < nmo; ++i) {
    olap(i) = U.const_pluck(i).const_subtensor({0}, {nocc}).norm();
  }

  // partial sort indices according to overlap so first nocc_ are the largest
  vector<ptrdiff_t> idx(nmo);
  iota(idx.begin(), idx.end(), 0);
  partial_sort(idx.begin(), idx.begin() + nocc, idx.end(),
               [&olap](ptrdiff_t i, ptrdiff_t j) { return olap(i) > olap(j); });
  // sort within occupied and virtual to keep order
  sort(idx.begin(), idx.begin() + nocc);
  sort(idx.begin() + nocc, idx.end());

  for (ptrdiff_t i = 0; i < nmo; ++i) {
    oldcoeff.pluck(i) = newcoeff.pluck(idx[i]);
  }

  newcoeff = oldcoeff;
}

yucca::RHFConverger::RHFConverger(const InputNode& inp, const ptrdiff_t& nocc) :
    SCFConverger(inp), nocc_(nocc)
{}

void RHFConverger::push(const qleve::TensorView<2>& fock, const qleve::TensorView<2>& err,
                        double energy)
{
  const ptrdiff_t nn = fock.size();
  assert(nn == fock.size() && nn == err.size());

  this->push_impl(fock.const_reshape(nn), err.const_reshape(nn), energy);
}

void RHFConverger::next(qleve::TensorView<2> next_coeff, qleve::TensorView<2> next_densmat)
{
  if (!coeffs_) {
    coeffs_ = std::make_unique<qleve::Tensor<2>>(next_coeff);
  }

  const ptrdiff_t nao = next_coeff.extent(0);
  const ptrdiff_t nmo = next_coeff.extent(1);

  // determine which optimizing mode to use
  if (mode_ == OptMode::diagonalize && use_second_order_) {
    if (errnrm_.back() < sec_order_switch_thresh_ && iter_ > sec_order_start_) {
      fmt::print("Switching to Second Order optimization.\n");
      mode_ = OptMode::second_order;

      // set reference set of coeffs
      *coeffs_ = next_coeff;
    }
  }

  // take a step
  if (mode_ == OptMode::diagonalize) {
    qleve::Tensor<2> fockao(nao, nao);
    diis_->extrapolate(fockao.reshape(fockao.size()));

    qleve::Tensor<2> fock_mo(nmo, nmo);

    // this will break if next_coeff are not current good coeffs
    qleve::matrix_transform(1.0, fockao, next_coeff, 0.0, fock_mo);
    if (level_shift_ != 0) {
      for (ptrdiff_t i = nocc_; i < nmo; ++i) {
        fock_mo(i, i) += level_shift_;
      }
    }
    qleve::linalg::diagonalize(fock_mo);

    qleve::gemm("n", "n", 1.0, next_coeff, fock_mo, 0.0, *coeffs_);
    if (orbital_select_ == "aufbau") {
      next_coeff = *coeffs_;
    } else if (orbital_select_ == "mom") {
      MOM_impl(nocc_, fock_mo, next_coeff, *coeffs_);
    }
  } else if (mode_ == OptMode::second_order) {
    qleve::Tensor<2> fockao(nao, nao);
    diis_->back(fockao.reshape(fockao.size()));

    if (!bfgs_) {
      qleve::Tensor<2> tmp(next_coeff);
      qleve::gemm("n", "n", 1.0, fockao, next_coeff, 0.0, tmp);

      qleve::Tensor<1> eigs(nmo);
      // factor = 4.0 <- 2 from spin and 2 from occ-virt + virt-occ
      qleve::contract(4.0, next_coeff, "up", tmp, "up", 0.0, eigs, "p");

      // use orbital energy differences for initial hessian
      qleve::Tensor<2> h0(nmo - nocc_, nocc_);
      for (ptrdiff_t i = 0; i < nocc_; ++i) {
        for (ptrdiff_t a = 0; a < (nmo - nocc_); ++a) {
          h0(a, i) = eigs(a + nocc_) - eigs(i);
        }
      }
      bfgs_ = std::make_shared<BFGS<double>>(h0.reshape(h0.size()), 50);
    }

    if (!kappa_) {
      kappa_ = std::make_unique<qleve::Tensor<2>>(nmo - nocc_, nocc_);
    }

    // compute Fia in current basis
    const ptrdiff_t nvir = nmo - nocc_;
    auto tmp = qleve::gemm("n", "n", 1.0, fockao, next_coeff.slice(0, nocc_));
    // 4 <- 2 (spin) * 2 (ai + ia)
    auto fock_ai = qleve::gemm("t", "n", 4.0, next_coeff.slice(nocc_, nmo), tmp);

    // push and extrapolate
    auto dkappa = bfgs_->push_and_extrapolate(kappa_->reshape(kappa_->size()),
                                              fock_ai.reshape(fock_ai.size()));

    // update kappa
    *kappa_ += dkappa;

    // generate unitary transform
    auto U = qleve::linalg::unitary_exp(*kappa_);
    qleve::gemm("n", "n", 1.0, *coeffs_, U, 0.0, next_coeff);
  }

  auto occ_coeff = next_coeff.slice(0, nocc_);
  qleve::gemm("n", "t", 1.0, occ_coeff, occ_coeff, 0.0, next_densmat);
}
