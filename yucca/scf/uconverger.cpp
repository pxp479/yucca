// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/converger.cpp
///
/// Implementation for convergence acceleration algorithms, like DIIS
#include <iostream>
#include <sstream>
#include <string>

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <qleve/diagonalize.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/unitary_exp.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/scf/uconverger.hpp>

using namespace yucca;
using namespace qleve;
using namespace std;

UHFConverger::UHFConverger(const InputNode& inp, const std::array<ptrdiff_t, 2>& nelec) :
    SCFConverger(inp), nelec_(nelec)
{}

void UHFConverger::push(const qleve::TensorView<3>& fock, const qleve::TensorView<3>& err,
                        double energy)
{
  const ptrdiff_t nn = fock.size();
  assert(nn == fock.size() && nn == err.size());

  this->push_impl(fock.const_reshape(nn), err.const_reshape(nn), energy);
}

void UHFConverger::next(qleve::TensorView<3> next_coeff, qleve::TensorView<3> next_densmat)
{
  if (!coeffs_) {
    coeffs_ = std::make_unique<qleve::Tensor<3>>(next_coeff);
  }

  const auto [nocca, noccb] = nelec_;

  const ptrdiff_t nao = next_coeff.extent(0);
  const ptrdiff_t nmo = next_coeff.extent(1);
  const ptrdiff_t nsh = next_coeff.extent(2);

  auto next_coa = next_coeff.pluck(0);
  auto next_cob = next_coeff.pluck(1);

  auto coa = coeffs_->pluck(0);
  auto cob = coeffs_->pluck(1);

  // determine which optimizing mode to use
  if (mode_ == OptMode::diagonalize && use_second_order_) {
    if (errnrm_.back() < sec_order_switch_thresh_ && iter_ > sec_order_start_) {
      fmt::print("Switching to Second Order optimization.\n");
      mode_ = OptMode::second_order;

      // set reference set of coeffs
      *coeffs_ = next_coeff;
    }
  }

  // take a step
  if (mode_ == OptMode::diagonalize) {
    qleve::Tensor<3> fockao(nao, nao, nsh);
    diis_->extrapolate(fockao.reshape(fockao.size()));

    qleve::Tensor<3> fock_mo(nmo, nmo, nsh);

    // this will break if next_coeff are not current good coeffs
    qleve::matrix_transform(1.0, fockao.const_pluck(0), next_coeff.const_pluck(0), 0.0,
                            fock_mo.pluck(0));
    qleve::matrix_transform(1.0, fockao.const_pluck(1), next_coeff.const_pluck(1), 0.0,
                            fock_mo.pluck(1));
    if (level_shift_ != 0) {
      for (ptrdiff_t a = nocca; a < nmo; ++a) {
        fock_mo(a, a, 0) += level_shift_;
      }
      for (ptrdiff_t a = noccb; a < nmo; ++a) {
        fock_mo(a, a, 1) += level_shift_;
      }
    }

    auto fock_moa = fock_mo.pluck(0);
    auto fock_mob = fock_mo.pluck(1);

    qleve::linalg::diagonalize(fock_moa);
    qleve::linalg::diagonalize(fock_mob);

    qleve::gemm("n", "n", 1.0, next_coa, fock_moa, 0.0, coa);
    qleve::gemm("n", "n", 1.0, next_cob, fock_mob, 0.0, cob);

    if (orbital_select_ == "aufbau") {
      next_coeff = *coeffs_;
    } else if (orbital_select_ == "mom") {
      MOM_impl(nocca, fock_moa, next_coa, coa);
      MOM_impl(noccb, fock_mob, next_cob, cob);
    }
  } else if (mode_ == OptMode::second_order) {
    qleve::Tensor<3> fockao(nao, nao, nsh);
    diis_->back(fockao.reshape(fockao.size()));

    const ptrdiff_t npha = nocca * (nmo - nocca);
    const ptrdiff_t nphb = noccb * (nmo - noccb);

    const ptrdiff_t nph = npha + nphb;

    if (!bfgs_) {
      qleve::Tensor<3> tmp(next_coeff);
      qleve::gemm("n", "n", 1.0, fockao.const_pluck(0), next_coa, 0.0, tmp.pluck(0));
      qleve::gemm("n", "n", 1.0, fockao.const_pluck(1), next_cob, 0.0, tmp.pluck(1));

      qleve::Tensor<2> eigs(nmo, nsh);
      // factor = 2.0 <- from occ-virt + virt-occ
      qleve::contract(2.0, next_coeff, "upx", tmp, "upx", 0.0, eigs, "px");

      // use orbital energy differences for initial hessian

      qleve::Tensor<1> h0(nph);

      auto h0a = h0.slice(0, npha).reshape(nmo - nocca, nocca);
      auto h0b = h0.slice(npha, npha + nphb).reshape(nmo - noccb, noccb);

      for (ptrdiff_t i = 0; i < nocca; ++i) {
        for (ptrdiff_t a = 0; a < (nmo - nocca); ++a) {
          h0a(a, i) = eigs(a + nocca, 0) - eigs(i, 0);
        }
      }
      for (ptrdiff_t i = 0; i < noccb; ++i) {
        for (ptrdiff_t a = 0; a < (nmo - noccb); ++a) {
          h0b(a, i) = eigs(a + noccb, 1) - eigs(i, 1);
        }
      }
      bfgs_ = std::make_shared<BFGS<double>>(h0.reshape(h0.size()), 50);
      // fyi: this will throw a segmentation fault after 50 iterations of BFGS 
      // just need to put a BFGS restart() call in downstream to fix, but I'm leaving it alone - ERM
    }

    if (!kappa_) {
      kappa_ = std::make_unique<qleve::Tensor<1>>(nph);
    }

    // compute Fia in current basis
    const ptrdiff_t nvira = nmo - nocca;
    const ptrdiff_t nvirb = nmo - noccb;

    auto tmpa = qleve::gemm("n", "n", 1.0, fockao.const_pluck(0), next_coa.slice(0, nocca));
    auto tmpb = qleve::gemm("n", "n", 1.0, fockao.const_pluck(1), next_cob.slice(0, noccb));

    auto fock_ai = kappa_->zeros_like();

    auto fock_ai_a = fock_ai.slice(0, npha).reshape(nvira, nocca);
    auto fock_ai_b = fock_ai.slice(npha, npha + nphb).reshape(nvirb, noccb);
    qleve::gemm("t", "n", 2.0, next_coa.slice(nocca, nmo), tmpa, 0.0, fock_ai_a);
    qleve::gemm("t", "n", 2.0, next_cob.slice(noccb, nmo), tmpb, 0.0, fock_ai_b);
    // 2 <- 2 (ai + ia)

    // push and extrapolate
    auto dkappa = bfgs_->push_and_extrapolate(kappa_->reshape(kappa_->size()),
                                              fock_ai.reshape(fock_ai.size()));

    // update kappa
    *kappa_ += dkappa;

    // generate unitary transform
    auto Ua = qleve::linalg::unitary_exp(kappa_->slice(0, npha).reshape(nvira, nocca));
    auto Ub = qleve::linalg::unitary_exp(kappa_->slice(npha, npha + nphb).reshape(nvirb, noccb));

    qleve::gemm("n", "n", 1.0, coa, Ua, 0.0, next_coa);
    qleve::gemm("n", "n", 1.0, cob, Ub, 0.0, next_cob);
  }

  auto occa = next_coa.slice(0, nocca);
  auto occb = next_cob.slice(0, noccb);

  qleve::gemm("n", "t", 1.0, occa, occa, 0.0, next_densmat.pluck(0));
  qleve::gemm("n", "t", 1.0, occb, occb, 0.0, next_densmat.pluck(1));
}
