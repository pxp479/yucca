// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/fock.cpp
///
/// Fock matrix builder
#include <tbb/enumerable_thread_specific.h>
#include <tbb/tbb.h>

#include <qleve/tensor_contract.hpp>

#include <yucca/dft/density_functional.hpp>
#include <yucca/dft/integrate_functional.hpp>
#include <yucca/dft/molecular_grid.hpp>
#include <yucca/scf/fock.hpp>
#include <yucca/structure/density_fitting.hpp>
#include <yucca/util/libint_interface.hpp>

using namespace std;
using namespace yucca;

RFockBuilder::RFockBuilder(const Geometry& g, const double hartree, const double exchange) :
    geometry_(g), hartree_(hartree), exchange_(exchange), dft_batchsize_(100), dft_thresh_(1e-12)
{}

RFockBuilder::RFockBuilder(const Geometry& g, const shared_ptr<DensityFunctional> dft,
                           const shared_ptr<MolecularGrid> molgrid) :
    geometry_(g), dft_(dft), molgrid_(molgrid), dft_batchsize_(100), dft_thresh_(1e-12)
{
  hartree_ = 1.0;
  exchange_ = dft_ ? dft_->exchange_factor() : 1.0;

  density_ = make_shared<qleve::Tensor<2>>(molgrid_->weights().size(), dft_->nxc_operations());
}

// dispatch based on availability of density fitting
void RFockBuilder::VHF_jk(const qleve::ConstTensorView<2>& coeffs,
                          const qleve::ConstTensorView<1>& occs,
                          const qleve::ConstTensorView<2>& rho, qleve::TensorView<2> fock)
{
  if (geometry_.density_fitting()) {
    this->VHF_jk_df(coeffs, occs, fock);
  } else {
    this->VHF_jk_direct(rho, fock);
  }
}

// dispatch based on availability of density fitting
void RFockBuilder::operator()(const qleve::ConstTensorView<2>& coeffs,
                              const qleve::ConstTensorView<1>& occs,
                              const qleve::ConstTensorView<2>& rho, qleve::TensorView<2> fock)
{

  VHF_jk(coeffs, occs, rho, fock);
  if (dft_) {
    auto [Exc, Vxc] = dft::integrate_exc_vxc(*geometry_.orbital_basis(), *molgrid_, *dft_, rho,
                                             *density_, dft_batchsize_, dft_thresh_);
    fock += 0.25 * Vxc;
  }
}

// dispatch multicoeff version based on availability of density fitting
void RFockBuilder::operator()(const qleve::ConstTensorView<3>& coeffs1,
                              const qleve::ConstTensorView<3>& coeffs2,
                              const qleve::ConstTensorView<2>& occs,
                              const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock)
{
  if (geometry_.density_fitting()) {
    this->operator()(coeffs1, coeffs2, occs, fock);
  } else {
    this->operator()(rho, fock);
  }
}

// version hardwired to run through integral direct
void RFockBuilder::operator()(const qleve::ConstTensorView<2>& rho, qleve::TensorView<2> fock)
{
  assert(!dft_);
  VHF_jk_direct(rho, fock);
}

void RFockBuilder::VHF_jk_direct(const qleve::ConstTensorView<2>& rho, qleve::TensorView<2> fock)
{
  const auto shells = libint::get_shells(*geometry_.orbital_basis());
  const auto& bshells = geometry_.orbital_basis()->basisshells();

  const ptrdiff_t nshells = shells.size();

  std::mutex fock_lock;

  tbb::enumerable_thread_specific<libint2::Engine> engines([&]() {
    return libint::get_engine(libint2::Operator::coulomb, 0, *geometry_.orbital_basis());
  });

  tbb::parallel_for(ptrdiff_t(0), nshells, [&](ptrdiff_t ish) {
    auto& engine = engines.local();
    const auto& buf = engine.results();
    const ptrdiff_t ioff = bshells[ish].offset();
    const ptrdiff_t isize = shells[ish].size();
    for (ptrdiff_t jsh = 0; jsh <= ish; ++jsh) {
      const ptrdiff_t joff = bshells[jsh].offset();
      const ptrdiff_t jsize = shells[jsh].size();
      const ptrdiff_t ijsh = ish * (ish + 1) / 2 + jsh;
      for (ptrdiff_t ksh = 0; ksh <= ish; ++ksh) {
        const ptrdiff_t koff = bshells[ksh].offset();
        const ptrdiff_t ksize = shells[ksh].size();
        for (ptrdiff_t lsh = 0; lsh <= ksh && (ksh * (ksh + 1) / 2 + lsh) <= ijsh; ++lsh) {
          const ptrdiff_t loff = bshells[lsh].offset();
          const ptrdiff_t lsize = shells[lsh].size();
          const ptrdiff_t klsh = ksh * (ksh + 1) / 2 + lsh;

          engine.compute2<libint2::Operator::coulomb, libint2::BraKet::xx_xx, 0>(
              shells[ish], shells[jsh], shells[ksh], shells[lsh]);
          const double* vijkl = buf[0];

          if (vijkl == nullptr)
            continue;

          const double ijfac = ish == jsh ? 0.5 : 1.0;
          const double klfac = lsh == ksh ? 0.5 : 1.0;
          const double ijklfac = ijsh == klsh ? 0.5 : 1.0;

          const double symfac = ijfac * klfac * ijklfac;
          const double jj = 2.0 * hartree_ * symfac;
          const double kk = -exchange_ * symfac;

          {
            std::lock_guard<mutex> lk(fock_lock);
            for (size_t i = ioff, ijkl = 0; i < ioff + isize; ++i) {
              for (size_t j = joff; j < joff + jsize; ++j) {
                for (size_t k = koff; k < koff + ksize; ++k) {
                  for (size_t l = loff; l < loff + lsize; ++l, ++ijkl) {
                    fock(i, j) += jj * vijkl[ijkl] * (rho(k, l) + rho(l, k));
                    fock(j, i) += jj * vijkl[ijkl] * (rho(k, l) + rho(l, k));
                    fock(k, l) += jj * vijkl[ijkl] * (rho(i, j) + rho(j, i));
                    fock(l, k) += jj * vijkl[ijkl] * (rho(i, j) + rho(j, i));

                    if (exchange_ != 0.0) {
                      fock(i, k) += kk * vijkl[ijkl] * rho(j, l);
                      fock(i, l) += kk * vijkl[ijkl] * rho(j, k);
                      fock(j, k) += kk * vijkl[ijkl] * rho(i, l);
                      fock(j, l) += kk * vijkl[ijkl] * rho(i, k);
                      fock(k, i) += kk * vijkl[ijkl] * rho(l, j);
                      fock(k, j) += kk * vijkl[ijkl] * rho(l, i);
                      fock(l, i) += kk * vijkl[ijkl] * rho(k, j);
                      fock(l, j) += kk * vijkl[ijkl] * rho(k, i);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  });
}

void RFockBuilder::VHF_jk_df(const qleve::ConstTensorView<2>& coeffs,
                             const qleve::ConstTensorView<1>& occs, qleve::TensorView<2> fock)
{
  assert(geometry_.density_fitting());
  assert(std::all_of(occs.data(), occs.data() + occs.size(),
                     [](const double x) { return std::abs(1.0 - x) < 1e-10; }));

  const auto& B = geometry_.density_fitting()->B(); // (uv|Q)(Q|P)^(-1/2)
  const ptrdiff_t nocc = occs.size();
  const ptrdiff_t nao = coeffs.extent(0);
  const ptrdiff_t nfit = B.extent(2);

  assert(coeffs.extent(1) == nocc);
  assert(B.extent(0) == nao && B.extent(1) == nao);

  const double jfac = hartree_ * 2.0;
  const double kfac = -exchange_;

  qleve::Tensor<3> half(nao, nocc, nfit);

  // half transform
  qleve::contract(1.0, B, "uvP", coeffs, "vi", 0.0, half, "uiP");

  if (exchange_ != 0.0) {
    // build K with half transform
    qleve::contract(kfac, half, "uiP", half, "viP", 1.0, fock, "uv");
  }

  qleve::Tensor<1> full(nfit);
  // second half transform
  qleve::contract(1.0, half, "uiP", coeffs, "ui", 0.0, full, "P");
  // build J with full transform
  qleve::contract(jfac, full, "P", B, "uvP", 1.0, fock, "uv");
}

// fock build with two different sets of MOs and occupation numbers
void RFockBuilder::operator()(const qleve::ConstTensorView<2>& coeffs1,
                              const qleve::ConstTensorView<2>& coeffs2,
                              const qleve::ConstTensorView<1>& occs, qleve::TensorView<2> fock)
{
  assert(geometry_.density_fitting());
  assert(!dft_);
  assert(coeffs1.shape() == coeffs2.shape());
  assert(std::all_of(occs.data(), occs.data() + occs.size(),
                     [](const double x) { return (x >= 0.0); }));

  const auto& B = geometry_.density_fitting()->B(); // (uv|Q)(Q|P)^(-1/2)
  const ptrdiff_t nocc = occs.size();
  const ptrdiff_t nao = coeffs1.extent(0);
  const ptrdiff_t nfit = B.extent(2);

  assert(coeffs1.extent(1) == nocc);
  assert(B.extent(0) == nao && B.extent(1) == nao);

  const double jfac = hartree_ * 2.0;
  const double kfac = -exchange_;

  qleve::Tensor<2> c1(coeffs1);
  qleve::Tensor<2> c2(coeffs2);
  for (ptrdiff_t i = 0; i < nocc; ++i) {
    c1.pluck(i) *= std::sqrt(occs(i));
    c2.pluck(i) *= std::sqrt(occs(i));
  }

  qleve::Tensor<3> half1(nao, nocc, nfit);
  qleve::contract(1.0, B, "uvP", c1, "vi", 0.0, half1, "uiP");

  qleve::Tensor<3> half2(nao, nocc, nfit);
  qleve::contract(1.0, B, "uvP", c2, "vi", 0.0, half2, "uiP");

  // build K with half transform
  qleve::contract(kfac, half1, "uiP", half2, "viP", 1.0, fock, "uv");

  qleve::Tensor<1> full(nfit);
  // second half transform
  qleve::contract(1.0, half1, "uiP", c2, "ui", 0.0, full, "P");
  // build J with full transform
  qleve::contract(jfac, full, "P", B, "uvP", 1.0, fock, "uv");
}

// many fock builds with distinct sets of MOs and occupation numbers
void RFockBuilder::operator()(const qleve::ConstTensorView<3>& coeffs1,
                              const qleve::ConstTensorView<3>& coeffs2,
                              const qleve::ConstTensorView<2>& occs, qleve::TensorView<3> fock)
{
  assert(!dft_);
  const ptrdiff_t nfock = fock.extent(2);
  assert(nfock == coeffs1.extent(2) && nfock == coeffs2.extent(2) && nfock == occs.extent(1));

  for (ptrdiff_t i = 0; i < nfock; ++i) {
    this->operator()(coeffs1.const_pluck(i), coeffs2.const_pluck(i), occs.const_pluck(i),
                     fock.pluck(i));
  }
}

qleve::Tensor<2> RFockBuilder::operator()(const qleve::ConstTensorView<2>& rho)
{
  qleve::Tensor<2> fock(rho.shape());
  (*this)(rho, fock);
  return fock;
}

// ResHF Fock Builder
void RFockBuilder::operator()(const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock)
{
  const ptrdiff_t n_rho_mat = rho.extent(2);
  const ptrdiff_t n_fock_mat = fock.extent(2);

  if (n_rho_mat != n_fock_mat)
    throw runtime_error("Number of rho does not match number of fock matrices!\n");

  const ptrdiff_t rho_mat_row = rho.extent(0);
  const ptrdiff_t rho_mat_colm = rho.extent(1);

  for (ptrdiff_t i = 0; i < n_rho_mat; i++) {
    auto rho_mat = rho.const_pluck(i);
    auto fock_mat = fock.pluck(i);
    (*this)(rho_mat, fock_mat);
  }
}

// Matadj-ResHF Routine for 2e- K_AB_vo block terms
void RFockBuilder::operator()(const qleve::ConstTensorView<1>& xi_AB_i,
                              const qleve::ConstTensorView<2>& xi_AB_ij,
                              const qleve::ConstTensorView<2>& s_AvBo,
                              const qleve::ConstTensorView<2>& A_occ,
                              const qleve::ConstTensorView<2>& A_vir,
                              const qleve::ConstTensorView<2>& B_occ, qleve::TensorView<2> K_AB,
                              const bool print_kab)
{
  assert(geometry_.density_fitting());
  assert(!dft_);
  assert(A_occ.shape() == B_occ.shape());
  assert(A_occ.extent(0) == A_vir.extent(0));
  assert(std::all_of(xi_AB_i.data(), xi_AB_i.data() + xi_AB_i.size(),
                     [](const double x) { return (x >= 0.0); }));
  // this is non-essential now, but might come into play with future screening of overlaps?
  // therefore, I'm not going to bother to make sure all elements of xi_AB_ij are also positive - ER

  const auto& uvP = geometry_.density_fitting()->B(); // (uv|Q)(Q|P)^(-1/2)
  const ptrdiff_t nocc = xi_AB_i.size();
  const ptrdiff_t nvir = A_vir.extent(1);
  const ptrdiff_t nao = A_occ.extent(0);
  const ptrdiff_t nfit = uvP.extent(2);

  assert(s_AvBo.extent(0) == nvir && s_AvBo.extent(1) == nocc);
  assert(s_AvBo.shape() == K_AB.shape());
  assert(A_occ.extent(1) == nocc && B_occ.extent(1) == nocc);
  assert(xi_AB_ij.extent(0) == nocc && xi_AB_ij.extent(1) == nocc);
  assert(uvP.extent(0) == nao && uvP.extent(1) == nao);

  const double jfac = hartree_ * 2.0;
  const double kfac = -exchange_;

  // End Goal: K_AB_2e = K1 + K3 + K2
  // K1_ai = sum_j [(ai|jj) - (aj|ji)] xi_i * xi_j
  // K3_ai = sum_jk [(jj|kk) - (jk|kj)] s_AB_ai * xi_ij * xi_k
  // K2_ai = - sum_jk [(ji|kk) - (jk|ki)] s_AB_aj * xi_ij * xi_k

  // * * * * * //
  // Building intermediates
  // * * * * * //

  // 3-center integrals with half of available AO indexes contracted to MO
  qleve::Tensor<3> halfAv(nvir, nao, nfit);
  qleve::contract(1.0, uvP, "uvP", A_vir, "ua", 0.0, halfAv, "avP");
  qleve::Tensor<3> halfAo(nocc, nao, nfit);
  qleve::contract(1.0, uvP, "uvP", A_occ, "uj", 0.0, halfAo, "jvP");

  // Sdet_B coefficients with Xi_i term incorporated
  qleve::Tensor<2> BoXo(B_occ);
  for (ptrdiff_t i = 0; i < nocc; ++i) {
    BoXo.pluck(i) *= xi_AB_i(i);
  }

  // 3-center integrals with all of available AO indexes contracted to MO
  qleve::Tensor<3> AoBo_P(nocc, nocc, nfit);
  qleve::contract(1.0, halfAo, "jvP", B_occ, "vi", 0.0, AoBo_P, "jiP");

  // Fully contracted 3-center integrals with Xi_i term incorporated
  qleve::Tensor<3> AvBoXo_P(nvir, nocc, nfit);
  qleve::contract(1.0, halfAv, "avP", BoXo, "vi", 0.0, AvBoXo_P, "aiP");
  qleve::Tensor<3> AoBoXo_P(nocc, nocc, nfit);
  qleve::contract(1.0, halfAo, "jvP", BoXo, "vi", 0.0, AoBoXo_P, "jiP");

  // Fully contracted 3-center integrals with Xi_ij term(s) incorporated
  qleve::Tensor<3> AoBoXoo_P(nocc, nocc, nfit);
  qleve::contract(1.0, AoBo_P, "jiP", xi_AB_ij, "ij", 0.0, AoBoXoo_P, "jiP");
  qleve::Tensor<2> AoXooBoXoo_P(nocc, nfit);
  qleve::contract(1.0, AoBoXoo_P, "jjP", xi_AB_ij, "ij", 0.0, AoXooBoXoo_P, "iP");

  // Fully contracted 3-center integrals with Xi_i(j) and s_vo terms incorporated
  qleve::Tensor<3> SvoAoBoXoo_P(nvir, nocc, nfit);
  qleve::contract(1.0, AoBoXoo_P, "jiP", s_AvBo, "aj", 0.0, SvoAoBoXoo_P, "aiP");

  qleve::Tensor<3> SvoXoo(nvir, nocc, nocc);
  qleve::contract(1.0, s_AvBo, "ai", xi_AB_ij, "ij", 0.0, SvoXoo, "aij");
  qleve::Tensor<3> SvoXooAoBo_P(nvir, nocc, nfit);
  qleve::contract(1.0, SvoXoo, "aij", AoBo_P, "jjP", 0.0, SvoXooAoBo_P, "aiP");

  // Reduced rank MO integrals with auxillary basis indexes contracted together
  qleve::Tensor<2> AoBoXo_AoBo(nocc, nocc);
  qleve::contract(1.0, AoBoXo_P, "jkP", AoBo_P, "kiP", 0.0, AoBoXo_AoBo, "ji");
  qleve::Tensor<2> AoBoXo_AoBoXoo_ji(nocc, nocc);
  qleve::contract(1.0, AoBoXo_AoBo, "ji", xi_AB_ij, "ij", 0.0, AoBoXo_AoBoXoo_ji, "ji");
  qleve::Tensor<2> AoBoXo_AoBoXoo_ii(nocc, nocc);
  qleve::contract(1.0, AoBoXo_AoBo, "jj", xi_AB_ij, "ij", 0.0, AoBoXo_AoBoXoo_ii, "ii");

  // 3-center MO integral with MO indexes contracted together
  qleve::Tensor<1> fullAoBoXo(nfit);
  qleve::contract(1.0, halfAo, "jvP", BoXo, "vj", 0.0, fullAoBoXo, "P");

  // * * * * * //
  // Building and Exporting K_AB_2e = K1 + K3 + K2
  // * * * * * //

  // K1_ai = sum_j [(ai|jj) - (aj|ji)] xi_i * xi_j
  // K1 coulomb term
  qleve::Tensor<2> K1(K_AB.zeros_like());
  qleve::contract(jfac, AvBoXo_P, "aiP", fullAoBoXo, "P", 0.0, K1, "ai");
  // K1 exchange term
  qleve::contract(kfac, AvBoXo_P, "ajP", AoBoXo_P, "jiP", 1.0, K1, "ai");

  // K3_ai = sum_jk [(jj|kk) - (jk|kj)] s_AB_ai * xi_ij * xi_k
  // K3 coulomb term
  qleve::Tensor<2> K3(K_AB.zeros_like());
  qleve::contract(jfac, SvoXooAoBo_P, "aiP", fullAoBoXo, "P", 0.0, K3, "ai");
  // K3 exchange term
  qleve::contract(kfac, AoBoXo_AoBoXoo_ii, "ii", s_AvBo, "ai", 1.0, K3, "ai");

  // K2_ai = - sum_jk [(ji|kk) - (jk|ki)] s_AB_aj * xi_ij * xi_k
  // K2 coulomb term
  qleve::Tensor<2> K2(K_AB.zeros_like());
  qleve::contract(-1.0 * jfac, SvoAoBoXoo_P, "aiP", fullAoBoXo, "P", 0.0, K2, "ai");
  // K2 exchange term
  qleve::contract(-1.0 * kfac, AoBoXo_AoBoXoo_ji, "ji", s_AvBo, "aj", 1.0, K2, "ai");

  if (print_kab) {
    fmt::print("-------------------\n");
    fmt::print("2e- terms:\n");
    fmt::print("  * K1_2e norm: {:1e}\n", K1.norm());
    fmt::print("  * K2_2e norm: {:1e}\n", K2.norm());
    fmt::print("  * K3_2e norm: {:1e}\n", K3.norm());

    fmt::print("\n");
    qleve::Tensor<2> K1K2K3 = K1 + K2 + K3;
    fmt::print("  * K1+K2+K3_2e norm: {:1e}\n", K1K2K3.norm());
  }

  K_AB = K1 + K3 + K2;
} // end matadj-ResHF fockbuilder

yucca::RSymmetricFockBuilder::RSymmetricFockBuilder(const Geometry& g, const double hartree,
                                                    const double exchange) :
    geometry_(g), hartree_(hartree), exchange_(exchange)
{}

void RSymmetricFockBuilder::operator()(const qleve::ConstTensorView<2>& rho,
                                       qleve::TensorView<2> fock)
{
  const auto shells = libint::get_shells(*geometry_.orbital_basis());
  const auto& bshells = geometry_.orbital_basis()->basisshells();
  auto engine = libint::get_engine(libint2::Operator::coulomb, 0, *geometry_.orbital_basis());

  const size_t nshells = shells.size();

  const auto& buf = engine.results();

  for (size_t ish = 0, ijsh = 0; ish < nshells; ++ish) {
    const size_t ioff = bshells[ish].offset();
    const size_t isize = shells[ish].size();
    for (size_t jsh = 0; jsh <= ish; ++jsh, ++ijsh) {
      const size_t joff = bshells[jsh].offset();
      const size_t jsize = shells[jsh].size();
      for (size_t ksh = 0, klsh = 0; ksh < nshells; ++ksh) {
        const size_t koff = bshells[ksh].offset();
        const size_t ksize = shells[ksh].size();
        for (size_t lsh = 0; lsh <= ksh && klsh <= ijsh; ++lsh, ++klsh) {
          const size_t loff = bshells[lsh].offset();
          const size_t lsize = shells[lsh].size();

          engine.compute(shells[ish], shells[jsh], shells[ksh], shells[lsh]);
          const double* vijkl = buf[0];

          if (vijkl == nullptr)
            continue;

          const double ijfac = ish == jsh ? 0.5 : 1.0;
          const double klfac = lsh == ksh ? 0.5 : 1.0;
          const double ijklfac = ijsh == klsh ? 0.5 : 1.0;

          const double symfac = ijfac * klfac * ijklfac;


          for (size_t i = ioff, ijkl = 0; i < ioff + isize; ++i) {
            for (size_t j = joff; j < joff + jsize; ++j) {
              for (size_t k = koff; k < koff + ksize; ++k) {
                for (size_t l = loff; l < loff + lsize; ++l, ++ijkl) {
                  // vijkl should have (ij|kl)
                  if (ish == jsh)
                    fock(i, j) += 2.0 * hartree_ * symfac * vijkl[ijkl] * 2.0 * rho(l, k);
                  fock(j, i) += 2.0 * hartree_ * symfac * vijkl[ijkl] * 2.0 * rho(l, k);
                  if (ksh == lsh)
                    fock(k, l) += 2.0 * hartree_ * symfac * vijkl[ijkl] * 2.0 * rho(j, i);
                  fock(l, k) += 2.0 * hartree_ * symfac * vijkl[ijkl] * 2.0 * rho(j, i);

                  fock(i, k) -= exchange_ * symfac * vijkl[ijkl] * rho(j, l);
                  fock(i, l) -= exchange_ * symfac * vijkl[ijkl] * rho(j, k);
                  fock(j, k) -= exchange_ * symfac * vijkl[ijkl] * rho(i, l);
                  fock(j, l) -= exchange_ * symfac * vijkl[ijkl] * rho(i, k);
                  fock(k, i) -= exchange_ * symfac * vijkl[ijkl] * rho(l, j);
                  fock(k, j) -= exchange_ * symfac * vijkl[ijkl] * rho(l, i);
                  fock(l, i) -= exchange_ * symfac * vijkl[ijkl] * rho(k, j);
                  fock(l, j) -= exchange_ * symfac * vijkl[ijkl] * rho(k, i);
                }
              }
            }
          }
        }
      }
    }
  }
}

qleve::Tensor<2> RSymmetricFockBuilder::operator()(const qleve::ConstTensorView<2>& rho)
{
  qleve::Tensor<2> fock(rho.shape());
  (*this)(rho, fock);
  const auto [ndim, mdim] = fock.shape();
  for (size_t i = 0; i < ndim; ++i) {
    for (size_t j = i + 1; j < mdim; ++j) {
      fock(j, i) = fock(i, j);
    }
  }
  return fock;
}
