// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/converger.hpp
///
/// Interface file for convergence acceleration like DIIS
#pragma once

#include <memory>

#include <qleve/array.hpp>
#include <qleve/tensor.hpp>

#include <yucca/algo/bfgs.hpp>
#include <yucca/algo/diis.hpp>

namespace yucca
{

// fwd declare
class InputNode; // #include <yucca/input/input_node.hpp>

class SCFConverger {
 protected:
  enum class OptMode
  {
    diagonalize,
    second_order
  };

 protected:
  double energy_thresh_;  ///< converge when dE is below this thresh
  double grad_thresh_;    ///< converge when gradient is below this thresh
  double density_thresh_; ///< converge when change in density is below this thresh

  ptrdiff_t diis_max_size_; ///< max. size of diis
  ptrdiff_t diis_min_size_; ///< min. size of diis
  ptrdiff_t diis_start_;    ///< start diis after this many iterations

  double level_shift_; ///< apply shift in Hartree to virtuals

  std::string orbital_select_; ///< algorithm to use to select new orbitals

  std::shared_ptr<DIIS<double>> diis_; ///< for extrapolation

  bool use_second_order_;          ///< flag for using second order algorithm
  double sec_order_switch_thresh_; ///< switch to second order below threshold
  ptrdiff_t sec_order_start_;      ///< switch to second order only after number of steps

  std::shared_ptr<BFGS<double>> bfgs_; ///< BFGS object for second order optimization

  std::vector<double> energies_; ///< list of previous energies
  std::vector<double> errnrm_;   ///< list of previous errors

  OptMode mode_;
  int iter_;

  std::string key_;

 protected:
  SCFConverger() = default;
  SCFConverger(const InputNode& inp, const std::string& key = "convergence");

  void push_impl(const qleve::ConstArrayView& fock, const qleve::ConstArrayView& err,
                 const double energy);

  void next_impl(qleve::ArrayView next_coeff, qleve::ArrayView next_guess) const;

  void MOM_impl(const ptrdiff_t nocc, const qleve::ConstTensorView<2>& U,
                qleve::TensorView<2> oldcoeff, qleve::TensorView<2> newcoeff);

 public:
  double dE() const
  {
    return (energies_.size() >= 2) ? energies_.back() - energies_[energies_.size() - 2] :
                                     energies_.front();
  }
};

class RHFConverger : public SCFConverger {
 protected:
  ptrdiff_t nocc_;

  std::unique_ptr<qleve::Tensor<2>> coeffs_; ///< reference set of coeffs
  std::unique_ptr<qleve::Tensor<2>> kappa_;  ///< rotation matrix for bfgs

 public:
  RHFConverger(const InputNode& inp, const ptrdiff_t& nocc);

  void push(const qleve::TensorView<2>& fock, const qleve::TensorView<2>& err, double energy);

  /// next_coeff should be the current set of coeffs
  void next(qleve::TensorView<2> next_coeff, qleve::TensorView<2> next_densmat);
};

} // namespace yucca
