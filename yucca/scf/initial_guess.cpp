// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/initial_guess.cpp
///
/// Initial guess classes like sum of atomic densities
#include <memory>
#include <unordered_map>

#include <fmt/core.h>

#include <qleve/matrix_transform.hpp>
#include <qleve/orthogonalize.hpp>

#include <yucca/scf/converger.hpp>
#include <yucca/scf/fock.hpp>
#include <yucca/scf/initial_guess.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/util/periodictable.hpp>

using namespace std;
using namespace yucca;

qleve::Tensor<2> yucca::initial_guess::hcore(const qleve::Tensor<2>& overlap,
                                             const qleve::Tensor<2>& hcore)
{
  qleve::Tensor<2> U = qleve::linalg::orthogonalize_metric(overlap);

  const auto [nao, nmo] = U.shape();

  qleve::Tensor<2> hcore_mo(nmo, nmo);
  qleve::matrix_transform(1.0, hcore, U, 0.0, hcore_mo);

  qleve::linalg::diagonalize(hcore_mo);

  qleve::Tensor<2> coeff(U);

  qleve::gemm("n", "n", 1.0, U, hcore_mo, 0.0, coeff);

  return coeff;
}

qleve::Tensor<2> yucca::initial_guess::atomic_densities(const yucca::Geometry& geometry)
{
  const auto& basis = *geometry.orbital_basis();
  if (!basis.spherical()) {
    throw runtime_error("initial_guess not designed for CAO");
  }
  const qleve::qtens_len_t nao = basis.nbasis();

  unordered_map<string, shared_ptr<qleve::Tensor<2>>> atom_densities;

  qleve::Tensor<2> overlap = geometry.overlap();

  qleve::Tensor<2> out = overlap.zeros_like();

  PeriodicTable pt;

  const int maxiter = 51; // does this need to be adjustable?

  for (auto& atom : basis.atomshells()) {
    shared_ptr<qleve::Tensor<2>> density;
    const ptrdiff_t off = atom.offset();
    const ptrdiff_t nbasis = atom.size();

    if (auto iatom = atom_densities.find(atom.symbol()); iatom == atom_densities.end()) {
      fmt::print("Computing atomic density for {}\n", atom.symbol());
      // compute atomic density for this atom
      qleve::Tensor<2> atomoverlap(overlap.subtensor({off, off}, {off + nbasis, off + nbasis}));

      // make new geometry object out of atomshell
      Molecule atom_mol({geometry.atoms()[atom.atom_index()]});
      auto atom_basis = make_shared<Basis>(vector<AtomShells>{atom}, nullptr);

      Geometry atomgeometry(atom_mol, atom_basis, nullptr);
      auto atomhcore = atomgeometry.hcore();

      auto coeff = make_shared<qleve::Tensor<2>>(nbasis, nbasis);
      ptrdiff_t nco = 0;
      // collect all shells by angular momentum
      vector<vector<const BasisShell*>> lshells(6); // up to h functions?
      for (auto& shell : atom.shells()) {
        lshells.at(shell.angular_momentum()).push_back(&shell);
      }
      vector<ptrdiff_t> shellsizes;
      for (auto& lsh : lshells) {
        const ptrdiff_t nsh = accumulate(lsh.begin(), lsh.end(), 0,
                                         [](int x, const BasisShell* b) { return x + b->size(); });
        shellsizes.push_back(nsh);
        if (nsh == 0)
          continue;

        qleve::Tensor<2> loverlap(nsh, nsh);
        ptrdiff_t jsh = 0;
        for (const BasisShell* jb : lsh) {
          const ptrdiff_t jsize = jb->size();
          const ptrdiff_t joff = jb->offset();
          ptrdiff_t ish = 0;
          for (const BasisShell* ib : lsh) {
            const ptrdiff_t isize = ib->size();
            const ptrdiff_t ioff = ib->offset();

            loverlap.subtensor({ish, jsh}, {ish + isize, jsh + jsize}) =
                overlap.subtensor({ioff, joff}, {ioff + isize, joff + jsize});

            ish += isize;
          }
          jsh += jsize;
        }

        auto lcoeff = qleve::linalg::orthogonalize_metric(loverlap);
        ptrdiff_t bsh = 0;
        for (const BasisShell* b : lsh) {
          const ptrdiff_t boff = b->offset() - off;
          coeff->subtensor({boff, nco}, {boff + b->size(), nco + lcoeff.extent(1)}) =
              lcoeff.subtensor({bsh, 0}, {bsh + b->size(), lcoeff.extent(1)});
          bsh += b->size();
        }
        nco += lcoeff.extent(1);
      }
      qleve::Tensor<2> orthog_coeff(
          coeff->slice(0, nco)); // orthogonal orbitals organized by angular momentum (shellsizes)
      qleve::Tensor<2> hcore_mo(nco, nco);

      qleve::matrix_transform(1.0, atomhcore, orthog_coeff, 0.0, hcore_mo);
      qleve::linalg::diagonalize_blocks(hcore_mo, shellsizes);

      qleve::Tensor<2> cc = orthog_coeff.zeros_like();
      qleve::gemm("n", "n", 1.0, orthog_coeff, hcore_mo, 0.0, cc);

      // now make open/closed density matrices
      const Element& elem = pt.element(atom.symbol());
      array<int, 4> nclos_ref; // number of double occupied orbitals in each shell
      array<int, 4> nopen_ref; // number of electrons in the remaining open shell

      qleve::Tensor<2> clos_dens = atomhcore.zeros_like();
      qleve::Tensor<2> open_dens = atomhcore.zeros_like();
      for (int i = 0; i < 4; ++i) {
        const int nl = 2 * (2 * i + 1);
        const int nconf = elem.configuration[i];
        const int nfull_shells = nconf / nl;
        nclos_ref[i] = nfull_shells * (2 * i + 1);
        nopen_ref[i] = nconf - 2 * nclos_ref[i];
      }

      density = make_shared<qleve::Tensor<2>>(nbasis, nbasis);

      const bool has_closed = nclos_ref[0] > 0;
      const bool has_open =
          std::any_of(nopen_ref.begin(), nopen_ref.end(), [](int i) { return i > 0; });

      // Build open and closed densities
      ptrdiff_t ish = 0;
      for (int i = 0; i < 4; ++i) {
        const int nl = 2 * i + 1;
        if (nclos_ref[i]) {
          auto cc_closed = cc.slice(ish, ish + nclos_ref[i]);
          qleve::gemm("n", "t", 1.0, cc_closed, cc_closed, 1.0, clos_dens);
        }
        *density = clos_dens + open_dens;

        if (nopen_ref[i]) {
          auto cc_open = cc.slice(ish + nclos_ref[i], ish + nclos_ref[i] + nl);
          const double openfac = 0.5 * nopen_ref[i] / nl;
          qleve::gemm("n", "t", openfac, cc_open, cc_open, 1.0, open_dens);
        }
        ish += shellsizes[i];
      }

      double last_energy = 0.0;

      RFockBuilder clos_fock(atomgeometry, 1.0, 1.0);
      RFockBuilder open_fock(atomgeometry, 1.0, 0.0);

      qleve::Tensor<2> last_fock = atomhcore.zeros_like();
      qleve::Tensor<2> fock = atomhcore.zeros_like();
      for (int iter = 0; iter < maxiter; ++iter) {
        fock = atomhcore;
        if (has_closed) {
          clos_fock(clos_dens, fock);
        }
        if (has_open) {
          open_fock(open_dens, fock);
        }

        const double energy1e =
            2.0 * qleve::dot_product(density->size(), density->data(), atomhcore.data());
        const double energy_fock =
            2.0 * qleve::dot_product(density->size(), density->data(), fock.data());
        const double energy = 0.5 * (energy1e + energy_fock);

        qleve::Tensor<2> residual = atomhcore.zeros_like();
        qleve::Tensor<2> tmp = atomhcore.zeros_like();
        qleve::gemm("n", "n", 1.0, fock, *density, 0.0, tmp);
        qleve::gemm("n", "n", 1.0, tmp, atomoverlap, 0.0, residual);
        residual = residual - residual.transpose();

        const double residual_norm = residual.norm();
        if (residual_norm < 1.0e-2 || fabs(energy - last_energy) < 1.0e-4)
          break;
        last_energy = energy;

        // diis.push_back() will only be effective when I have real DIIS implemented
        // average this and last fock matrix to even things out
        if (iter == 0) {
          last_fock = fock;
        }
        qleve::Tensor<2> extrap_fock = 0.5 * (fock + last_fock);
        last_fock = fock;

        qleve::Tensor<2> fock_mo(nco, nco);
        qleve::matrix_transform(1.0, extrap_fock, orthog_coeff, 0.0, fock_mo);
        qleve::linalg::diagonalize_blocks(fock_mo, shellsizes);
        qleve::gemm("n", "n", 1.0, orthog_coeff, fock_mo, 0.0, cc);

        // Build open and closed densities
        ish = 0;
        open_dens = 0.0;
        clos_dens = 0.0;
        for (int i = 0; i < 4; ++i) {
          const int nl = 2 * i + 1;
          if (nclos_ref[i]) {
            auto cc_closed = cc.slice(ish, ish + nclos_ref[i]);
            qleve::gemm("n", "t", 1.0, cc_closed, cc_closed, 1.0, clos_dens);
          }

          if (nopen_ref[i]) {
            auto cc_open = cc.slice(ish + nclos_ref[i], ish + nclos_ref[i] + nl);
            const double openfac = 0.5 * nopen_ref[i] / nl;
            qleve::gemm("n", "t", openfac, cc_open, cc_open, 1.0, open_dens);
          }
          ish += shellsizes[i];
        }
        *density = clos_dens + open_dens;
      }

      atom_densities.insert({atom.symbol(), density});
    } else {
      density = iatom->second;
    }

    out.subtensor({off, off}, {off + nbasis, off + nbasis}) = *density;
  }

  return out;
}
