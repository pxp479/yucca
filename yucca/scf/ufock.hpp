// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/fock.hpp
///
/// Unrestricted Fock matrix builder
#pragma once

#include <yucca/structure/geometry.hpp>

namespace yucca
{

class DensityFunctional; // fwd <src/dft/density_functional.hpp>
class MolecularGrid;     // fwd <src/dft/molecular_grid.hpp>

class UFockBuilder {
 protected:
  const Geometry& geometry_;

  // DFT specific quantities
  const std::shared_ptr<DensityFunctional> dft_; //< Select the density functional
  const std::shared_ptr<MolecularGrid> molgrid_; //< select the molecular grid for integration
  double Exc_;                                   //< for DFT, compute Exc while computing Vxc
  std::shared_ptr<qleve::Tensor<3>> density_;    //< for DFT, store last density
  ptrdiff_t dft_batchsize_;                      //< batch size for DFT integration
  double dft_thresh_;                            //< density threshold for DFT inteegration

  double hartree_;
  double exchange_;

  static constexpr ptrdiff_t nsh_ = 2; //< number of spin channels

 public:
  UFockBuilder(const Geometry& g, const double hartree = 1.0, const double exchange = 1.0);
  UFockBuilder(const Geometry& g, std::shared_ptr<DensityFunctional> dft,
               std::shared_ptr<MolecularGrid> molgrid);

  double Exc() const { return Exc_; }
  std::shared_ptr<qleve::Tensor<3>> density() const { return density_; }

  void VHF_jk(const qleve::ConstTensorView<3>& coeffs, const qleve::ConstTensorView<2>& occs,
              const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock);
  void VHF_jk_direct(const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock);
  void VHF_jk_df(const qleve::ConstTensorView<3>& coeffs, const qleve::ConstTensorView<2>& occs,
                 qleve::TensorView<3> fock);

  void operator()(const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock);
  qleve::Tensor<3> operator()(const qleve::ConstTensorView<3>& rho);

  /// Most general operator that can work based on coeffs, or density matrix
  /// Will dispatch to density fitting or integral direct based on whether it DF data
  void operator()(const qleve::ConstTensorView<3>& coeffs, const qleve::ConstTensorView<2>& occs,
                  const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock);

  // Fock build based on density fitting
  void operator()(const qleve::ConstTensorView<2>& coeffs, const qleve::ConstTensorView<1>& occs,
                  qleve::TensorView<2> fock);

#if 0
  // Multi-Fock build dispatch based on availability of density fitting
  void operator()(const qleve::ConstTensorView<3>& coeffs1,
                  const qleve::ConstTensorView<3>& coeffs2, const qleve::ConstTensorView<2>& occs,
                  const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock);
#endif

#if 0
  // Fock build using two sets of orbitals and occupation numbers
  void operator()(const qleve::ConstTensorView<2>& coeffs1,
                  const qleve::ConstTensorView<2>& coeffs2, const qleve::ConstTensorView<1>& occs,
                  qleve::TensorView<2> fock);
#endif

#if 0
  // Multi-Fock build using two sets of orbitals and occupation numbers
  void operator()(const qleve::ConstTensorView<3>& coeffs1,
                  const qleve::ConstTensorView<3>& coeffs2, const qleve::ConstTensorView<2>& occs,
                  qleve::TensorView<3> fock);
#endif
};

} // namespace yucca
