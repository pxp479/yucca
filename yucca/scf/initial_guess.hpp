// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/initial_guess.hpp
///
/// Initial guess functions like atomic_densities
#pragma once

#include <memory>

using namespace std;

//#include <yucca/structure/geometry.hpp>
namespace yucca
{
class Geometry;
}

namespace yucca
{

namespace initial_guess
{
/// Use core Hamiltonian to produce guess orbitals
///
/// returns Tensor<2>(nAO, nMO) of coefficients
qleve::Tensor<2> hcore(const qleve::Tensor<2>& overlap, const qleve::Tensor<2>& hcore);

/// Use atomic densities to produce guess of initial density matrix
///
/// returns Tensor<2>(nAO, nAO) of initial density matrix
qleve::Tensor<2> atomic_densities(const Geometry& geometry);

} // namespace initial_guess

} // namespace yucca
