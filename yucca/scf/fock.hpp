// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/fock.hpp
///
/// Fock matrix builder
#pragma once

#include <yucca/structure/geometry.hpp>

namespace yucca
{

enum class Fock
{
  direct,
  df
};

class DensityFunctional; // fwd <src/dft/density_functional.hpp>
class MolecularGrid;     // fwd <src/dft/molecular_grid.hpp>

class RFockBuilder {
 protected:
  const Geometry& geometry_;

  // DFT specific quantities
  const std::shared_ptr<DensityFunctional> dft_; //< Select the density functional
  const std::shared_ptr<MolecularGrid> molgrid_; //< select the molecular grid for integration
  double Exc_;                                   //< for DFT, compute Exc while computing Vxc
  std::shared_ptr<qleve::Tensor<2>> density_;    //< for DFT, store last density
  ptrdiff_t dft_batchsize_;                      //< batch size for DFT integration
  double dft_thresh_;                            //< density threshold for DFT inteegration

  double hartree_;
  double exchange_;

 public:
  RFockBuilder(const Geometry& g, const double hartree = 1.0, const double exchange = 1.0);
  RFockBuilder(const Geometry& g, std::shared_ptr<DensityFunctional> dft,
               std::shared_ptr<MolecularGrid> molgrid);

  double Exc() const { return Exc_; }
  std::shared_ptr<qleve::Tensor<2>> density() const { return density_; }

  void VHF_jk(const qleve::ConstTensorView<2>& coeffs, const qleve::ConstTensorView<1>& occs,
              const qleve::ConstTensorView<2>& rho, qleve::TensorView<2> fock);
  void VHF_jk_direct(const qleve::ConstTensorView<2>& rho, qleve::TensorView<2> fock);
  void VHF_jk_df(const qleve::ConstTensorView<2>& coeffs, const qleve::ConstTensorView<1>& occs,
                 qleve::TensorView<2> fock);

  void operator()(const qleve::ConstTensorView<2>& rho, qleve::TensorView<2> fock);
  qleve::Tensor<2> operator()(const qleve::ConstTensorView<2>& rho);

  /// Most general operator that can work based on coeffs, or density matrix
  /// Will dispatch to density fitting or integral direct based on whether it DF data
  void operator()(const qleve::ConstTensorView<2>& coeffs, const qleve::ConstTensorView<1>& occs,
                  const qleve::ConstTensorView<2>& rho, qleve::TensorView<2> fock);

  // Fock build based on density fitting
  void operator()(const qleve::ConstTensorView<2>& coeffs, const qleve::ConstTensorView<1>& occs,
                  qleve::TensorView<2> fock);

  // Multi-Fock build dispatch based on availability of density fitting
  void operator()(const qleve::ConstTensorView<3>& coeffs1,
                  const qleve::ConstTensorView<3>& coeffs2, const qleve::ConstTensorView<2>& occs,
                  const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock);

  // Fock build using two sets of orbitals and occupation numbers
  void operator()(const qleve::ConstTensorView<2>& coeffs1,
                  const qleve::ConstTensorView<2>& coeffs2, const qleve::ConstTensorView<1>& occs,
                  qleve::TensorView<2> fock);

  // Multi-Fock build using two sets of orbitals and occupation numbers
  void operator()(const qleve::ConstTensorView<3>& coeffs1,
                  const qleve::ConstTensorView<3>& coeffs2, const qleve::ConstTensorView<2>& occs,
                  qleve::TensorView<3> fock);

  // Fock build for resHF
  void operator()(const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock);

  // Matadj-ResHF Routine for 2e- K_AB_vo block terms
  void operator()(const qleve::ConstTensorView<1>& xi_AB_i,
                  const qleve::ConstTensorView<2>& xi_AB_ij,
                  const qleve::ConstTensorView<2>& s_AvBo, const qleve::ConstTensorView<2>& AU_occ,
                  const qleve::ConstTensorView<2>& AU_vir, const qleve::ConstTensorView<2>& BV_occ,
                  qleve::TensorView<2> K_AB, const bool print_kab = false);
};

/// Fock builder in which only the upper triangle is ever read or updated
class RSymmetricFockBuilder {
 protected:
  const Geometry& geometry_;

  double hartree_;
  double exchange_;

 public:
  RSymmetricFockBuilder(const Geometry& g, const double hartree, const double exchange);

  void operator()(const qleve::ConstTensorView<2>& rho, qleve::TensorView<2> fock);
  qleve::Tensor<2> operator()(const qleve::ConstTensorView<2>& rho);
};

} // namespace yucca
