// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/uscf.cpp
///
/// Unrestricted SCF driver class
#include <fmt/core.h>

#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/orthogonalize.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

#include <yucca/dft/density_functional.hpp>
#include <yucca/dft/integrate_functional.hpp>
#include <yucca/dft/molecular_grid.hpp>
#include <yucca/input/input_node.hpp>
#include <yucca/scf/initial_guess.hpp>
#include <yucca/scf/uconverger.hpp>
#include <yucca/scf/ufock.hpp>
#include <yucca/scf/uscf.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/util/timer.hpp>
#include <yucca/wfn/single_reference.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

yucca::USCF::USCF(const USCF& x) : Method_(x)
{
  this->nmo_ = x.nmo_;
  this->nao_ = x.nao_;
  this->nelec_ = x.nelec_;
  this->nocc_ = x.nocc_;
  this->charge_ = x.charge_;
  this->spin_ = x.spin_;

  this->overlap_ = std::make_shared<qleve::Tensor<2>>(*x.overlap_);
  this->hcore_ = std::make_shared<qleve::Tensor<2>>(*x.hcore_);
  this->coeffs_ = std::make_shared<qleve::Tensor<3>>(*x.coeffs_);
  this->rho_ = std::make_shared<qleve::Tensor<3>>(*x.rho_);
  this->fock_ = std::make_shared<qleve::Tensor<3>>(*x.fock_);
  this->eigs_ = std::make_shared<qleve::Tensor<2>>(*x.eigs_);

  this->max_iter_ = x.max_iter_;
  this->conv_energy_ = x.conv_energy_;
  this->conv_error_ = x.conv_error_;
  this->conv_density_ = x.conv_density_;
}


yucca::USCF::USCF(const InputNode& input, const shared_ptr<Geometry>& g) : Method_(g)
{
  fmt::print("Setting up SCF calculation using Unrestricted Orbitals\n");

  max_iter_ = input.get<ptrdiff_t>("max_iter", 30);

  charge_ = input.get<ptrdiff_t>("charge", 0);
  const auto nelec = geometry_->total_nuclear_charge() - charge_;

  if (input.contains("spin")) {
    spin_ = input.get<ptrdiff_t>("spin");
    // a + b = n, a - b = s
    // a = (n + s)/2, b = n - a
    nelec_[0] = (nelec + spin_) / 2;
    nelec_[1] = (nelec - spin_) / 2;

    if (nelec_[0] < 0 || nelec_[1] < 0 || (nelec_[0] + nelec_[1]) != nelec) {
      throw runtime_error(
          fmt::format("The specified combination of spin and charge is impossible"));
    }
  } else { // default to low spin TODO revisit
    const ptrdiff_t ndouble = nelec / 2;
    const ptrdiff_t na = nelec - ndouble;
    const ptrdiff_t nb = nelec - na;
    spin_ = na - nb;
    fmt::print("No spin specified, using low-spin guess.\n");
    nelec_[0] = na;
    nelec_[1] = nb;
  }
  nocc_ = max(nelec_[0], nelec_[1]);
  nao_ = geometry_->orbital_basis()->nbasis();

  // this is an important block: set it up rigorously externally somewhere
  string conv_key = input.get_if_value<string>("convergence", "default");
  if (conv_key == "loose") {
    conv_energy_ = 1e-6;
    conv_error_ = 1e-3;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "default") {
    conv_energy_ = 1e-7;
    conv_error_ = 1e-5;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "tight") {
    conv_energy_ = 1e-8;
    conv_error_ = 1e-6;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "ultratight") {
    conv_energy_ = 1e-9;
    conv_error_ = 1e-7;
    conv_density_ = 1e10; // TODO
  }

  // allow specific overrides
  conv_energy_ = input.get<double>("conv_energy", conv_energy_);
  conv_error_ = input.get<double>("conv_error", conv_error_);
  conv_density_ = input.get<double>("conv_density", conv_density_);

  fmt::print("Convergence criteria:\n");
  fmt::print("  - change in energy (dE) < {:1e}\n", conv_energy_);
  fmt::print("  - gradient norm (grad) < {:1e}\n", conv_error_);
  fmt::print("  - change in density (drho) < {:1e}\n", conv_density_);
  fmt::print("\n");

  auto dft = input.get_node_if("dft");
  if (dft) {
    dft_ = make_shared<DensityFunctional>(*dft, 1);
    molgrid_ = make_shared<MolecularGrid>(*dft, geometry_->atoms());
    density_ =
        make_shared<qleve::Tensor<3>>(molgrid_->weights().size(), dft_->nxc_operations(), nsh_);

    dft_batch_ = dft->get<ptrdiff_t>("batch size", 512);
    dft_thresh_ = dft->get<double>("density threshold", 1e-7);

    dft_->print_header();
  }

  fmt::print("Electronic configuration:\n");
  fmt::print("  - nalpha: {:d}, nbeta: {:d}\n", nelec_[0], nelec_[1]);

  guess_ = input.get<string>("guess", "atomic");

  converger_ = make_unique<UHFConverger>(input, nelec_);
}

yucca::USCF::USCF(const InputNode& input, const shared_ptr<Wfn>& wfn) : USCF(input, wfn->geometry())
{
  const shared_ptr<SingleReference> restricted_sref = dynamic_pointer_cast<SingleReference>(wfn);

  if (restricted_sref) {
    const auto& co = *restricted_sref->coeffs();
    nmo_ = co.extent(1);
    coeffs_ = make_shared<Tensor<3>>(nao_, nmo_, nsh_);
    coeffs_->pluck(0) = co;
    coeffs_->pluck(1) = co;

    rho_ = make_shared<qleve::Tensor<3>>(nao_, nao_, nsh_);

    generate_rho(*coeffs_, nelec_, *rho_);
  }
}

void yucca::USCF::generate_rho(const qleve::ConstTensorView<3>& coeffs,
                               const array<ptrdiff_t, 2>& noccs, qleve::TensorView<3> rho) const
{
  for (ptrdiff_t isp = 0; isp < nsh_; ++isp) {
    qleve::ConstTensorView<2> Cocc = coeffs.const_pluck(isp).const_slice(0, noccs[isp]);
    qleve::gemm("n", "t", 1.0, Cocc, Cocc, 0.0, rho.pluck(isp));
  }
}

void yucca::USCF::compute()
{
  Timer timer("USCF");

  // Set up required quantities
  if (!hcore_)
    hcore_ = make_shared<qleve::Tensor<2>>(geometry_->hcore()); // hcore in AO
  if (!overlap_)
    overlap_ = make_shared<qleve::Tensor<2>>(geometry_->overlap()); // overlap in AO

  if (!rho_) {
    timer.mark();
    if (coeffs_) {
      fmt::print("Generating initial guess from provided orbitals\n");

      nmo_ = coeffs_->extent(1);

      rho_ = make_shared<qleve::Tensor<3>>(nao_, nao_, nsh_);
      generate_rho(*coeffs_, nelec_, *rho_);
    } else if (guess_ == "hcore") {
      fmt::print("Generating initial guess from hcore\n");
      auto core_guess = initial_guess::hcore(*overlap_, *hcore_);
      nmo_ = core_guess.extent(1);
      coeffs_ = make_shared<qleve::Tensor<3>>(nao_, nmo_, nsh_);

      coeffs_->pluck(0) = core_guess;
      coeffs_->pluck(1) = core_guess;

      rho_ = make_shared<qleve::Tensor<3>>(nao_, nao_, nsh_);
      generate_rho(*coeffs_, nelec_, *rho_);
    } else if (guess_ == "atomic") {
      fmt::print("Generating initial guess from sum of atomic densities\n");

      auto atom_guess = initial_guess::atomic_densities(*geometry_);
      auto co = qleve::linalg::orthogonalize_metric(*overlap_, 1.0e-10);
      nmo_ = co.extent(1);

      // purify rho_ and replace coeffs_ TODO: separate this as a function?
      auto SC = qleve::gemm("n", "n", 1.0, *overlap_, co);
      qleve::Tensor<2> rhomo(nmo_, nmo_);
      // multiply by -1 so that the largest eigenvalues are sorted first
      qleve::matrix_transform(-1.0, atom_guess, SC, 0.0, rhomo);
      auto rhoeigs = qleve::linalg::diagonalize(rhomo);

      qleve::gemm("n", "n", 1.0, co, rhomo, 0.0, SC);
      coeffs_ = make_shared<qleve::Tensor<3>>(nao_, nmo_, nsh_);
      coeffs_->pluck(0) = SC;
      coeffs_->pluck(1) = SC;

      rho_ = make_shared<qleve::Tensor<3>>(nao_, nao_, nsh_);
      generate_rho(*coeffs_, nelec_, *rho_);
    }

    const double na = qleve::dot_product(nao_ * nao_, &rho_->at(0, 0, 0), overlap_->data());
    const double nb = qleve::dot_product(nao_ * nao_, &rho_->at(0, 0, 1), overlap_->data());
    fmt::print("Initial density: {:12.4f} alpha electrons, {:12.4f} beta electrons\n", na, nb);
    timer.tick_print("initial guess");
  }

  fmt::print("  - nmo: {:d}\n\n", nmo_);

  fmt::print("Starting USCF calculation\n");

  const double nuclear_repulsion = geometry_->nuclear_repulsion();

  if (!fock_builder_) {
    fock_builder_ =
        make_unique<UFockBuilder>(*geometry_, 1.0, dft_ ? dft_->exchange_factor() : 1.0);
  }

  double last_energy;
  bool conv = false;

  fmt::print("{:>4s} {:>16s} {:>16s} {:>16s} {:>10s} {:>10s} {:>8s}\n", "iter", "1e", "2e",
             "energy", "error", "dE", "time");
  fmt::print("{:-^90s}\n", "");

  qleve::Tensor<3> fock(nao_, nao_, nsh_);
  const ptrdiff_t maxocc = std::max(nelec_[0], nelec_[1]);
  qleve::Tensor<2> occs(maxocc, nsh_);

  occs.pluck(0).slice(0, nelec_[0]) = 1.0;
  occs.pluck(1).slice(0, nelec_[1]) = 1.0;

  qleve::Tensor<2> totaldensity(nao_, nao_);
  timer.mark();

  for (size_t iscf = 0; iscf < max_iter_; ++iscf) {
    fock.pluck(0) = *hcore_;
    fock.pluck(1) = *hcore_;
    (*fock_builder_)(*coeffs_, occs, *rho_, fock);

    double focktime = timer.tick("fock build");

    const auto focka = fock.pluck(0);
    const auto fockb = fock.pluck(1);

    const auto rhoa = rho_->pluck(0);
    const auto rhob = rho_->pluck(1);

    totaldensity = rhoa + rhob;

    // compute energies
    const double energy0a =
        qleve::dot_product(rhoa, focka); // zeroth-order energy (sum of eigenvalues)
    const double energy0b =
        qleve::dot_product(rhob, fockb); // zeroth-order energy (sum of eigenvalues)
    const double energy0 = energy0a + energy0b;
    const double energy1e = qleve::dot_product(totaldensity, *hcore_);
    const double energy2e = 0.5 * (energy0 - energy1e);

    energy_ = 0.5 * (energy1e + energy0) + nuclear_repulsion;
#if 0
    if (dft_) {
      auto [Exc, Vxc] = dft::integrate_exc_vxc(*geometry_->orbital_basis(), *molgrid_, *dft_, *rho_,
                                               *density_, dft_batch_, dft_thresh_);

      double vxctime = timer.tick("Vxc build");
      focktime += vxctime;

      energy_ += Exc;
      fock += Vxc;
    }
#endif

    auto err = fock.zeros_like();
    auto tmp = fock.pluck(0).zeros_like();
    for (ptrdiff_t isp = 0; isp < nsh_; ++isp) {
      auto f = fock.pluck(isp);
      auto r = rho_->pluck(isp);
      auto e = err.pluck(isp);

      qleve::gemm("n", "n", 1.0, f, r, 0.0, tmp);
      qleve::gemm("n", "n", 1.0, tmp, *overlap_, 0.0, e);
      e = e - e.transpose();
    }
    const double error_norm = err.norm();

    converger_->push(fock, err, energy_);

    const double dE = converger_->dE();
    last_energy = energy_;

    // print results
    fmt::print("{:4d} {:16.8f} {:16.8f} {:16.8f} {:10.1e} ", iscf, energy1e, energy2e, energy_,
               error_norm);
    if (iscf != 0) {
      fmt::print("{:10.2e}", dE);
    } else {
      fmt::print("{:>10s}", "---");
    }
    fmt::print(" {:8.3f}\n", focktime);

    conv = converged(dE, error_norm, 0.0);

    if (conv) {
      break;
    }

    converger_->next(*coeffs_, *rho_);
  }


  if (conv) {
    fmt::print("\n\n  Converged!\n");
    fmt::print("Final energy: {:20.8f}\n", energy_);
    fock_ = make_shared<qleve::Tensor<3>>(nmo_, nmo_, nsh_);
    for (ptrdiff_t isp = 0; isp < nsh_; ++isp) {
      auto f = fock.const_pluck(isp);
      auto c = coeffs_->const_pluck(isp);
      auto ff = fock_->pluck(isp);
      qleve::matrix_transform(1.0, f, c, 0.0, ff);
    }
    eigs_ = make_shared<qleve::Tensor<2>>(nmo_, nsh_);
    for (ptrdiff_t isp = 0; isp < nsh_; ++isp) {
      for (ptrdiff_t i = 0; i < nmo_; ++i) {
        eigs_->operator()(i, isp) = fock_->operator()(i, i, isp);
      }
    }
  } else {
    throw runtime_error("Failed to converge");
  }

#if 0
  if (dft_) {
    const double dens = qleve::dot_product(density_->slice(0), molgrid_->weights());
    fmt::print("integrated density: {:f}\n", dens);
  }
#endif

  fmt::print("Computing multipole moments\n");
  timer.mark();
  auto multipole = geometry_->multipole(2);
  qleve::Tensor<1> moments(multipole.extent(2));
  qleve::contract(-1.0, multipole, "uvp", totaldensity, "uv", 0.0, moments, "p");
  timer.tick_print("multipole moments");

  auto nuclear_moments = geometry_->nuclear_multipole(2);
  auto total_moments = nuclear_moments.clone();
  total_moments += moments;

  const double spin_squared = compute_spin_squared(coeffs_->pluck(0).slice(0, nelec_[0]),
                                                   coeffs_->pluck(1).slice(0, nelec_[1]));

  fmt::print("<S^2> = {:18.8f}\n", spin_squared);

  fmt::print("Electronic Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", moments(1), moments(2),
             moments(3));
  fmt::print("   Nuclear Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", nuclear_moments(1),
             nuclear_moments(2), nuclear_moments(3));
  fmt::print("     Total Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", total_moments(1),
             total_moments(2), total_moments(3));
  fmt::print("\n");
  fmt::print("Electronic Quadrupole (xx, xy, xz, yy, yz, zz): {:18.8f} {:18.8f} {:18.8f} "
             "{:18.8f} {:18.8f} {:18.8f}\n",
             moments(4), moments(5), moments(6), moments(7), moments(8), moments(9));
  fmt::print("\n");

  timer.summarize();
}

void USCF::compute_gradient_impl()
{
  gradient_ = make_unique<qleve::Tensor<2>>(3, geometry_->natoms());
}

bool USCF::converged(const double dE, const double error, const double drho) const
{
  bool conv = (abs(dE) < conv_energy_) && (abs(error) < conv_error_) && (abs(drho) < conv_density_);
  return conv;
}

shared_ptr<Wfn> USCF::wavefunction() { return nullptr; }

double USCF::compute_spin_squared(const qleve::ConstTensorView<2>& alpha,
                                  const qleve::ConstTensorView<2>& beta) const
{
  const auto nocca = alpha.extent(1);
  const auto noccb = beta.extent(1);
  const auto nao = alpha.extent(0);

  assert(nao == beta.extent(0));

  auto SCa = qleve::gemm("n", "n", 1.0, *overlap_, alpha);
  auto delta = qleve::gemm("t", "n", 1.0, beta, SCa);

  const double dd = qleve::dot_product(delta, delta);

  const double n2 = 0.5 * (nocca + noccb);

  const double ss = n2 * (n2 + 1.0) - nocca * noccb - dd;

  return ss;
}

