// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/ufock.cpp
///
/// Fock matrix builder for unrestricted Hartree-Fock
#include <tbb/enumerable_thread_specific.h>
#include <tbb/tbb.h>

#include <qleve/tensor_contract.hpp>

#include <yucca/dft/density_functional.hpp>
#include <yucca/dft/integrate_functional.hpp>
#include <yucca/dft/molecular_grid.hpp>
#include <yucca/scf/ufock.hpp>
#include <yucca/structure/density_fitting.hpp>
#include <yucca/util/libint_interface.hpp>

using namespace std;
using namespace yucca;

UFockBuilder::UFockBuilder(const Geometry& g, const double hartree, const double exchange) :
    geometry_(g), hartree_(hartree), exchange_(exchange), dft_batchsize_(100), dft_thresh_(1e-12)
{}

UFockBuilder::UFockBuilder(const Geometry& g, const shared_ptr<DensityFunctional> dft,
                           const shared_ptr<MolecularGrid> molgrid) :
    geometry_(g), dft_(dft), molgrid_(molgrid), dft_batchsize_(100), dft_thresh_(1e-12)
{
  hartree_ = 1.0;
  exchange_ = dft_ ? dft_->exchange_factor() : 1.0;

  density_ =
      make_shared<qleve::Tensor<3>>(molgrid_->weights().size(), dft_->nxc_operations(), nsh_);
}

// dispatch based on availability of density fitting
void UFockBuilder::VHF_jk(const qleve::ConstTensorView<3>& coeffs,
                          const qleve::ConstTensorView<2>& occs,
                          const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock)
{
  if (geometry_.density_fitting()) {
    this->VHF_jk_df(coeffs, occs, fock);
  } else {
    this->VHF_jk_direct(rho, fock);
  }
}

// dispatch based on availability of density fitting
void UFockBuilder::operator()(const qleve::ConstTensorView<3>& coeffs,
                              const qleve::ConstTensorView<2>& occs,
                              const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock)
{

  VHF_jk(coeffs, occs, rho, fock);
#if 0
  if (dft_) {
    auto [Exc, Vxc] = dft::integrate_exc_vxc(*geometry_.orbital_basis(), *molgrid_, *dft_, rho,
                                             *density_, dft_batchsize_, dft_thresh_);
    fock += 0.25 * Vxc;
  }
#endif
}

#if 0
// dispatch multicoeff version based on availability of density fitting
void UFockBuilder::operator()(const qleve::ConstTensorView<3>& coeffs1,
                              const qleve::ConstTensorView<3>& coeffs2,
                              const qleve::ConstTensorView<2>& occs,
                              const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock)
{
  if (geometry_.density_fitting()) {
    this->operator()(coeffs1, coeffs2, occs, fock);
  } else {
    this->operator()(rho, fock);
  }
}
#endif

#if 1
// version hardwired to run through integral direct
void UFockBuilder::operator()(const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock)
{
  assert(!dft_);
  VHF_jk_direct(rho, fock);
}
#endif

void UFockBuilder::VHF_jk_direct(const qleve::ConstTensorView<3>& rho, qleve::TensorView<3> fock)
{
  const auto shells = libint::get_shells(*geometry_.orbital_basis());
  const auto& bshells = geometry_.orbital_basis()->basisshells();

  const ptrdiff_t nshells = shells.size();

  std::mutex fock_lock;

  tbb::enumerable_thread_specific<libint2::Engine> engines([&]() {
    return libint::get_engine(libint2::Operator::coulomb, 0, *geometry_.orbital_basis());
  });

  auto kernel = [&](ptrdiff_t ish) {
    auto& engine = engines.local();
    const auto& buf = engine.results();
    const ptrdiff_t ioff = bshells[ish].offset();
    const ptrdiff_t isize = shells[ish].size();
    for (ptrdiff_t jsh = 0; jsh <= ish; ++jsh) {
      const ptrdiff_t joff = bshells[jsh].offset();
      const ptrdiff_t jsize = shells[jsh].size();
      const ptrdiff_t ijsh = ish * (ish + 1) / 2 + jsh;
      for (ptrdiff_t ksh = 0; ksh <= ish; ++ksh) {
        const ptrdiff_t koff = bshells[ksh].offset();
        const ptrdiff_t ksize = shells[ksh].size();
        for (ptrdiff_t lsh = 0; lsh <= ksh && (ksh * (ksh + 1) / 2 + lsh) <= ijsh; ++lsh) {
          const ptrdiff_t loff = bshells[lsh].offset();
          const ptrdiff_t lsize = shells[lsh].size();
          const ptrdiff_t klsh = ksh * (ksh + 1) / 2 + lsh;

          engine.compute2<libint2::Operator::coulomb, libint2::BraKet::xx_xx, 0>(
              shells[ish], shells[jsh], shells[ksh], shells[lsh]);
          const double* vijkl = buf[0];

          if (vijkl == nullptr)
            continue;

          const double ijfac = ish == jsh ? 0.5 : 1.0;
          const double klfac = lsh == ksh ? 0.5 : 1.0;
          const double ijklfac = ijsh == klsh ? 0.5 : 1.0;

          const double symfac = ijfac * klfac * ijklfac;
          const double jj = hartree_ * symfac;
          const double kk = -exchange_ * symfac;

          {
            std::lock_guard<mutex> lk(fock_lock);
            for (size_t i = ioff, ijkl = 0; i < ioff + isize; ++i) {
              for (size_t j = joff; j < joff + jsize; ++j) {
                for (size_t k = koff; k < koff + ksize; ++k) {
                  for (size_t l = loff; l < loff + lsize; ++l, ++ijkl) {
                    // coulomb contribution
                    const double Jrhokl = rho(k, l, 0) + rho(l, k, 0) + rho(k, l, 1) + rho(l, k, 1);
                    const double Jrhoij = rho(i, j, 0) + rho(j, i, 0) + rho(i, j, 1) + rho(j, i, 1);

                    fock(i, j, 0) += jj * vijkl[ijkl] * Jrhokl;
                    fock(j, i, 0) += jj * vijkl[ijkl] * Jrhokl;
                    fock(k, l, 0) += jj * vijkl[ijkl] * Jrhoij;
                    fock(l, k, 0) += jj * vijkl[ijkl] * Jrhoij;

                    fock(i, j, 1) += jj * vijkl[ijkl] * Jrhokl;
                    fock(j, i, 1) += jj * vijkl[ijkl] * Jrhokl;
                    fock(k, l, 1) += jj * vijkl[ijkl] * Jrhoij;
                    fock(l, k, 1) += jj * vijkl[ijkl] * Jrhoij;

                    // exchange contribution
                    fock(i, k, 0) += kk * vijkl[ijkl] * rho(j, l, 0);
                    fock(i, l, 0) += kk * vijkl[ijkl] * rho(j, k, 0);
                    fock(j, k, 0) += kk * vijkl[ijkl] * rho(i, l, 0);
                    fock(j, l, 0) += kk * vijkl[ijkl] * rho(i, k, 0);
                    fock(k, i, 0) += kk * vijkl[ijkl] * rho(l, j, 0);
                    fock(k, j, 0) += kk * vijkl[ijkl] * rho(l, i, 0);
                    fock(l, i, 0) += kk * vijkl[ijkl] * rho(k, j, 0);
                    fock(l, j, 0) += kk * vijkl[ijkl] * rho(k, i, 0);

                    fock(i, k, 1) += kk * vijkl[ijkl] * rho(j, l, 1);
                    fock(i, l, 1) += kk * vijkl[ijkl] * rho(j, k, 1);
                    fock(j, k, 1) += kk * vijkl[ijkl] * rho(i, l, 1);
                    fock(j, l, 1) += kk * vijkl[ijkl] * rho(i, k, 1);
                    fock(k, i, 1) += kk * vijkl[ijkl] * rho(l, j, 1);
                    fock(k, j, 1) += kk * vijkl[ijkl] * rho(l, i, 1);
                    fock(l, i, 1) += kk * vijkl[ijkl] * rho(k, j, 1);
                    fock(l, j, 1) += kk * vijkl[ijkl] * rho(k, i, 1);
                  }
                }
              }
            }
          }
        }
      }
    }
  };

  tbb::parallel_for(ptrdiff_t(0), nshells, kernel);
}

void UFockBuilder::VHF_jk_df(const qleve::ConstTensorView<3>& coeffs,
                             const qleve::ConstTensorView<2>& occs, qleve::TensorView<3> fock)
{
  assert(geometry_.density_fitting());

  const auto& B = geometry_.density_fitting()->B(); // (uv|Q)(Q|P)^(-1/2)
  const ptrdiff_t nao = coeffs.extent(0);
  const ptrdiff_t nfit = B.extent(2);

  const ptrdiff_t nocc = occs.extent(0);

  // nocc is the maximum number of occupied orbitals in both alpha and beta
  // nocca/noccb are the number of occupied orbitals in alpha/beta
  // compute nocca/noccb by finding how many entries in occs are 1.0
  ptrdiff_t nocca = 0;
  ptrdiff_t noccb = 0;
  for (ptrdiff_t i = 0; i < nocc; ++i) {
    if (std::abs(occs(i, 0) - 1.0) < 1e-12) { // TODO isclose()
      ++nocca;
    }
    if (std::abs(occs(i, 1) - 1.0) < 1e-12) {
      ++noccb;
    }
  }

  assert(B.extent(0) == nao && B.extent(1) == nao);

  const double jfac = hartree_;
  const double kfac = -exchange_;

  const auto coa = coeffs.const_pluck(0).const_slice(0, nocca);
  const auto cob = coeffs.const_pluck(1).const_slice(0, noccb);

  auto focka = fock.pluck(0);
  auto fockb = fock.pluck(1);

  qleve::Tensor<3> halfa(nao, nocca, nfit);
  qleve::Tensor<3> halfb(nao, noccb, nfit);

  // half transform alpha
  qleve::contract(1.0, B, "uvP", coa, "vi", 0.0, halfa, "uiP");
  qleve::contract(1.0, B, "uvP", cob, "vi", 0.0, halfb, "uiP");

  if (exchange_ != 0.0) {
    // build K with half transform
    qleve::contract(kfac, halfa, "uiP", halfa, "viP", 1.0, focka, "uv");
    qleve::contract(kfac, halfb, "uiP", halfb, "viP", 1.0, fockb, "uv");
  }

  qleve::Tensor<1> full(nfit);
  // second half transform
  qleve::contract(1.0, halfa, "uiP", coa, "ui", 0.0, full, "P");
  qleve::contract(1.0, halfb, "uiP", cob, "ui", 1.0, full, "P");

  // build J with full transform
  qleve::contract(jfac, full, "P", B, "uvP", 1.0, focka, "uv");
  qleve::contract(jfac, full, "P", B, "uvP", 1.0, fockb, "uv");
}

#if 0
// fock build with two different sets of MOs and occupation numbers
void RFockBuilder::operator()(const qleve::ConstTensorView<2>& coeffs1,
                              const qleve::ConstTensorView<2>& coeffs2,
                              const qleve::ConstTensorView<1>& occs, qleve::TensorView<2> fock)
{
  assert(geometry_.density_fitting());
  assert(!dft_);
  assert(coeffs1.shape() == coeffs2.shape());
  assert(std::all_of(occs.data(), occs.data() + occs.size(),
                     [](const double x) { return (x >= 0.0); }));

  const auto& B = geometry_.density_fitting()->B(); // (uv|Q)(Q|P)^(-1/2)
  const ptrdiff_t nocc = occs.size();
  const ptrdiff_t nao = coeffs1.extent(0);
  const ptrdiff_t nfit = B.extent(2);

  assert(coeffs1.extent(1) == nocc);
  assert(B.extent(0) == nao && B.extent(1) == nao);

  const double jfac = hartree_ * 2.0;
  const double kfac = -exchange_;

  qleve::Tensor<2> c1(coeffs1);
  qleve::Tensor<2> c2(coeffs2);
  for (ptrdiff_t i = 0; i < nocc; ++i) {
    c1.slice(i) *= std::sqrt(occs(i));
    c2.slice(i) *= std::sqrt(occs(i));
  }

  qleve::Tensor<3> half1(nao, nocc, nfit);
  qleve::contract(1.0, B, "uvP", c1, "vi", 0.0, half1, "uiP");

  qleve::Tensor<3> half2(nao, nocc, nfit);
  qleve::contract(1.0, B, "uvP", c2, "vi", 0.0, half2, "uiP");

  // build K with half transform
  qleve::contract(kfac, half1, "uiP", half2, "viP", 1.0, fock, "uv");

  qleve::Tensor<1> full(nfit);
  // second half transform
  qleve::contract(1.0, half1, "uiP", c2, "ui", 0.0, full, "P");
  // build J with full transform
  qleve::contract(jfac, full, "P", B, "uvP", 1.0, fock, "uv");
}
#endif

#if 0
// many fock builds with distinct sets of MOs and occupation numbers
void RFockBuilder::operator()(const qleve::ConstTensorView<3>& coeffs1,
                              const qleve::ConstTensorView<3>& coeffs2,
                              const qleve::ConstTensorView<2>& occs, qleve::TensorView<3> fock)
{
  assert(!dft_);
  const ptrdiff_t nfock = fock.extent(2);
  assert(nfock == coeffs1.extent(2) && nfock == coeffs2.extent(2) && nfock == occs.extent(1));

  for (ptrdiff_t i = 0; i < nfock; ++i) {
    this->operator()(coeffs1.const_slice(i), coeffs2.const_slice(i), occs.const_slice(i),
                     fock.slice(i));
  }
}
#endif

qleve::Tensor<3> UFockBuilder::operator()(const qleve::ConstTensorView<3>& rho)
{
  auto fock = rho.zeros_like();
  (*this)(rho, fock);
  return fock;
}

