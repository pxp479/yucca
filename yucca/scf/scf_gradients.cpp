// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/scf_gradients.cpp
///
/// \brief prepare the quantities for SCF gradients
#include <iostream>

#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

#include <yucca/grad/gradients.hpp>
#include <yucca/scf/scf.hpp>
#include <yucca/structure/density_fitting.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

void yucca::SCF::compute_gradient_impl()
{
  const auto natoms = geometry_->natoms();
  gradient_ = make_unique<Tensor<2>>(3, natoms);

  yucca::gradient::nuclear_repulsion(geometry_, *gradient_);

  qleve::Tensor<2> P(2.0 * *rho_);
  qleve::Tensor<2> W = P.zeros_like();
  qleve::TensorView<1> e_occ(eigs_->data(), nocc_);
  qleve::weighted_gemm("n", "t", -2.0, coeffs_->slice(0, nocc_), e_occ, coeffs_->slice(0, nocc_),
                       0.0, W);

  gradient::hcore(geometry_, P, W, *gradient_);

  qleve::Tensor<1> occs(nocc_);
  occs = 1.0;

  compute_gradient_2e(coeffs_->slice(0, nocc_), occs, P, *gradient_);

  gradient::print(geometry_, *gradient_);
}

void yucca::SCF::compute_gradient_2e(const qleve::ConstTensorView<2>& coeffs,
                                     const qleve::ConstTensorView<1>& occs,
                                     const qleve::ConstTensorView<2>& P, qleve::TensorView<2> ddx)
{
  if (geometry_->density_fitting()) {
    compute_grad_2e_df(coeffs, occs, ddx);
  } else {
    compute_grad_2e_direct(P, ddx);
  }
}

void yucca::SCF::compute_grad_2e_direct(const qleve::ConstTensorView<2>& P,
                                        qleve::TensorView<2> ddx)
{
  const double coulomb = 0.5;
  const double exchange = -0.25;

  auto Gijkl = [&](const ptrdiff_t& ish, const ptrdiff_t& ioff, const ptrdiff_t& isize,
                   const ptrdiff_t& jsh, const ptrdiff_t& joff, const ptrdiff_t& jsize,
                   const ptrdiff_t& ksh, const ptrdiff_t& koff, const ptrdiff_t& ksize,
                   const ptrdiff_t& lsh, const ptrdiff_t& loff, const ptrdiff_t& lsize,
                   double* gamma) {
    const double ijfac = (ish == jsh) ? 0.5 : 1.0;
    const double klfac = (ksh == lsh) ? 0.5 : 1.0;
    const double ijklfac = ((jsh * (jsh + 1) / 2 + ish) == (lsh * (lsh + 1) / 2 + ksh)) ? 0.5 : 1.0;
    const double symfac = ijfac * klfac * ijklfac;

    const double coulomb_fac = coulomb * symfac;
    const double exchange_fac = exchange * symfac;

    for (ptrdiff_t i = ioff, ijkl = 0; i < ioff + isize; ++i) {
      for (ptrdiff_t j = joff; j < joff + jsize; ++j) {
        for (ptrdiff_t k = koff; k < koff + ksize; ++k) {
          for (ptrdiff_t l = loff; l < loff + lsize; ++l, ++ijkl) {

            gamma[ijkl] = coulomb_fac * 2.0 * (P(i, j) + P(j, i)) * (P(k, l) + P(l, k));
            gamma[ijkl] +=
                exchange_fac * 2.0
                * (P(i, l) * P(j, k) + P(k, j) * P(l, i) + P(i, k) * P(j, l) + P(k, i) * P(l, j));
          }
        }
      }
    }
  };

  gradient::contract_2e_4c_impl(geometry_, libint2::Operator::coulomb, Gijkl, ddx);
}

void yucca::SCF::compute_grad_2e_df(const qleve::ConstTensorView<2>& coeffs,
                                    const qleve::ConstTensorView<1>& occs, qleve::TensorView<2> ddx)
{
  const double coulomb = 2.0;
  const double exchange = -1.0;

  const auto& B = geometry_->density_fitting()->B(); // (uv|P) (P|Q)^-1/2
  const ptrdiff_t nocc = occs.size();
  const ptrdiff_t nao = coeffs.extent(0);
  const ptrdiff_t nfit = B.extent(2);

  assert(coeffs.extent(1) == nocc);
  assert(B.extent(0) == nao && B.extent(1) == nao);

  auto BB = geometry_->density_fitting()->compute_B_Jinv();

  // half transform
  qleve::Tensor<3> half(nao, nocc, nfit);
  qleve::contract(1.0, BB, "uvP", coeffs, "vi", 0.0, half, "uiP");

  // full -> [P]
  qleve::Tensor<1> PP(nfit);
  qleve::contract(1.0, half, "uiP", coeffs, "ui", 0.0, PP, "P");

  // coulomb 2c -> [P].T * (P|Q)' * [P]
  auto ppmat = PP.reshape(nfit, 1);
  qleve::Tensor<2> PQ(nfit, nfit);

  // extra factor of -1 comes from derivative of J^{-1}
  qleve::gemm("n", "t", -coulomb, ppmat, ppmat, 0.0, PQ);

  // exchange 2c -> (ij|P) (P|Q)' (Q|ij)
  qleve::Tensor<3> full(nocc, nocc, nfit);
  qleve::contract(1.0, half, "ujP", coeffs, "ui", 0.0, full, "ijP");

  // extra factor of -1 comes from derivative of J^{-1}
  qleve::gemm("t", "n", -exchange, full.reshape(nocc * nocc, nfit), full.reshape(nocc * nocc, nfit),
              1.0, PQ);

  // coulomb 3c -> 2 * c_ui c_vi (uv|P)' [P]
  qleve::Tensor<3> uvP(nao, nao, nfit);

  qleve::Tensor<2> dens(nao, nao);
  qleve::gemm("n", "t", 2.0, coeffs.const_slice(0, nocc), coeffs.const_slice(0, nocc), 0.0, dens);

  auto densvec = dens.reshape(nao * nao, 1);
  qleve::gemm("n", "t", coulomb, densvec, ppmat, 0.0, uvP.reshape(nao * nao, nfit));

  // exchange 3c ->
  // c_ui c_vj (ux|wv)' c_wi c_xj
  // c_ui c_vj (ux|P)'(P|Q)^-1(Q|wv) c_wi c_xj
  // c_ui c_vj (ux|P)'(~P|wv) c_wi c_xj
  // c_ui (ux|P)'(~P|ij) c_xj
  // (ux|P)'C_{~P|~u~x}

  // (~P|ij) c_xj -> half(xiP)
  qleve::contract(2.0, full, "ijP", coeffs, "xi", 0.0, half, "xjP");

  // uvP += c_ui half(xiP)
  qleve::contract(exchange, half, "xiP", coeffs, "ui", 1.0, uvP, "xuP");

  // Gamma2c and Gamma3c are prepared, now contract with gradients
  // gradient += PQ * (P|Q)'
  gradient::contract_2e_2c(geometry_, libint2::Operator::coulomb, PQ, ddx);

  // gradient += uvP * (uv|P)'
  gradient::contract_2e_3c(geometry_, libint2::Operator::coulomb, uvP, ddx);
}
