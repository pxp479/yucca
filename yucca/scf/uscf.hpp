// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/uscf.hpp
///
/// Unrestricted SCF driver class
#pragma once

#include <memory>

#include <yucca/scf/ufock.hpp>
#include <yucca/util/method.hpp>

namespace yucca
{

// use forward declarations here to prevent compilation cascades
class Geometry;          // #include <yucca/structure/geometry.hpp>
class Wfn;               // #include <yucca/wfn/wfn.hpp>
class UHFConverger;      // #include <yucca/scf/uconverger.hpp>
class DensityFunctional; // #include <yucca/dft/density_functional.hpp>
class MolecularGrid;     // #include <yucca/dft/molecular_grid.hpp>

class USCF : public Method_ {
 public:
  static constexpr ptrdiff_t nsh_ = 2;

 protected:
  std::size_t nmo_;
  std::size_t nao_;
  std::array<ptrdiff_t, 2> nelec_;
  ptrdiff_t nocc_; // max(alpha, beta)
  ptrdiff_t charge_;
  ptrdiff_t spin_;

  // density functional
  std::shared_ptr<DensityFunctional> dft_;
  std::shared_ptr<MolecularGrid> molgrid_;
  std::shared_ptr<qleve::Tensor<3>> density_;
  ptrdiff_t dft_batch_;
  ptrdiff_t dft_thresh_;

  // important intermediates
  std::shared_ptr<qleve::Tensor<2>> overlap_;
  std::shared_ptr<qleve::Tensor<2>> hcore_;

  // output
  std::shared_ptr<qleve::Tensor<3>> coeffs_;
  std::shared_ptr<qleve::Tensor<3>> rho_;
  std::shared_ptr<qleve::Tensor<3>> fock_;
  std::shared_ptr<qleve::Tensor<2>> eigs_;
  double energy_;

  // numerical parameters
  int max_iter_;
  double conv_energy_;
  double conv_error_;
  double conv_density_;

  // initial guess
  std::string guess_;

  // fock builder
  std::unique_ptr<UFockBuilder> fock_builder_;

  // convergence helper
  std::unique_ptr<UHFConverger> converger_;

  bool converged(const double dE, const double error, const double drho) const;

 public:
  USCF(const InputNode& input, const std::shared_ptr<Geometry>& g);
  USCF(const InputNode& input, const std::shared_ptr<Wfn>& wfn);

  // copy constructor
  USCF(const USCF& x);

  // access functions
  std::size_t nmo() { return nmo_; }
  std::size_t nao() { return nao_; }
  std::shared_ptr<qleve::Tensor<2>> hcore() { return hcore_; }
  std::shared_ptr<qleve::Tensor<3>> fock() { return fock_; }
  std::shared_ptr<qleve::Tensor<3>> coeffs() { return coeffs_; }

  void compute() override;

  double energy() override { return energy_; }
  std::vector<double> energies() override { return std::vector<double>{energy_}; }

  std::shared_ptr<Wfn> wavefunction() override;

 protected: // helper functions to simplify doing the same thing for alpha and beta constantly
  void generate_rho(const qleve::ConstTensorView<3>& coeffs, const std::array<ptrdiff_t, 2>& noccs,
                    qleve::TensorView<3> rho) const;

  void compute_gradient_impl() override;

  double compute_spin_squared(const qleve::ConstTensorView<2>& alpha,
                              const qleve::ConstTensorView<2>& beta) const;
};

} // namespace yucca
