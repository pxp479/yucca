// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/uconverger.cpp
///
/// Interface file for convergence acceleration like DIIS, UHF

#include <fmt/core.h>

#include <qleve/diagonalize.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/unitary_exp.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/resmf/uconverger.hpp>

using namespace yucca;
using namespace qleve;
using namespace std;

static const bool debug = false;

UResHFConverger::UResHFConverger(const InputNode& inp, const array<ptrdiff_t, 2>& nelec) :
    SCFConverger(), nelec_(nelec)
{
  string conv_key = inp.get_if_value<string>("convergence", "default");
  auto conv = inp.get_node_if("convergence");
  if (conv && conv->is_map()) {
    conv_key = conv->get<string>("thresholds", conv_key);
  }

  diis_max_size_ = 5;
  diis_min_size_ = 1;
  diis_start_ = 2;
  level_shift_ = 0.0;
  orbital_select_ = "mom";

  use_second_order_ = inp.get<bool>("second order", true); // change input to conv?
  sec_order_switch_thresh_ = 5.0e-3;
  sec_order_start_ = 5;

  if (conv_key == "loose") {
    energy_thresh_ = 1.0e-6;
    grad_thresh_ = 1.0e-4;
    density_thresh_ = 1.0e-6;
  } else if (conv_key == "default") {
    energy_thresh_ = 1.0e-8;
    grad_thresh_ = 1.0e-6;
    density_thresh_ = 1.0e-8;
  } else if (conv_key == "tight") {
    energy_thresh_ = 1.0e-10;
    grad_thresh_ = 1.0e-7;
    density_thresh_ = 1.0e-10;
  } else if (conv_key == "ultratight") {
    energy_thresh_ = 1.0e-12;
    grad_thresh_ = 1.0e-9;
    density_thresh_ = 1.0e-12;
  } else {
    throw runtime_error("Unknown convergence option: use loose, default or tight");
  }

  if (auto conv = inp.get_node_if("convergence"); conv && conv->is_map()) {
    energy_thresh_ = conv->get<double>("energy", energy_thresh_);
    grad_thresh_ = conv->get<double>("grad", grad_thresh_);
    density_thresh_ = conv->get<double>("density", density_thresh_);
    diis_max_size_ = conv->get<ptrdiff_t>("diis max", diis_max_size_);
    diis_min_size_ = conv->get<ptrdiff_t>("diis min", diis_min_size_);
    diis_start_ = conv->get<ptrdiff_t>("diis start", diis_start_);
    level_shift_ = conv->get<double>("level shift", level_shift_);
    orbital_select_ = conv->get<string>("orbital select", orbital_select_);

    sec_order_switch_thresh_ = conv->get<double>("second order switch", sec_order_switch_thresh_);
    sec_order_start_ = conv->get<ptrdiff_t>("second order start", sec_order_start_);
    if (conv->contains("second order switch") || conv->contains("second order start")) {
      use_second_order_ = true;
    }
  }

  // transformations
  transform(orbital_select_.begin(), orbital_select_.end(), orbital_select_.begin(), ::tolower);

  // sanity checks
  if (!(orbital_select_ == "aufbau" || orbital_select_ == "mom")) {
    throw runtime_error("Unrecognized orbital selection option: must be aufbau or mom");
  }

  iter_ = 0;

  // report options chosen
  fmt::print("Convergence Options:\n");
  fmt::print("  Thresholds:\n");
  fmt::print("    - energy change: {:6.2g}\n", energy_thresh_);
  // fmt::print("    - density change: {:6.2f}\n", density_thresh_);
  fmt::print("    - orbital gradient: {:6.2g}\n", grad_thresh_);

  fmt::print("\n");
  fmt::print("Convergence Acceleration:\n");
  fmt::print("  DIIS:\n");
  fmt::print("    max size: {:d}\n", diis_max_size_);
  fmt::print("    min size: {:d}\n", diis_min_size_);
  fmt::print("    start: {:d}\n", diis_start_);
  if (level_shift_ != 0.0)
    fmt::print("    level shift: {:6.2g} Hartree\n", level_shift_);
  if (orbital_select_ == "mom")
    fmt::print("    using maximum-overlap-method for orbital selection\n");

  if (use_second_order_) {
    fmt::print("Second order optimization turned on. Switching to second order when:\n");
    fmt::print("  - gradient norm < {:12.6f}\n", sec_order_switch_thresh_);
    fmt::print("  - minimum steps: {:d}\n", sec_order_start_);
  }

  mode_ = OptMode::diagonalize;
}

void UResHFConverger::push(const TensorView<4>& fock, const TensorView<4>& err, double energy)
{
  const ptrdiff_t nn = fock.size();
  assert(nn == fock.size() && nn == err.size());

  this->push_impl(fock.const_reshape(nn), err.const_reshape(nn), energy);
}

void UResHFConverger::next(TensorView<4> next_coeff)
{
  // initialize a copy of the current determinants from first optimization cycle
  if (!coeffs_) {
    coeffs_ = std::make_unique<qleve::Tensor<4>>(next_coeff);
  }

  // pull out info from input
  const auto [nocca, noccb] = nelec_;

  const ptrdiff_t nao = next_coeff.extent(0);
  const ptrdiff_t nmo = next_coeff.extent(1);
  const ptrdiff_t ndet = next_coeff.extent(2); // # of determinants
  const ptrdiff_t nsh = next_coeff.extent(3);  // # of spins (should be 2)

  // determine which optimizing mode to use
  if (mode_ == OptMode::diagonalize && use_second_order_) {
    if (errnrm_.back() < sec_order_switch_thresh_ && iter_ > sec_order_start_) {
      fmt::print("Switching to Second Order optimization.\n");
      mode_ = OptMode::second_order;

      // set reference set of coeffs
      *coeffs_ = next_coeff;
    }
  }

  // take a step to optimize coeffs
  if (mode_ == OptMode::diagonalize) {
    // get processed AO basis fock matrices from DIIS
    Tensor<4> fockao(nao, nao, ndet, nsh);
    diis_->extrapolate(fockao.reshape(fockao.size()));

    // initialize holder for MO fock matrices
    Tensor<4> fock_mo(nmo, nmo, ndet, nsh);

    // iterate through each determinant and modify the respective next_coeffs
    for (ptrdiff_t idet = 0; idet < ndet; idet++) {
      // transform from fock matrices from AO to MO basis
      // note from Shane: this will break if next_coeff are not current good coeffs
      matrix_transform(1.0, fockao.const_pluck(idet, 0), next_coeff.const_pluck(idet, 0), 0.0,
                       fock_mo.pluck(idet, 0)); // alpha spin
      matrix_transform(1.0, fockao.const_pluck(idet, 1), next_coeff.const_pluck(idet, 1), 0.0,
                       fock_mo.pluck(idet, 1)); // beta spin

      // apply level shift (from DIIS)
      if (level_shift_ != 0) {
        for (ptrdiff_t a = nocca; a < nmo; ++a) {
          fock_mo(a, a, idet, 0) += level_shift_;
        }
        for (ptrdiff_t a = noccb; a < nmo; ++a) {
          fock_mo(a, a, idet, 1) += level_shift_;
        }
      }

      // set up single determinant coeff and fock holders
      auto next_coa = next_coeff.pluck(idet, 0);
      auto next_cob = next_coeff.pluck(idet, 1);

      auto coa = coeffs_->pluck(idet, 0);
      auto cob = coeffs_->pluck(idet, 1);

      auto fockA_moa = fock_mo.pluck(idet, 0);
      auto fockA_mob = fock_mo.pluck(idet, 1);

      // diagonalize single determinant MO fock matrix and generate new coeffs
      linalg::diagonalize(fockA_moa);
      linalg::diagonalize(fockA_mob);

      gemm("n", "n", 1.0, next_coa, fockA_moa, 0.0, coa);
      gemm("n", "n", 1.0, next_cob, fockA_mob, 0.0, cob);

      // finalize the next coeff guess
      if (orbital_select_ == "aufbau") {
        next_coa = coa;
        next_cob = cob;
      } else if (orbital_select_ == "mom") {
        MOM_impl(nocca, fockA_moa, next_coa, coa);
        MOM_impl(noccb, fockA_mob, next_cob, cob);
      }

    } // move on to next determinant!
  } else if (mode_ == OptMode::second_order) {
    // get processed AO basis fock matrices from DIIS
    Tensor<4> fockao(nao, nao, nsh, ndet);
    diis_->back(fockao.reshape(fockao.size()));

    const ptrdiff_t npha = nocca * (nmo - nocca);
    const ptrdiff_t nphb = noccb * (nmo - noccb);

    const ptrdiff_t nph = npha + nphb;

    const ptrdiff_t nvira = nmo - nocca;
    const ptrdiff_t nvirb = nmo - noccb;

    if (!bfgs_) {
      fmt::print("Setting up BFGS optimizer.\n");
      qleve::Tensor<2> h0(nph, ndet);

      for (ptrdiff_t idet = 0; idet < ndet; ++idet) {
        auto next_coa = next_coeff.pluck(idet, 0);
        auto next_cob = next_coeff.pluck(idet, 1);

        qleve::Tensor<3> tmp(nao, nmo, nsh);
        qleve::gemm("n", "n", 1.0, fockao.const_pluck(idet, 0), next_coa, 0.0, tmp.pluck(0));
        qleve::gemm("n", "n", 1.0, fockao.const_pluck(idet, 1), next_cob, 0.0, tmp.pluck(1));

        qleve::Tensor<2> eigs(nmo, nsh);
        qleve::Tensor<3> next_co(nao, nmo, nsh);
        next_co.pluck(0) = next_coa;
        next_co.pluck(1) = next_cob;
        qleve::contract(2.0, next_co, "upx", tmp, "upx", 0.0, eigs, "px");
        // factor = 2.0 <- from occ-virt + virt-occ

        // use orbital energy differences for initial hessian
        auto h0a = h0.pluck(idet).slice(0, npha).reshape(nvira, nocca);
        auto h0b = h0.pluck(idet).slice(npha, npha + nphb).reshape(nvirb, noccb);

        for (ptrdiff_t i = 0; i < nocca; ++i) {
          for (ptrdiff_t a = 0; a < nvira; ++a) {
            h0a(a, i) = eigs(a + nocca, 0) - eigs(i, 0);
          }
        }
        for (ptrdiff_t i = 0; i < noccb; ++i) {
          for (ptrdiff_t a = 0; a < nvirb; ++a) {
            h0b(a, i) = eigs(a + noccb, 1) - eigs(i, 1);
          }
        }
      }
      bfgs_ = std::make_shared<BFGS<double>>(h0.reshape(h0.size()), 20);
    }

    if (!kappa_) {
      kappa_ = std::make_unique<qleve::Tensor<2>>(nph, ndet);
    }

    // compute Fia in current basis
    auto fock_ai = kappa_->zeros_like();

    for (ptrdiff_t idet = 0; idet < ndet; ++idet) {
      auto next_coa = next_coeff.pluck(idet, 0);
      auto next_cob = next_coeff.pluck(idet, 1);

      auto tmpa = qleve::gemm("n", "n", 1.0, fockao.const_pluck(idet, 0), next_coa.slice(0, nocca));
      auto tmpb = qleve::gemm("n", "n", 1.0, fockao.const_pluck(idet, 1), next_cob.slice(0, noccb));

      auto fock_ai_a = fock_ai.pluck(idet).slice(0, npha).reshape(nvira, nocca);
      auto fock_ai_b = fock_ai.pluck(idet).slice(npha, npha + nphb).reshape(nvirb, noccb);
      qleve::gemm("t", "n", 2.0, next_coa.slice(nocca, nmo), tmpa, 0.0, fock_ai_a);
      qleve::gemm("t", "n", 2.0, next_cob.slice(noccb, nmo), tmpb, 0.0, fock_ai_b);
      // 2 <- 2 (ai + ia)
    }

    // push and extrapolate
    auto dkappa = bfgs_->push_and_extrapolate(kappa_->reshape(kappa_->size()),
                                              fock_ai.reshape(fock_ai.size()));

    // update kappa
    *kappa_ += dkappa;

    if (debug) {
      const double fai_norm = fock_ai.norm();
      const double kappa_norm = kappa_->norm();
      const double dkappa_norm = dkappa.norm();

      fmt::print("BFGS debug: |Fai| = {:8.4g}, |kappa| = {:8.4g}, |dkappa| = {:8.4g}\n", fai_norm,
                 kappa_norm, dkappa_norm);
    }

    // generate unitary transform and update coeffs
    for (ptrdiff_t idet = 0; idet < ndet; ++idet) {
      auto next_coa = next_coeff.pluck(idet, 0);
      auto next_cob = next_coeff.pluck(idet, 1);
      auto coa = coeffs_->pluck(idet, 0);
      auto cob = coeffs_->pluck(idet, 1);

      auto Ua = qleve::linalg::unitary_exp(kappa_->pluck(idet).slice(0, npha).reshape(nvira, nocca));
      auto Ub = qleve::linalg::unitary_exp(kappa_->pluck(idet).slice(npha, npha + nphb).reshape(nvirb, noccb));

      qleve::gemm("n", "n", 1.0, coa, Ua, 0.0, next_coa);
      qleve::gemm("n", "n", 1.0, cob, Ub, 0.0, next_cob);
    }

    if ((iter_ % 20) == 0) {
      bfgs_.reset();
    }
  }

  // orbitals updated! if wanted, we could compute density matrices now?

} // end next()
