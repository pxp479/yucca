// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------

///
/// \file yucca/resmf/reshf.hpp
/// \brief ResHF with restricted determinants
///
/// This file defines the ResHF method with restricted determinants as inputs. Currently can accept
/// rHF, rCIS, and rResHF determinants as initial guess inputs.
///
#pragma once

#include <memory>

#include <yucca/input/input_node.hpp>
#include <yucca/resmf/converger.hpp>
#include <yucca/resmf/fock.hpp> // brute-force FDNR implementation
#include <yucca/util/method.hpp>


namespace yucca
{

// use forward declarations here to prevent compilation cascades
class Geometry; // #include <yucca/structure/geometry.hpp>
class Wfn;      // #include <yucca/wfn/wfn.hpp>

class ResHF : public Method_ {
 protected:
  std::ptrdiff_t nao_;     // number of atomic orbitals
  std::ptrdiff_t nmo_;     // number of molecular orbitals
  std::ptrdiff_t nocc_;    // number of occupied molecular orbitals
  std::ptrdiff_t nvir_;    // number of unoccupied (virtual) molecular orbitals
  std::ptrdiff_t nelec_;   // number of electrons
  int charge_;             // overall charge of molecule
  std::ptrdiff_t nSD_;     // number of Slater determinants in ResHF wavefunction
  std::ptrdiff_t nstates_; // number of ResHF states


  InputNode guess_; // YAML style user input for initial ResHF wavefunction guess
  std::vector<std::string> guess_types_; // Internal holder of user-specified initial guess types

  // Orbital rotation initial guess - HF ground state molecular orbitals to be rotated
  // Set by user input file
  std::vector<std::vector<std::vector<ptrdiff_t>>> rot_orbitals_;
  // Orbital rotation initial guess - Angle (degrees) to rotate HF ground state molecular orbitals
  // by Set by user input file
  std::vector<std::vector<double>> rot_angles_;
  // Orbital rotation initial guess - Collection of paired orbitals and the angle to rotate them
  // together by Internally set; no direct modifications from user input
  std::vector<std::vector<std::tuple<ptrdiff_t, ptrdiff_t, double>>> rot_pairs_;

  // Identification of CIS states to build initial ResHF Slater determinants from
  std::vector<ptrdiff_t> cis_states_;
  // Identification of the number of NTO hole-particle pairs per CIS state to build initial ResHF
  // Slater determinants from
  std::vector<ptrdiff_t> cis_ntos_;

  // Flag to control unitary transformation of Slater determinant basis during calculation of ResHF
  // Hamiltonian. If set to "true", Slater determinants derived from NTO hole-particle pairs of CIS
  // excited states (see CIS initial guess routine) are linearly combined to generate an open-shell
  // representation. This functionality is designed so that open-shell phenomena can be represented
  // using closed-shell, restricted Slater determinants.
  bool cis_transform_;
  // Unitary transformation matrix of the Slater determinant basis.
  // Built to transform Slater determinants generated from CIS excited states, but leave other
  // Slater determinants alone.
  std::shared_ptr<qleve::Tensor<2>> transform_;

  // Flag to control the constraint of Slater determinants derived from CIS initial guesses during
  // ResHF Fock build.
  bool cis_constrain_;
  // Pairs of Slater determinants to be held in constraint with each other during ResHF Fock build.
  std::vector<std::pair<ptrdiff_t, ptrdiff_t>> constrained_pairs_;

  // Flag to control the use of numerically stable matrix-adjugate ResHF Fock build routine
  bool use_matadj_;

  // Flag to turn on/off debugging print statements for KAB matrix
  bool print_kab_;

  // Lower bound to the SVD diagonalized sigma matrix values.
  // Necessary in non-matrix adjugate ResHF Fock builds to damp down numeric instability caused by
  // nearly orthogonal Slater determinants with very small overlap values.
  double svd_floor_;

  // Weights to be applied to ResHF electronic states during state-averaging
  std::vector<double> weight_;

  std::shared_ptr<qleve::Tensor<2>> overlap_;     // overlap of atomic orbitals
  std::shared_ptr<qleve::Tensor<2>> hcore_;       // core hamiltonian matrix in atomic orbital basis
  std::shared_ptr<qleve::Tensor<2>> coeffs_;      // HF ground state determinant
  std::shared_ptr<qleve::Tensor<2>> eigenstates_; // CIS excited state expansion coefficients

  // Nonorthogonal Slater determinants used to build the ResHF wavefunction
  std::shared_ptr<qleve::Tensor<3>> sdets_;

  // Expansion coefficients of Slater determinants used to build the ResHF wavefunction
  std::shared_ptr<qleve::Tensor<2>> resHF_coeffs_;
  // ResHF state energies
  std::shared_ptr<qleve::Tensor<1>> energies_;
  double energy0_;  // ResHF ground state energy
  double energySA_; // ResHF state-averaged energy

  // nonorthogonal MO overlap matrix
  std::shared_ptr<qleve::Tensor<2>> detS_AB_; // determinant of occ,occ block of S_AB_
  // Interdeterminant density matrix. Numerically unstable, except for the cases where sdet_A =
  // sdet_B.
  std::shared_ptr<qleve::Tensor<4>> W_AB_;
  // Interdeterminant Hamiltonian matrix
  std::shared_ptr<qleve::Tensor<2>> H_AB_;

  // Diagnostic that examines the "conditioning" of the overlap of nonorthogonal molecular orbitals
  double orbital_condition_;
  // Diagnostic that examines the "conditioning" of the overlap of nonorthogonal Slater determinants
  double cond_number_;

  // ResHF Fock matrices
  // There is one matrix for each Slater determinant present in the ResHF wavefunction
  std::shared_ptr<qleve::Tensor<3>> resHF_fock_;
  // Manages the building routine for the ResHF Fock matrices
  std::shared_ptr<ResFockBuilder> resfockbuilder_;

  // Manages the wavefunction optimization routine.
  // For a given iteration of the wavefunction optimization, propagates Slater determinants forward
  // based on calculated ResHF Fock matrices
  std::unique_ptr<ResMFConverger> converger_;

  // Check for convergence during wavefunction optimization
  bool converged(const double dE, const double error, const double drho) const;
  int max_iter_;        // maximum number of wavefunction optimization iterations
  double conv_energy_;  // wavefunction optimization dE convergence threshold
  double conv_error_;   // wavefunction optimization orbital gradient convergence threshold
  double conv_density_; // wavefunction optimization ??? convergence threshold
  double orb_gradient_norm_;

  // FDNR toggles (move to ResMFConverger class eventually!)
  bool use_fdnr_;            // user input parameter to control use of FDNR converger
  ptrdiff_t fdnr_start_;     // wavefunction optimization iteration that FDNR is turned "on" for
  int fdnr_order_;           // order of finite-difference for FDNR converger
  double fdnr_scale_;        // finite-difference step size for FDNR converger
  double krylov_residue_;    // convergence threshold for Krylov iterations within FDNR converger
  int krylov_iter_;          // maximum iterations for Krylov iterations within FDNR converger
  ptrdiff_t krylov_restart_; // number of iterations before Krylov solver restarts; turn "off" by
                             // setting to -1
  double krylov_shift_; // energy shift in hartrees applied to eigenvalues during Krylov iterations
                        // within FDNR converger

 public:
  // constructors
  ResHF(const InputNode& input, const std::shared_ptr<Geometry>& g);
  ResHF(const InputNode& input, const std::shared_ptr<Wfn>& wfn);

  // copy constructor
  ResHF(const ResHF& x);

  // class functions

  void compute() override;

  double energy() override { return energySA_; }
  std::vector<double> energies() override { return std::vector<double>{energySA_}; }

  qleve::Tensor<3> compute_1rdms(const qleve::ConstTensorView<3> W_AB,
                                 const qleve::ConstTensorView<2> S_AB,
                                 const qleve::ConstTensorView<2> C_AB) const;
  qleve::Tensor<3> compute_1tdms(const qleve::ConstTensorView<3> W_AB,
                                 const qleve::ConstTensorView<2> S_AB,
                                 const qleve::ConstTensorView<2> C_AB) const;

  // Debugging access functions

  std::shared_ptr<ResFockBuilder> resfockbuilder() const { return resfockbuilder_; }
  std::shared_ptr<qleve::Tensor<3>> sdets() const { return sdets_; }
  std::shared_ptr<qleve::Tensor<2>> detsab() const { return detS_AB_; }
  std::shared_ptr<qleve::Tensor<2>> hab() const { return H_AB_; }
  std::shared_ptr<qleve::Tensor<3>> FOCK() const { return resHF_fock_; }
  std::shared_ptr<qleve::Tensor<1>> Ek() const { return energies_; }
  std::vector<double> weight() const { return weight_; }
  std::ptrdiff_t nmo() const { return nmo_; }
  std::ptrdiff_t nao() const { return nao_; }
  std::ptrdiff_t nocc() const { return nocc_; }
  std::ptrdiff_t nSD() const { return nSD_; }
  bool use_transform() const { return cis_transform_; }

  double orb_gradient() const { return orb_gradient_norm_; };

  std::shared_ptr<Wfn> wavefunction() override;

 protected:
  void compute_gradient_impl() override;
};

void fd_fock_rreshf(const std::shared_ptr<Method_>& method,
                    const std::shared_ptr<yucca::Geometry>& geometry, const double h);

} // namespace yucca
