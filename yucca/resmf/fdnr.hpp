// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/fdnr.hpp
///
/// Finite Difference Newton-Raphson interface
#pragma once


#include <yucca/resmf/fock.hpp>

namespace yucca
{

class FDNR_func {
 protected:
  // input
  std::shared_ptr<qleve::Tensor<3>> initial_sdets_;
  std::shared_ptr<qleve::Tensor<3>> initial_fock_;

  std::shared_ptr<ResFockBuilder> resfockbuilder_;

  double scale_;
  // intermediates
  std::ptrdiff_t nocc_;
  std::ptrdiff_t nvir_;
  std::ptrdiff_t nmo_;
  std::ptrdiff_t nSD_;

  std::shared_ptr<qleve::Tensor<3>> kappa_; // maybe make a unique_ptr?
  std::shared_ptr<qleve::Tensor<3>> shifted_sdets_;
  std::shared_ptr<qleve::Tensor<3>> shifted_fock_;

  std::shared_ptr<qleve::Tensor<3>> hessian_;

  // output
  std::shared_ptr<qleve::Tensor<3>> orb_grad_;

  std::vector<std::pair<int, double>> fd_stencil_;

 public:
  FDNR_func(const qleve::ConstTensorView<3> sd, const qleve::ConstTensorView<3> F,
            const std::shared_ptr<ResFockBuilder>& builder, const int fd_order, const double h);

  void operator()(const qleve::ConstTensorView<2>& V, qleve::TensorView<2> W);

  void shift_sdets();
};
} // namespace yucca
