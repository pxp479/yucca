// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/resmf/reshf_1rdm.cpp
///
/// Implementation for 1RDM and 1TDM calculations

#include <yucca/resmf/reshf.hpp>

using namespace yucca;
using namespace qleve;
using namespace std;

qleve::Tensor<3> ResHF::compute_1rdms(const qleve::ConstTensorView<3> W_AB,
                                      const qleve::ConstTensorView<2> S_AB,
                                      const qleve::ConstTensorView<2> C_AB) const
{
  const ptrdiff_t ndet = C_AB.extent(0);
  const ptrdiff_t nst = C_AB.extent(1);
  const ptrdiff_t nao = W_AB.extent(0);
  assert(nao == W_AB.extent(1));
  assert(W_AB.extent(2) == ndet * ndet);

  Tensor<3> out(nao, nao, nst);

  auto wab = W_AB.const_reshape(nao, nao, ndet, ndet);
  for (ptrdiff_t istate = 0; istate < nst; ++istate) {
    for (ptrdiff_t A = 0; A < ndet; ++A) {
      for (ptrdiff_t B = 0; B < ndet; ++B) {
        const double c = C_AB.at(A, istate) * C_AB.at(B, istate) * S_AB(A, B);
        out.pluck(istate) += c * wab.const_pluck(A, B);
      }
    }
  }

  return out;
}

qleve::Tensor<3> ResHF::compute_1tdms(const qleve::ConstTensorView<3> W_AB,
                                      const qleve::ConstTensorView<2> S_AB,
                                      const qleve::ConstTensorView<2> C_AB) const
{
  const ptrdiff_t ndet = C_AB.extent(0);
  const ptrdiff_t nst = C_AB.extent(1);
  const ptrdiff_t nao = W_AB.extent(0);
  assert(nao == W_AB.extent(1));
  assert(W_AB.extent(2) == ndet * ndet);

  const ptrdiff_t npairs = nst * (nst - 1) / 2;

  Tensor<3> out(nao, nao, npairs);

  auto wab = W_AB.const_reshape(nao, nao, ndet, ndet);
  for (ptrdiff_t jstate = 0, ij = 0; jstate < nst; ++jstate) {
    for (ptrdiff_t istate = 0; istate < jstate; ++istate, ++ij) {
      for (ptrdiff_t A = 0; A < ndet; ++A) {
        for (ptrdiff_t B = 0; B < ndet; ++B) {
          const double c = C_AB.at(A, istate) * C_AB.at(B, jstate) * S_AB(A, B);
          out.pluck(ij) += c * wab.const_pluck(A, B);
        }
      }
    }
  }

  return out;
}
