// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/ufock.hpp
///
/// uResHF Fock matrix builder

#pragma once
#include <memory>

#include <yucca/structure/geometry.hpp>

namespace yucca
{

class uResFockBuilder {
 protected:
  const Geometry& geometry_;

  std::ptrdiff_t nao_; // number of atomic orbitals
  std::ptrdiff_t nmo_; // 1/2 * number of molecular orbitals (nmo_alpha = nmo_beta)

  std::shared_ptr<qleve::Tensor<2>> overlap_; // overlap of atomic orbitals
  std::shared_ptr<qleve::Tensor<2>> hcore_;   // core hamiltonian matrix in atomic orbital basis

  std::ptrdiff_t ne_a_;   // number of alpha-spin electrons
  std::ptrdiff_t ne_b_;   // number of beta-spin electrons
  std::ptrdiff_t nvir_a_; // number of alpha-spin virtual MO
  std::ptrdiff_t nvir_b_; // number of beta-spin virtual MO

  std::ptrdiff_t nsd_; // number of Slater determinants in ResHF wavefunction

  // Weights to be applied to ResHF electronic states during state-averaging
  std::vector<double> state_weight_;

  // Flag to control the use of numerically stable matrix-adjugate ResHF Fock build routine
  bool use_matadj_;
  // Flag to turn on/off debugging print statements for KAB matrix
  bool print_kab_;
  // Minimum singular value threshold for SVD of MO overlap matrices
  // Necessary in non-matrix adjugate ResHF Fock builds to damp down numeric instability caused by
  // nearly orthogonal Slater determinants with very small overlap values.
  double svd_floor_;

  // ResHF state energies
  std::shared_ptr<qleve::Tensor<1>> energies_;
  // Expansion coefficients of Slater determinants used to build the uResHF wavefunction
  std::shared_ptr<qleve::Tensor<2>> resHF_coeffs_;
  // ResHF state-averaged energy
  double ESA_;

  // total alpha-spin fock matrix in AO basis
  std::shared_ptr<qleve::Tensor<3>> a_fock_;
  // total beta-spin fock matrix in AO basis
  std::shared_ptr<qleve::Tensor<3>> b_fock_;

  // orbital gradient error for alpha and beta fock matrices
  std::shared_ptr<qleve::Tensor<4>> fockerr_;

 public:
  uResFockBuilder(const Geometry& g, const std::ptrdiff_t ne_a, const std::ptrdiff_t ne_b,
                  const std::ptrdiff_t nmo, const std::vector<double>& weight,
                  const double svd_floor = 0.0, const bool use_matadj = true,
                  const bool print_kab = false);

  // Take a set of Slater determinants and generate ResHF Fock matrices for each.
  // When energies_only option is set to "true", only energies will be calculated.
  qleve::Tensor<4> operator()(const qleve::ConstTensorView<3>& sd_a,
                              const qleve::ConstTensorView<3>& sd_b,
                              const bool& energies_only = false);

  std::tuple<qleve::Tensor<1>, qleve::Tensor<2>, qleve::Tensor<2>, qleve::Tensor<2>,
             qleve::Tensor<2>>
  get_mo_overlap_svd(const qleve::ConstTensorView<2>& CA, const qleve::ConstTensorView<2>& CB,
                     const qleve::ConstTensorView<2>& sao, const double& floor);

  qleve::Tensor<2> get_density(const qleve::ConstTensorView<1>& sigma,
                               const qleve::ConstTensorView<2>& AU,
                               const qleve::ConstTensorView<2>& BV);

  qleve::Tensor<2> get_unstable_density(const qleve::ConstTensorView<1>& sigma,
                                        const qleve::ConstTensorView<2>& AU,
                                        const qleve::ConstTensorView<2>& BV);

  double get_ham_1e(const qleve::ConstTensorView<2>& hcore, const qleve::ConstTensorView<2>& Q,
                    const double& detSAB, const double& detUVUV);

  double get_ham_2e_df(const double& detSAB_a, const double& detSAB_b, const double& detUVUV,
                       const qleve::ConstTensorView<1>& sig_a,
                       const qleve::ConstTensorView<1>& sig_b, const qleve::ConstTensorView<2>& Aoa,
                       const qleve::ConstTensorView<2>& Boa, const qleve::ConstTensorView<2>& Aob,
                       const qleve::ConstTensorView<2>& Bob);

  double get_ham_2e_og(const double& detSAB, const double& detUVUV,
                       const qleve::ConstTensorView<2>& G_a, const qleve::ConstTensorView<2>& G_b,
                       const qleve::ConstTensorView<2>& W_a, const qleve::ConstTensorView<2>& W_b);

  qleve::Tensor<2> get_gaa_ao_df(const qleve::ConstTensorView<2>& Ao_up,
                                 const qleve::ConstTensorView<2>& Ao_down);

  qleve::Tensor<2> get_gab_no_df(const qleve::ConstTensorView<2>& W_up,
                                 const qleve::ConstTensorView<2>& W_down);

  qleve::Tensor<2> get_gab_ao_og(const qleve::ConstTensorView<2>& Ao_up,
                                 const qleve::ConstTensorView<2>& Ao_down,
                                 const qleve::ConstTensorView<2>& Bo_up,
                                 const qleve::ConstTensorView<2>& Bo_down,
                                 const qleve::ConstTensorView<1>& invsig_up,
                                 const qleve::ConstTensorView<1>& invsig_down);

  std::tuple<qleve::Tensor<2>, qleve::Tensor<2>, qleve::Tensor<2>> get_kab_mo_vo_1e(
      const double& detSAB_down, const double& detUVUV, const qleve::ConstTensorView<2>& hcore,
      const qleve::ConstTensorView<2>& sao, const qleve::ConstTensorView<1>& sig_up,
      const qleve::ConstTensorView<1>& sig_down, const qleve::ConstTensorView<2>& Ao_up,
      const qleve::ConstTensorView<2>& Bo_up, const qleve::ConstTensorView<2>& Ao_down,
      const qleve::ConstTensorView<2>& Bo_down, const qleve::ConstTensorView<2>& Av_up,
      const qleve::ConstTensorView<2>& Bv_up, const qleve::ConstTensorView<2>& Av_down,
      const qleve::ConstTensorView<2>& Bv_down);

  std::tuple<qleve::Tensor<2>, qleve::Tensor<2>, qleve::Tensor<2>> get_kab_mo_vo_2e_df(
      const double& detSAB_down, const double& detUVUV, const qleve::ConstTensorView<2>& sao,
      const qleve::ConstTensorView<1>& sig_up, const qleve::ConstTensorView<1>& sig_down,
      const qleve::ConstTensorView<2>& Ao_up, const qleve::ConstTensorView<2>& Bo_up,
      const qleve::ConstTensorView<2>& Ao_down, const qleve::ConstTensorView<2>& Bo_down,
      const qleve::ConstTensorView<2>& Av_up, const qleve::ConstTensorView<2>& Bv_up,
      const qleve::ConstTensorView<2>& Av_down, const qleve::ConstTensorView<2>& Bv_down);

  qleve::Tensor<2> get_kab_mo_vo_ESA_term(const double& ESA, const double& detSAB_down,
                                          const double& detUVUV,
                                          const qleve::ConstTensorView<2>& sao,
                                          const qleve::ConstTensorView<1>& sig_up,
                                          const qleve::ConstTensorView<2>& Av_up,
                                          const qleve::ConstTensorView<2>& Bo_up);

  // Calculate orbital rotation gradients (FOCK * W_AA * S_AO - S_AO * W_AA * FOCK)
  qleve::Tensor<3> orbital_gradients(const qleve::ConstTensorView<3>& fock,
                                     const qleve::ConstTensorView<3>& sdet,
                                     const qleve::ConstTensorView<2>& sao, const double& nocc);

  // Access functions!
  std::shared_ptr<qleve::Tensor<1>> energies() const { return energies_; }
  std::shared_ptr<qleve::Tensor<2>> coeffs() const { return resHF_coeffs_; }

  double ESA() const { return ESA_; }

  std::shared_ptr<qleve::Tensor<4>> error() const { return fockerr_; }

  std::tuple<qleve::Tensor<3>, qleve::Tensor<3>> const aofock_total()
  {
    return std::make_tuple(*a_fock_, *b_fock_);
  }
};

} // namespace yucca
