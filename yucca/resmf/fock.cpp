// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/fock.cpp
///
/// ResHF Fock matrix builder
#include <fmt/core.h>

#include <qleve/blas_interface.hpp>
#include <qleve/determinant.hpp>
#include <qleve/diagonalize.hpp>
#include <qleve/excluded_product.hpp> // for stable matadj implementation
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/orthogonalize.hpp>
#include <qleve/rotate.hpp>
#include <qleve/svd.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

#include <yucca/resmf/fock.hpp>
#include <yucca/scf/fock.hpp>
#include <yucca/structure/density_fitting.hpp>
#include <yucca/util/libint_interface.hpp>

using namespace std;
using namespace yucca;

static const bool verbose = false;

ResFockBuilder::ResFockBuilder(
    const Geometry& g, const std::vector<double>& weight, const qleve::ConstTensorView<2>& U,
    const std::vector<std::pair<ptrdiff_t, ptrdiff_t>>& constrained_pairs,
    const std::ptrdiff_t nocc, const std::ptrdiff_t nmo, const std::ptrdiff_t nsd,
    const double hartree, const double exchange, const bool cis_transform, const bool cis_constrain,
    const double svd_floor, const bool use_matadj, const bool print_kab) :
    geometry_(g),
    weight_(weight),
    hartree_(hartree),
    exchange_(exchange),
    cis_transform_(cis_transform),
    nocc_(nocc),
    nmo_(nmo),
    nSD_(nsd),
    svd_floor_(svd_floor),
    cis_constrain_(cis_constrain),
    constrained_pairs_(constrained_pairs),
    use_matadj_(use_matadj),
    print_kab_(print_kab)
{
  overlap_ = make_shared<qleve::Tensor<2>>(geometry_.overlap());
  hcore_ = make_shared<qleve::Tensor<2>>(geometry_.hcore());
  transform_ = make_shared<qleve::Tensor<2>>(U);

  nao_ = overlap_->extent(0);
  nvir_ = nmo_ - nocc_;

  if (cis_transform_) {
    nstates_ = transform_->extent(1);
  } else {
    nstates_ = nSD_;
  }
  // Note for later: all these holders should be initialized within the operator() function near by
  // when they are needed, since many of them are only used in either the stable or unstable
  // routines

  sigma_AB_ = make_shared<qleve::Tensor<3>>(nocc_, nSD_, nSD_);
  UAb_occ_ = make_shared<qleve::Tensor<4>>(nocc_, nocc_, nSD_, nSD_);
  VBa_occ_ = make_shared<qleve::Tensor<4>>(nocc_, nocc_, nSD_, nSD_);
  AU_occ_ = make_shared<qleve::Tensor<4>>(nao_, nocc_, nSD_, nSD_);
  BV_occ_ = make_shared<qleve::Tensor<4>>(AU_occ_->zeros_like());

  invsigma_AB_ = make_shared<qleve::Tensor<3>>(nocc_, nSD_, nSD_);

  detS_AB_ = make_shared<qleve::Tensor<2>>(nSD_, nSD_);

  W_AB_ = make_shared<qleve::Tensor<4>>(nao_, nao_, nSD_, nSD_);
  F_AB_ = make_shared<qleve::Tensor<4>>(nao_, nao_, nSD_, nSD_);

  H_AB_ = make_shared<qleve::Tensor<2>>(nSD_, nSD_);
  resHF_coeffs_ = make_shared<qleve::Tensor<2>>(nSD_, nstates_);
  energies_ = make_shared<qleve::Tensor<1>>(nstates_);

  xi_AB_j_ = make_shared<qleve::Tensor<3>>(nocc_, nSD_, nSD_);
  xi_AB_ij_ = make_shared<qleve::Tensor<4>>(nocc_, nocc_, nSD_, nSD_);

  Q_AB_ = make_shared<qleve::Tensor<4>>(W_AB_->zeros_like());

} // end constructor

void ResFockBuilder::operator()(const qleve::ConstTensorView<3>& sd_coeffs,
                                qleve::TensorView<3> fock, bool energies_only)
{
  assert(nao_ == sd_coeffs.extent(0) && nao_ == fock.extent(0) && nao_ == fock.extent(1));
  assert(nmo_ == sd_coeffs.extent(1));
  assert(nSD_ == sd_coeffs.extent(2) && nSD_ == fock.extent(2));

  sdets_ = make_shared<qleve::Tensor<3>>(sd_coeffs);

  orbital_condition_ = 1.0;
  cond_number_ = 1.0;

  for (ptrdiff_t sdet_A = 0; sdet_A < nSD_; ++sdet_A) {
    for (ptrdiff_t sdet_B = 0; sdet_B < nSD_; ++sdet_B) {
      // Pull out molecular orbital coefficients of Slater determinants

      auto C_A = sdets_->const_pluck(sdet_A);
      auto C_B = sdets_->const_pluck(sdet_B);
      auto C_A_occ = sdets_->const_pluck(sdet_A).const_slice(0, nocc_);
      auto C_B_occ = sdets_->const_pluck(sdet_B).const_slice(0, nocc_);

      // Perform SVD

      auto sig = sigma_AB_->pluck(sdet_A, sdet_B);
      auto U = UAb_occ_->pluck(sdet_A, sdet_B);
      auto V = VBa_occ_->pluck(sdet_A, sdet_B);

      mo_overlap_svd(C_A, C_B, *overlap_, svd_floor_, sig, U, V, orbital_condition_);

      // Calculate overlap between Slater determinants

      const double detsig =
          std::accumulate(sig.data(), sig.data() + sig.size(), 1.0, std::multiplies<double>());
      detS_AB_->at(sdet_A, sdet_B) = detsig;

      // Apply SVD transform matrices to MO coefficents

      auto Ao = AU_occ_->pluck(sdet_A, sdet_B);
      auto Bo = BV_occ_->pluck(sdet_A, sdet_B);
      qleve::gemm("n", "n", 1.0, C_A_occ, U, 0.0, Ao);
      qleve::gemm("n", "n", 1.0, C_B_occ, V, 0.0, Bo);

      // Calculate interdeterminant density matrices

      auto siginv = invsigma_AB_->pluck(sdet_A, sdet_B);
      auto xi_i = xi_AB_j_->pluck(sdet_A, sdet_B);
      auto xi_ij = xi_AB_ij_->pluck(sdet_A, sdet_B);
      auto W = W_AB_->pluck(sdet_A, sdet_B);
      auto Q = Q_AB_->pluck(sdet_A, sdet_B);

      calc_densities(sig, Ao, Bo, siginv, W, xi_i, xi_ij, Q);

    } // move to next sdet_B
  }   // move to next sdet_A

  // Check orthogonality of Slater determinants
  {
    qleve::Tensor<2> ss(*detS_AB_ * *detS_AB_);
    auto ss_eigs = qleve::linalg::diagonalize(ss);
    cond_number_ = ss_eigs(nSD_ - 1) / ss_eigs(0);
  }
  if (verbose)
    fmt::print("orbital conditioning: {:3e}, determinant conditioning: {:3e}\n", orbital_condition_,
               cond_number_);

  // Build interdeterminant fock matrices

  *F_AB_ = 0.0;
  RFockBuilder fockbuilder(geometry_, hartree_, exchange_);
  fockbuilder(BV_occ_->reshape(nao_, nocc_, nSD_ * nSD_),
              AU_occ_->reshape(nao_, nocc_, nSD_ * nSD_), invsigma_AB_->reshape(nocc_, nSD_ * nSD_),
              W_AB_->reshape(nao_, nao_, nSD_ * nSD_), F_AB_->reshape(nao_, nao_, nSD_ * nSD_));
  // only has 2e- part at this stage (add in 1e- part later)

  qleve::Tensor<4> GAB(F_AB_->zeros_like());
  fockbuilder(BV_occ_->reshape(nao_, nocc_, nSD_ * nSD_),
              AU_occ_->reshape(nao_, nocc_, nSD_ * nSD_), xi_AB_j_->reshape(nocc_, nSD_ * nSD_),
              Q_AB_->reshape(nao_, nao_, nSD_ * nSD_),
              GAB.reshape(nao_, nao_, nSD_ * nSD_)); // numerically stable

  // Build interdeterminant Hamiltonian matrix

  // 1e- Hamiltonian term
  qleve::contract(1.0, *hcore_, "pq", *Q_AB_, "qpab", 0.0, *H_AB_, "ab");
  *H_AB_ *= 2.0 * (*detS_AB_);
  // Q_AB has an equivalent of |sigma_AB| already built in
  // additional equivalent of |sigma_AB| multiplied in due to restricted MOs

  // 2e- Hamiltonian term
  if (use_matadj_) {
    // mult by 1 instead of 0.5 due to restricted MOs
    // G_AB contains one equivalent of |sigma_AB|, so H_AB now has two equivalents of |sigma_AB|
    // baked in
    qleve::contract(1.0, GAB, "pqab", *Q_AB_, "qpab", 1.0, *H_AB_, "ab");

  } else {
    qleve::Tensor<2> HAB_2e(H_AB_->zeros_like());
    qleve::contract(0.5, *F_AB_, "pqab", *W_AB_, "qpab", 0.0, HAB_2e, "ab");

    qleve::Tensor<2> ss(*detS_AB_ * *detS_AB_);
    HAB_2e *= 2.0 * ss;

    *H_AB_ += HAB_2e;
  }

  // Add hcore (1e- part) to each F_AB (2e- part)
  for (ptrdiff_t a = 0; a < nSD_; a++) {
    for (ptrdiff_t b = 0; b < nSD_; b++) {
      F_AB_->pluck(a, b) += *hcore_;
    }
  }

  // Calculate ResHF state energies and wavefunction expansion coefficients
  build_coeffs_energies(*detS_AB_, *H_AB_, cis_transform_, *transform_, *resHF_coeffs_, *energies_);

  // If energies_only = true, don't build Fock matrix
  if (!energies_only) {
    // build final ResHF fock matrix using class member variables
    fock = build_fock_ao();
  }
}

void ResFockBuilder::mo_overlap_svd(const qleve::ConstTensorView<2>& CA,
                                    const qleve::ConstTensorView<2>& CB,
                                    const qleve::ConstTensorView<2> Sao, const double svd_floor,
                                    qleve::TensorView<1> sig, qleve::TensorView<2> U,
                                    qleve::TensorView<2> V, double& orb_cond)
{
  assert(CA.size() == CB.size());
  assert(CA.extent(0) == Sao.extent(0) && CB.extent(1) == Sao.extent(1));
  assert(sig.size() == U.extent(0) && sig.size() == U.extent(1));
  assert(U.size() == V.size());

  ptrdiff_t nao = Sao.extent(0);
  ptrdiff_t nmo = CA.extent(1);
  ptrdiff_t nocc = sig.size();

  // build MO overlap matrix
  qleve::Tensor<2> SAB(nmo, nmo);
  {
    qleve::Tensor<2> tmp(nao, nmo);
    qleve::gemm("t", "n", 1.0, CA, Sao, 0.0, tmp);
    qleve::gemm("n", "n", 1.0, tmp, CB, 0.0, SAB);
  }

  // SVD on occ,occ block of S_AB_mo
  qleve::Tensor<2> s_occ_occ(SAB.const_subtensor({0, 0}, {nocc, nocc}));
  auto [sigma, Ua, Vbt] = qleve::linalg::svd(s_occ_occ); // Caution: svd modifies input matrix

  // export SVD transform matrices
  U = Ua;
  V = Vbt.transpose();

  // apply SVD floor before exporting singular values
  const double smallest_singular = sigma(nocc - 1) / (nocc > 1 ? sigma(0) : 1.0);
  if (smallest_singular < svd_floor) {
    sigma = qleve::max(sigma, (nocc > 1 ? sigma(0) : 1.0) * svd_floor);
  }
  sig = sigma;

  // monitor orthogonality of MOs using singular values
  orb_cond = std::max(orb_cond, sigma(0) / sigma(nocc - 1));

} // end mo_overlap_svd()

void ResFockBuilder::calc_densities(const qleve::ConstTensorView<1>& sigAB,
                                    const qleve::ConstTensorView<2>& Ao,
                                    const qleve::ConstTensorView<2>& Bo,
                                    qleve::TensorView<1> sigAB_inv, qleve::TensorView<2> WAB,
                                    qleve::TensorView<1> xi_AB_i, qleve::TensorView<2> xi_AB_ij,
                                    qleve::TensorView<2> QAB)
{
  assert(sigAB.size() == sigAB_inv.size() && sigAB.size() == xi_AB_i.size());
  assert(sigAB.size() == Ao.extent(1) && Ao.size() == Bo.size());
  assert(sigAB.size() == xi_AB_ij.extent(0) && sigAB.size() == xi_AB_ij.extent(1));
  assert(WAB.extent(0) == Ao.extent(0) && WAB.extent(1) == Ao.extent(0));
  assert(WAB.size() == QAB.size());

  // build numerically unstable MO overlap inverse matrix
  sigAB_inv = 1.0 / sigAB;

  // build numerically unstable interdeterminant density matrix
  qleve::weighted_gemm("n", "t", 1.0, Bo, sigAB_inv, Ao, 0.0, WAB);

  // build numerically stable products of MO overlap inverse and determinant
  std::make_tuple(xi_AB_i, xi_AB_ij) = qleve::excluded_product_2(sigAB);

  // build numerically unstable interdeterminant density matrix (with an overlap determinant baked
  // in for stability!)
  qleve::weighted_gemm("n", "t", 1.0, Bo, xi_AB_i, Ao, 0.0, QAB);

} // end calc_densities()

void ResFockBuilder::build_coeffs_energies(const qleve::ConstTensorView<2> detSAB,
                                           const qleve::ConstTensorView<2> HAB,
                                           const bool NTO_transform,
                                           const qleve::ConstTensorView<2> U_transform,
                                           qleve::TensorView<2> coeffs,
                                           qleve::TensorView<1> E_state)
{
  assert(HAB.size() == detSAB.size());
  assert(HAB.extent(0) == coeffs.extent(0));
  assert(E_state.size() == coeffs.extent(1));
  assert(HAB.extent(0) == U_transform.extent(0));

  ptrdiff_t nstates = U_transform.extent(1);

  qleve::Tensor<2> ss(detSAB * detSAB);

  if (NTO_transform) {
    // transform overlap and Hamiltonian into nstates_ basis
    qleve::Tensor<2> S_transformed(nstates, nstates);
    qleve::matrix_transform(1.0, ss, U_transform, 0.0, S_transformed);

    qleve::Tensor<2> H_transformed(nstates, nstates);
    qleve::matrix_transform(1.0, HAB, U_transform, 0.0, H_transformed);

    // calculate resHF coeffs and energies
    qleve::Tensor<2> X(qleve::linalg::orthogonalize_metric(S_transformed));
    qleve::Tensor<2> H_ortho(H_transformed.zeros_like());
    qleve::matrix_transform(1.0, H_transformed, X, 0.0, H_ortho);

    E_state = qleve::linalg::diagonalize(H_ortho);

    // Transform eigenvectors back into non-orthogonal basis
    qleve::Tensor<2> H_tmp(H_ortho.zeros_like());
    qleve::gemm("n", "n", 1.0, X, H_ortho, 0.0, H_tmp);

    // Transform eigenvectors back into nSD_ basis
    qleve::gemm("n", "n", 1.0, U_transform, H_tmp, 0.0, coeffs);

  } // end NTO-transform path
  else {
    // calculate resHF coeffs and energies
    qleve::Tensor<2> X(qleve::linalg::orthogonalize_metric(ss));
    qleve::Tensor<2> H_ortho(HAB.zeros_like());
    qleve::matrix_transform(1.0, HAB, X, 0.0, H_ortho);

    E_state = qleve::linalg::diagonalize(H_ortho);

    // Transform eigenvectors back into non-orthogonal basis
    qleve::gemm("n", "n", 1.0, X, H_ortho, 0.0, coeffs);
  } // end no NTO-transform path

} // end build_coeffs_energies()

qleve::Tensor<3> ResFockBuilder::build_fock_ao()
{
  qleve::Tensor<3> F(nao_, nao_, nSD_);
  RFockBuilder matadj_fockbuilder(geometry_, 1.0, 1.0);


  for (ptrdiff_t sdet_A = 0; sdet_A < nSD_; sdet_A++) {
    auto ResHF_fock = F.pluck(sdet_A);

    // Build diagonal case term for ResHF fock matrix
    // The diagonal case is already stable! No matrix adjugate schenanegains required!

    // Assemble state weights and coeffs together
    double weight_coeffs_AA = 0.0;
    for (ptrdiff_t state = 0; state < weight_.size(); state++) {
      weight_coeffs_AA +=
          weight_[state] * resHF_coeffs_->at(sdet_A, state) * resHF_coeffs_->at(sdet_A, state);
    }

    // Add in diagonal case to ResHF fock matix
    auto F_AA = F_AB_->const_pluck(sdet_A, sdet_A);
    ResHF_fock += F_AA * weight_coeffs_AA;

    // Build K_AB terms for ResHF fock matrix
    for (ptrdiff_t sdet_B = 0; sdet_B < nSD_; sdet_B++) {
      // Skip diagonal K_AB terms
      if (sdet_B == sdet_A) {
        continue;
      }
      // Assemble state weights and coeffs together
      double weight_coeffs_AB = 0.0;
      double ESA_coeffs_AB = 0.0;
      for (ptrdiff_t state = 0; state < weight_.size(); state++) {
        weight_coeffs_AB +=
            weight_[state] * resHF_coeffs_->at(sdet_A, state) * resHF_coeffs_->at(sdet_B, state);
        ESA_coeffs_AB += energies_->at(state) * weight_[state] * resHF_coeffs_->at(sdet_A, state)
                         * resHF_coeffs_->at(sdet_B, state);
      }

      auto A_occ = AU_occ_->const_pluck(sdet_A, sdet_B);
      auto A_vir = sdets_->const_pluck(sdet_A).const_slice(nocc_, nmo_);
      auto B_occ = BV_occ_->const_pluck(sdet_A, sdet_B);

      auto sigma_AB = sigma_AB_->const_pluck(sdet_A, sdet_B); // not used
      auto detS_AB = detS_AB_->at(sdet_A, sdet_B);
      auto ss = detS_AB * detS_AB;

      auto xi_AB_i = xi_AB_j_->const_pluck(sdet_A, sdet_B);
      auto xi_AB_ij = xi_AB_ij_->const_pluck(sdet_A, sdet_B);

      auto H_AB = H_AB_->at(sdet_A, sdet_B);
      auto W_AB = W_AB_->const_pluck(sdet_A, sdet_B);
      auto F_AB = F_AB_->const_pluck(sdet_A, sdet_B);

      if (use_matadj_) {

        qleve::Tensor<2> s_AvBo(nvir_, nocc_);
        {
          qleve::Tensor<2> tmp(nvir_, nao_);
          qleve::gemm("t", "n", 1.0, A_vir, *overlap_, 0.0, tmp);
          qleve::gemm("n", "n", 1.0, tmp, B_occ, 0.0, s_AvBo);
        }
        qleve::Tensor<2> h_AvBo(nvir_, nocc_);
        {
          qleve::Tensor<2> tmp(nvir_, nao_);
          qleve::gemm("t", "n", 1.0, A_vir, *hcore_, 0.0, tmp);
          qleve::gemm("n", "n", 1.0, tmp, B_occ, 0.0, h_AvBo);
        }
        qleve::Tensor<2> h_AoBo(nocc_, nocc_);
        {
          qleve::Tensor<2> tmp(nocc_, nao_);
          qleve::gemm("t", "n", 1.0, A_occ, *hcore_, 0.0, tmp);
          qleve::gemm("n", "n", 1.0, tmp, B_occ, 0.0, h_AoBo);
        }

        // building K_AB in SVD basis
        // K_AB vir,vir block is zero in SVD basis
        // K_AB occ,vir block is zero in SVD basis
        qleve::Tensor<2> svd_K_AB(nmo_, nmo_);

        // building K_AB occ,occ block in SVD basis
        auto svd_K_AB_oo = svd_K_AB.subtensor({0, 0}, {nocc_, nocc_});
        qleve::Tensor<2> K3oo(nocc_, nocc_);
        qleve::Tensor<2> K4oo(nocc_, nocc_);
        for (ptrdiff_t i = 0; i < nocc_; i++) {
          K3oo(i, i) = H_AB * weight_coeffs_AB;
          K4oo(i, i) = -1.0 * detS_AB * detS_AB * ESA_coeffs_AB;
        }
        svd_K_AB_oo = K3oo + K4oo;

        // building K_AB vir,occ block in SVD basis
        auto svd_K_AB_vo = svd_K_AB.subtensor({nocc_, 0}, {nmo_, nocc_});

        // Use matrix element routine for 1e- part of K_AB vir,occ block
        qleve::Tensor<2> K1K2K3_vo_1e(nvir_, nocc_);
        qleve::Tensor<2> K1_1e(K1K2K3_vo_1e.zeros_like());
        qleve::Tensor<2> K2_1e(K1K2K3_vo_1e.zeros_like());
        qleve::Tensor<2> K3_1e(K1K2K3_vo_1e.zeros_like());
        qleve::Tensor<2> K4_tot(K1K2K3_vo_1e.zeros_like());

        qleve::Tensor<2> xi_ij_det(xi_AB_ij * detS_AB);
        for (ptrdiff_t i = 0; i < xi_AB_ij.extent(0); i++) {
          xi_ij_det(i, i) = 1.0;
        }

        for (ptrdiff_t a = 0; a < nvir_; a++) {
          for (ptrdiff_t i = 0; i < nocc_; i++) {
            auto xi_i = xi_AB_i(i) * detS_AB;

            // K_AB_vo_1e(a, i) += (h_AvBo(a, i) - E_state * s_AvBo(a, i)) * xi_i; // K1_1e + K4
            double K1 = h_AvBo(a, i) * xi_i;
            K1_1e(a, i) = K1;
            double K4 = -1.0 * ESA_coeffs_AB * s_AvBo(a, i) * xi_i;
            K4_tot(a, i) = K4;
            K1K2K3_vo_1e(a, i) = K1;

            for (ptrdiff_t j = 0; j < nocc_; j++) {
              auto xi_ij = xi_ij_det(i, j);
              // auto xi_ij = xi_AB_i(i) * xi_AB_i(j);
              // does NOT work if you do xi_ij = xi_AB_ij(i,j) * detS_AB

              // K_AB_vo_1e(a, i) +=
              //     (2.0 * s_AvBo(a, i) * h_AoBo(j, j) - s_AvBo(a, j) * h_AoBo(j, i))
              //     * xi_ij; // K3_1e + K2_1e
              double K3 = 2.0 * s_AvBo(a, i) * h_AoBo(j, j) * xi_ij;
              K3_1e(a, i) += K3;
              double K2 = -1.0 * s_AvBo(a, j) * h_AoBo(j, i) * xi_ij;
              K2_1e(a, i) += K2;
              K1K2K3_vo_1e(a, i) += K3 + K2;

            } // end j summation
          }   // next i element
        }     // next a element

        if (print_kab_) {
          fmt::print("\n");
          fmt::print("--********************************--\n");
          fmt::print("SD A{}, B{}\n", sdet_A, sdet_B);
          fmt::print("--********************************--\n");

          fmt::print("-------------------\n");
          fmt::print("KAB Vir, Occ block:\n");
          fmt::print("-------------------\n");
          fmt::print("1e- terms:\n");
          fmt::print("  * K1_1e norm: {:1e}\n", K1_1e.norm());
          fmt::print("  * K2_1e norm: {:1e}\n", K2_1e.norm());
          fmt::print("  * K3_1e norm: {:1e}\n", K3_1e.norm());

          fmt::print("\n");
          fmt::print("  * K1+K2+K3_1e norm: {:1e}\n", K1K2K3_vo_1e.norm());
        }

        // Use efficient density fitting routine for 2e- part of K_AB vir,occ block
        qleve::Tensor<2> K1K2K3_vo_2e(nvir_, nocc_);
        matadj_fockbuilder(xi_AB_i, xi_AB_ij, s_AvBo, A_occ, A_vir, B_occ, K1K2K3_vo_2e,
                           print_kab_);

        if (print_kab_) {
          fmt::print("-------------------\n");
          fmt::print("Total E term (with state weights + coeffs):\n");
          fmt::print("  * K4 norm: {:1e}\n", K4_tot.norm());
        }

        svd_K_AB_vo = ((K1K2K3_vo_1e + K1K2K3_vo_2e) * weight_coeffs_AB) + K4_tot;

        // converting from SVD MO basis to general sdA MO basis
        qleve::Tensor<2> genA_K_AB(nmo_, nmo_);
        auto genA_K_AB_oo = genA_K_AB.subtensor({0, 0}, {nocc_, nocc_});
        genA_K_AB_oo = svd_K_AB_oo; // reduce noise from matrix transform

        auto genA_K_AB_vo = genA_K_AB.subtensor({nocc_, 0}, {nmo_, nocc_});
        auto U = UAb_occ_->const_pluck(sdet_A, sdet_B); // nocc x nocc
        qleve::gemm("n", "t", 1.0, svd_K_AB_vo, U, 0.0, genA_K_AB_vo);

        // converting from general sdA MO basis to AO basis
        auto sdA = sdets_->const_pluck(sdet_A);
        qleve::Tensor<2> sdA_ortho(sdA.zeros_like());
        qleve::gemm("n", "n", 1.0, *overlap_, sdA, 0.0, sdA_ortho);

        qleve::Tensor<2> ao_K_AB(nao_, nao_);
        {
          qleve::Tensor<2> tmp(nao_, nmo_);
          qleve::gemm("n", "n", 1.0, sdA_ortho, genA_K_AB, 0.0, tmp);
          qleve::gemm("n", "t", 1.0, tmp, sdA_ortho, 0.0, ao_K_AB);
        }

        if (print_kab_) {
          fmt::print("-------------------\n");
          fmt::print("Assembled KAB_vo matrix (SVD MO basis + state weights + coeffs):\n");
          fmt::print("  * KAB_vo norm: {:1e}\n", svd_K_AB_vo.norm());

          fmt::print("-------------------\n");
          fmt::print("Assembled KAB_vo matrix (general MO basis + state weights + coeffs):\n");
          fmt::print("  * KAB_vo norm: {:1e}\n", genA_K_AB_vo.norm());

          fmt::print("-------------------\n");
          fmt::print("KAB Occ, Occ block:\n");
          fmt::print("-------------------\n");
          fmt::print("Assembled KAB_oo matrix (general MO basis + state weights + coeffs):\n");
          fmt::print("  * KAB_oo norm: {:1e}\n", genA_K_AB_oo.norm());

          fmt::print("-------------------\n");
          fmt::print("KAB in general MO basis (with state weights and coeffs):\n");
          fmt::print("-------------------\n");
          fmt::print("  * KAB_mo norm: {:1e}\n", genA_K_AB.norm());

          fmt::print("-------------------\n");
          fmt::print("KAB in AO basis (with state weights and coeffs):\n");
          fmt::print("-------------------\n");
          fmt::print("  * KAB in AO norm: {:1e}\n", ao_K_AB.norm());
          fmt::print("\n");
        }

        ResHF_fock += ao_K_AB + ao_K_AB.transpose();

      } // end matadj K_AB builder routine
      else {
        qleve::Tensor<2> KAB(nao_, nao_);
        qleve::Tensor<2> SW(nao_, nao_);
        qleve::Tensor<2> WS(nao_, nao_);

        qleve::gemm("n", "n", 1.0, *overlap_, W_AB, 0.0, SW);
        qleve::gemm("n", "n", 1.0, W_AB, *overlap_, 0.0, WS);

        qleve::Tensor<2> K1(nao_, nao_);
        qleve::Tensor<2> K2(nao_, nao_);
        qleve::Tensor<2> K3(nao_, nao_);
        qleve::Tensor<2> K1K2K3(nao_, nao_);

        qleve::gemm("n", "n", ss, F_AB, WS, 0.0, K1);

        qleve::Tensor<2> tmp(nao_, nao_);
        qleve::gemm("n", "n", -1.0 * ss, SW, F_AB, 0.0, tmp);
        qleve::gemm("n", "n", 1.0, tmp, WS, 0.0, K2);

        qleve::gemm("n", "n", H_AB, *overlap_, WS, 0.0, K3);

        K1K2K3 = (K1 + K2 + K3) * weight_coeffs_AB;

        qleve::Tensor<2> K4(nao_, nao_);
        qleve::gemm("n", "n", -1.0 * ss * ESA_coeffs_AB, *overlap_, WS, 0.0, K4);

        KAB = K1K2K3 + K4;

        ResHF_fock += KAB + KAB.transpose();

      } // end non-matadj K_AB builder routine

    } // cycle to new sdet B

  } // move to next SDet A

  // average occvirt blocks of constrained pairs
  if (cis_constrain_) {
    for (const auto [detA, detB] : constrained_pairs_) {
      auto fockA = F.pluck(detA);
      auto fockB = F.pluck(detB);

      auto CA = sdets_->pluck(detA);
      auto CB = sdets_->pluck(detB);

      // compute averaged density matrix in basis of CA
      auto SC_A = gemm("n", "n", 1.0, *overlap_, CA);
      auto ASB = gemm("t", "n", 1.0, SC_A, CB.slice(0, nocc_));

      // multiplying by -1 so the eigenvectors are sorted starting from occupied
      // -gamma_B
      auto rho = gemm("n", "t", -1.0, ASB, ASB);
      // -gamma_A
      for (ptrdiff_t i = 0; i < nocc_; ++i) {
        rho(i, i) -= 1.0;
      }

      auto occupations = qleve::linalg::diagonalize(rho);
      // occupations *= -1.0;
      // occupations.print("occupations");
      auto CC = gemm("n", "n", 1.0, CA, rho);

      // compute gamma_A - gamma_B in the open subspace
      auto M = gemm("n", "t", -1.0, ASB, ASB);
      for (ptrdiff_t i = 0; i < nocc_; ++i) {
        M(i, i) += 1.0;
      }

      qleve::Tensor<2> Mopen(2, 2);
      matrix_transform(1.0, M, rho.slice(nocc_ - 1, nocc_ + 1), 0.0, Mopen);

      // do one jacobi rotation
      const double b = 0.5 * (Mopen(0, 0) - Mopen(1, 1)) / Mopen(0, 1);
      const double t = (b > 0 ? 1.0 : -1.0) / (std::abs(b) + std::sqrt(b * b + 1));
      const double c = 1.0 / std::sqrt(t * t + 1);
      const double s = c * t;
      qleve::rotate(nao_, &CC(0, nocc_ - 1), 1, &CC(0, nocc_), 1, c, s);

      qleve::Tensor<2> F_mo_A(nmo_, nmo_);
      matrix_transform(1.0, fockA, CC, 0.0, F_mo_A);
      qleve::Tensor<2> F_mo_B(nmo_, nmo_);
      matrix_transform(1.0, fockB, CC, 0.0, F_mo_B);

      const ptrdiff_t nclo = nocc_ - 1;
      const ptrdiff_t nclopen = nocc_ + 1;
      auto Fov_A = F_mo_A.subtensor({0, nclopen}, {nclo, nmo_});
      auto Fov_B = F_mo_B.subtensor({0, nclopen}, {nclo, nmo_});
      auto Fvo_A = F_mo_A.subtensor({nclopen, 0}, {nmo_, nclo});
      auto Fvo_B = F_mo_B.subtensor({nclopen, 0}, {nmo_, nclo});

      Fov_A = 0.5 * (Fov_A + Fov_B);
      Fov_B = Fov_A;

      Fvo_A = 0.5 * (Fvo_A + Fvo_B);
      Fvo_B = Fvo_A;

      // still unsure what the correct constraint ought to be here
      F_mo_A(nocc_ - 1, nocc_) = 0.0 * (F_mo_A(nocc_ - 1, nocc_) + F_mo_B(nocc_ - 1, nocc_));
      F_mo_B(nocc_ - 1, nocc_) = -F_mo_A(nocc_ - 1, nocc_);
      F_mo_A(nocc_, nocc_ - 1) = F_mo_A(nocc_ - 1, nocc_);
      F_mo_B(nocc_, nocc_ - 1) = -F_mo_A(nocc_ - 1, nocc_);

      auto SCC = gemm("n", "n", 1.0, *overlap_, CC);

      matrix_transform("t", 1.0, F_mo_A, SCC, 0.0, fockA);
      matrix_transform("t", 1.0, F_mo_B, SCC, 0.0, fockB);
    } // move to next constrained SDet pair
  }   // end cis_constrain = true path

  return F;
} // end build_fock()
