// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/ufock.cpp
///
/// uResHF Fock matrix builder
#include <fmt/core.h>

#include <qleve/determinant.hpp>
#include <qleve/diagonalize.hpp>
#include <qleve/excluded_product.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/orthogonalize.hpp>
#include <qleve/svd.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/tensor_dot.hpp>
#include <qleve/trace.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

#include <yucca/resmf/ufock.hpp>
#include <yucca/structure/density_fitting.hpp>

using namespace yucca;
using namespace std;
using namespace qleve;

///////////////////////////////////////////////////////////////////////////////////////
//      Begin uResFockBuilder Method Constructor                                     //
///////////////////////////////////////////////////////////////////////////////////////

uResFockBuilder::uResFockBuilder(const Geometry& g, const ptrdiff_t ne_a, const ptrdiff_t ne_b,
                                 const ptrdiff_t nmo, const vector<double>& weight,
                                 const double svd_floor, const bool use_matadj,
                                 const bool print_kab) :
    geometry_(g),
    state_weight_(weight),
    svd_floor_(svd_floor),
    use_matadj_(use_matadj),
    print_kab_(print_kab),
    ne_a_(ne_a),
    ne_b_(ne_b),
    nmo_(nmo)
{
  overlap_ = make_shared<Tensor<2>>(geometry_.overlap());
  hcore_ = make_shared<Tensor<2>>(geometry_.hcore());

  nao_ = overlap_->extent(0);

  nvir_a_ = nmo_ - ne_a_;
  nvir_b_ = nmo_ - ne_b_;

  nsd_ = weight.size();

} // end uResFockBuilder constructor

///////////////////////////////////////////////////////////////////////////////////////
//      Begin uResFockBuilder Operator Function                                      //
///////////////////////////////////////////////////////////////////////////////////////
Tensor<4> uResFockBuilder::operator()(const ConstTensorView<3>& sd_a,
                                      const ConstTensorView<3>& sd_b, const bool& energies_only)
{
  assert(sd_a.extent(2) == nsd_ && sd_b.extent(2) == nsd_);
  assert(sd_a.extent(0) == nao_ && sd_b.extent(0) == nao_);
  assert(sd_a.extent(1) == nmo_ && sd_b.extent(1) == nmo_);

  Tensor<3> sigmaAB_a(ne_a_, nsd_, nsd_);
  Tensor<4> UAB_a(ne_a_, ne_a_, nsd_, nsd_);
  Tensor<4> VAB_a(ne_a_, ne_a_, nsd_, nsd_);
  Tensor<4> AoUAB_a(nao_, ne_a_, nsd_, nsd_); // occupied alpha MOs in SVD basis (C_A * U)
  Tensor<4> BoVAB_a(nao_, ne_a_, nsd_, nsd_); // occupied alpha MOs in SVD basis (C_B * V)
  Tensor<4> QAB_a(nao_, nao_, nsd_, nsd_);
  Tensor<4> WAB_a(QAB_a.zeros_like());
  Tensor<4> GAB_a(QAB_a.zeros_like());
  Tensor<2> detSAB_a(nsd_, nsd_);

  Tensor<3> sigmaAB_b(ne_b_, nsd_, nsd_);
  Tensor<4> UAB_b(ne_b_, ne_b_, nsd_, nsd_);
  Tensor<4> VAB_b(ne_b_, ne_b_, nsd_, nsd_);
  Tensor<4> AoUAB_b(nao_, ne_b_, nsd_, nsd_); // occupied beta MOs in SVD basis (C_A * U)
  Tensor<4> BoVAB_b(nao_, ne_b_, nsd_, nsd_); // occupied beta MOs in SVD basis (C_B * V)
  Tensor<4> QAB_b(QAB_a.zeros_like());
  Tensor<4> WAB_b(QAB_a.zeros_like());
  Tensor<4> GAB_b(QAB_a.zeros_like());
  Tensor<2> detSAB_b(detSAB_a.zeros_like());

  Tensor<2> HAB(nsd_, nsd_);
  Tensor<2> HAB_1e(nsd_, nsd_);
  Tensor<2> HAB_2e(nsd_, nsd_);

  Tensor<3> aGAA(nao_, nao_, nsd_);
  Tensor<3> bGAA(nao_, nao_, nsd_);

  Tensor<2> det_UaVaUbVb(nsd_, nsd_);

  for (ptrdiff_t sdA = 0; sdA < nsd_; sdA++) {
    for (ptrdiff_t sdB = 0; sdB < nsd_; sdB++) {

      // calculate MO_alpha overlap using SVD for occ, occ block of S_AB matrix
      auto [sig_a, U_a, V_a, AUo_a, BVo_a] =
          get_mo_overlap_svd(sd_a.const_pluck(sdA).const_slice(0, ne_a_),
                             sd_a.const_pluck(sdB).const_slice(0, ne_a_), *overlap_, svd_floor_);
      // store products of SVD
      sigmaAB_a.pluck(sdA, sdB) = sig_a;
      UAB_a.pluck(sdA, sdB) = U_a;
      VAB_a.pluck(sdA, sdB) = V_a;
      AoUAB_a.pluck(sdA, sdB) = AUo_a;
      BoVAB_a.pluck(sdA, sdB) = BVo_a;
      // calculate determinant overlap
      //  NOTE: might need to return to this and account for |U| and |Vt| (+- 1.0)
      detSAB_a(sdA, sdB) =
          accumulate(sig_a.data(), sig_a.data() + sig_a.size(), 1.0, multiplies<double>());

      // calculate MO_beta overlap using SVD for occ, occ block of S_AB matrix
      auto [sig_b, U_b, V_b, AUo_b, BVo_b] =
          get_mo_overlap_svd(sd_b.const_pluck(sdA).const_slice(0, ne_b_),
                             sd_b.const_pluck(sdB).const_slice(0, ne_b_), *overlap_, svd_floor_);
      // store products of SVD
      sigmaAB_b.pluck(sdA, sdB) = sig_b;
      UAB_b.pluck(sdA, sdB) = U_b;
      VAB_b.pluck(sdA, sdB) = V_b;
      AoUAB_b.pluck(sdA, sdB) = AUo_b;
      BoVAB_b.pluck(sdA, sdB) = BVo_b;
      // calculate determinant overlap
      detSAB_b(sdA, sdB) =
          accumulate(sig_b.data(), sig_b.data() + sig_b.size(), 1.0, multiplies<double>());

      // calculate product of |U_a| * |V_a| * |U_b| * |V_b|
      auto detUa = linalg::determinant(U_a);
      auto detVa = linalg::determinant(V_a);
      auto detUb = linalg::determinant(U_b);
      auto detVb = linalg::determinant(V_b);
      det_UaVaUbVb(sdA, sdB) = detUa * detVa * detUb * detVb;

      // calculate interdeterminant density matrices (for alpha and beta spin)
      QAB_a.pluck(sdA, sdB) = get_density(sig_a, AUo_a, BVo_a);
      QAB_b.pluck(sdA, sdB) = get_density(sig_b, AUo_b, BVo_b);

      WAB_a.pluck(sdA, sdB) = get_unstable_density(sig_a, AUo_a, BVo_a);
      WAB_b.pluck(sdA, sdB) = get_unstable_density(sig_b, AUo_b, BVo_b);

      // calculate Hamiltonian matrix element for determinant pair
      if (use_matadj_) {
        HAB_1e(sdA, sdB) = get_ham_1e(*hcore_, QAB_a.const_pluck(sdA, sdB), detSAB_b(sdA, sdB),
                                      det_UaVaUbVb(sdA, sdB));
        HAB_1e(sdA, sdB) += get_ham_1e(*hcore_, QAB_b.const_pluck(sdA, sdB), detSAB_a(sdA, sdB),
                                       det_UaVaUbVb(sdA, sdB));

        HAB_2e(sdA, sdB) =
            get_ham_2e_df(detSAB_a(sdA, sdB), detSAB_b(sdA, sdB), det_UaVaUbVb(sdA, sdB), sig_a,
                          sig_b, AUo_a, BVo_a, AUo_b, BVo_b);

        HAB(sdA, sdB) = HAB_1e(sdA, sdB) + HAB_2e(sdA, sdB);
      } else {
        double detSAB = detSAB_a(sdA, sdB) * detSAB_b(sdA, sdB);
        HAB_1e(sdA, sdB) =
            get_ham_1e(*hcore_, WAB_a.const_pluck(sdA, sdB), detSAB, det_UaVaUbVb(sdA, sdB));
        HAB_1e(sdA, sdB) +=
            get_ham_1e(*hcore_, WAB_b.const_pluck(sdA, sdB), detSAB, det_UaVaUbVb(sdA, sdB));

        // add 2e- HAB function (for sanity checking!)

        // compute GAB matrix
        // Tensor<1> invsig_a = 1.0 / sig_a;
        // Tensor<1> invsig_b = 1.0 / sig_b;
        // GAB_a.pluck(sdA, sdB) = get_gab_ao_og(AUo_a, AUo_b, BVo_a, BVo_b, invsig_a, invsig_b);
        // GAB_b.pluck(sdA, sdB) = get_gab_ao_og(AUo_b, AUo_a, BVo_b, BVo_a, invsig_b, invsig_a);

        GAB_a.pluck(sdA, sdB) =
            get_gab_no_df(WAB_a.const_pluck(sdA, sdB), WAB_b.const_pluck(sdA, sdB));
        GAB_b.pluck(sdA, sdB) =
            get_gab_no_df(WAB_b.const_pluck(sdA, sdB), WAB_a.const_pluck(sdA, sdB));

        HAB_2e(sdA, sdB) = 0.5 * get_ham_1e(GAB_a.const_pluck(sdA, sdB), WAB_a.const_pluck(sdA, sdB),
                                      detSAB, det_UaVaUbVb(sdA, sdB));

        HAB_2e(sdA, sdB) += 0.5 * get_ham_1e(GAB_b.const_pluck(sdA, sdB), WAB_b.const_pluck(sdA, sdB),
                                       detSAB, det_UaVaUbVb(sdA, sdB));

        HAB(sdA, sdB) = HAB_1e(sdA, sdB) + HAB_2e(sdA, sdB);
      }

      // calculate 2e- component of diagonal Fock matrices
      if (sdA == sdB) {
        aGAA.pluck(sdA) = get_gaa_ao_df(sd_a.const_pluck(sdA).const_slice(0, ne_a_),
                                        sd_b.const_pluck(sdA).const_slice(0, ne_b_));
        bGAA.pluck(sdA) = get_gaa_ao_df(sd_b.const_pluck(sdA).const_slice(0, ne_b_),
                                        sd_a.const_pluck(sdA).const_slice(0, ne_a_));
      }
    } // next sdB
  } // next sdA
  // HAB.print("Total Hamiltonian Matrix!");
  // HAB_1e.print("1e- Hamiltonian Matrix!");
  // HAB_2e.print("2e- Hamiltonian Matrix!");
  // detSAB_a.print("Alpha Spin |SAB|");
  // detSAB_b.print("Beta Spin |SAB|");

  // calculate state energies and reshf wavefunction coefficients

  // orthogonalize Hamiltonian matrix
  Tensor<2> ss(detSAB_a * detSAB_b * det_UaVaUbVb);
  Tensor<2> X(linalg::orthogonalize_metric(ss));
  Tensor<2> H_ortho(HAB.zeros_like());
  matrix_transform(1.0, HAB, X, 0.0, H_ortho);

  // find eigenvalues (state energies) and eigenvectors (ResHF state wavefunction coeffs) for
  // orthogonalized Hamiltonian
  energies_ = make_shared<Tensor<1>>(nsd_);
  *energies_ = linalg::diagonalize(H_ortho);

  // Transform eigenvectors back into non-orthogonal basis
  resHF_coeffs_ = make_shared<Tensor<2>>(nsd_, nsd_);
  gemm("n", "n", 1.0, X, H_ortho, 0.0, *resHF_coeffs_);

  // calculate state averaged energy
  ESA_ = 0.0;
  for (ptrdiff_t state = 0; state < nsd_; state++) {
    ESA_ += state_weight_[state] * energies_->at(state);
  }

  if (energies_only) {
    // placeholder fock matrices
    Tensor<3> a_fock(nao_, nao_, nsd_);
    Tensor<3> b_fock(nao_, nao_, nsd_);
    Tensor<4> fock(nao_, nao_, nsd_, 2);

    // placeholder orbital gradients
    auto a_fockerr = orbital_gradients(a_fock, sd_a, *overlap_, ne_a_);
    auto b_fockerr = orbital_gradients(b_fock, sd_b, *overlap_, ne_b_);

    fockerr_ = make_shared<Tensor<4>>(nao_, nao_, nsd_, 2);
    fockerr_->pluck(0) = a_fockerr;
    fockerr_->pluck(1) = b_fockerr;

    return fock;
  }

  // build uResHF fock matrix
  a_fock_ = make_shared<Tensor<3>>(nao_, nao_, nsd_);
  b_fock_ = make_shared<Tensor<3>>(nao_, nao_, nsd_);

  for (ptrdiff_t sdA = 0; sdA < nsd_; sdA++) {
    // access Fock matrix holder for sdA
    auto aFA_ao = a_fock_->pluck(sdA);
    auto bFA_ao = b_fock_->pluck(sdA);

    // compute \sum_I [ state_weight_I * |c_AI|^2 ]
    double weight_coeffs_AA = 0.0;
    for (ptrdiff_t state = 0; state < state_weight_.size(); state++) {
      weight_coeffs_AA +=
          state_weight_[state] * resHF_coeffs_->at(sdA, state) * resHF_coeffs_->at(sdA, state);
    }

    // build 1e- diagonal term
    aFA_ao += weight_coeffs_AA * *hcore_;
    bFA_ao += weight_coeffs_AA * *hcore_;

    // build 2e- diagonal term
    aFA_ao += weight_coeffs_AA * aGAA.const_pluck(sdA);
    bFA_ao += weight_coeffs_AA * bGAA.const_pluck(sdA);

    // build and assemble off-diagonal KAB terms
    for (ptrdiff_t sdB = 0; sdB < nsd_; sdB++) {
      // skip over sdB == sdA case
      if (sdB == sdA) {
        continue;
      }

      double weight_coeffs_AB = 0.0;
      double ESA_coeffs_AB = 0.0;
      for (ptrdiff_t state = 0; state < state_weight_.size(); state++) {
        weight_coeffs_AB +=
            state_weight_[state] * resHF_coeffs_->at(sdA, state) * resHF_coeffs_->at(sdB, state);
        ESA_coeffs_AB += energies_->at(state) * state_weight_[state] * resHF_coeffs_->at(sdA, state)
                         * resHF_coeffs_->at(sdB, state);
      }

      Tensor<2> KAB_ao_a(nao_, nao_);
      Tensor<2> KAB_ao_b(nao_, nao_);

      if (print_kab_) {
        fmt::print("\n");
        fmt::print("--********************************--\n");
        fmt::print("SD A{}, B{}\n", sdA, sdB);
        fmt::print("--********************************--\n");
      }

      if (use_matadj_) {
        // pull out needed intermediates
        auto Av_a = sd_a.const_pluck(sdA).const_slice(ne_a_, nmo_); // virtual alpha MOs for sdA
        auto Bv_a = sd_a.const_pluck(sdB).const_slice(ne_a_, nmo_); // virtual alpha MOs for sdB
        auto Av_b = sd_b.const_pluck(sdA).const_slice(ne_b_, nmo_); // virtual beta MOs for sdA
        auto Bv_b = sd_b.const_pluck(sdB).const_slice(ne_b_, nmo_); // virtual beta MOs for sdB

        auto Ao_a = AoUAB_a.const_pluck(sdA, sdB); // occupied alpha SVD basis MOs for sdA
        auto Bo_a = BoVAB_a.const_pluck(sdA, sdB); // occupied alpha SVD basis MOs for sdB
        auto Ao_b = AoUAB_b.const_pluck(sdA, sdB); // occupied beta SVD basis MOs for sdA
        auto Bo_b = BoVAB_b.const_pluck(sdA, sdB); // occupied beta SVD basis MOs for sdB

        auto U_a = UAB_a.const_pluck(sdA, sdB); // U matrix from SVD of alpha SAB overlap matrix
        auto U_b = UAB_b.const_pluck(sdA, sdB); // U matrix from SVD of beta SAB overlap matrix

        auto sig_a = sigmaAB_a.const_pluck(sdA, sdB); // alpha singular values
        auto sig_b = sigmaAB_b.const_pluck(sdA, sdB); // beta singular values
        auto detS_a = detSAB_a(sdA, sdB);             // determinant of alpha singular values
        auto detS_b = detSAB_b(sdA, sdB);             // determinant of beta singular values

        double detUVUV = det_UaVaUbVb(sdA, sdB);


        // calculate terms of vir,occ block of KAB matrix in SVD basis
        auto [voK1_1e_a, voK2_1e_a, voK3_1e_a] =
            get_kab_mo_vo_1e(detS_b, detUVUV, *hcore_, *overlap_, sig_a, sig_b, Ao_a, Bo_a, Ao_b,
                             Bo_b, Av_a, Bv_a, Av_b, Bv_b);
        auto [voK1_2e_a, voK2_2e_a, voK3_2e_a] =
            get_kab_mo_vo_2e_df(detS_b, detUVUV, *overlap_, sig_a, sig_b, Ao_a, Bo_a, Ao_b, Bo_b,
                                Av_a, Bv_a, Av_b, Bv_b);

        auto [voK1_1e_b, voK2_1e_b, voK3_1e_b] =
            get_kab_mo_vo_1e(detS_a, detUVUV, *hcore_, *overlap_, sig_b, sig_a, Ao_b, Bo_b, Ao_a,
                             Bo_a, Av_b, Bv_b, Av_a, Bv_a);
        auto [voK1_2e_b, voK2_2e_b, voK3_2e_b] =
            get_kab_mo_vo_2e_df(detS_a, detUVUV, *overlap_, sig_b, sig_a, Ao_b, Bo_b, Ao_a, Bo_a,
                                Av_b, Bv_b, Av_a, Bv_a);

        Tensor<2> K1K2K3_1e_a = voK1_1e_a + voK2_1e_a + voK3_1e_a;
        Tensor<2> K1K2K3_2e_a = voK1_2e_a + voK2_2e_a + voK3_2e_a;

        Tensor<2> K1K2K3_1e_b = voK1_1e_b + voK2_1e_b + voK3_1e_b;
        Tensor<2> K1K2K3_2e_b = voK1_2e_b + voK2_2e_b + voK3_2e_b;

        auto voK4_a =
            get_kab_mo_vo_ESA_term(ESA_coeffs_AB, detS_b, detUVUV, *overlap_, sig_a, Av_a, Bo_a);
        auto voK4_b =
            get_kab_mo_vo_ESA_term(ESA_coeffs_AB, detS_a, detUVUV, *overlap_, sig_b, Av_b, Bo_b);

        if (print_kab_) {
          fmt::print("-------------------\n");
          fmt::print("KAB Vir, Occ block:\n");
          fmt::print("-------------------\n");
          fmt::print("1e- terms:\n");
          fmt::print("  * K1_1e_alpha norm: {:1e}\n", voK1_1e_a.norm());
          fmt::print("  * K1_1e_beta norm: {:1e}\n", voK1_1e_b.norm());

          fmt::print("  * K2_1e_alpha norm: {:1e}\n", voK2_1e_a.norm());
          fmt::print("  * K2_1e_beta norm: {:1e}\n", voK2_1e_b.norm());

          fmt::print("  * K3_1e_alpha norm: {:1e}\n", voK3_1e_a.norm());
          fmt::print("  * K3_1e_beta norm: {:1e}\n", voK3_1e_b.norm());

          fmt::print("\n");
          fmt::print("  * K1+K2+K3_1e_alpha norm: {:1e}\n", K1K2K3_1e_a.norm());
          fmt::print("  * K1+K2+K3_1e_beta norm: {:1e}\n", K1K2K3_1e_b.norm());

          fmt::print("-------------------\n");
          fmt::print("2e- terms:\n");
          fmt::print("  * K1_2e_alpha norm: {:1e}\n", voK1_2e_a.norm());
          fmt::print("  * K1_2e_beta norm: {:1e}\n", voK1_2e_b.norm());

          fmt::print("  * K2_2e_alpha norm: {:1e}\n", voK2_2e_a.norm());
          fmt::print("  * K2_2e_beta norm: {:1e}\n", voK2_2e_b.norm());

          fmt::print("  * K3_2e_alpha norm: {:1e}\n", voK3_2e_a.norm());
          fmt::print("  * K3_2e_beta norm: {:1e}\n", voK3_2e_b.norm());

          fmt::print("\n");
          fmt::print("  * K1+K2+K3_2e_alpha norm: {:1e}\n", K1K2K3_2e_a.norm());
          fmt::print("  * K1+K2+K3_2e_beta norm: {:1e}\n", K1K2K3_2e_b.norm());

          fmt::print("-------------------\n");
          fmt::print("Total E terms (with state weights + coeffs):\n");
          fmt::print("  * K4_total_alpha norm: {:1e}\n", voK4_a.norm());
          fmt::print("  * K4_total_beta norm: {:1e}\n", voK4_b.norm());
          fmt::print("\n");
        }

        // collect vir,occ block terms
        Tensor<2> voKAB_total_a = (K1K2K3_1e_a + K1K2K3_2e_a) * weight_coeffs_AB + voK4_a;
        Tensor<2> voKAB_total_b = (K1K2K3_1e_b + K1K2K3_2e_b) * weight_coeffs_AB + voK4_b;

        if (print_kab_) {
          fmt::print("-------------------\n");
          fmt::print("Assembled KAB_vo matrix (SVD MO basis + state weights + coeffs):\n");
          fmt::print("  * KAB_vo_alpha norm: {:1e}\n", voKAB_total_a.norm());
          fmt::print("  * KAB_vo_beta norm: {:1e}\n", voKAB_total_b.norm());
          fmt::print("\n");
        }

        // transform vir,occ block to general MO basis
        gemm("n", "t", 1.0, voKAB_total_a, U_a, 0.0, voKAB_total_a);
        gemm("n", "t", 1.0, voKAB_total_b, U_b, 0.0, voKAB_total_b);

        // calculate terms of occ,occ block of KAB matrix in general MO basis
        Tensor<2> ooK3_a(ne_a_, ne_a_);
        Tensor<2> ooK4_a(ne_a_, ne_a_);
        for (ptrdiff_t i = 0; i < ne_a_; i++) {
          ooK3_a(i, i) = HAB(sdA, sdB) * weight_coeffs_AB;

          ooK4_a(i, i) = -1.0 * detS_a * detS_b * detUVUV * ESA_coeffs_AB;
        }

        Tensor<2> ooK3_b(ne_b_, ne_b_);
        Tensor<2> ooK4_b(ne_b_, ne_b_);
        for (ptrdiff_t i = 0; i < ne_b_; i++) {
          ooK3_b(i, i) = HAB(sdA, sdB) * weight_coeffs_AB;

          ooK4_b(i, i) = -1.0 * detS_a * detS_b * detUVUV * ESA_coeffs_AB;
        }

        // assemble KAB matrix in general MO basis
        Tensor<2> KAB_mo_a(nmo_, nmo_);
        KAB_mo_a.subtensor({0, 0}, {ne_a_, ne_a_}) = ooK3_a + ooK4_a;
        KAB_mo_a.subtensor({ne_a_, 0}, {nmo_, ne_a_}) = voKAB_total_a;

        Tensor<2> KAB_mo_b(nmo_, nmo_);
        KAB_mo_b.subtensor({0, 0}, {ne_b_, ne_b_}) = ooK3_b + ooK4_b;
        KAB_mo_b.subtensor({ne_b_, 0}, {nmo_, ne_b_}) = voKAB_total_b;

        if (print_kab_) {
          fmt::print("-------------------\n");
          fmt::print("Assembled KAB_vo matrix (general MO basis + state weights + coeffs):\n");
          fmt::print("  * KAB_vo_alpha norm: {:1e}\n", voKAB_total_a.norm());
          fmt::print("  * KAB_vo_beta norm: {:1e}\n", voKAB_total_b.norm());
          fmt::print("\n");

          fmt::print("-------------------\n");
          fmt::print("KAB Occ, Occ block:\n");
          fmt::print("-------------------\n");
          fmt::print("  * K3_oo_alpha norm: {:1e}\n", ooK3_a.norm());
          fmt::print("  * K3_oo_beta norm: {:1e}\n", ooK3_b.norm());
          fmt::print("\n");

          fmt::print("  * K4_oo_alpha norm: {:1e}\n", ooK4_a.norm());
          fmt::print("  * K4_oo_beta norm: {:1e}\n", ooK4_b.norm());
          fmt::print("\n");

          fmt::print("-------------------\n");
          fmt::print("Assembled KAB_oo matrix (general MO basis + state weights + coeffs):\n");
          Tensor<2> KAB_oo_a = ooK3_a + ooK4_a;
          fmt::print("  * KAB_oo_alpha norm: {:1e}\n", KAB_oo_a.norm());
          Tensor<2> KAB_oo_b = ooK3_b + ooK4_b;
          fmt::print("  * KAB_oo_beta norm: {:1e}\n", KAB_oo_b.norm());
          fmt::print("\n");

          fmt::print("-------------------\n");
          fmt::print("KAB in general MO basis (with state weights and coeffs):\n");
          fmt::print("-------------------\n");
          fmt::print("  * KAB_mo_alpha norm: {:1e}\n", KAB_mo_a.norm());
          fmt::print("  * KAB_mo_beta norm: {:1e}\n", KAB_mo_b.norm());
          fmt::print("\n");
        }

        // transform to AO basis
        Tensor<2> CA_ortho_a(nao_, nmo_);
        gemm("n", "n", 1.0, *overlap_, sd_a.const_pluck(sdA), 0.0, CA_ortho_a);

        matrix_transform("n", "t", 1.0, CA_ortho_a, KAB_mo_a, CA_ortho_a, 0.0, KAB_ao_a);

        Tensor<2> CA_ortho_b(nao_, nmo_);
        gemm("n", "n", 1.0, *overlap_, sd_b.const_pluck(sdA), 0.0, CA_ortho_b);

        matrix_transform("n", "t", 1.0, CA_ortho_b, KAB_mo_b, CA_ortho_b, 0.0, KAB_ao_b);
      } else {
        // now let's do things with non-matadj style!
        Tensor<2> FAB_a(nao_, nao_);
        FAB_a = *hcore_ + GAB_a.const_pluck(sdA, sdB);
        Tensor<2> FAB_b(nao_, nao_);
        FAB_b = *hcore_ + GAB_b.const_pluck(sdA, sdB);

        auto W_a = WAB_a.const_pluck(sdA, sdB);
        auto W_b = WAB_b.const_pluck(sdA, sdB);

        Tensor<2> WS_a(nao_, nao_);
        gemm("n", "n", 1.0, W_a, *overlap_, 0.0, WS_a);
        Tensor<2> SW_a(nao_, nao_);
        gemm("n", "n", 1.0, *overlap_, W_a, 0.0, SW_a);

        Tensor<2> WS_b(nao_, nao_);
        gemm("n", "n", 1.0, W_b, *overlap_, 0.0, WS_b);
        Tensor<2> SW_b(nao_, nao_);
        gemm("n", "n", 1.0, *overlap_, W_b, 0.0, SW_b);

        double detsab = detSAB_a(sdA, sdB) * detSAB_b(sdA, sdB) * det_UaVaUbVb(sdA, sdB);

        Tensor<2> K1_a(nao_, nao_);
        gemm("n", "n", 1.0, FAB_a, WS_a, 0.0, K1_a);
        Tensor<2> K1_b(nao_, nao_);
        gemm("n", "n", 1.0, FAB_b, WS_b, 0.0, K1_b);

        Tensor<2> K2_a(nao_, nao_);
        {
          Tensor<2> tmp(nao_, nao_);
          gemm("n", "n", 1.0, SW_a, FAB_a, 0.0, tmp);
          gemm("n", "n", 1.0, tmp, WS_a, 0.0, K2_a);
        }
        Tensor<2> K2_b(nao_, nao_);
        {
          Tensor<2> tmp(nao_, nao_);
          gemm("n", "n", 1.0, SW_b, FAB_b, 0.0, tmp);
          gemm("n", "n", 1.0, tmp, WS_b, 0.0, K2_b);
        }

        Tensor<2> K3_a(nao_, nao_);
        gemm("n", "n", 1.0, *overlap_, WS_a, 0.0, K3_a);
        Tensor<2> K3_b(nao_, nao_);
        gemm("n", "n", 1.0, *overlap_, WS_b, 0.0, K3_b);

        KAB_ao_a = detsab * weight_coeffs_AB * (K1_a - K2_a)
                   + (K3_a * HAB(sdA, sdB) * weight_coeffs_AB)
                   - (K3_a * ESA_coeffs_AB * detsab);
        KAB_ao_b = detsab * weight_coeffs_AB * (K1_b - K2_b)
                   + (K3_b * HAB(sdA, sdB) * weight_coeffs_AB)
                   - (K3_b * ESA_coeffs_AB * detsab);
      }

      if (print_kab_) {
        fmt::print("-------------------\n");
        fmt::print("KAB in AO basis (with state weights and coeffs):\n");
        fmt::print("-------------------\n");
        fmt::print("  * KAB_ao_alpha norm: {:1e}\n", KAB_ao_a.norm());
        fmt::print("  * KAB_ao_beta norm: {:1e}\n", KAB_ao_b.norm());
        fmt::print("\n");
      }

      // assemble for fock matrices!!!
      aFA_ao += KAB_ao_a + KAB_ao_a.transpose();
      bFA_ao += KAB_ao_b + KAB_ao_b.transpose();

    } // move to next sdB (KAB matrix generation)
  } // move to next sdA (Fock matrix)

  // calculate orbital gradients
  auto a_fockerr = orbital_gradients(*a_fock_, sd_a, *overlap_, ne_a_);
  auto b_fockerr = orbital_gradients(*b_fock_, sd_b, *overlap_, ne_b_);

  // store orbital gradients for access later
  fockerr_ = make_shared<Tensor<4>>(nao_, nao_, nsd_, 2);
  fockerr_->pluck(0) = a_fockerr;
  fockerr_->pluck(1) = b_fockerr;

  // return fock matrices!
  Tensor<4> fock(nao_, nao_, nsd_, 2);
  fock.pluck(0) = *a_fock_;
  fock.pluck(1) = *b_fock_;

  return fock;

} // end operator()

///////////////////////////////////////////////////////////////////////////////////////
//      Begin uResFockBuilder Class Functions                                        //
///////////////////////////////////////////////////////////////////////////////////////

tuple<Tensor<1>, Tensor<2>, Tensor<2>, Tensor<2>, Tensor<2>> uResFockBuilder::get_mo_overlap_svd(
    const ConstTensorView<2>& CA, const ConstTensorView<2>& CB, const ConstTensorView<2>& sao,
    const double& floor)
{
  assert(CA.size() == CB.size());
  assert(sao.extent(0) == CA.extent(0) && sao.extent(1) == CA.extent(0));
  assert(floor >= 0.0);

  ptrdiff_t nmo = CA.extent(1);
  ptrdiff_t nao = sao.extent(0);

  Tensor<2> Smo(nmo, nmo);
  {
    Tensor<2> tmp(nmo, nao);
    gemm("t", "n", 1.0, CA, sao, 0.0, tmp);
    gemm("n", "n", 1.0, tmp, CB, 0.0, Smo);
  }

  auto [sigma, U, Vt] = linalg::svd(Smo);

  // apply SVD floor to singular values
  const double smallest_singular = sigma(nmo - 1) / (nmo > 1 ? sigma(0) : 1.0);
  if (smallest_singular < floor) {
    sigma = max(sigma, (nmo > 1 ? sigma(0) : 1.0) * floor);
  }

  // transform input MOs into SVD basis
  Tensor<2> AU(CA.zeros_like());
  gemm("n", "n", 1.0, CA, U, 0.0, AU);

  Tensor<2> BV(CB.zeros_like());
  gemm("n", "t", 1.0, CB, Vt, 0.0, BV);

  return make_tuple(sigma, U, Vt.transpose(), AU, BV);

  // ideas for unit testing:
  // check that Smo = U sigma Vt
  // check that sigma = AU Smo BV.t
  // check that SVD floor does what I want it to
}

Tensor<2> uResFockBuilder::get_density(const ConstTensorView<1>& sigma,
                                       const ConstTensorView<2>& AU, const ConstTensorView<2>& BV)
{
  assert(AU.size() == BV.size());
  assert(AU.extent(1) == sigma.size());

  // calculate psuedoinverse and determinant product (numerically stable!)
  auto xi_i = excluded_product_1(sigma);

  // calculate density matrix with determinant incorporated (numerically stable!)
  ptrdiff_t nao = AU.extent(0);
  Tensor<2> Q(nao, nao);
  weighted_gemm("n", "t", 1.0, BV, xi_i, AU, 0.0, Q);

  return Q;

  // ideas for unit testing:
  // would still be idempotent for AA case (since detSAB = 1)?
}

Tensor<2> uResFockBuilder::get_unstable_density(const ConstTensorView<1>& sigma,
                                                const ConstTensorView<2>& AU,
                                                const ConstTensorView<2>& BV)
{
  assert(AU.size() == BV.size());
  assert(AU.extent(1) == sigma.size());

  // calculate psuedoinverse (numerically unstable!)
  Tensor<1> invsig(sigma.zeros_like());
  for (ptrdiff_t i = 0; i < sigma.size(); i++) {
    invsig(i) = 1.0 / sigma(i);
  }

  // calculate density matrix (numerically unstable!)
  ptrdiff_t nao = AU.extent(0);
  Tensor<2> W(nao, nao);
  weighted_gemm("n", "t", 1.0, BV, invsig, AU, 0.0, W);

  return W;
}

double uResFockBuilder::get_ham_1e(const ConstTensorView<2>& hcore, const ConstTensorView<2>& Q,
                                   const double& detSAB, const double& detUVUV)
{
  Tensor<2> tmp(hcore.zeros_like());
  gemm("n", "n", detSAB * detUVUV, hcore, Q, 0.0, tmp);
  double ham1e = linalg::trace(tmp);

  return ham1e;
  // re-use this for the non-matadj case, just modify the inputs!!!!
  // hopefully I have all the determinants correctly included...
  // won't be able to tell really for this AA case, since detSAA = 1.0
  // should I use this as a "unit test" for the density?
}

double uResFockBuilder::get_ham_2e_df(const double& detSAB_a, const double& detSAB_b,
                                      const double& detUVUV, const ConstTensorView<1>& sig_a,
                                      const ConstTensorView<1>& sig_b,
                                      const ConstTensorView<2>& Aoa, const ConstTensorView<2>& Boa,
                                      const ConstTensorView<2>& Aob, const ConstTensorView<2>& Bob)
{
  assert(geometry_.density_fitting());
  const auto& uvP = geometry_.density_fitting()->B(); // (uv|Q)(Q|P)^(-1/2)
  ptrdiff_t nfit = uvP.extent(2);

  ptrdiff_t ne_a = sig_a.size();
  ptrdiff_t ne_b = sig_b.size();

  assert(Aoa.extent(0) == nao_ && Aoa.extent(1) == ne_a);
  assert(Aoa.size() == Boa.size());

  assert(Aob.extent(0) == nao_ && Aob.extent(1) == ne_b);
  assert(Aob.size() == Bob.size());

  auto [xi_j_a, xi_jk_a] = excluded_product_2(sig_a);
  auto [xi_j_b, xi_jk_b] = excluded_product_2(sig_b);

  // 3-center integrals with half of available AO indexes contracted to MO
  Tensor<3> halfAoa(ne_a, nao_, nfit);
  contract(1.0, uvP, "uvP", Aoa, "uj", 0.0, halfAoa, "jvP");
  Tensor<3> halfAob(ne_b, nao_, nfit);
  contract(1.0, uvP, "uvP", Aob, "uj", 0.0, halfAob, "jvP");

  // 3-center integrals with all of available AO indexes contracted to MOs
  Tensor<3> AoaBoa_P(ne_a, ne_a, nfit);
  contract(1.0, halfAoa, "jvP", Boa, "vk", 0.0, AoaBoa_P, "jkP");
  Tensor<3> AobBob_P(ne_b, ne_b, nfit);
  contract(1.0, halfAob, "jvP", Bob, "vk", 0.0, AobBob_P, "jkP");

  // Sdet_B coefficients with Xi_i term incorporated
  Tensor<2> BoaXoa(Boa);
  for (ptrdiff_t i = 0; i < ne_a; ++i) {
    BoaXoa.pluck(i) *= xi_j_a(i);
  }

  Tensor<2> BobXob(Bob);
  for (ptrdiff_t i = 0; i < ne_b; ++i) {
    BobXob.pluck(i) *= xi_j_b(i);
  }

  // * * * * * * * * * * * * * * * * * //
  //  Building and Exporting H_AB_2e  //
  // * * * * * * * * * * * * * * * * //

  Tensor<1> h2e_alpha_only(1);
  Tensor<1> h2e_beta_only(1);
  Tensor<1> h2e_mixed(1);

  // Build alpha-alpha exchange (K) term
  Tensor<3> AoaBoaXooa_P(ne_a, ne_a, nfit);
  contract(-1.0, AoaBoa_P, "kjP", xi_jk_a, "jk", 0.0, AoaBoaXooa_P, "kjP");
  contract(1.0, AoaBoa_P, "jkP", AoaBoaXooa_P, "kjP", 0.0, h2e_alpha_only, "D");

  // Build beta-beta exchange (K) term
  Tensor<3> AobBobXoob_P(ne_b, ne_b, nfit);
  contract(-1.0, AobBob_P, "kjP", xi_jk_b, "jk", 0.0, AobBobXoob_P, "kjP");
  contract(1.0, AobBob_P, "jkP", AobBobXoob_P, "kjP", 0.0, h2e_beta_only, "D");

  // Build alpha-alpha coulomb (J) term
  Tensor<2> AoaBoaXooa_jj_P(ne_a, nfit);
  contract(1.0, AoaBoa_P, "kkP", xi_jk_a, "jk", 0.0, AoaBoaXooa_jj_P, "jP");
  contract(1.0, AoaBoa_P, "jjP", AoaBoaXooa_jj_P, "jP", 1.0, h2e_alpha_only, "D");
  h2e_alpha_only *= detSAB_b * detUVUV;

  // Build beta-beta coulomb (J) term
  Tensor<2> AobBobXoob_jj_P(ne_b, nfit);
  contract(1.0, AobBob_P, "kkP", xi_jk_b, "jk", 0.0, AobBobXoob_jj_P, "jP");
  contract(1.0, AobBob_P, "jjP", AobBobXoob_jj_P, "jP", 1.0, h2e_beta_only, "D");
  h2e_beta_only *= detSAB_a * detUVUV;

  // Build alpha-beta coulomb (J) term(s)
  Tensor<1> AoaBoaXoa_full(nfit);
  contract(1.0, halfAoa, "jvP", BoaXoa, "vj", 0.0, AoaBoaXoa_full, "P");
  Tensor<1> AobBobXob_full(nfit);
  contract(1.0, halfAob, "jvP", BobXob, "vj", 0.0, AobBobXob_full, "P");
  contract(2.0 * detUVUV, AoaBoaXoa_full, "P", AobBobXob_full, "P", 0.0, h2e_mixed, "D");

  double h2e = 0.5 * (h2e_alpha_only(0) + h2e_beta_only(0) + h2e_mixed(0));
  return h2e;
}
double uResFockBuilder::get_ham_2e_og(const double& detSAB, const double& detUVUV,
                                      const ConstTensorView<2>& G_a, const ConstTensorView<2>& G_b,
                                      const ConstTensorView<2>& W_a, const ConstTensorView<2>& W_b)
{
  double h2e = 0.0;

  Tensor<2> tmp_a(W_a.zeros_like());
  gemm("n", "n", detSAB * detUVUV, G_a, W_a, 0.0, tmp_a);
  h2e += linalg::trace(tmp_a);

  Tensor<2> tmp_b(W_b.zeros_like());
  gemm("n", "n", detSAB * detUVUV, G_b, W_b, 0.0, tmp_b);
  h2e += linalg::trace(tmp_b);

  return h2e;
}

Tensor<2> uResFockBuilder::get_gaa_ao_df(const ConstTensorView<2>& Ao_up,
                                         const ConstTensorView<2>& Ao_down)
{
  ptrdiff_t nao = Ao_up.extent(0);
  assert(Ao_down.extent(0) == nao);

  ptrdiff_t nocc_up = Ao_up.extent(1);
  ptrdiff_t nocc_down = Ao_down.extent(1);

  const auto& B = geometry_.density_fitting()->B(); // (uv|Q)(Q|P)^(-1/2)
  ptrdiff_t nfit = B.extent(2);

  // Half contracted 3 center term with spin up MOs
  Tensor<3> half_up(nao, nocc_up, nfit);
  contract(1.0, B, "uvP", Ao_up, "vi", 0.0, half_up, "uiP");

  // Half contracted 3 center term with spin down MOs
  Tensor<3> half_down(nao, nocc_down, nfit);
  contract(1.0, B, "uvP", Ao_down, "vi", 0.0, half_down, "uiP");

  // Build GAA matrix
  Tensor<2> GAA(nao, nao);

  // Build alpha K term into GAA
  contract(-1.0, half_up, "uiP", half_up, "viP", 1.0, GAA, "uv");

  // Build alpha J term into GAA
  Tensor<1> full_up(nfit);
  contract(1.0, half_up, "uiP", Ao_up, "ui", 0.0, full_up, "P");
  contract(1.0, full_up, "P", B, "uvP", 1.0, GAA, "uv");

  // Build beta J term into GAA
  Tensor<1> full_down(nfit);
  contract(1.0, half_down, "uiP", Ao_down, "ui", 0.0, full_down, "P");
  contract(1.0, full_down, "P", B, "uvP", 1.0, GAA, "uv");

  return GAA;
} // end GAA explicit builder

Tensor<2> uResFockBuilder::get_gab_no_df(const ConstTensorView<2>& W_up,
                                         const ConstTensorView<2>& W_down)
{
  assert(W_up.size() == W_down.size());
  ptrdiff_t nao = W_up.extent(0);

  const auto& uvP = geometry_.density_fitting()->B(); // (uv|Q)(Q|P)^(-1/2)
  Tensor<4> ao_integral(nao, nao, nao, nao);
  contract(1.0, uvP, "uvP", uvP, "loP", 0.0, ao_integral, "uvlo");

  Tensor<2> G(nao, nao);
  contract(1.0, W_up, "ol", ao_integral, "uvlo", 0.0, G, "uv");
  contract(1.0, W_down, "ol", ao_integral, "uvlo", 1.0, G, "uv");

  contract(-1.0, W_up, "ol", ao_integral, "uolv", 1.0, G, "uv");

  return G;
}

Tensor<2> uResFockBuilder::get_gab_ao_og(const ConstTensorView<2>& Ao_up,
                                         const ConstTensorView<2>& Ao_down,
                                         const ConstTensorView<2>& Bo_up,
                                         const ConstTensorView<2>& Bo_down,
                                         const ConstTensorView<1>& invsig_up,
                                         const ConstTensorView<1>& invsig_down)
{
  ptrdiff_t nao = Ao_up.extent(0);
  assert(Ao_down.extent(0) == nao);
  assert(Bo_up.extent(0) == nao);
  assert(Bo_down.extent(0) == nao);

  ptrdiff_t nocc_up = Ao_up.extent(1);
  assert(Bo_up.extent(1) == nocc_up);
  assert(invsig_up.extent(0) == nocc_up);
  ptrdiff_t nocc_down = Ao_down.extent(1);
  assert(Bo_down.extent(1) == nocc_down);
  assert(invsig_down.extent(0) == nocc_down);

  const auto& uvP = geometry_.density_fitting()->B(); // (uv|Q)(Q|P)^(-1/2)
  ptrdiff_t nfit = uvP.extent(2);

  // transform coeffs with invsig
  qleve::Tensor<2> Aup(Ao_up);
  qleve::Tensor<2> Bup(Bo_up);
  for (ptrdiff_t i = 0; i < nocc_up; ++i) {
    Aup.pluck(i) *= std::sqrt(invsig_up(i));
    Bup.pluck(i) *= std::sqrt(invsig_up(i));
  }

  qleve::Tensor<2> Adown(Ao_down);
  qleve::Tensor<2> Bdown(Bo_down);
  for (ptrdiff_t i = 0; i < nocc_down; ++i) {
    Adown.pluck(i) *= std::sqrt(invsig_down(i));
    Bdown.pluck(i) *= std::sqrt(invsig_down(i));
  }

  // build gamma_AB matrices
  Tensor<2> gamma_up(nao, nao);
  weighted_gemm("n", "t", 1.0, Bo_up, invsig_up, Ao_up, 0.0, gamma_up);
  Tensor<2> gamma_down(nao, nao);
  weighted_gemm("n", "t", 1.0, Bo_down, invsig_down, Ao_down, 0.0, gamma_down);

  // build half contracted integrals
  Tensor<3> halfAo_up(nocc_up, nao, nfit);
  contract(1.0, uvP, "uvP", Aup, "uj", 0.0, halfAo_up, "jvP");
  Tensor<3> halfBo_up(nao, nocc_up, nfit);
  contract(1.0, uvP, "uvP", Bup, "vj", 0.0, halfBo_up, "ujP");

  Tensor<3> halfAo_down(nocc_down, nao, nfit);
  contract(1.0, uvP, "uvP", Adown, "uj", 0.0, halfAo_down, "jvP");
  Tensor<3> halfBo_down(nao, nocc_down, nfit);
  contract(1.0, uvP, "uvP", Bdown, "vj", 0.0, halfBo_down, "ujP");

  // build fully contracted integrals
  Tensor<1> full_up(nfit);
  contract(1.0, halfAo_up, "jvP", Bup, "vj", 0.0, full_up, "P");
  Tensor<1> full_down(nfit);
  contract(1.0, halfAo_down, "jvP", Bdown, "vj", 0.0, full_down, "P");

  // build GAB matrix!
  Tensor<2> GAB(nao, nao);

  // spin up coloumb term
  contract(0.5, uvP, "uvP", full_up, "P", 1.0, GAB, "vu");

  // spin down coloumb term
  contract(0.5, uvP, "uvP", full_down, "P", 1.0, GAB, "vu");

  // spin up exchange term
  contract(-0.5, halfAo_up, "jvP", halfBo_up, "ujP", 1.0, GAB, "uv");

  return GAB;
}

tuple<Tensor<2>, Tensor<2>, Tensor<2>> uResFockBuilder::get_kab_mo_vo_1e(
    const double& detSAB_down, const double& detUVUV, const ConstTensorView<2>& hcore,
    const ConstTensorView<2>& sao, const ConstTensorView<1>& sig_up,
    const ConstTensorView<1>& sig_down, const ConstTensorView<2>& Ao_up,
    const ConstTensorView<2>& Bo_up, const ConstTensorView<2>& Ao_down,
    const ConstTensorView<2>& Bo_down, const ConstTensorView<2>& Av_up,
    const ConstTensorView<2>& Bv_up, const ConstTensorView<2>& Av_down,
    const ConstTensorView<2>& Bv_down)
{
  ptrdiff_t nao = hcore.extent(0);

  ptrdiff_t ne_up = sig_up.size();
  ptrdiff_t ne_down = sig_down.size();
  ptrdiff_t nvir_up = Av_up.extent(1);
  ptrdiff_t nvir_down = Av_down.extent(1);

  assert(hcore.size() == sao.size());
  assert(Ao_up.size() == Bo_up.size());
  assert(Ao_down.size() == Bo_down.size());
  assert(Av_up.size() == Bv_up.size());
  assert(Av_down.size() == Bv_down.size());

  assert(Av_up.extent(0) == nao && Av_down.extent(0) == nao);
  assert(Ao_up.extent(0) == nao && Ao_down.extent(0) == nao);

  Tensor<2> uu_sAB_vo(nvir_up, ne_up);
  matrix_transform("t", "n", 1.0, Av_up, sao, Bo_up, 0.0, uu_sAB_vo);

  Tensor<2> uu_hAB_vo(nvir_up, ne_up);
  matrix_transform("t", "n", 1.0, Av_up, hcore, Bo_up, 0.0, uu_hAB_vo);
  Tensor<2> uu_hAB_oo(ne_up, ne_up);
  matrix_transform("t", "n", 1.0, Ao_up, hcore, Bo_up, 0.0, uu_hAB_oo);
  Tensor<2> dd_hAB_oo(ne_down, ne_down);
  matrix_transform("t", "n", 1.0, Ao_down, hcore, Bo_down, 0.0, dd_hAB_oo);

  auto [xi_o_up, xi_oo_up] = excluded_product_2(sig_up);
  auto xi_o_down = excluded_product_1(sig_down);

  Tensor<2> xi_oo_det(xi_oo_up * detSAB_down * detUVUV);
  for (ptrdiff_t i = 0; i < ne_up; i++) {
    xi_oo_det(i, i) = detUVUV;
  }

  qleve::Tensor<2> uxi_i_dxi_j_det(ne_up, ne_down);
  for (ptrdiff_t iu = 0; iu < ne_up; iu++) {
    for (ptrdiff_t jd = 0; jd < ne_down; jd++) {
      uxi_i_dxi_j_det(iu, jd) = xi_o_up(iu) * xi_o_down(jd) * detUVUV;
      if (iu == jd) {
        uxi_i_dxi_j_det(iu, jd) = detUVUV;
      }
    }
  }

  qleve::Tensor<2> K1_1e(nvir_up, ne_up);
  qleve::Tensor<2> K2_1e(nvir_up, ne_up);
  qleve::Tensor<2> K3_1e(nvir_up, ne_up);
  for (ptrdiff_t a = 0; a < nvir_up; a++) {
    for (ptrdiff_t i = 0; i < ne_up; i++) {
      K1_1e(a, i) += uu_hAB_vo(a, i) * xi_o_up(i) * detSAB_down * detUVUV; // blue

      for (ptrdiff_t ju = 0; ju < ne_up; ju++) {
        K3_1e(a, i) += uu_sAB_vo(a, i) * uu_hAB_oo(ju, ju) * xi_oo_det(ju, i); // green

        K2_1e(a, i) -= uu_sAB_vo(a, ju) * uu_hAB_oo(ju, i) * xi_oo_det(ju, i); // yellow
      }

      for (ptrdiff_t jd = 0; jd < ne_down; jd++) {
        K3_1e(a, i) += uu_sAB_vo(a, i) * dd_hAB_oo(jd, jd) * uxi_i_dxi_j_det(i, jd); // green
      }
    }
  }

  return make_tuple(K1_1e, K2_1e, K3_1e);
} // end KAB vir,occ 1e- term builder

tuple<Tensor<2>, Tensor<2>, Tensor<2>> uResFockBuilder::get_kab_mo_vo_2e_df(
    const double& detSAB_down, const double& detUVUV, const ConstTensorView<2>& sao,
    const ConstTensorView<1>& sig_up, const ConstTensorView<1>& sig_down,
    const ConstTensorView<2>& Ao_up, const ConstTensorView<2>& Bo_up,
    const ConstTensorView<2>& Ao_down, const ConstTensorView<2>& Bo_down,
    const ConstTensorView<2>& Av_up, const ConstTensorView<2>& Bv_up,
    const ConstTensorView<2>& Av_down, const ConstTensorView<2>& Bv_down)
{
  ptrdiff_t ne_up = sig_up.size();
  ptrdiff_t ne_down = sig_down.size();
  assert(Ao_up.extent(1) == sig_up.size());
  assert(Ao_down.extent(1) == sig_down.size());

  ptrdiff_t nao = Ao_up.extent(0);
  ptrdiff_t nvir_up = Av_up.extent(1);
  ptrdiff_t nvir_down = Av_down.extent(1);

  assert(Ao_up.size() == Bo_up.size());
  assert(Ao_down.size() == Bo_down.size());
  assert(Av_up.size() == Bv_up.size());
  assert(Av_down.size() == Bv_down.size());

  // build 2e- integral explicitly in AO basis
  const auto& uvP = geometry_.density_fitting()->B(); // (uv|Q)(Q|P)^(-1/2)
  ptrdiff_t nfit = uvP.extent(2);

  auto [u_xi_o, u_xi_oo, u_xi_ooo] = excluded_product_3(sig_up);
  auto [d_xi_o, d_xi_oo] = excluded_product_2(sig_down);

  Tensor<3> u_xi_ooo_det(u_xi_ooo * detSAB_down * detUVUV);
  for (ptrdiff_t i = 0; i < ne_up; i++) {
    for (ptrdiff_t j = 0; j < ne_up; j++) {
      u_xi_ooo_det(j, j, i) = 1.0;
      u_xi_ooo_det(j, i, j) = 1.0;
      u_xi_ooo_det(i, j, j) = 1.0;
    }
  }

  Tensor<2> u_xi_oo_det(u_xi_oo * detSAB_down * detUVUV);
  for (ptrdiff_t i = 0; i < ne_up; i++) {
    u_xi_oo_det(i, i) = 1.0;
  }

  Tensor<2> uu_sAB_vo(nvir_up, ne_up);
  matrix_transform("t", "n", 1.0, Av_up, sao, Bo_up, 0.0, uu_sAB_vo);

  // 3-center integrals with half of available AO indexes contracted to MO
  Tensor<3> halfAvu(nvir_up, nao_, nfit);
  contract(1.0, uvP, "uvP", Av_up, "ua", 0.0, halfAvu, "avP");
  Tensor<3> halfAou(ne_up, nao_, nfit);
  contract(1.0, uvP, "uvP", Ao_up, "uj", 0.0, halfAou, "jvP");
  Tensor<3> halfAod(ne_down, nao_, nfit);
  contract(1.0, uvP, "uvP", Ao_down, "uj", 0.0, halfAod, "jvP");

  // 3-center integrals with all of available AO indexes contracted to MOs
  Tensor<3> AvuBou_P(nvir_up, ne_up, nfit);
  contract(1.0, halfAvu, "avP", Bo_up, "vk", 0.0, AvuBou_P, "akP");
  Tensor<3> AouBou_P(ne_up, ne_up, nfit);
  contract(1.0, halfAou, "jvP", Bo_up, "vk", 0.0, AouBou_P, "jkP");
  Tensor<3> AodBod_P(ne_down, ne_down, nfit);
  contract(1.0, halfAod, "jvP", Bo_down, "vk", 0.0, AodBod_P, "jkP");

  // Sdet_B coefficients with Xi_i term incorporated
  Tensor<2> BouXou(Bo_up);
  for (ptrdiff_t i = 0; i < ne_up; ++i) {
    BouXou.pluck(i) *= u_xi_o(i);
  }

  Tensor<2> BodXod(Bo_down);
  for (ptrdiff_t i = 0; i < ne_down; ++i) {
    BodXod.pluck(i) *= d_xi_o(i);
  }

  // Contraction of MO 3-center integrals with various excluded products (Xi)
  Tensor<1> AodBodXod_coul(nfit); // (Aj Bj | P) Xi_j --> T_P (spin down)
  contract(1.0, halfAod, "jvP", BodXod, "vj", 0.0, AodBodXod_coul, "P");
  Tensor<3> AodBodXood_P_exch(ne_down, ne_down, nfit); // (Aj Bk | P) Xi_jk --> T_jkP (spin down)
  contract(1.0, AodBod_P, "jkP", d_xi_oo, "jk", 0.0, AodBodXood_P_exch, "jkP");

  Tensor<2> AouBouXoou_P_coul(ne_up, nfit); // (Aj Bj | P) Xi_ji --> T_iP (spin up)
  contract(1.0, AouBou_P, "jjP", u_xi_oo, "ji", 0.0, AouBouXoou_P_coul, "iP");
  Tensor<2> AouBouXoouDet_P_coul(ne_up, nfit); // (Aj Bj | P) Xi_ji |det| --> T_iP (spin up)
  contract(1.0, AouBou_P, "jjP", u_xi_oo_det, "ji", 0.0, AouBouXoouDet_P_coul, "iP");
  Tensor<3> AouBouXoouDet_P_exch(ne_up, ne_up, nfit); // (Aj Bi | P) Xi_ji |det| --> T_jiP (spin up)
  contract(1.0, AouBou_P, "jiP", u_xi_oo_det, "ji", 0.0, AouBouXoouDet_P_exch, "jiP");

  // (Ak Bk | P) Xi_ijk |det| --> T_jiP (spin up)
  Tensor<3> AouBouXooouDet_P_coul(ne_up, ne_up, nfit);
  contract(1.0, u_xi_ooo_det, "ijk", AouBou_P, "kkP", 0.0, AouBouXooouDet_P_coul, "jiP");
  // (Ak Bi | P) Xi_ijk |det| --> T_kijP (spin up)
  Tensor<4> AouBouXooouDet_P_exch_ik(ne_up, ne_up, ne_up, nfit);
  contract(1.0, u_xi_ooo_det, "ijk", AouBou_P, "kiP", 0.0, AouBouXooouDet_P_exch_ik, "kijP");
  // (Aj Bk | P) Xi_ijk |det| --> T_ijkP (spin up)
  Tensor<4> AouBouXooouDet_P_exch_jk(ne_up, ne_up, ne_up, nfit);
  contract(1.0, u_xi_ooo_det, "ijk", AouBou_P, "jkP", 0.0, AouBouXooouDet_P_exch_jk, "ijkP");

  // *** //
  Tensor<2> K1_2e(nvir_up, ne_up);
  // *** //

  Tensor<3> AvuBouXou_P(nvir_up, ne_up, nfit); // (Aa Bj | P) Xi_j --> T_ajP (spin up)
  contract(1.0, halfAvu, "avP", BouXou, "vj", 0.0, AvuBouXou_P, "ajP");

  // K1_2e coulomb term (spin up)
  contract(1.0, AvuBou_P, "aiP", AouBouXoouDet_P_coul, "iP", 1.0, K1_2e, "ai");

  // K1_2e exchange term
  contract(-1.0, AvuBou_P, "ajP", AouBouXoouDet_P_exch, "jiP", 1.0, K1_2e, "ai");

  // K1_2e coulomb term (spin down)
  contract(detUVUV, AvuBouXou_P, "aiP", AodBodXod_coul, "P", 1.0, K1_2e, "ai");

  // *** //
  Tensor<2> K2_2e(nvir_up, ne_up);
  // *** //

  Tensor<4> AouBouSvou_P(nvir_up, ne_up, ne_up, nfit); // S_aj (Aj Bi | P) --> T_ajiP (spin up)
  contract(1.0, uu_sAB_vo, "aj", AouBou_P, "jiP", 0.0, AouBouSvou_P, "ajiP");

  Tensor<3> SvouXoou(nvir_up, ne_up, ne_up); // S_aj Xi_ij --> T_aji (spin up)
  contract(1.0, uu_sAB_vo, "aj", u_xi_oo, "ij", 0.0, SvouXoou, "aji");
  Tensor<3> AouBouSvouXoou_P(nvir_up, ne_up, nfit); // S_aj Xi_ij (Aj Bi | P) --> T_aiP (spin up)
  contract(1.0, SvouXoou, "aji", AouBou_P, "jiP", 0.0, AouBouSvouXoou_P, "aiP");

  // K2_2e coulomb term (spin up) - matches!
  contract(-1.0, AouBouSvou_P, "ajiP", AouBouXooouDet_P_coul, "jiP", 1.0, K2_2e, "ai");

  // K2_2e exchange term - matches!
  contract(1.0, AouBouSvou_P, "ajkP", AouBouXooouDet_P_exch_ik, "kijP", 1.0, K2_2e, "ai");

  // K2_2e coulomb term (spin down) - matches!
  contract(-1.0 * detUVUV, AouBouSvouXoou_P, "aiP", AodBodXod_coul, "P", 1.0, K2_2e, "ai");

  // *** //
  Tensor<2> K3_2e(nvir_up, ne_up);
  // *** //

  // (Aj Bj | P) (P | Ak Bk) --> T_jk (spin up)
  Tensor<2> AouBouAouBou_coul(ne_up, ne_up);
  contract(1.0, AouBou_P, "jjP", AouBou_P, "kkP", 0.0, AouBouAouBou_coul, "jk");
  // (Aj Bj | P) (P | Ak Bk) Xi_ijk |det| --> T_i (spin up)
  Tensor<1> XooouDetAouBouAouBou_coul(ne_up);
  contract(1.0, AouBouAouBou_coul, "jk", u_xi_ooo_det, "ijk", 0.0, XooouDetAouBouAouBou_coul, "i");
  // (Aj Bk | P) (P | Ak Bj) Xi_ijk |det| --> T_i (spin up)
  Tensor<1> XooouDetAouBouAouBou_exch(ne_up);
  contract(1.0, AouBouXooouDet_P_exch_jk, "ijkP", AouBou_P, "kjP", 0.0, XooouDetAouBouAouBou_exch,
           "i");

  // K3_2e coulomb term (spin up only)
  contract(0.5, uu_sAB_vo, "ai", XooouDetAouBouAouBou_coul, "i", 1.0, K3_2e, "ai");

  // K3_2e exchange (spin up)
  contract(-0.5, uu_sAB_vo, "ai", XooouDetAouBouAouBou_exch, "i", 1.0, K3_2e, "ai");

  // (Aj Bj | P) (P | Ak Bk) --> T_jk (spin down)
  Tensor<2> AodBodAodBod_coul(ne_down, ne_down);
  contract(1.0, AodBod_P, "jjP", AodBod_P, "kkP", 0.0, AodBodAodBod_coul, "jk");
  // (Aj Bj | P) (P | Ak Bk) Xi_jk |det| --> scalar (spin down)
  Tensor<1> XoodAodBodAodBod_coul(1);
  contract(detUVUV, AodBodAodBod_coul, "jk", d_xi_oo, "jk", 0.0, XoodAodBodAodBod_coul, "D");
  // (Aj Bk | P) (P | Ak Bj) Xi_jk |det| --> scalar (spin down)
  Tensor<1> XoodAodBodAodBod_exch(1);
  contract(detUVUV, AodBodXood_P_exch, "jkP", AodBod_P, "kjP", 0.0, XoodAodBodAodBod_exch, "D");

  // K3_2e coulomb term (spin down only)
  contract(0.5 * XoodAodBodAodBod_coul(0), uu_sAB_vo, "ai", u_xi_o, "i", 1.0, K3_2e, "ai");

  // K3_2e exchange (spin down)
  contract(-0.5 * XoodAodBodAodBod_exch(0), uu_sAB_vo, "ai", u_xi_o, "i", 1.0, K3_2e, "ai");

  // (Aj Bj | P) Xi_ij (spin up) * (P | Ak Bk) Xi_k (spin down) |det| --> T_i
  Tensor<1> AouBouXoou_AodBodXod(ne_up);
  contract(1.0, AouBouXoou_P_coul, "iP", AodBodXod_coul, "P", 0.0, AouBouXoou_AodBodXod, "i");

  // K3_2e coulomb term (mixed spin)
  contract(detUVUV, uu_sAB_vo, "ai", AouBouXoou_AodBodXod, "i", 1.0, K3_2e, "ai");

  return make_tuple(K1_2e, K2_2e, K3_2e);
}

Tensor<2> uResFockBuilder::get_kab_mo_vo_ESA_term(const double& ESA, const double& detSAB_down,
                                                  const double& detUVUV,
                                                  const ConstTensorView<2>& sao,
                                                  const ConstTensorView<1>& sig_up,
                                                  const ConstTensorView<2>& Av_up,
                                                  const ConstTensorView<2>& Bo_up)
{
  ptrdiff_t nao = sao.extent(0);
  ptrdiff_t nvir_up = Av_up.extent(1);
  ptrdiff_t ne_up = Bo_up.extent(1);

  Tensor<2> KAB(nvir_up, ne_up);
  matrix_transform("t", "n", -1.0 * detSAB_down * detUVUV * ESA, Av_up, sao, Bo_up, 0.0, KAB);

  auto u_xi_o = excluded_product_1(sig_up);
  for (ptrdiff_t i = 0; i < u_xi_o.size(); i++) {
    KAB.pluck(i) *= u_xi_o(i);
  }

  return KAB;
}

Tensor<3> uResFockBuilder::orbital_gradients(const ConstTensorView<3>& fock,
                                             const ConstTensorView<3>& sdet,
                                             const ConstTensorView<2>& sao, const double& nocc)
{
  ptrdiff_t nao = sao.extent(0);
  ptrdiff_t nsd = sdet.extent(2);
  assert(fock.extent(2) == nsd);
  assert(sdet.extent(0) == nao);

  Tensor<3> fockerr(nao, nao, nsd);
  for (ptrdiff_t isd = 0; isd < nsd; isd++) {
    auto err = fockerr.pluck(isd);
    auto CAo = sdet.const_pluck(isd).const_slice(0, nocc);
    auto FA = fock.const_pluck(isd);

    Tensor<2> WAA(nao, nao);
    gemm("n", "t", 1.0, CAo, CAo, 0.0, WAA);

    matrix_transform("n", "n", 1.0, FA, WAA, sao, 0.0, err);

    err = err - err.transpose();
  }

  return fockerr;
}
