// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/resmf/converger.hpp
///
/// Interface file for ResMF convergence acceleration like DIIS
#pragma once

#include <memory>

#include <qleve/array.hpp>
#include <qleve/tensor.hpp>

#include <yucca/algo/diis.hpp>
#include <yucca/scf/converger.hpp>

namespace yucca
{

// fwd declare
class InputNode; // #include <yucca/input/input_node.hpp>

class ResMFConverger : public SCFConverger {
 protected:
  ptrdiff_t nocc_;
  ptrdiff_t ndet_;
  std::string key_;

  std::unique_ptr<qleve::Tensor<3>> coeffs_;
  std::unique_ptr<qleve::Tensor<3>> kappa_; ///< rotation matrix for bfgs


 public:
  ResMFConverger(const InputNode& inp, const ptrdiff_t nocc, const ptrdiff_t ndet,
                 const std::string& key = "convergence");

  void push(const qleve::ConstTensorView<3>& fock, const qleve::ConstTensorView<3>& err,
            const double energy);

  void next(qleve::TensorView<3> next_coeff);
};

} // namespace yucca
