// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/resmf/converger.cpp
///
/// Implementation for convergence acceleration algorithms, like DIIS
#include <iostream>
#include <sstream>
#include <string>

#include <qleve/diagonalize.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/unitary_exp.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/resmf/converger.hpp>
#include <yucca/resmf/fock.hpp>

using namespace yucca;
using namespace qleve;
using namespace std;

static const bool debug = false;

ResMFConverger::ResMFConverger(const InputNode& input, const ptrdiff_t nocc, const ptrdiff_t ndet,
                               const std::string& key) :
    SCFConverger(), nocc_(nocc), ndet_(ndet), key_(key)
{
  string conv_key = input.get_if_value<string>("convergence", "default");
  auto conv = input.get_node_if("convergence");
  if (conv && conv->is_map()) {
    conv_key = conv->get<string>("thresholds", conv_key);
  }

  diis_max_size_ = 5;
  diis_min_size_ = 1;
  diis_start_ = 2;
  level_shift_ = 0.0;
  orbital_select_ = "mom";

  use_second_order_ = input.get<bool>("second order", true); // change input to conv?
  sec_order_switch_thresh_ = 5.0e-3;
  sec_order_start_ = 5;

  if (conv_key == "loose") {
    energy_thresh_ = 1.0e-6;
    grad_thresh_ = 1.0e-4;
    density_thresh_ = 1.0e-6;
  } else if (conv_key == "default") {
    energy_thresh_ = 1.0e-8;
    grad_thresh_ = 1.0e-6;
    density_thresh_ = 1.0e-8;
  } else if (conv_key == "tight") {
    energy_thresh_ = 1.0e-10;
    grad_thresh_ = 1.0e-7;
    density_thresh_ = 1.0e-10;
  } else if (conv_key == "ultratight") {
    energy_thresh_ = 1.0e-12;
    grad_thresh_ = 1.0e-9;
    density_thresh_ = 1.0e-12;
  } else {
    throw runtime_error("Unknown convergence option: use loose, default or tight");
  }

  if (auto conv = input.get_node_if("convergence"); conv && conv->is_map()) {
    energy_thresh_ = conv->get<double>("energy", energy_thresh_);
    grad_thresh_ = conv->get<double>("grad", grad_thresh_);
    density_thresh_ = conv->get<double>("density", density_thresh_);
    diis_max_size_ = conv->get<ptrdiff_t>("diis max", diis_max_size_);
    diis_min_size_ = conv->get<ptrdiff_t>("diis min", diis_min_size_);
    diis_start_ = conv->get<ptrdiff_t>("diis start", diis_start_);
    level_shift_ = conv->get<double>("level shift", level_shift_);
    orbital_select_ = conv->get<string>("orbital select", orbital_select_);

    sec_order_switch_thresh_ = conv->get<double>("second order switch", sec_order_switch_thresh_);
    sec_order_start_ = conv->get<ptrdiff_t>("second order start", sec_order_start_);
    if (conv->contains("second order switch") || conv->contains("second order start")) {
      use_second_order_ = true;
    }
  }

  // transformations
  transform(orbital_select_.begin(), orbital_select_.end(), orbital_select_.begin(), ::tolower);

  // sanity checks
  if (!(orbital_select_ == "aufbau" || orbital_select_ == "mom")) {
    throw runtime_error("Unrecognized orbital selection option: must be aufbau or mom");
  }

  iter_ = 0;

  // report options chosen
  fmt::print("Convergence Options:\n");
  fmt::print("  Thresholds:\n");
  fmt::print("    - energy change: {:6.2g}\n", energy_thresh_);
  // fmt::print("    - density change: {:6.2f}\n", density_thresh_);
  fmt::print("    - orbital gradient: {:6.2g}\n", grad_thresh_);

  fmt::print("\n");
  fmt::print("Convergence Acceleration:\n");
  fmt::print("  DIIS:\n");
  fmt::print("    max size: {:d}\n", diis_max_size_);
  fmt::print("    min size: {:d}\n", diis_min_size_);
  fmt::print("    start: {:d}\n", diis_start_);
  if (level_shift_ != 0.0)
    fmt::print("    level shift: {:6.2g} Hartree\n", level_shift_);
  if (orbital_select_ == "mom")
    fmt::print("    using maximum-overlap-method for orbital selection\n");

  if (use_second_order_) {
    fmt::print("Second order optimization turned on. Switching to second order when:\n");
    fmt::print("  - gradient norm < {:12.6f}\n", sec_order_switch_thresh_);
    fmt::print("  - minimum steps: {:d}\n", sec_order_start_);
  }

  mode_ = OptMode::diagonalize;
}

void ResMFConverger::push(const qleve::ConstTensorView<3>& fock,
                          const qleve::ConstTensorView<3>& err, const double energy)
{
  const ptrdiff_t nn = fock.size();
  assert(nn == fock.size() && nn == err.size());

  this->push_impl(fock.const_reshape(nn), err.const_reshape(nn), energy);
}

void ResMFConverger::next(qleve::TensorView<3> next_coeff)
{
  if (!coeffs_) {
    coeffs_ = std::make_unique<qleve::Tensor<3>>(next_coeff);
  }

  const ptrdiff_t nao = next_coeff.extent(0);
  const ptrdiff_t nmo = next_coeff.extent(1);
  assert(ndet_ == next_coeff.extent(2));

  // determine which optimizing mode to use
  if (mode_ == OptMode::diagonalize && use_second_order_) {
    if (errnrm_.back() < sec_order_switch_thresh_ && iter_ > sec_order_start_) {
      fmt::print("Switching to Second Order optimization.\n");
      mode_ = OptMode::second_order;
    }
  }

  if (mode_ == OptMode::diagonalize) {
    qleve::Tensor<3> fockao(nao, nao, ndet_);
    diis_->extrapolate(fockao.reshape(fockao.size()));

    qleve::Tensor<3> fock_mo(nmo, nmo, ndet_);

    for (int idet = 0; idet < ndet_; ++idet) {
      auto fockaoA = fockao.pluck(idet);
      auto fockmoA = fock_mo.pluck(idet);
      auto co = coeffs_->pluck(idet);
      auto next_co = next_coeff.pluck(idet);

      qleve::matrix_transform(1.0, fockaoA, next_co, 0.0, fockmoA);

      if (level_shift_ != 0.0) {
        for (ptrdiff_t i = nocc_; i < nmo; ++i) {
          fockmoA(i, i) += level_shift_;
        }
      }

      qleve::linalg::diagonalize(fockmoA);

      qleve::gemm("n", "n", 1.0, next_co, fockmoA, 0.0, co);
      if (orbital_select_ == "aufbau") {
        next_co = co;
      } else if (orbital_select_ == "mom") {
        MOM_impl(nocc_, fockmoA, next_co, co);
      }
    }
  } else if (mode_ == OptMode::second_order) {
    qleve::Tensor<3> fockao(nao, nao, ndet_);
    diis_->back(fockao.reshape(fockao.size()));

    if (!kappa_) {
      kappa_ = std::make_unique<qleve::Tensor<3>>(nmo - nocc_, nocc_, ndet_);
    }

    if (!bfgs_) {
      fmt::print("Setting up BFGS optimizer.\n");
      // set reference set of coeffs
      *coeffs_ = next_coeff;

      qleve::Tensor<3> h0(nmo - nocc_, nocc_, ndet_);

      for (ptrdiff_t idet = 0; idet < ndet_; ++idet) {
        auto co = next_coeff.pluck(idet);
        auto fo = fockao.pluck(idet);

        auto tmp = qleve::gemm("n", "n", 1.0, fo, co);
        qleve::Tensor<1> eigs(nmo);
        qleve::contract(4.0, co, "up", tmp, "up", 0.0, eigs, "p");

        auto hh = h0.pluck(idet);
        for (ptrdiff_t i = 0; i < nocc_; ++i) {
          for (ptrdiff_t a = 0; a < (nmo - nocc_); ++a) {
            hh(a, i) = std::max(eigs(a + nocc_) - eigs(i), 0.01);
          }
        }
      }

      *kappa_ = 0.0;
      bfgs_ = std::make_shared<BFGS<double>>(h0.reshape(h0.size()), 20);
    }

    auto fock_ai = kappa_->zeros_like();

    for (ptrdiff_t idet = 0; idet < ndet_; ++idet) {
      auto co = next_coeff.pluck(idet);
      auto fo = fockao.pluck(idet);
      auto fai = fock_ai.pluck(idet);

      // compute Fia in current basis
      const ptrdiff_t nvir = nmo - nocc_;
      auto tmp = qleve::gemm("n", "n", 1.0, fo, co.slice(0, nocc_));
      qleve::gemm("t", "n", 4.0, co.slice(nocc_, nmo), tmp, 0.0, fai);
    }

    auto dkappa = bfgs_->push_and_extrapolate(kappa_->reshape(kappa_->size()),
                                              fock_ai.reshape(fock_ai.size()));

    const double dkappa_norm = dkappa.norm();
    const double alpha = dkappa_norm < 1.0e-2 ? 1.0 : 1e-2 / dkappa_norm;

    // update kappa
    *kappa_ += alpha * dkappa;

    if (debug) {
      const double fai_norm = fock_ai.norm();
      const double kappa_norm = kappa_->norm();

      fmt::print("BFGS debug: |Fai| = {:8.4g}, |kappa| = {:8.4g}, |dkappa| = {:8.4g}\n", fai_norm,
                 kappa_norm, alpha * dkappa_norm);
    }

    for (ptrdiff_t idet = 0; idet < ndet_; ++idet) {
      auto co = next_coeff.pluck(idet);
      auto refco = coeffs_->pluck(idet);
      auto kap = kappa_->pluck(idet);

      auto U = qleve::linalg::unitary_exp(kap);
      qleve::gemm("n", "n", 1.0, refco, U, 0.0, co);
    }

    if ((iter_ % 20) == 0) {
      bfgs_.reset();
    }
  }
}
