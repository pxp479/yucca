// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/resmf/hessian.hpp
///
/// Hessian matrix builder
#pragma once

#include <yucca/structure/geometry.hpp>

namespace yucca
{

enum class Hessian
{
  direct,
  df
};

class RHessianBuilder {
 protected:
  const Geometry& geometry_;

 public:
  RHessianBuilder(const Geometry& g);

  void operator()(const qleve::ConstTensorView<3>& S_AB, const qleve::ConstTensorView<3>& invS_AB,
                  const qleve::ConstTensorView<2>& detS_AB, const qleve::ConstTensorView<3>& sd,
                  const qleve::ConstTensorView<2>& coeffs, const qleve::ConstTensorView<2> E_AB,
                  qleve::TensorView<1> step, qleve::TensorView<1> gradient);
};

} // namespace yucca
