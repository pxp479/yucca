// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/resmf/hessian.cpp
///
/// Hessian matrix builder

#include <qleve/tensor_contract.hpp>

#include <yucca/resmf/hessian.hpp>
#include <yucca/util/libint_interface.hpp>

using namespace std;
using namespace yucca;

RHessianBuilder::RHessianBuilder(const Geometry& g) : geometry_(g) {} // is this constructor needed?

// explicit Hessian
void RHessianBuilder::operator()(const qleve::ConstTensorView<3>& S_AB,
                                 const qleve::ConstTensorView<3>& invS_AB,
                                 const qleve::ConstTensorView<2>& detS_AB,
                                 const qleve::ConstTensorView<3>& sd,
                                 const qleve::ConstTensorView<2>& coeffs,
                                 const qleve::ConstTensorView<2> E_AB,
                                 qleve::TensorView<1> step,     // make const?
                                 qleve::TensorView<1> gradient) // make return output?
{
  // Notes on what each input should be (and purpose)

  // Use to build Hessian Matrix:
  // rank 3 S_AB: nmo x nmo x (nSD * nSD)
  // rank 3 invS_AB: nmo x nmo x (nSD * nSD)
  // rank 2 detS_AB: nSD x nSD --> should only need determinant of (occ,occ) block of S_AB
  // rank 3 sd: nao x nmo x nSD --> for A, B matrices
  // rank 2 coeffs: nstate x nSD --> ResHF coefficients
  // rank 2 E_AB: 2 x (nSD * nSD) --> 1e- and 2e- energies for each A,B sdet pairing

  // Multiply with Hessian matrix:
  // rank 1 step: (nSD * nmo) + (nstate * nSD) --> step update vector (delta X) for NR solver
  // Should `step` be constant?

  // Store result of Hessian and step Matrix-Vector multiplication:
  // rank 1 gradient: (nSD * nmo) + (nstate * nSD) --> orbital gradient vector
  // Do I want this to be a function that returns gradient?
}
