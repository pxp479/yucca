// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/reshf.cpp
///
/// ResHF driver class

#include <tuple>

#include <fmt/core.h>

#include <qleve/blas_interface.hpp>
#include <qleve/checks.hpp>
#include <qleve/determinant.hpp>
#include <qleve/diagonalize.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/orthogonalize.hpp>
#include <qleve/rotate.hpp>
#include <qleve/svd.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/unitary_exp.hpp> // brute-force FDNR implementation
#include <qleve/weighted_matrix_multiply.hpp>

#include <yucca/algo/krylov_lse.hpp> // brute-force FDNR implementation
#include <yucca/input/input_node.hpp>
#include <yucca/physical/constants.hpp>
#include <yucca/resmf/fdnr.hpp> // brute-force FDNR implementation
#include <yucca/resmf/hessian.hpp>
#include <yucca/resmf/initial_guess.hpp>
#include <yucca/resmf/reshf.hpp>
#include <yucca/scf/fock.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/util/timer.hpp>
#include <yucca/wfn/cis_reference.hpp>
#include <yucca/wfn/reshf_reference.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

yucca::ResHF::ResHF(const InputNode& input, const shared_ptr<Geometry>& g) : Method_(g)
{
  fmt::print("Setting up ResHF calculation\n");

  max_iter_ = input.get<ptrdiff_t>("max_iter", 30);

  // this is an important block: set it up rigorously externally somewhere
  string conv = input.get_if_value<string>("convergence", "default");
  if (conv == "loose") {
    conv_energy_ = 1e-6;
    conv_error_ = 1e-3;
    conv_density_ = 1e10; // TODO
  } else if (conv == "default") {
    conv_energy_ = 1e-7;
    conv_error_ = 1e-5;
    conv_density_ = 1e10; // TODO
  } else if (conv == "tight") {
    conv_energy_ = 1e-8;
    conv_error_ = 1e-6;
    conv_density_ = 1e10; // TODO
  } else if (conv == "ultratight") {
    conv_energy_ = 1e-9;
    conv_error_ = 1e-7;
    conv_density_ = 1e10; // TODO
  }

  // allow specific overrides
  conv_energy_ = input.get<double>("conv_energy", conv_energy_);
  conv_error_ = input.get<double>("conv_error", conv_error_);
  conv_density_ = input.get<double>("conv_density", conv_density_);

  fmt::print("Convergence criteria:\n");
  fmt::print("  - change in energy (dE) < {:1e}\n", conv_energy_);
  fmt::print("  - gradient norm (grad) < {:1e}\n", conv_error_);
  fmt::print("  - change in density (drho) < {:1e}\n", conv_density_);
  fmt::print("\n");

  nao_ = geometry_->orbital_basis()->nbasis();
} // end first ResHF constructor

yucca::ResHF::ResHF(const InputNode& input, const shared_ptr<Wfn>& wfn) :
    ResHF(input, wfn->geometry())
{
  svd_floor_ = input.get<double>("svd_floor", 1e-8);

  fmt::print("Suppressing singular values to have a floor of {:3e}\n", svd_floor_);
  fmt::print("\n");

  const shared_ptr<CISReference> cisref = dynamic_pointer_cast<CISReference>(wfn);
  const shared_ptr<ResHFReference> reshfref = dynamic_pointer_cast<ResHFReference>(wfn);

  fmt::print("Building ResHF wavefunction\n");
  fmt::print("\n");

  if (cisref) {
    fmt::print("Initial guess generated from preliminary HF/CIS calculation:\n");

    coeffs_ = make_shared<qleve::Tensor<2>>(*cisref->coeffs());
    eigenstates_ = make_shared<qleve::Tensor<2>>(*cisref->eigenstates());
    nmo_ = coeffs_->extent(1);

    // processing initial guess user input
    nocc_ = cisref->nocc();
    nelec_ = nocc_ * 2;
    charge_ = -(nelec_ - wfn->geometry()->total_nuclear_charge());

    cis_transform_ = false;

    nSD_ = 0;
    nstates_ = 0;

    std::vector<std::vector<std::vector<ptrdiff_t>>> hold_rot_orbitals{};
    std::vector<std::vector<double>> hold_rot_angles{};
    std::vector<std::vector<double>> hold_transform{};

    // pull out initial guess node map from the larger ResHF stage map in the input file
    guess_ = input.get_node("initial guess");

    // recast initial guess InputNode as a vector so that you can iterate through each element
    auto guessvec = std::get<std::vector<recursive_wrapper<InputNode>>>(guess_.data_);

    // access each recursive_wrapper within vector via for loop
    for (auto& element : guessvec) {
      // access the InputNode stored within each recursive_wrapper
      const InputNode& typenode = element.get();

      // access and store each initial guess type
      auto key = typenode.get_if_value<string>("type", "default");
      guess_types_.push_back(key);

      // Set downstream parameters according to guess type
      if (key == "hf" || key == "default") {
        fmt::print("  - Pulling initial guess from HF ground state\n");

        // build transform matrix column
        std::vector<double> column_transform{};

        for (ptrdiff_t det = 0; det < nstates_; det++) {
          column_transform.push_back(0); // placeholders for prior generated SD
        }
        column_transform.push_back(1);

        hold_transform.push_back(column_transform);

        // reserve SD slots
        nSD_++;
        nstates_++;

      } else if (key == "rotate") {
        fmt::print("  - Pulling initial guess from orbital rotations\n");

        // access orbital pairs to be rotated

        // pull out orbital node containing vectors in each element
        InputNode orbitnode;
        if (auto orbitcheck = typenode.get_node_if("orbitals"); orbitcheck) {
          orbitnode = typenode.get_node("orbitals");
        } else {
          throw runtime_error("No rotation orbitals defined! 'orbitals:' key is missing");
        }
        std::vector<std::vector<ptrdiff_t>> orbitpair{};
        for (ptrdiff_t irot = 0; irot < orbitnode.size(); irot++) {
          // access node vector in each element
          auto nvec = orbitnode.get_node(irot);
          orbitpair.push_back(nvec.as_vector<ptrdiff_t>());

          // check to make sure everything looks ok
          for (auto& mo : nvec.as_vector<ptrdiff_t>()) {
            if (mo < 0) {
              throw runtime_error("Orbital index values must be greater than or equal to 0!");
            } else if (mo > nmo_) {
              throw runtime_error("Requested orbital index exceeds available MOs!");
            }
          }
        }
        hold_rot_orbitals.push_back(orbitpair);

        // access and store rotation angles
        if (auto anglenode = typenode.get_node_if("angles"); anglenode) {
          if (anglenode->is_vector()) {
            hold_rot_angles.push_back(typenode.get_vector<double>("angles"));
          }
        } else {
          throw runtime_error("No rotation angles defined! 'angles:' key is missing");
        }

        for (ptrdiff_t idet = 0; idet < hold_rot_orbitals.size(); idet++) {
          if (hold_rot_orbitals[idet].size() != hold_rot_angles[idet].size()) {
            throw runtime_error(
                "Must provide same number of orbital pairs and rotation angles for a "
                "given determinant!");
          }
        }
        // build transform matrix column for this SD
        std::vector<double> column_transform{};

        for (ptrdiff_t det = 0; det < nstates_; det++) {
          column_transform.push_back(0); // placeholders for prior generated SD
        }
        column_transform.push_back(1);

        hold_transform.push_back(column_transform); // store column transform in overall matrix

        nSD_++;     // reserve SD slots
        nstates_++; // reserve ResHF state slots

      } else if (key == "cis") {
        fmt::print("  - Pulling initial guess from CIS excited state(s)\n");

        cis_transform_ = typenode.get<bool>("transform", false);
        if (cis_transform_) {
          fmt::print("    NTO transform enabled\n");
        }
        cis_constrain_ = typenode.get<bool>("constrain", true);

        // access CIS states and NTOs and check for user input issues
        if (auto statenode = typenode.get_node_if("states"); statenode) {
          if (statenode->is_vector()) {
            cis_states_ = typenode.get_vector<ptrdiff_t>("states");
          }
        } else {
          throw runtime_error("No CIS states defined!  'states:' key is missing");
        }

        if (auto ntonode = typenode.get_node_if("nnto"); ntonode) {
          if (ntonode->is_vector()) {
            cis_ntos_ = typenode.get_vector<ptrdiff_t>("nnto");
          }
        } else {
          throw runtime_error("No NTO pairs defined! 'nnto:' key is missing");
        }

        if (cis_states_.size() != cis_ntos_.size()) {
          throw runtime_error("Must provide same number of states and NTOs as input!");
        }

        for (auto state : cis_states_) {
          if (state <= 0) {
            throw runtime_error("All requested CIS states must be greater than zero");
          }

          if (state > eigenstates_->extent(1)) {
            throw runtime_error("Not enough CIS states calculated prior to ResHF!");
          }
        }

        for (auto nto : cis_ntos_) {
          if (nto <= 0) {
            throw runtime_error("All requested NTOs must be greater than zero");
          }

          // build and store transform matrix columns
          for (ptrdiff_t into = 0; into < nto; into++) {
            std::vector<double> plus_column_transform{};
            std::vector<double> minus_column_transform{};

            for (ptrdiff_t det = 0; det < nstates_; det++) {
              plus_column_transform.push_back(0);  // placeholders for prior generated SD
              minus_column_transform.push_back(0); // placeholders for prior generated SD
            }
            plus_column_transform.push_back(1.0 / std::sqrt(2.0));
            minus_column_transform.push_back(-1.0 / std::sqrt(2.0));

            constrained_pairs_.emplace_back(hold_transform.size(), hold_transform.size() + 1);

            hold_transform.push_back(plus_column_transform);
            hold_transform.push_back(minus_column_transform);

            nSD_ += 2; // reserve SD slots
            if (cis_transform_) {
              nstates_++;
            } else {
              nstates_ += 2;
            } // reserve ResHF state slots

          } // loop to next requested NTO pairing
        }   // loop to next CIS state

      } else {
        throw runtime_error("Unknown initial guess input specified in 'type:' key!");
      }
    }

    // organizing data from input

    // restructuring orbital rotations and angles into a single vector holder:
    for (ptrdiff_t ipair = 0; ipair < hold_rot_angles.size(); ipair++) {
      auto orbitvec = hold_rot_orbitals[ipair];
      auto anglevec = hold_rot_angles[ipair];
      std::vector<std::tuple<ptrdiff_t, ptrdiff_t, double>> pairvec{};

      for (ptrdiff_t irot = 0; irot < orbitvec.size(); irot++) {
        auto orbit_pair = orbitvec[irot];
        auto angle = anglevec[irot];

        auto rot_set = std::make_tuple(orbit_pair[0], orbit_pair[1], angle);

        pairvec.push_back(rot_set);
      }
      rot_pairs_.push_back(pairvec);
    }

    // restructuring transform matrix from hold_transform:
    if (cis_transform_) {
      fmt::print("Building NTO transformation matrix:\n");
      qleve::Tensor<2> U(nSD_, nstates_);
      ptrdiff_t irow = 0;
      for (auto& row : hold_transform) {
        for (ptrdiff_t icol = 0; icol < row.size(); icol++) {
          U(irow, icol) = row[icol];
        }
        irow++;
      }
      U.print("Transformation Matrix");
      transform_ = make_shared<qleve::Tensor<2>>(U);
      fmt::print("\n");
    } else {
      transform_ = make_shared<qleve::Tensor<2>>(nSD_, nstates_); // empty placeholder
    }
  } else if (reshfref) {
    fmt::print("Initial guess generated from previous ResHF calculation\n");

    sdets_ = reshfref->orbital_coeffs();
    coeffs_ = make_shared<qleve::Tensor<2>>(sdets_->pluck(0)); // uhhhh, this seems wrong???
    auto sdcc = reshfref->sd_coeffs();

    nelec_ = reshfref->nelec();
    nocc_ = reshfref->nocc();
    nmo_ = coeffs_->extent(1);
    nSD_ = sdcc->extent(0);
    nstates_ = sdcc->extent(1);

    transform_ = reshfref->transform();
    cis_transform_ = input.get<bool>("transform", static_cast<bool>(transform_));

    constrained_pairs_ = reshfref->constrained_pairs();
    cis_constrain_ = input.get<bool>("constrain", !constrained_pairs_.empty());
  } else {
    throw std::runtime_error("Need to start ResHF from a CIS or ResHF calculation");
  } // end ResHF wavefunction builder
  fmt::print("\n");

  if (nelec_ % 2 != 0) {
    throw runtime_error("Number of electrons must be even for restricted calculation!");
  }

  nvir_ = nmo_ - nocc_;

  fmt::print("Electronic configuration:\n");
  fmt::print("  - nocc: {:d}\n", nocc_);
  fmt::print("  - nvir: {:d}\n", nvir_);
  fmt::print("  - nmo: {:d}\n", nmo_);
  fmt::print("\n");

  fmt::print("State averaging set up:\n");
  if (auto weightnode = input.get_node_if("weights"); weightnode) {
    if (weightnode->is_vector()) {
      weight_ = input.get_vector<double>("weights");
      double weight_sum = 0.0;
      for (ptrdiff_t i = 0; i < weight_.size(); i++) {
        if (weight_[i] > 0.0) {
          fmt::print("  - State {} selected for averaging with weight {}\n", i, weight_[i]);
        }
        if (weight_[i] < 0.0) {
          throw runtime_error("Weights to be applied to states must be non-negative!");
        }
        weight_sum += weight_[i];
      }
      fmt::print("\n");

      if (weight_sum != 1.00) {
        fmt::print("Normalizing weights to 1.0\n");
        fmt::print("New weights: ");
        for (ptrdiff_t i = 0; i < weight_.size(); ++i) {
          weight_[i] /= weight_sum;
          fmt::print("{:12.6f}", weight_[i]);
        }
        fmt::print("\n");
      }
    }
  } else { // default to ground state SS-ResHF
    weight_ = std::vector<double>{1.00};
    fmt::print("Default: SS-ResHF with optimization of ground state selected\n\n");
    // this doesn't work right currently...
  }

  // if cis_transform_ = false, nstates_ = nSD_
  if (weight_.size() > nstates_) {
    throw runtime_error("Number of states to be averaged must be less than or equal to number of "
                        "total ResHF states!");
  }

  // include DIIS and other things
  // currently only used for SCF and BFGS
  converger_ = make_unique<ResMFConverger>(input, nocc_, nSD_);

  // set up for FDNR convergence solver
  // eventually, this will be incorporated into converger_ class alongside BFGS and SCF!
  use_fdnr_ = false;
  if (auto conv = input.get_node_if("convergence"); conv && conv->is_map()) {
    auto fdnr_node = conv->get_node_if("fdnr");
    if (fdnr_node) {
      use_fdnr_ = true;

      fdnr_start_ = fdnr_node->get<ptrdiff_t>("fdnr start", 5);
      fdnr_order_ = fdnr_node->get<ptrdiff_t>("fd order", 1);
      fdnr_scale_ = fdnr_node->get<double>("fd step size", 1.0e-3);

      krylov_iter_ = fdnr_node->get<ptrdiff_t>("krylov iter", 10);
      krylov_residue_ = fdnr_node->get<double>("krylov residue", 1.0e-3);
      krylov_restart_ = fdnr_node->get<ptrdiff_t>("krylov restart", 2);
      krylov_shift_ = fdnr_node->get<double>("krylov shift", 0.0);
    }
  }

  use_matadj_ = input.get<bool>("matrix adjugate", false);
  if (use_matadj_) {
    fmt::print("\nUsing matrix adjugate ResHF Fock build implementation!\n\n");
  }

  print_kab_ = input.get<bool>("print kab", false);
  if (print_kab_) {
    fmt::print("\nDebugging print mode selected: KAB matrix term will be printed\n");
    if (nSD_ < 2) {
      fmt::print("  * Warning: KAB routines won't be triggered unless there is more than 1 "
                 "determinant in ResHF wavefunction!");
    }
    if (!use_matadj_) {
      fmt::print(
          "  * Warning: KAB printing not implemented for non-matrix adjugate ResHF routine!");
    }
  }

  // Pull overlap and hcore in AO from prior RHF calculation
  if (!overlap_) {
    overlap_ = make_shared<qleve::Tensor<2>>(geometry_->overlap());
  }
  if (!hcore_) {
    hcore_ = make_shared<qleve::Tensor<2>>(geometry_->hcore());
  }

  // initialize important intermediates

  // note: if symmetric orthogonalization, nmo_ = nao_
  detS_AB_ = make_shared<qleve::Tensor<2>>(nSD_, nSD_);
  W_AB_ = make_shared<qleve::Tensor<4>>(nao_, nao_, nSD_, nSD_);
  H_AB_ = make_shared<qleve::Tensor<2>>(nSD_, nSD_);
  resHF_fock_ = make_shared<qleve::Tensor<3>>(nao_, nao_, nSD_);

  // note: if cis_transform_ = false, nstates_ = nSD_
  resHF_coeffs_ = make_shared<qleve::Tensor<2>>(nSD_, nstates_);
  energies_ = make_shared<qleve::Tensor<1>>(nstates_);

  energy0_ = 0.0;
  energySA_ = 0.0;
} // end final ResHF constructor

yucca::ResHF::ResHF(const ResHF& x) : Method_(x)
{
  this->nmo_ = x.nmo_;
  this->nao_ = x.nao_;
  this->nocc_ = x.nocc_;
  this->nvir_ = x.nvir_;
  this->nelec_ = x.nelec_;
  this->charge_ = x.charge_;

  this->guess_ = x.guess_;
  this->guess_types_ = x.guess_types_;

  this->rot_orbitals_ = x.rot_orbitals_;
  this->rot_angles_ = x.rot_angles_;
  this->rot_pairs_ = x.rot_pairs_;

  this->cis_states_ = x.cis_states_;
  this->cis_ntos_ = x.cis_ntos_;
  this->cis_transform_ = x.cis_transform_;
  this->cis_constrain_ = x.cis_constrain_;
  this->transform_ = std::make_shared<qleve::Tensor<2>>(*x.transform_);
  this->constrained_pairs_ = x.constrained_pairs_;

  this->nSD_ = x.nSD_;
  this->nstates_ = x.nstates_;

  this->weight_ = x.weight_;

  this->overlap_ = std::make_shared<qleve::Tensor<2>>(*x.overlap_);
  this->hcore_ = std::make_shared<qleve::Tensor<2>>(*x.hcore_);
  this->coeffs_ = std::make_shared<qleve::Tensor<2>>(*x.coeffs_);
  this->eigenstates_ = std::make_shared<qleve::Tensor<2>>(*x.eigenstates_);

  this->detS_AB_ = std::make_shared<qleve::Tensor<2>>(*x.detS_AB_);
  this->W_AB_ = std::make_shared<qleve::Tensor<4>>(*x.W_AB_);
  this->H_AB_ = std::make_shared<qleve::Tensor<2>>(*x.H_AB_);
  this->resHF_fock_ = std::make_shared<qleve::Tensor<3>>(*x.resHF_fock_);
  this->sdets_ = std::make_shared<qleve::Tensor<3>>(*x.sdets_);
  this->resHF_coeffs_ = std::make_shared<qleve::Tensor<2>>(*x.resHF_coeffs_);
  this->energies_ = std::make_shared<qleve::Tensor<1>>(*x.energies_);

  this->energy0_ = x.energy0_;
  this->energySA_ = x.energySA_;

  this->use_matadj_ = x.use_matadj_;

  this->max_iter_ = x.max_iter_;
  this->conv_energy_ = x.conv_energy_;
  this->conv_error_ = x.conv_error_;
  this->conv_density_ = x.conv_density_;

  this->svd_floor_ = x.svd_floor_;
  this->orbital_condition_ = x.orbital_condition_;
  this->cond_number_ = x.cond_number_;

  this->use_fdnr_ = x.use_fdnr_;
  this->fdnr_start_ = x.fdnr_start_;
  this->fdnr_order_ = x.fdnr_order_;
  this->fdnr_scale_ = x.fdnr_scale_;
  this->krylov_residue_ = x.krylov_residue_;
  this->krylov_iter_ = x.krylov_iter_;
  this->krylov_restart_ = x.krylov_restart_;
  this->krylov_shift_ = x.krylov_shift_;

  // this->converger_ = std::make_unique<yucca::ResMFConverger>(*x.converger_);
  // needs to be a shared_ptr in order to be accessed via copy constructor (I think...)

  this->resfockbuilder_ = std::make_shared<yucca::ResFockBuilder>(*x.resfockbuilder_);
} // end copy constructor

void yucca::ResHF::compute()
{
  Timer timer("ResHF");
  fmt::print("Starting ResHF calculation\n\n");

  if (!sdets_) { // generate sdets_ using initial guess functions
    sdets_ = make_shared<Tensor<3>>(nao_, nmo_, nSD_);

    fmt::print("Building initial ResHF wavefunction from {} Slater determinants:\n", nSD_);
    fmt::print("(Slater determinant counter starting at 0)\n\n");
    timer.mark();

    ptrdiff_t det = 0;
    ptrdiff_t rdet = 0;
    for (auto& key : guess_types_) {
      if (key == "hf" || key == "default") {
        fmt::print("  - Storing ground state HF as Slater determinant #{}\n", det);
        sdets_->pluck(det) = *coeffs_;
        det++;
      } else if (key == "rotate") {
        fmt::print("  - Building Slater determinant #{} from orbital rotations\n", det);

        qleve::TensorView<2> SDview = sdets_->pluck(det);

        initial_guess::rotate_guess(*coeffs_, rot_pairs_[rdet], SDview);

        det++;
        rdet++;
      } else if (key == "cis") {
        for (ptrdiff_t istate = 0; istate < cis_states_.size(); istate++) {
          auto state = cis_states_[istate];
          auto nnto = cis_ntos_[istate];

          fmt::print("  - Building Slater determinants #{} through #{}\n", det,
                     det + (2 * nnto) - 1);
          qleve::TensorView<3> SDview = sdets_->slice(det, det + (2 * nnto));
          initial_guess::CIS_guess(*coeffs_, *eigenstates_, state - 1, nnto, nocc_, SDview);

          det += (2 * nnto);
        }
      } else {
        throw runtime_error("Unknown initial guess type!");
      }
    }
    timer.tick_print("initial_guess");
    fmt::print("\n");
  }

  if (cis_transform_) {
    fmt::print("Applying NTO transform matrix to Hamiltonian\n");
  }

  { // make sure orbitals are orthogonal within each set
    Tensor<2> olap(nmo_, nmo_);
    bool any_nonorthogonal = false;
    for (ptrdiff_t i = 0; i < nSD_; ++i) {
      auto cc = sdets_->pluck(i);
      matrix_transform(1.0, *overlap_, cc, 0.0, olap);
      const double id = qleve::linalg::ident(olap);
      if (id > 1e-12) {
        any_nonorthogonal = true;
        auto u = qleve::linalg::orthogonalize_metric(olap, 1e-12);
        auto ccnew = qleve::gemm("n", "n", 1.0, cc, u);
        cc = ccnew;
      }
    }
    if (any_nonorthogonal) {
      fmt::print("Input orbitals found to be nonorthogonal. Orthogonalizing.\n");
    }
  }

  fmt::print("Starting wavefunction optimization iterations\n");
  fmt::print("Note: defaults to use of SCF converger unless otherwise stated\n\n");

  fmt::print("{:>17s} {:>14s} {:>14s} {:>8s} {:>8s} {:>8s} {:>10s} \n", "iter", "E0 E", "E_SA",
             "|gradient|", "det-cond", "orb-cond", "dE");
  fmt::print("{:-^93s}\n", "");

  double old_energy = 0.0;
  double nuclear_repulsion = geometry_->nuclear_repulsion();

  bool conv = false;

  resfockbuilder_ = make_shared<ResFockBuilder>(
      *geometry_, weight_, *transform_, constrained_pairs_, nocc_, nmo_, nSD_, 1.0, 1.0,
      cis_transform_, cis_constrain_, svd_floor_, use_matadj_, print_kab_);
  timer.mark();
  for (ptrdiff_t iscf = 0; iscf < max_iter_; ++iscf) {

    (*resfockbuilder_)(*sdets_, *resHF_fock_);
    timer.tick("fock build");
    energies_ = resfockbuilder_->energies();
    W_AB_ = resfockbuilder_->wab();
    orbital_condition_ = resfockbuilder_->orbital_condition();
    cond_number_ = resfockbuilder_->cond_number();
    resHF_coeffs_ = resfockbuilder_->coeffs();
    H_AB_ = resfockbuilder_->hab();
    detS_AB_ = resfockbuilder_->detsab();

    // Calculate state-averaged energy and energy difference
    double E_SA = 0.0;
    for (ptrdiff_t state = 0; state < weight_.size(); state++) {
      E_SA += weight_[state] * energies_->at(state);
    }

    double dE = 0.0;
    if (iscf != 0) {
      dE = E_SA - old_energy;
    }
    old_energy = E_SA;

    // Calculate orbital rotation gradients (FOCK * W_AA * S_AO - S_AO * W_AA * FOCK)
    qleve::Tensor<1> error_norm(nSD_);
    qleve::Tensor<3> fockerr(nao_, nao_, nSD_);
    for (ptrdiff_t i = 0; i < nSD_; i++) {
      auto err = fockerr.pluck(i);
      qleve::Tensor<2> tmp(nao_, nao_);

      auto F_A = resHF_fock_->const_pluck(i);
      auto W_AA = W_AB_->const_pluck(i, i);

      qleve::gemm("n", "n", 1.0, F_A, W_AA, 0.0, tmp);
      qleve::gemm("n", "n", 1.0, tmp, *overlap_, 0.0, err);

      err = err - err.transpose();
    }
    orb_gradient_norm_ = fockerr.norm();

    converger_->push(*resHF_fock_, fockerr, E_SA); // for SCF and BFGS only!

    energySA_ = E_SA + nuclear_repulsion;

    // print results
    fmt::print("{:s} {:6d} {:14.8f} {:14.8f} {:8.2g} {:8.2g} {:8.2g}", "ResHF iter:", iscf,
               energies_->at(0) + nuclear_repulsion, energySA_, orb_gradient_norm_, cond_number_,
               orbital_condition_);
    if (iscf != 0) {
      fmt::print("{:10.2g}", dE);
    } else {
      fmt::print("{:>8s}", "---");
    }
    fmt::print("\n");
    conv = converged(dE, orb_gradient_norm_, 0.0);
    if (conv) {
      fmt::print("ResHF Variational Optimization has converged! :D \n\n");
      break;
    }


    if (iscf != (max_iter_ - 1)) { // if this is the final wavefunction optimization iteration,
                                   // do not update orbital coefficients!

      if (use_fdnr_) {
        if (fdnr_start_ > (iscf + 1)) {
          // initial use of SCF and/or BFGS converger
          converger_->next(*sdets_);
          timer.tick("orbital update");
        } else {
          fmt::print("Switching to FDNR converger\n");
          // using FDNR converger
          // converting resHF_fock_ to MO basis
          assert(nmo_ == nao_);
          auto Fock_mo = make_shared<qleve::Tensor<3>>(resHF_fock_->zeros_like());

          for (std::ptrdiff_t SD_A = 0; SD_A < nSD_; SD_A++) {
            auto FA_ao = resHF_fock_->const_pluck(SD_A);
            auto FA_mo = Fock_mo->pluck(SD_A);

            auto sdA = sdets_->pluck(SD_A);

            qleve::matrix_transform(1.0, FA_ao, sdA, 0.0, FA_mo);

            qleve::Tensor<2> FA_mo_copy(FA_mo);
            std::vector<ptrdiff_t> blocksizes{nocc_, nvir_};
            auto eigenvalues = qleve::linalg::diagonalize_blocks(FA_mo_copy, blocksizes);

            qleve::matrix_transform(1.0, FA_mo, FA_mo_copy, 0.0, FA_mo);
            qleve::Tensor<2> tmp(sdA.zeros_like());
            qleve::gemm("n", "n", 1.0, sdA, FA_mo_copy, 0.0, tmp);
            sdA = tmp;
          }

          // Build FDNR function; initialize with sdets_, resHF_fock_, and ResFockBuilder
          FDNR_func fdnr(*sdets_, *Fock_mo, resfockbuilder_, fdnr_order_, fdnr_scale_);

          // Use resHF_fock_ to initialize krylov_lse solver;
          // make orbital_gradients and hessian_diagonal elements
          // make sure dX that will be produced is the dKAPPA_aiA matrix!

          auto FA_ai = Fock_mo->const_subtensor({nocc_, 0, 0}, {nmo_, nocc_, nSD_});
          qleve::Tensor<2> rhs(FA_ai.size(), 1);
          rhs.reshape(nvir_, nocc_, nSD_) = -1.0 * FA_ai;

          qleve::Tensor<1> freq(rhs.extent(1));
          freq = -krylov_shift_;

          auto diag = make_shared<qleve::Tensor<1>>(rhs.extent(0));
          std::ptrdiff_t count = 0;
          for (std::ptrdiff_t SD_A = 0; SD_A < nSD_; SD_A++) {
            for (std::ptrdiff_t i = 0; i < nocc_; i++) {
              for (std::ptrdiff_t a = nocc_; a < nmo_; a++) {
                // AA block
                diag->at(count) = Fock_mo->at(a, a, SD_A) - Fock_mo->at(i, i, SD_A);
                count++;
              }
            }
          }

          KrylovLSE<double> krylov(rhs, freq, diag, krylov_iter_);

          // Use krylov_lse solver to update sdets_; rotate sdets_ using U(dKAPPA_aiA)
          auto kappa = krylov.solve(fdnr, krylov_iter_, krylov_residue_, krylov_restart_,
                                    /*recompute_mv*/ true, /*save_best*/ true, /*verbose*/ true);

          qleve::Tensor<3> K = kappa.reshape(nvir_, nocc_, nSD_);

          for (std::ptrdiff_t SD_A = 0; SD_A < nSD_; SD_A++) {
            // turn dX (kappa) into a unitary matrix (MO rotation matrix!)
            auto kappaA_ai = K.const_pluck(SD_A);
            auto UA = qleve::linalg::unitary_exp(kappaA_ai);

            // rotate each sdet
            auto sdetA = sdets_->pluck(SD_A);
            qleve::Tensor<2> sdetA_new(sdetA.zeros_like());
            qleve::gemm("n", "n", 1.0, sdetA, UA, 0.0, sdetA_new);

            sdetA = sdetA_new;
          }
        }
      } else {
        // using SCF or BFGS converger
        converger_->next(*sdets_);
        timer.tick("orbital update");
      } // end conditional SCF/BFGS/FDNR paths

    } // end conditional orbital propagation (iscf != max_iter)

  } // progress to the next SCF loop (iscf)

  if (!conv) {
    fmt::print("ResHF Variational Optimization has NOT converged :```( \n\n");
  }

  // store last ground state energy so YuccaTest can look at this info
  energy0_ = energies_->at(0);

  fmt::print("Final ResHF State Energies:\n");
  for (int istate = 0; istate < nstates_; ++istate) {
    const double total_energy = energies_->at(istate) + nuclear_repulsion;
    fmt::print("  - State {:d} electronic energy: {:20.8f}\n", istate, energies_->at(istate));
    fmt::print("  - State {:d} total energy: {:20.8f}\n", istate, total_energy);
  }
  fmt::print("\n");

  fmt::print("ResHF Excitation Energies:\n");
  const double E0 = energies_->at(0);
  fmt::print("{:>8s} {:>20s} {:>20s} {:>20s}\n", "state", "Hartree", "eV", "nm");
  fmt::print("{:8d} {:20.8f} {:20.8f} {:>20s}\n", 0, 0.0, 0.0, "infinity");
  for (int istate = 1; istate < nstates_; ++istate) {
    const double en_H = energies_->at(istate) - E0;
    const double en_eV = en_H * physical::Hartree2eV;
    const double en_nm = physical::Hartree2nm / en_H;
    fmt::print("{:8d} {:20.8f} {:20.8f} {:>20.8f}\n", istate, en_H, en_eV, en_nm);
  }

  resHF_coeffs_->print("Final ResHF Coefficients");
  fmt::print("\n");
  qleve::Tensor<2> ss(detS_AB_->zeros_like());
  ss = *detS_AB_ * *detS_AB_;
  ss.print("Final |S_AB|");
  fmt::print("\n");

  fmt::print("Overlap (|S_AB|) with HF state: \n");
  for (ptrdiff_t i = 0; i < nSD_; i++) {
    auto tens_a = sdets_->const_pluck(i);

    qleve::Tensor<2> tmp(nmo_, nao_);
    qleve::Tensor<2> s_mo(nmo_, nmo_);
    qleve::gemm("t", "n", 1.0, tens_a, *overlap_, 0.0, tmp);
    qleve::gemm("n", "n", 1.0, tmp, *coeffs_, 0.0, s_mo);

    auto s_occ_occ = s_mo.subtensor({0, 0}, {nocc_, nocc_});

    auto HF_overlap = std::pow(qleve::linalg::determinant(s_occ_occ), 2);

    fmt::print("  - Det #{}: {} \n", i, HF_overlap);
  }
  fmt::print("\n");

  H_AB_->print("Final Hamiltonian");
  fmt::print("\n");

  fmt::print("Computing multipole moments\n");
  timer.mark();
  auto multipole = geometry_->multipole(2);
  qleve::Tensor<2> moments(multipole.extent(2), nstates_);
  auto densities =
      compute_1rdms(W_AB_->const_reshape(nao_, nao_, nSD_ * nSD_), *detS_AB_, *resHF_coeffs_);
  qleve::contract(-2.0, multipole, "uvp", densities, "uvK", 0.0, moments, "pK");
  timer.tick_print("multipole moments");

  auto nuclear_moments = geometry_->nuclear_multipole(2);
  auto total_moments = nuclear_moments.zeros_like();

  for (int istate = 0; istate < nstates_; ++istate) {
    total_moments = nuclear_moments + moments.pluck(istate);
    fmt::print("Properties for state {:d}\n", istate);
    fmt::print("Electronic Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", moments(1, istate),
               moments(2, istate), moments(3, istate));
    fmt::print("   Nuclear Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", nuclear_moments(1),
               nuclear_moments(2), nuclear_moments(3));
    fmt::print("     Total Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", total_moments(1),
               total_moments(2), total_moments(3));
    fmt::print("\n");
    fmt::print("Electronic Quadrupole (xx, xy, xz, yy, yz, zz): {:18.8f} {:18.8f} {:18.8f} "
               "{:18.8f} {:18.8f} {:18.8f}\n",
               moments(4, istate), moments(5, istate), moments(6, istate), moments(7, istate),
               moments(8, istate), moments(9, istate));
    fmt::print("\n");
  }

  auto tdens =
      compute_1tdms(W_AB_->const_reshape(nao_, nao_, nSD_ * nSD_), *detS_AB_, *resHF_coeffs_);
  qleve::Tensor<2> transition_moments(multipole.extent(2), tdens.extent(2));
  qleve::contract(-2.0, multipole, "uvp", tdens, "uvK", 0.0, transition_moments, "pK");

  for (int jstate = 0, ij = 0; jstate < nstates_; ++jstate) {
    for (int istate = 0; istate < jstate; ++istate, ++ij) {
      fmt::print("Transition Properties for <{:d}|V|{:d}>\n", istate, jstate);
      const double x = transition_moments(1, ij);
      const double y = transition_moments(2, ij);
      const double z = transition_moments(3, ij);
      fmt::print("<{:d}|µ|{:d}> (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", istate, jstate, x, y, z);

      const double dipnorm = std::sqrt(x * x + y * y + z * z);
      const double omega = energies_->at(jstate) - energies_->at(istate);
      const double fosc = dipnorm * dipnorm * 2.0 / 3.0 * omega;
      fmt::print("|<{0:d}|µ|{1:d}>| = {2:18.8f}, f_{0:d},{1:d} = {3:18.8f}\n", istate, jstate,
                 dipnorm, fosc);

      fmt::print("\n");
    }
  }

  fmt::print("Compute SA 1RDM and properties\n");
  // use HF coeffs as base orthogonal orbitals
  auto SC = qleve::gemm("n", "n", 1.0, *overlap_, *coeffs_);
  qleve::Tensor<2> gamma_sa(nao_, nao_);
  qleve::Tensor<1> w(nstates_);
  std::copy_n(weight_.data(), weight_.size(), w.data());
  qleve::contract(1.0, densities, "uvk", w, "k", 0.0, gamma_sa, "uv");

  qleve::Tensor<2> gamma_mo(nmo_, nmo_);
  // minus one factor so largest values come first
  qleve::matrix_transform(-1.0, gamma_sa, SC, 0.0, gamma_mo);
  auto occs = qleve::linalg::diagonalize(gamma_mo);
  occs *= -1.0;
  for (ptrdiff_t i = 0; i < occs.size(); ++i) {
    if (occs(i) < 1e-6) {
      break;
    }
    fmt::print("{:6d} {:10.6f}\n", i, occs(i));
  }

  timer.summarize();
  // end compute function
}

void ResHF::compute_gradient_impl()
{
  gradient_ = make_unique<qleve::Tensor<2>>(3, geometry_->natoms());
}

bool ResHF::converged(const double dE, const double error, const double drho) const
{
  bool conv = (abs(dE) < conv_energy_) && (abs(error) < conv_error_) && (abs(drho) < conv_density_);
  return conv;
}

shared_ptr<Wfn> ResHF::wavefunction()
{
  return make_shared<ResHFReference>(geometry_, sdets_, resHF_coeffs_, transform_,
                                     constrained_pairs_, resHF_fock_, weight_, nelec_, nocc_, nvir_,
                                     energySA_);
}

void yucca::fd_fock_rreshf(const shared_ptr<Method_>& method,
                           const shared_ptr<yucca::Geometry>& geometry, const double h)
{
  auto reshf = dynamic_pointer_cast<yucca::ResHF>(method);

  if (!reshf) {
    throw runtime_error("Dynamic cast of base method to ResHF method failed!");
  }

  const ptrdiff_t nao = reshf->nao();
  const ptrdiff_t nmo = reshf->nmo();
  const ptrdiff_t nocc = reshf->nocc();
  const ptrdiff_t ndets = reshf->nSD();
  const bool use_transform = reshf->use_transform();
  const auto weight = reshf->weight();

  const double nuclear_repulsion = geometry->nuclear_repulsion();

  auto compute_energy = [&](ptrdiff_t sd, ptrdiff_t i, ptrdiff_t a, const double r) {
    yucca::ResHF reshf_t(*reshf);
    auto resfockbuilder = reshf_t.resfockbuilder();

    // apply FD orbital rotation to MOs
    std::shared_ptr<qleve::Tensor<3>> sdets_t = reshf_t.sdets();
    qleve::rotate(nao, &(sdets_t->pluck(sd))(0, i), &(sdets_t->pluck(sd))(0, a), r);

    // generate energies with rotated sdets
    qleve::Tensor<3> fock(nao, nao, sdets_t->extent(2)); // placeholder, not filled
    (*resfockbuilder)(*sdets_t, fock, true);
    auto energies_t = resfockbuilder->energies();

    // Calculate state-averaged energy
    double E_SA_t = nuclear_repulsion;
    for (ptrdiff_t state = 0; state < weight.size(); state++) {
      E_SA_t += weight[state] * energies_t->at(state);
    }

    return E_SA_t;
  };

  // run finite difference calculation of energy derivative
  qleve::Tensor<3> dE(nmo - nocc, nocc, ndets);

  for (ptrdiff_t sd = 0; sd < ndets; sd++) {
    for (ptrdiff_t i = 0; i < nocc; i++) {
      for (ptrdiff_t a = nocc; a < nmo; a++) {
        double energy_plus = compute_energy(sd, i, a, h);
        double energy_minus = compute_energy(sd, i, a, -h);

        dE(a - nocc, i, sd) = (energy_plus - energy_minus) / (2.0 * h);
      }
    }
  }
  // check FD results against original ResHF FOCK matrix
  qleve::Tensor<3> fock(*reshf->FOCK());
  fmt::print("fock norm: {:f}\n", fock.norm());

  for (ptrdiff_t sd = 0; sd < ndets; sd++) {
    auto dE_A = dE.pluck(sd);
    qleve::Tensor<2> fock_A(nmo, nmo);

    // transform fock matrix from AO to MO basis
    auto C_A = reshf->sdets()->pluck(sd);
    fmt::print("coeff norm: {:f}\n", C_A.norm());
    auto S = geometry->overlap();
    auto SC = qleve::gemm("n", "n", 1.0, S, C_A);
    auto CSC = qleve::gemm("t", "n", 1.0, C_A, SC);
    fmt::print("identity? : {:g}\n", qleve::linalg::ident(CSC));


    auto fa = fock.pluck(sd);
    auto FC = qleve::gemm("n", "n", 1.0, fa, C_A);
    qleve::gemm("t", "n", 4.0, C_A, FC, 0.0, fock_A);

    qleve::Tensor<2> zero_diff_A(dE.extent(0), dE.extent(1));
    for (ptrdiff_t a = nocc; a < nmo; a++) {
      for (ptrdiff_t i = 0; i < nocc; i++) {
        zero_diff_A(a - nocc, i) = std::abs(dE_A(a - nocc, i)) - std::abs(fock_A(a, i));
      }
    }

    fmt::print("Matrix norm of dE_ai - F_ia for S det {} is: {}\n", sd, zero_diff_A.norm());
  } // move to next Sdet
}