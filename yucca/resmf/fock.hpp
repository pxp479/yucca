// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/fock.hpp
///
/// ResHF Fock matrix builder
#pragma once

#include <yucca/structure/geometry.hpp>

namespace yucca
{

class ResFockBuilder {
 protected:
  // class containing info about the input molecule - nuclear arrangement, charge, basis set, etc
  const Geometry& geometry_;
  double hartree_;
  double exchange_;

  std::shared_ptr<qleve::Tensor<2>> overlap_; // overlap of atomic orbitals
  std::shared_ptr<qleve::Tensor<2>> hcore_;   // core hamiltonian matrix in atomic orbital basis

  std::ptrdiff_t nao_;  // number of atomic orbitals
  std::ptrdiff_t nmo_;  // number of molecular orbitals
  std::ptrdiff_t nocc_; // number of occupied molecular orbitals
  std::ptrdiff_t nvir_; // number of unoccupied (virtual) molecular orbitals

  std::ptrdiff_t nSD_;     // number of Slater determinants in ResHF wavefunction
  std::ptrdiff_t nstates_; // number of ResHF states

  // Nonorthogonal Slater determinants used to build the ResHF wavefunction
  std::shared_ptr<qleve::Tensor<3>> sdets_;

  // Diagonal matrix resulting from SVD of S_AB into U * sigma_AB * V^t
  std::shared_ptr<qleve::Tensor<3>> sigma_AB_;

  // Unitary transform matrix U resulting from SVD of S_AB into U * sigma_AB * V^t
  // Please note that there is a unique U matrix for every AB pairing of Slater determinants
  std::shared_ptr<qleve::Tensor<4>> UAb_occ_;
  std::shared_ptr<qleve::Tensor<4>> VBa_occ_;

  std::shared_ptr<qleve::Tensor<4>> AU_occ_; // C_A * UAb_occ_
  std::shared_ptr<qleve::Tensor<4>> BV_occ_; // C_B * VBa_occ_

  // Inverse of sigma_AB_ diagonal matrix. Numerically unstable!
  std::shared_ptr<qleve::Tensor<3>> invsigma_AB_;

  // Lower bound to the SVD diagonalized sigma matrix values.
  // Necessary in non-matrix adjugate ResHF Fock builds to damp down numeric instability caused by
  // nearly orthogonal Slater determinants with very small overlap values.
  double svd_floor_;

  // Diagnostic that examines the "conditioning" of the overlap of nonorthogonal molecular orbitals
  double orbital_condition_;
  // Diagnostic that examines the "conditioning" of the overlap of nonorthogonal Slater determinants
  double cond_number_;

  // Determinant of the occupied,occupied block of the overlap matrix of nonorthogonal molecular
  // orbitals. Generated from SVD diagonalized sigma_AB_, assuming |S_AB_| = |sigma_AB_|. This
  // assumption should hold for the restricted determinant case, since the end result is squared.
  // However, there may be a factor of +/- 1 resulting from the determinants of the SVD transform
  // matrices |U| and |V^t| that needs to be considered in future work.
  std::shared_ptr<qleve::Tensor<2>> detS_AB_;

  // Interdeterminant density matrix. Numerically unstable, except for the cases where sdet_A =
  // sdet_B.
  std::shared_ptr<qleve::Tensor<4>> W_AB_;
  // Interdeterminant fock matrix. Numerically unstable, except for the cases where sdet_A = sdet_B.
  // Not the same thing as the ResHF fock matrix!!!
  std::shared_ptr<qleve::Tensor<4>> F_AB_;

  // Interdeterminant Hamiltonian matrix
  std::shared_ptr<qleve::Tensor<2>> H_AB_;
  // Expansion coefficients of Slater determinants used to build the ResHF wavefunction
  std::shared_ptr<qleve::Tensor<2>> resHF_coeffs_;
  // ResHF state energies
  std::shared_ptr<qleve::Tensor<1>> energies_;
  // Weights to be applied to ResHF electronic states during state-averaging
  std::vector<double> weight_;

  // Flag to control the use of numerically stable matrix-adjugate ResHF Fock build routine
  bool use_matadj_;

  // Flag to turn on/off debugging print statements for KAB matrix
  bool print_kab_;

  // Numerically stable product of determinant and inverse overlap matrix
  std::shared_ptr<qleve::Tensor<3>> xi_AB_j_;
  // Numerically stable product of determinant and 2 inverse overlap matrices
  // NOTE: these quantities depend on the use of restricted determinants!
  std::shared_ptr<qleve::Tensor<4>> xi_AB_ij_;
  // Numerically stable interdeterminant density matrix that has a single equivalent of |S_AB| baked
  // in
  std::shared_ptr<qleve::Tensor<4>> Q_AB_;

  // Flag to control unitary transformation of Slater determinant basis during calculation of ResHF
  // Hamiltonian. If set to "true", Slater determinants derived from NTO hole-particle pairs of CIS
  // excited states (see CIS initial guess routine) are linearly combined to generate an open-shell
  // representation. This functionality is designed so that open-shell phenomena can be represented
  // using closed-shell, restricted Slater determinants.
  bool cis_transform_;
  // Unitary transformation matrix of the Slater determinant basis.
  // Built to transform Slater determinants generated from CIS excited states, but leave other
  // Slater determinants alone.
  std::shared_ptr<qleve::Tensor<2>> transform_;

  // Flag to control the constraint of Slater determinants derived from CIS initial guesses during
  // ResHF Fock build.
  bool cis_constrain_;
  // Pairs of Slater determinants to be held in constraint with each other during ResHF Fock build.
  std::vector<std::pair<ptrdiff_t, ptrdiff_t>> constrained_pairs_;

 public:
  // Initialization constructor:
  // Take parameters set by user input from resmf/reshf.cpp,
  // and set up the ResFockBuilder class which will build the ResHF Fock matrix.
  // Notably, sdets_ is NOT one of the input parameters taken from resmf/reshf.cpp
  // during the initialization of this class.
  // This is to allow for flexibility for use with the FDNR converger class,
  // which requires the calculation of several additional ResHF Fock matrices.
  ResFockBuilder(const Geometry& g, const std::vector<double>& weight,
                 const qleve::ConstTensorView<2>& U,
                 const std::vector<std::pair<ptrdiff_t, ptrdiff_t>>& constrained_pairs,
                 const std::ptrdiff_t nocc, const std::ptrdiff_t nmo, const std::ptrdiff_t nsd,
                 const double hartree = 1.0, const double exchange = 1.0,
                 const bool cis_transform = true, const bool cis_constrain = true,
                 const double svd_floor = 0.0, const bool use_matadj = true,
                 const bool print_kab = false);

  // Take a set of Slater determinants and generate ResHF Fock matrices for each.
  // When energies_only option is set to "true", only energies will be calculated.
  void operator()(const qleve::ConstTensorView<3>& sd_coeffs, qleve::TensorView<3> fock,
                  bool energies_only = false);

  // Perform singular value decomposition (SVD) on occupied, occupied block of
  // the molecular orbital overlap matrix between two sets of molecular orbitals.
  // SVD floor input is applied to the generated singular values. Diagnostic value for the
  // orthogonality (condition) of the molecular orbitals is monitored and stored.
  void mo_overlap_svd(const qleve::ConstTensorView<2>& CA, const qleve::ConstTensorView<2>& CB,
                      const qleve::ConstTensorView<2> Sao, const double svd_floor,
                      qleve::TensorView<1> sig, qleve::TensorView<2> U, qleve::TensorView<2> V,
                      double& orb_cond);

  // Calculate the interdeterminant density matrices from two sets of molecular orbitals and
  // their inverse overlap. The W_AB density matrix is numerically unstable, while
  // the Q_AB density matrix incorporates the determinant of the overlap to ensure
  // numeric stability. Numerically stable products between the overlap determinant and inverse (xi)
  // are also generated and stored.
  void calc_densities(const qleve::ConstTensorView<1>& sigAB, const qleve::ConstTensorView<2>& Ao,
                      const qleve::ConstTensorView<2>& Bo, qleve::TensorView<1> sigAB_inv,
                      qleve::TensorView<2> WAB, qleve::TensorView<1> xi_AB_i,
                      qleve::TensorView<2> xi_AB_ij, qleve::TensorView<2> QAB);

  // Using interdeterminant Hamiltonian and overlap of Slater determinants, calculate the expansion
  // coefficients of Slater determinants in the ResHF wavefunction. State energies are also
  // calculated. Different routines are used depending on whether or not NTO transform is "on".
  void build_coeffs_energies(const qleve::ConstTensorView<2> detSAB,
                             const qleve::ConstTensorView<2> HAB, const bool NTO_transform,
                             const qleve::ConstTensorView<2> U_transform,
                             qleve::TensorView<2> coeffs, qleve::TensorView<1> E_state);

  // Using ResFockBuilder class members, calculate ResHF Fock matrices for each provided Slater
  // determinant, applying state averaging weights as necessary. Different routines are used
  // depending on whether or not matrix adjugate implementation is requested. nSD_ number of ResHF
  // Fock matrices are generated in the atomic orbital basis.
  qleve::Tensor<3> build_fock_ao();

  std::ptrdiff_t nocc() const { return nocc_; }
  std::shared_ptr<qleve::Tensor<1>> energies() const { return energies_; }
  std::shared_ptr<qleve::Tensor<4>> wab() const { return W_AB_; }
  double orbital_condition() const { return orbital_condition_; }
  double cond_number() const { return cond_number_; }
  std::shared_ptr<qleve::Tensor<2>> coeffs() const { return resHF_coeffs_; }
  std::shared_ptr<qleve::Tensor<2>> hab() const { return H_AB_; }
  std::shared_ptr<qleve::Tensor<2>> detsab() const { return detS_AB_; }
};

} // namespace yucca
