// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/resmf/initial_guess.hpp
///
/// Initial guess functions like orbital rotation and CIS-NTO
#pragma once

#include <memory>

using namespace std;

namespace yucca::initial_guess
{
// function for orbital rotation initial guess generation
void rotate_guess(qleve::ConstTensorView<2> HF,
                  const vector<tuple<ptrdiff_t, ptrdiff_t, double>>& rotations,
                  qleve::TensorView<2> SDview);

// function for CIS initial guess generation
void CIS_guess(qleve::ConstTensorView<2> HF, qleve::ConstTensorView<2> CIS_excitations,
               const ptrdiff_t& cis, const ptrdiff_t& nnto, const ptrdiff_t& nocc,
               qleve::TensorView<3> SDview);

} // namespace yucca::initial_guess
