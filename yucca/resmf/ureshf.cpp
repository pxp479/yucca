// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/ureshf.cpp
///
/// uResHF driver class

#include <fmt/core.h>

#include <qleve/checks.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/orthogonalize.hpp>
#include <qleve/rotate.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/physical/constants.hpp>
#include <yucca/resmf/ureshf.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/wfn/single_reference.hpp>
#include <yucca/wfn/ureshf_reference.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

///////////////////////////////////////////////////////////////////////////////////////
//      Begin uResHF Method Constructors                                             //
///////////////////////////////////////////////////////////////////////////////////////

yucca::uResHF::uResHF(const InputNode& input, const shared_ptr<Geometry>& g) : Method_(g)
{
  fmt::print("Setting up uResHF calculation\n");

  // set up wavefunction optimization cutoffs
  max_iter_ = input.get<ptrdiff_t>("max_iter", 30);

  // this is an important block: set it up rigorously externally somewhere
  string conv_key = input.get_if_value<string>("convergence", "default");
  if (conv_key == "loose") {
    conv_energy_ = 1e-6;
    conv_error_ = 1e-3;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "default") {
    conv_energy_ = 1e-7;
    conv_error_ = 1e-5;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "tight") {
    conv_energy_ = 1e-8;
    conv_error_ = 1e-6;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "ultratight") {
    conv_energy_ = 1e-9;
    conv_error_ = 1e-7;
    conv_density_ = 1e10; // TODO
  }

  // allow specific overrides
  conv_energy_ = input.get<double>("conv_energy", conv_energy_);
  conv_error_ = input.get<double>("conv_error", conv_error_);
  conv_density_ = input.get<double>("conv_density", conv_density_);

  fmt::print("Convergence criteria:\n");
  fmt::print("  - change in energy (dE) < {:1e}\n", conv_energy_);
  fmt::print("  - gradient norm (grad) < {:1e}\n", conv_error_);
  fmt::print("  - change in density (drho) < {:1e}\n", conv_density_);
  fmt::print("\n");

  // set up AO basis quantities
  nao_ = geometry_->orbital_basis()->nbasis();
  if (!hcore_)
    hcore_ = make_shared<qleve::Tensor<2>>(geometry_->hcore());
  if (!overlap_)
    overlap_ = make_shared<qleve::Tensor<2>>(geometry_->overlap());

  // set up uResHF method options
  svd_floor_ = input.get<double>("svd_floor", 0.0);
  fmt::print("Suppressing singular values to have a floor of {:3e}\n", svd_floor_);

  use_matadj_ = input.get<bool>("matrix adjugate", true);
  if (use_matadj_) {
    fmt::print("Using matrix adjugate uResHF Fock build implementation!\n");
  }

  print_kab_ = input.get<bool>("print kab", false);
  if (print_kab_) {
    fmt::print("Fock matrix debug mode - Printing KAB terms enabled!\n");
  }

  fmt::print("\n");

  // Not real happy with this current code flow...
  // There's a "silent error" when you forget to specify the SCF
  // precursor stage... Do we really need both of these constructors?
  // Should we look into making (u)ResHF able to start without precursor calcs? - ERM

} // end first uResHF constructor

yucca::uResHF::uResHF(const InputNode& input, const shared_ptr<Wfn>& wfn) :
    uResHF(input, wfn->geometry())
{
  fmt::print("Pulling in starting orbitals for uResHF wavefunction initialization:\n\n");

  // dynamic cast of input wavefunction to a restricted SCF wavefunction
  const shared_ptr<SingleReference> rscf_ref = dynamic_pointer_cast<SingleReference>(wfn);
  const shared_ptr<uResHFReference> ureshf_ref = dynamic_pointer_cast<uResHFReference>(wfn);

  // initialize class variables using previously run restricted SCF calculation
  if (rscf_ref) {
    // set up molecular orbitals from a prior restricted HF calculation
    rscf_coeffs_ = make_shared<qleve::Tensor<2>>(*rscf_ref->coeffs());
    assert(rscf_coeffs_);

    nmo_ = rscf_coeffs_->extent(1);
    nelec_ = 2 * rscf_ref->nocc();
    charge_ = rscf_ref->geometry()->total_nuclear_charge() - nelec_;

    fmt::print("Preliminary restricted SCF reference properties:\n");
    fmt::print("  - nmo:    {}\n", nmo_);
    fmt::print("  - nelec:  {}\n", nelec_);
    fmt::print("  - charge: {}\n", charge_);
    fmt::print("\n");

    multiplicity_ = input.get<string>("multiplicity", "singlet");
    fmt::print("User requested uResHF wavefunction of {} spin multiplicity.\n\n", multiplicity_);
    if (multiplicity_ == "singlet") {

      nel_alpha_ = nelec_ / 2;
      nel_beta_ = nel_alpha_;

      nvir_alpha_ = nmo_ - nel_alpha_;
      nvir_beta_ = nvir_alpha_;

    } else if (multiplicity_ == "triplet") {

      nel_alpha_ = nelec_ / 2;
      nel_beta_ = nel_alpha_;
      nel_alpha_ += 1;
      nel_beta_ -= 1;
      assert(nel_alpha_ == (nel_beta_ + 2));

      nvir_alpha_ = nmo_ - nel_alpha_;
      nvir_beta_ = nmo_ - nel_beta_;

    } else {
      throw runtime_error("Unrecognized spin multiplicity! Currently only able to accomodate "
                          "singlet and triplet uResHF wavefunctions.");
    }

    fmt::print("Casting restricted MOs into unrestricted MO basis:\n");
    fmt::print("For alpha-spin electrons:\n");
    fmt::print("  - alpha-spin nmo:   {}\n", nmo_);
    fmt::print("  - alpha-spin nelec: {}\n", nel_alpha_);
    fmt::print("  - alpha-spin nvir:  {}\n", nvir_alpha_);

    fmt::print("For beta-spin electrons:\n");
    fmt::print("  - beta-spin nmo:   {}\n", nmo_);
    fmt::print("  - beta-spin nelec: {}\n", nel_beta_);
    fmt::print("  - beta-spin nvir:  {}\n", nvir_beta_);
    fmt::print("\n");

    // set up initial Slater determinants from user-input
    fmt::print("User-specified initial Slater determinants:\n");

    nSD_ = 0;
    nstates_ = 0;
    nrotSD_ = 0;

    // initialize holders for orbital rotation initial guess
    bool rotate_requested = false;
    vector<vector<vector<ptrdiff_t>>> hold_rot_orbitals{};
    vector<vector<double>> hold_rot_angles{};
    vector<vector<string>> hold_rot_spin{};

    auto guessnode = input.get_node("initial guess");
    // recast initial guess node as vector to allow for iteration through
    auto guessvec = get<vector<recursive_wrapper<InputNode>>>(guessnode.data_);
    for (auto& element : guessvec) {

      // pull out the "type" of initial guess and store as key variable
      const InputNode& typenode = element.get();
      string key = typenode.get_if_value<string>("type", "default");

      // save key for later to use when actually building initial guess determinants
      guess_types_.push_back(key);

      if (key == "hf" || key == "default") {
        fmt::print("  - Slater determinant #{}: HF ground state\n", nSD_);

        // update counters to track determinants and uResHF states
        nSD_++;
        nstates_++;
      } else if (key == "rotate") {
        fmt::print("  - Slater determinant #{}: Orbital rotations of HF ground state\n", nSD_);
        rotate_requested = true;

        // pull out "orbital:" node containing vectors in each element
        InputNode orbitnode;
        if (auto orbitcheck = typenode.get_node_if("orbitals"); orbitcheck) {
          orbitnode = typenode.get_node("orbitals");
        } else {
          throw runtime_error("No rotation orbitals defined! 'orbitals:' key is missing");
        }

        // initialize holder for pairs of orbitals to be rotated
        vector<vector<ptrdiff_t>> orbitpair{};

        // extract and validate each user-specified orbital pair (- [i,a]) in "orbitals:"
        for (ptrdiff_t ipair = 0; ipair < orbitnode.size(); ipair++) {

          // access and store orbital pair vector (- [i,a])
          auto pairvec = orbitnode.get_node(ipair);
          orbitpair.push_back(pairvec.as_vector<ptrdiff_t>());

          // check to make sure the user didn't put in anything weird
          for (auto& mo : pairvec.as_vector<ptrdiff_t>()) {
            if (mo < 0) {
              throw runtime_error("Orbital index values must be greater than or equal to 0!");
            }

            if (mo > nmo_) {
              throw runtime_error("Requested orbital index exceeds available MOs!");
            }
            // NOTE: I think this is robust enough? Because there's no real reason for the TOTAL
            // number of beta orbitals to be different from alpha orbitals, even if the number of
            // OCCUPIED orbitals might be different between alpha and beta. - ER

          } // to next MO index

        } // to next orbital pair

        // save all requested orbital rotation pairs for this determinant
        hold_rot_orbitals.push_back(orbitpair); // will be reorganized later!


        // access and store rotation angles for each orbital rotation in this determinant
        if (auto anglenode = typenode.get_node_if("angles"); anglenode) {
          if (anglenode->is_vector()) {
            hold_rot_angles.push_back(
                typenode.get_vector<double>("angles")); // will be reorganized later!
          }
        } else {
          throw runtime_error("No rotation angles defined! 'angles:' key is missing");
        }

        // access and store spin labels for each orbital rotation in this determinant
        if (auto spinnode = typenode.get_node_if("spin"); spinnode) {
          if (spinnode->is_vector()) {
            hold_rot_spin.push_back(
                typenode.get_vector<string>("spin")); // will be reorganized later!
          }
        } else {
          throw runtime_error("No orbital spin specified! 'spin:' key is missing");
        }

        // sanity check that the user gave the correct amounts of entries for everything
        for (ptrdiff_t idet = 0; idet < hold_rot_orbitals.size(); idet++) {
          if (hold_rot_orbitals[idet].size() != hold_rot_angles[idet].size()
              || hold_rot_orbitals[idet].size() != hold_rot_spin[idet].size()
              || hold_rot_spin[idet].size() != hold_rot_angles[idet].size()) {
            throw runtime_error("Must provide same number of orbital pairs, rotation angles, and "
                                "spin identifiers for a "
                                "given determinant!");
          }
        }

        // update counters to track determinants and uResHF states
        nSD_++;
        nstates_++;
        nrotSD_++;

      } else if (key == "cis") {
        throw runtime_error(
            "CIS initial guess not yet implemented for uResHF! Pro-tip: you can probably do what "
            "you were hoping to by using the orbital rotation initial guess!");
      } else {
        throw runtime_error("Unknown initial guess input specified in 'type:' key!");
      }
    } // move to next user-input initial guess type

    // reorganizing orbital rotation pairs, angles, and spin labels into a single vector holder
    // (rot_pairs_), which will be used later to build initial determinants
    if (rotate_requested) {
      for (ptrdiff_t idet = 0; idet < hold_rot_angles.size(); idet++) {
        auto orbitvec = hold_rot_orbitals[idet];
        auto anglevec = hold_rot_angles[idet];
        auto spinvec = hold_rot_spin[idet];
        vector<tuple<ptrdiff_t, ptrdiff_t, double, string>> detvec{};

        // iterate through all orbital rotations requested for a given determinant
        for (ptrdiff_t irot = 0; irot < orbitvec.size(); irot++) {
          auto orbit_pair = orbitvec[irot];
          auto angle = anglevec[irot];
          auto spin = spinvec[irot];

          auto rot_set = std::make_tuple(orbit_pair[0], orbit_pair[1], angle, spin);

          detvec.push_back(rot_set);
        }
        rot_pairs_.push_back(detvec);
      } // to the next determinant
    }

    fmt::print("\n");

  } else if (ureshf_ref) {
    fmt::print("Initial guess generated from previous uResHF calculation\n");

    rscf_coeffs_ = ureshf_ref->scf();
    sdets_ = ureshf_ref->sdets();

    nmo_ = ureshf_ref->nmo();
    auto [nea, neb] = ureshf_ref->nelec();
    nel_alpha_ = nea;
    nel_beta_ = neb;
    nvir_alpha_ = nmo_ - nel_alpha_;
    nvir_beta_ = nmo_ - nel_beta_;

    nelec_ = nel_alpha_ + nel_beta_;
    charge_ = ureshf_ref->geometry()->total_nuclear_charge() - nelec_;

    auto wfnco = ureshf_ref->wfn_coeffs();
    nSD_ = wfnco->extent(0);
    nstates_ = wfnco->extent(1);

  } else {
    throw runtime_error("Uh oh, I need some starting MOs to work with! Please run an SCF "
                        "or uResHF calculation prior to running uResHF!");
  } // end prior calculation if/else conditional

  fmt::print("State averaging set up:\n");

  // check if user has specified the "weights:" node for state averaging,
  // and if not, then throw an error (no default set!)
  if (auto weightnode = input.get_node_if("weights"); weightnode) {
    // check if user provided a vector, and if not, throw error
    if (weightnode->is_vector()) {

      // access and validate state averaging weights
      state_weight_ = input.get_vector<double>("weights");
      double weight_sum = 0.0; // tracker for weight normalization
      for (ptrdiff_t i = 0; i < state_weight_.size(); i++) {
        if (state_weight_[i] > 0.0) {
          fmt::print("  - State {} selected for averaging with weight {}\n", i, state_weight_[i]);
        }
        if (state_weight_[i] < 0.0) {
          throw runtime_error("Weights to be applied to states must be non-negative!");
        }
        weight_sum += state_weight_[i];
      }
      fmt::print("\n");

      // normalize state averaging weights to 1.0 if needed
      if (weight_sum != 1.00) {
        fmt::print("Normalizing weights to 1.0\n");
        fmt::print("New weights: ");
        for (ptrdiff_t i = 0; i < state_weight_.size(); ++i) {
          state_weight_[i] /= weight_sum;
          fmt::print("{:12.6f}", state_weight_[i]);
        }
        fmt::print("\n");
      }
    } else {
      throw runtime_error("'weights:' block incorrectly formatted! Please format as vector, such "
                          "as 'weights: [1.0, 1.0]'");
    }
  } else {
    throw runtime_error(
        "Please specify State Averaging with 'weights:' block. If single-state "
        "ResHF is desired, use 'weights: [1.0]' for ground SS-ResHF, or 'weights: [0.0, "
        "1.0]' for S1 state SS-ResHF, etc.");
  }

  assert(nSD_ == nstates_); // might need to change this later if we end up doing tricks like in
                            // restricted ResHF

  if (state_weight_.size() > nstates_) {
    throw runtime_error("Number of states to be averaged must be less than or equal to number of "
                        "total ResHF states (number of determinants)!");
  }

  fmt::print("\n");

  // here's a flag to switch between convergers!
  use_good_conv_ = input.get<bool>("good conv", true);

  if (use_good_conv_) {
    // initialize converger (uconverger)
    std::array<ptrdiff_t, 2> nelec = {nel_alpha_, nel_beta_};
    converger_ = make_unique<UResHFConverger>(input, nelec);
  } else {
    // initialize convergers for alpha and beta fock matrices
    converger_alpha_ = make_unique<ResMFConverger>(input, nel_alpha_, nSD_);
    fmt::print("\n");
    converger_beta_ = make_unique<ResMFConverger>(input, nel_beta_, nSD_);
    fmt::print("\n");
  }


} // end second uResHF constructor

///////////////////////////////////////////////////////////////////////////////////////
//      Begin uResHF Debug Copy Constructor                                          //
///////////////////////////////////////////////////////////////////////////////////////

yucca::uResHF::uResHF(const uResHF& x) : Method_(x)
{
  this->nao_ = x.nao_;
  this->nmo_ = x.nmo_;

  this->overlap_ = make_shared<Tensor<2>>(*x.overlap_);
  this->hcore_ = make_shared<Tensor<2>>(*x.hcore_);

  this->rscf_coeffs_ = make_shared<Tensor<2>>(*x.rscf_coeffs_);
  this->nelec_ = x.nelec_;
  this->charge_ = x.charge_;

  this->multiplicity_ = x.multiplicity_;

  this->nel_alpha_ = x.nel_alpha_;
  this->nel_beta_ = x.nel_beta_;
  this->nvir_alpha_ = x.nvir_alpha_;
  this->nvir_beta_ = x.nvir_beta_;

  this->nSD_ = x.nSD_;
  this->nstates_ = x.nstates_;

  this->state_weight_ = x.state_weight_;

  this->sdets_ = make_shared<Tensor<4>>(*x.sdets_);

  this->guess_types_ = x.guess_types_;

  this->nrotSD_ = x.nrotSD_;
  this->rot_pairs_ = x.rot_pairs_;

  this->uresfockbuilder_ = make_shared<uResFockBuilder>(*x.uresfockbuilder_);
  this->print_kab_ = x.print_kab_;
  this->use_matadj_ = x.use_matadj_;
  this->svd_floor_ = x.svd_floor_;

  this->energies_ = make_shared<Tensor<1>>(*x.energies_);
  this->resHF_coeffs_ = make_shared<Tensor<2>>(*x.resHF_coeffs_);
  this->energySA_ = x.energySA_;

  this->conv_energy_ = x.conv_energy_;
  this->conv_error_ = x.conv_error_;
  this->conv_density_ = x.conv_density_;

  this->max_iter_ = x.max_iter_;

  // skipping copying of converger_alpha_ and converger_beta_ because unique pointers get weird...

  this->orb_gradient_norm_ = x.orb_gradient_norm_;

} // end uResHF copy constructor

///////////////////////////////////////////////////////////////////////////////////////
//      Begin uResHF Compute Function                                                //
///////////////////////////////////////////////////////////////////////////////////////

void yucca::uResHF::compute()
{
  fmt::print("Starting Unrestricted ResHF calculation\n\n");

  if (!sdets_) {
    fmt::print("Building initial uResHF wavefucntion from user-specified determinants:\n");

    // initialize holders for Slater determinants
    sdets_ = make_shared<Tensor<4>>(nao_, nmo_, nSD_, 2);

    // set Slater determinant counters to zero
    ptrdiff_t det = 0;  // counter for all Slater determinants
    ptrdiff_t rdet = 0; // counter for orbital rotation Slater determinants

    // begin iterating through all user-specified determinant initial guesses
    for (auto& key : guess_types_) {
      if (key == "hf" || key == "default") {
        fmt::print("  - Storing ground state HF as Slater determinant #{}\n", det);

        // initialize alpha and beta orbitals from the same set of restriced HF orbitals,
        // and store MOs directly with no further modification

        auto sdview_alpha = sdets_->pluck(det, 0);
        assert(sdview_alpha.extent(1) == rscf_coeffs_->extent(1));
        sdview_alpha = *rscf_coeffs_;

        auto sdview_beta = sdets_->pluck(det, 1);
        assert(sdview_beta.extent(1) == rscf_coeffs_->extent(1));
        sdview_beta = *rscf_coeffs_;

        det++;

      } else if (key == "rotate") {
        fmt::print("  - Storing orbital rotated HF determinant as Slater determinant #{}\n", det);

        // access the determinant placeholder from class variables

        auto sdview_alpha = sdets_->pluck(det, 0);
        assert(sdview_alpha.extent(1) == rscf_coeffs_->extent(1));

        auto sdview_beta = sdets_->pluck(det, 1);
        assert(sdview_beta.extent(1) == rscf_coeffs_->extent(1));

        // access information about orbitals to be rotated
        auto rotvec = rot_pairs_[rdet];

        // initialize alpha and beta orbitals from the same set of restriced HF orbitals
        sdview_alpha = *rscf_coeffs_;
        sdview_beta = *rscf_coeffs_;

        // iterate through each orbital rotation requested for this determinant
        for (ptrdiff_t irot = 0; irot < rotvec.size(); irot++) {
          auto [i, a, theta, omega] = rotvec[irot];

          double theta_rad = (theta / 180) * physical::pi<double>;

          // determine whether to rotate alpha or beta orbitals
          if (omega == "alpha") {
            // print info about which orbitals will be rotated
            fmt::print("     * Rotating alpha-spin orbitals:\n");
            if (i < nel_alpha_) {
              fmt::print("       occupied orbital {}\n", i);
            } else {
              fmt::print("       virtual orbital {}\n", i);
            }
            if (a < nel_alpha_) {
              fmt::print("       occupied orbital {}\n", a);
            } else {
              fmt::print("       virtual orbital {}\n", a);
            }
            fmt::print("       rotation angle {}\n", theta);

            // perform orbital rotation
            rotate(nao_, &sdview_alpha(0, i), 1, &sdview_alpha(0, a), 1, cos(theta_rad),
                   sin(theta_rad));

          } else if (omega == "beta") {
            // print info about which orbitals will be rotated
            fmt::print("     * Rotating beta-spin orbitals:\n");
            if (i < nel_beta_) {
              fmt::print("       occupied orbital {}\n", i);
            } else {
              fmt::print("       virtual orbital {}\n", i);
            }
            if (a < nel_beta_) {
              fmt::print("       occupied orbital {}\n", a);
            } else {
              fmt::print("       virtual orbital {}\n", a);
            }
            fmt::print("       rotation angle {}\n", theta);

            // perform orbital rotation
            rotate(nao_, &sdview_beta(0, i), 1, &sdview_beta(0, a), 1, cos(theta_rad),
                   sin(theta_rad));

          } else {
            throw runtime_error("Unknown spin identifier! Please input either alpha or beta!");
            // hopefully you'll never trigger this, as this should be caught during input parsing
          }
        }

        det++;
        rdet++;

      } else if (key == "cis") {
        throw runtime_error("Not yet implemented!");
      } else {
        throw runtime_error("Unknown initial guess type!");
      }
    } // to next guess type key
  } // end conditional initial guess generation


  // make sure orbitals are orthogonal within each set
  fmt::print("Checking orthogonality of MOs within each Slater determinant:\n");
  bool any_nonorthogonal = false;

  fmt::print("  - Alpha Spin MO's:\n");
  Tensor<2> olap_a(nmo_, nmo_);
  for (ptrdiff_t sd = 0; sd < nSD_; ++sd) {
    auto cc_a = sdets_->pluck(sd, 0);
    matrix_transform(1.0, *overlap_, cc_a, 0.0, olap_a);
    const double id = linalg::ident(olap_a);
    fmt::print("     * SD{} CSC ident? = {}\n", sd, id);
    if (id > 1e-12) {
      any_nonorthogonal = true;
      auto u_a = linalg::orthogonalize_metric(olap_a, 1e-12);
      auto ccnew_a = gemm("n", "n", 1.0, cc_a, u_a);
      cc_a = ccnew_a;
    }
  }

  fmt::print("  - Beta Spin MO's:\n");
  Tensor<2> olap_b(nmo_, nmo_);
  for (ptrdiff_t sd = 0; sd < nSD_; ++sd) {
    auto cc_b = sdets_->pluck(sd, 1);
    matrix_transform(1.0, *overlap_, cc_b, 0.0, olap_b);
    const double id = linalg::ident(olap_b);
    fmt::print("     * SD{} CSC ident? = {}\n", sd, id);
    if (id > 1e-12) {
      any_nonorthogonal = true;
      auto u_b = linalg::orthogonalize_metric(olap_b, 1e-12);
      auto ccnew_b = gemm("n", "n", 1.0, cc_b, u_b);
      cc_b = ccnew_b;
    }
  }

  if (any_nonorthogonal) {
    fmt::print("Input orbitals found to be nonorthogonal. Orthogonalizing.\n\n");
  } else {
    fmt::print("Input orbitals are good to go!\n\n");
  }

  // initialize uResHF fock builder
  uresfockbuilder_ = make_shared<uResFockBuilder>(
      *geometry_, nel_alpha_, nel_beta_, nmo_, state_weight_, svd_floor_, use_matadj_, print_kab_);

  // initialize key holders for information (to be printed after optimization)
  energies_ = make_shared<Tensor<1>>(nstates_);
  resHF_coeffs_ = make_shared<Tensor<2>>(nSD_, nstates_);
  fock_ = make_shared<Tensor<4>>(nao_, nao_, nSD_, 2);

  // set variables to control wavefunc. opt. convergence and printing
  energySA_ = 0.0;
  double nuclear_repulsion = geometry_->nuclear_repulsion();
  double old_energy = 0.0;
  bool conv = false;

  fmt::print("Starting wavefunction optimization\n");
  fmt::print("Note: defaults to use of SCF converger unless otherwise stated\n\n");

  fmt::print("{:>17s} {:>14s} {:>14s} {:>8s} {:>8s} {:>8s} {:>10s} \n", "iter", "E0 E", "E_SA",
             "|gradient|", "det-cond", "orb-cond", "dE");
  fmt::print("{:-^93s}\n", "");

  // begin wavefunction optimization iterations
  for (ptrdiff_t iopt = 0; iopt < max_iter_; iopt++) {

    // build alpha and beta fock matrices from current iteration of Slater determinants
    *fock_ = (*uresfockbuilder_)(sdets_->pluck(0), sdets_->pluck(1));

    // access some important intermediates created during fock building
    energies_ = uresfockbuilder_->energies();
    energySA_ = uresfockbuilder_->ESA() + nuclear_repulsion;
    resHF_coeffs_ = uresfockbuilder_->coeffs();
    auto fockerr = *(uresfockbuilder_->error());

    // update convergers with status info about this optimization iteration
    if (use_good_conv_) {
      converger_->push(*fock_, fockerr, energySA_);
    } else {
      converger_alpha_->push(fock_->const_pluck(0), fockerr.const_pluck(0), energySA_);
      converger_beta_->push(fock_->const_pluck(1), fockerr.const_pluck(1), energySA_);
    }

    // get some key results ready for printing
    double a_orb_grad_norm = fockerr.const_pluck(0).norm();
    double b_orb_grad_norm = fockerr.const_pluck(1).norm();

    orb_gradient_norm_ = (a_orb_grad_norm + b_orb_grad_norm) / 2.0;

    double dE = 0.0;
    if (iopt != 0) {
      dE = energySA_ - old_energy;
    } else {
      dE = 1000.00; // just a funny number to prevent converger from thinking 1 iteration is "done"
    }
    old_energy = energySA_;

    // print said results
    fmt::print("{:s} {:6d} {:14.8f} {:14.8f} {:8.2g} {:8.2g} {:8.2g}", "uResHF iter:", iopt,
               energies_->at(0) + nuclear_repulsion, energySA_, orb_gradient_norm_, 0.0, 42.0);

    if (iopt != 0) {
      fmt::print("{:10.2g}", dE);
    } else {
      fmt::print("{:>8s}", "---");
    }
    fmt::print("\n");

    // check wavefunction convergence
    conv = converged(dE, a_orb_grad_norm, b_orb_grad_norm, 0.0);
    if (conv) {
      fmt::print("uResHF Variational Optimization has converged! :D \n\n");
      break;
    }

    // update Slater determinants (if not final iteration)
    if (iopt != (max_iter_ - 1)) {
      if (use_good_conv_) {
        converger_->next(*sdets_);
      } else {
        converger_alpha_->next(sdets_->pluck(0));
        converger_beta_->next(sdets_->pluck(1));
      }
    }
  } // onwards to the next optimization step!

  // print some properties and such
  fmt::print("How about some final results?\n\n");

  fmt::print("uResHF State (Total) Energies (hartrees): \n");
  for (ptrdiff_t ist = 0; ist < energies_->size(); ist++) {
    fmt::print("   * uResHF State #{}: {:14.8f}\n", ist, energies_->at(ist) + nuclear_repulsion);
  }
  fmt::print("\n");

  fmt::print("uResHF State (Electronic) Energies (hartrees): \n");
  for (ptrdiff_t ist = 0; ist < energies_->size(); ist++) {
    fmt::print("   * uResHF State #{}: {:14.8f}\n", ist, energies_->at(ist));
  }
  fmt::print("\n");

  fmt::print("uResHF Wavefunction Composition: \n");
  for (ptrdiff_t ist = 0; ist < resHF_coeffs_->extent(1); ist++) {
    fmt::print("   * uResHF State #{}: \n", ist);
    for (ptrdiff_t idet = 0; idet < resHF_coeffs_->extent(0); idet++)
      fmt::print("      - Slater determinant #{} contribution: {:14.8f} \n", idet,
                 resHF_coeffs_->at(idet, ist));
  }
  fmt::print("\n");

} // end uResHF compute function

///////////////////////////////////////////////////////////////////////////////////////
//      uResHF Class Function Definitions                                            //
///////////////////////////////////////////////////////////////////////////////////////

void uResHF::compute_gradient_impl()
{
  gradient_ = make_unique<qleve::Tensor<2>>(3, geometry_->natoms());
}

bool uResHF::converged(const double dE, const double alpha_error, const double beta_error,
                       const double drho) const
{
  bool conv = (abs(dE) < conv_energy_) && (abs(alpha_error) < conv_error_)
              && (abs(beta_error) < conv_error_) && (abs(drho) < conv_density_);
  return conv;
}

shared_ptr<Wfn> uResHF::wavefunction()
{ 
  std::array<ptrdiff_t, 2> nel_array;
  nel_array[0] = nel_alpha_;
  nel_array[1] = nel_beta_;
  return make_shared<uResHFReference>(geometry_, rscf_coeffs_, sdets_, resHF_coeffs_, fock_,
                                      state_weight_, nel_array, nmo_, energySA_);
}

///////////////////////////////////////////////////////////////////////////////////////
//      FD debugger for uResHF fock matrix                                           //
///////////////////////////////////////////////////////////////////////////////////////

void yucca::fd_fock_ureshf(const shared_ptr<Method_>& method,
                           const shared_ptr<yucca::Geometry>& geometry, const double h)
{
  auto ureshf = dynamic_pointer_cast<yucca::uResHF>(method);

  if (!ureshf) {
    throw runtime_error("Dynamic cast of base method to uResHF method failed!");
  }

  // pull out size placeholders
  const ptrdiff_t nao = ureshf->nao();
  const ptrdiff_t nmo = ureshf->nmo();
  const ptrdiff_t nocc_alpha = ureshf->nocc_alpha();
  const ptrdiff_t nocc_beta = ureshf->nocc_beta();
  const ptrdiff_t ndets = ureshf->nSD();

  const double nuclear_repulsion = geometry->nuclear_repulsion();

  // function to compute the FD energies for a given orbital rotation
  auto compute_energy = [&](const ptrdiff_t sd, const ptrdiff_t i, const ptrdiff_t a,
                            const string spin, const double r) {
    // make a copy of the uResHF method class
    yucca::uResHF ureshf_test(*ureshf);

    // copy the pointer of test uResHF fockbuilder function
    auto test_uresfockbuilder = ureshf_test.uresfockbuilder();

    // apply FD orbital rotation to MOs, dependent on input spin!
    std::shared_ptr<qleve::Tensor<4>> test_sd = ureshf_test.sdets();

    assert(spin == "alpha" || spin == "beta");
    if (spin == "alpha") {
      // rotate selected alpha-spin MOs (sdets_t->pluck(sd))
      qleve::rotate(nao, &(test_sd->pluck(sd, 0))(0, i), &(test_sd->pluck(sd, 0))(0, a), r);
    } else if (spin == "beta") {
      // rotate selected beta-spin MOs
      qleve::rotate(nao, &(test_sd->pluck(sd, 1))(0, i), &(test_sd->pluck(sd, 1))(0, a), r);
    }

    // Generate state-averaged energies with rotated sdets with a call to ufockbuilder
    auto empty_fock = (*test_uresfockbuilder)(test_sd->pluck(0), test_sd->pluck(1), true);
    auto state_E = test_uresfockbuilder->energies();
    double test_ESA_total = test_uresfockbuilder->ESA();

    return test_ESA_total;
  };

  // run finite difference calculation of energy derivative
  // calculate for both alpha spin and beta spin MO rotations
  fmt::print("Running Finite Difference of E_SA derivatives:\n");
  qleve::Tensor<3> dE_e1_alpha(nmo - nocc_alpha, nocc_alpha, ndets);
  qleve::Tensor<3> dE_e2_alpha(dE_e1_alpha.zeros_like());
  qleve::Tensor<3> dESA_alpha(dE_e1_alpha.zeros_like());
  for (ptrdiff_t sd = 0; sd < ndets; sd++) {
    fmt::print(" - Calculating alpha-spin dE matrix for Slater Determinant #{}\n", sd);
    for (ptrdiff_t iu = 0; iu < nocc_alpha; iu++) {
      for (ptrdiff_t au = nocc_alpha; au < nmo; au++) {
        double ESA_plus = compute_energy(sd, iu, au, "alpha", h);
        double ESA_minus = compute_energy(sd, iu, au, "alpha", -h);

        dESA_alpha(au - nocc_alpha, iu, sd) = (ESA_plus - ESA_minus) / (4.0 * h);
        // divide by extra factor of 2 to match fock matrices
      }
    }
  }

  // rinse and repeat for beta MOs
  qleve::Tensor<3> dE_e1_beta(nmo - nocc_beta, nocc_beta, ndets);
  qleve::Tensor<3> dE_e2_beta(dE_e1_beta.zeros_like());
  qleve::Tensor<3> dESA_beta(dE_e1_beta.zeros_like());
  for (ptrdiff_t sd = 0; sd < ndets; sd++) {
    fmt::print(" - Calculating beta-spin dE matrix for Slater Determinant #{}\n", sd);
    for (ptrdiff_t iu = 0; iu < nocc_beta; iu++) {
      for (ptrdiff_t au = nocc_beta; au < nmo; au++) {
        double ESA_plus = compute_energy(sd, iu, au, "beta", h);
        double ESA_minus = compute_energy(sd, iu, au, "beta", -h);

        dESA_beta(au - nocc_beta, iu, sd) = (ESA_plus - ESA_minus) / (4.0 * h);
        // divide by extra factor of 2 to match fock matrices
      }
    }
  }

  // pull out fock matrix terms from original (NOT copied!) uResHF method
  auto OG_uresfockbuilder = ureshf->uresfockbuilder();
  auto [OG_aofock_a, OG_aofock_b] = OG_uresfockbuilder->aofock_total();

  // check FD results against original ResHF FOCK matrix

  // cycle through each sdet and compare dE_SA to Fock matrix elements
  for (ptrdiff_t sd = 0; sd < ndets; sd++) {
    auto aoFA_total_a = OG_aofock_a.const_pluck(sd);
    auto aoFA_total_b = OG_aofock_b.const_pluck(sd);

    // initialize MO fock matrix holders
    qleve::Tensor<2> moFA_total_a(nmo, nmo);
    qleve::Tensor<2> moFA_total_b(nmo, nmo);

    // transform fock matrix from AO to MO basis
    qleve::Tensor<2> sao = geometry->overlap();
    fmt::print("Checking orthonormality of MO coeffs for SD#{}:\n", sd);
    qleve::Tensor<2> C_A_a(ureshf->sdets()->const_pluck(sd, 0));
    qleve::Tensor<2> CSC_a(nmo, nmo);
    qleve::matrix_transform(1.0, sao, C_A_a, 0.0, CSC_a);
    fmt::print(" - Alpha CA orthonormal? : {:g}\n", qleve::linalg::ident(CSC_a));

    qleve::matrix_transform("t", "n", 1.0, C_A_a, aoFA_total_a, C_A_a, 0.0, moFA_total_a);

    qleve::Tensor<2> C_A_b(ureshf->sdets()->const_pluck(sd, 1));
    qleve::Tensor<2> CSC_b(nmo, nmo);
    qleve::matrix_transform(1.0, sao, C_A_b, 0.0, CSC_b);
    fmt::print(" - Beta CA orthonormal? : {:g}\n", qleve::linalg::ident(CSC_b));

    qleve::matrix_transform("t", "n", 1.0, C_A_b, aoFA_total_b, C_A_b, 0.0, moFA_total_b);

    // access the appropriate dE_SA matrix
    auto dESA_a = dESA_alpha.const_pluck(sd);
    auto dESA_b = dESA_beta.const_pluck(sd);

    // calculate difference between Fia and dEai
    fmt::print("uFock Matrix Error Norms for SD#{}:\n", sd);
    qleve::Tensor<2> diffAtotal_a(nmo, nocc_alpha);
    for (ptrdiff_t au = nocc_alpha; au < nmo; au++) {
      for (ptrdiff_t iu = 0; iu < nocc_alpha; iu++) {
        diffAtotal_a(au - nocc_alpha, iu) = dESA_a(au - nocc_alpha, iu) - moFA_total_a(iu, au);
      }
    }
    fmt::print("  - Alpha-spin Total Fock term error: {}\n", diffAtotal_a.norm());

    qleve::Tensor<2> diffAtotal_b(nmo, nocc_beta);
    for (ptrdiff_t ad = nocc_beta; ad < nmo; ad++) {
      for (ptrdiff_t id = 0; id < nocc_beta; id++) {
        diffAtotal_b(ad - nocc_beta, id) = dESA_b(ad - nocc_beta, id) - moFA_total_b(id, ad);
      }
    }
    fmt::print("  - Beta-spin Total Fock term error: {}\n", diffAtotal_b.norm());

  } // move to next Sdet
}
