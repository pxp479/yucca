// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/ureshf.hpp
///
/// Unrestricted ResHF driver class

#pragma once

#include <memory>

#include <yucca/resmf/converger.hpp>
#include <yucca/resmf/uconverger.hpp>
#include <yucca/resmf/ufock.hpp>
#include <yucca/util/method.hpp>

namespace yucca
{

// use forward declarations here to prevent compilation cascades
class Geometry; // #include <yucca/structure/geometry.hpp>
class Wfn;      // #include <yucca/wfn/wfn.hpp>

class uResHF : public Method_ {
 protected:
  std::ptrdiff_t nao_; // number of atomic orbitals
  std::ptrdiff_t nmo_; // 1/2 * number of molecular orbitals (nmo_alpha = nmo_beta)

  std::shared_ptr<qleve::Tensor<2>> overlap_; // overlap of atomic orbitals
  std::shared_ptr<qleve::Tensor<2>> hcore_;   // core hamiltonian matrix in atomic orbital basis

  std::shared_ptr<qleve::Tensor<2>> rscf_coeffs_; // restricted HF ground state determinant
  std::ptrdiff_t nelec_;                          // number of total electrons
  int charge_;                                    // overall charge of molecule

  std::string multiplicity_; // uResHF wavefunction spin multiplicity (singlet or triplet)

  std::ptrdiff_t nel_alpha_;  // number of alpha-spin electrons
  std::ptrdiff_t nel_beta_;   // number of beta-spin electrons
  std::ptrdiff_t nvir_alpha_; // number of alpha-spin virtual MO
  std::ptrdiff_t nvir_beta_;  // number of beta-spin virtual MO

  std::ptrdiff_t nSD_;     // number of Slater determinants in ResHF wavefunction
  std::ptrdiff_t nstates_; // number of ResHF states

  // Weights to be applied to ResHF electronic states during state-averaging
  std::vector<double> state_weight_;

  // Nonorthogonal Slater determinants used to build the ResHF wavefunction
  std::shared_ptr<qleve::Tensor<4>> sdets_;

  // Internal holder of user-specified initial guess types
  std::vector<std::string> guess_types_;

  // Number of Slater determinants generated from orbital rotation initial guess
  std::ptrdiff_t nrotSD_;
  // Orbital pairs, angle of rotation, and spin of orbitals to be rotated
  // Used for orbital rotation initial guess
  std::vector<std::vector<std::tuple<ptrdiff_t, ptrdiff_t, double, std::string>>> rot_pairs_;

  // fock matrix!
  std::shared_ptr<qleve::Tensor<4>> fock_;
  // Manages the building routine for the uResHF Fock matrices
  std::shared_ptr<uResFockBuilder> uresfockbuilder_;
  // Flag to turn on/off debugging print statements for KAB matrix
  bool print_kab_;
  // Flag to control the use of numerically stable matrix-adjugate ResHF Fock build routine
  bool use_matadj_;
  // Minimum singular value threshold for SVD of MO overlap matrices
  // Necessary in non-matrix adjugate ResHF Fock builds to damp down numeric instability caused by
  // nearly orthogonal Slater determinants with very small overlap values.
  double svd_floor_;

  // ResHF state energies
  std::shared_ptr<qleve::Tensor<1>> energies_;
  // Expansion coefficients of Slater determinants used to build the ResHF wavefunction
  std::shared_ptr<qleve::Tensor<2>> resHF_coeffs_;
  double energySA_; // ResHF state-averaged energy

  // Function to determine convergence of wavefunction optimization procedure
  bool converged(const double dE, const double alpha_error, const double beta_error,
                 const double drho) const;
  double conv_energy_;  // wavefunction optimization dE convergence threshold
  double conv_error_;   // wavefunction optimization orbital gradient convergence threshold
  double conv_density_; // wavefunction optimization ??? convergence threshold

  int max_iter_; // maximum number of wavefunction optimization iterations

  // Manages the wavefunction optimization routine.
  // For a given iteration of the wavefunction optimization, propagates Slater determinants forward
  // based on calculated uResHF Fock matrices
  std::unique_ptr<ResMFConverger> converger_alpha_;
  std::unique_ptr<ResMFConverger> converger_beta_;
  std::unique_ptr<UResHFConverger> converger_;
  bool use_good_conv_;

  void compute_gradient_impl() override; // placeholder
  double orb_gradient_norm_;             // what is this friend for?

 public:
  // constructors
  uResHF(const InputNode& input, const std::shared_ptr<Geometry>& g);
  uResHF(const InputNode& input, const std::shared_ptr<Wfn>& wfn);
  uResHF(const uResHF& x);

  void compute() override;
  double energy() override { return energySA_; }

  std::vector<double> energies() override
  {
    std::vector<double> energies_vector(energies_->size());
    for (std::ptrdiff_t n = 0; n < energies_vector.size(); n++) {
      energies_vector[n] = energies_->at(n);
    }

    return energies_vector;
  }

  std::shared_ptr<Wfn> wavefunction() override; // placeholder

  std::shared_ptr<qleve::Tensor<1>> state_energies() const { return energies_; }
  double orb_gradient() const { return orb_gradient_norm_; };

  std::ptrdiff_t nao() const { return nao_; }
  std::ptrdiff_t nmo() const { return nmo_; }
  std::ptrdiff_t nocc_alpha() const { return nel_alpha_; }
  std::ptrdiff_t nocc_beta() const { return nel_beta_; }
  std::ptrdiff_t nSD() const { return nSD_; }

  std::vector<double> state_weights() const { return state_weight_; }
  std::shared_ptr<qleve::Tensor<4>> sdets() const { return sdets_; }

  std::shared_ptr<uResFockBuilder> uresfockbuilder() const { return uresfockbuilder_; }
};

void fd_fock_ureshf(const std::shared_ptr<Method_>& method,
                    const std::shared_ptr<yucca::Geometry>& geometry, const double h);
} // namespace yucca