// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/algo/davidson.hpp
///
/// Utility algorithm for Davidson diagonalization
#pragma once

#include <memory>
#include <vector>

#include <qleve/matrix_multiply.hpp>
#include <qleve/orthogonalize.hpp>
#include <qleve/tensor.hpp>
#include <qleve/vector_ops.hpp>

namespace yucca
{

template <typename DataType>
class Davidson {
 public:
  using MatType = qleve::Tensor_<DataType, 2>;
  using MatViewType = qleve::TensorView_<DataType, 2>;
  using ArrayType = qleve::Tensor_<DataType, 1>;

 protected:
  ptrdiff_t neigs_;

  ptrdiff_t size_;
  ptrdiff_t max_subspace_;
  ptrdiff_t ncurrent_;

  std::shared_ptr<ArrayType> diag_;

  std::unique_ptr<MatType> V_; ///< Basis vectors
  std::unique_ptr<MatType> W_; ///< Matrix multiplies
  std::unique_ptr<MatType> H_; ///< Subspace Hamiltonian
  std::unique_ptr<MatType> S_; ///< Subspace Overlap

 public:
  Davidson(const size_t neigs, std::shared_ptr<ArrayType> diag, const size_t max_subspace) :
      neigs_(neigs),
      size_(diag->size()),
      max_subspace_(max_subspace),
      ncurrent_(0),
      diag_(diag),
      V_(std::make_unique<MatType>(size_, max_subspace_)),
      W_(std::make_unique<MatType>(size_, max_subspace_)),
      H_(std::make_unique<MatType>(max_subspace_, max_subspace_)),
      S_(std::make_unique<MatType>(max_subspace_, max_subspace_))
  {}

  template <typename Func>
  std::tuple<ArrayType, MatType> diagonalize(Func H, const size_t maxiter, const double thresh)
  {
    const size_t nguess = std::min(size_, neigs_);
    std::vector<size_t> inds(size_);
    std::iota(inds.begin(), inds.end(), 0ull);
    std::partial_sort(inds.begin(), inds.begin() + nguess, inds.end(),
                      [this](int i, int j) { return diag_->operator()(i) < diag_->operator()(j); });

    // V0 and W0 will just hold the memory for the matrix vector products
    MatType V0(size_, nguess);
    for (size_t i = 0; i < nguess; ++i) {
      V0(inds[i], i) = 1.0;
    }
    MatType W0 = V0.zeros_like();

    size_t nnew = nguess;

    for (size_t iter = 0; iter < maxiter; ++iter) {
      qleve::ConstTensorView<2> VV(V0.data(), V0.slice(0, nnew).shape());
      qleve::TensorView<2> WW(W0.data(), W0.slice(0, nnew).shape());

      H(VV, WW);
      this->push_back(VV, WW);
      auto [eigs, V, residual] = this->estimate();

      nnew = 0;
      std::vector<double> resnrm(neigs_);
      std::vector<bool> conv(neigs_);
      for (size_t i = 0; i < neigs_; ++i) {
        resnrm[i] = std::sqrt(qleve::dot_product(size_, &residual(0, i), &residual(0, i)));
        conv[i] = (resnrm[i] < thresh);
      }

      if (std::all_of(conv.begin(), conv.end(), [](const bool x) { return x; })) { // converged!
        return {eigs, V};
      }

      nnew = precondition(residual, eigs, conv);
      nnew = regularize_new_vectors(residual.slice(0, nnew));
      V0.slice(0, nnew) = residual.slice(0, nnew);
    }

    auto [eigs, V, res] = this->estimate();
    return {eigs, V};
  }

  void push_back(const qleve::ConstTensorView_<DataType, 2>& v,
                 const qleve::ConstTensorView_<DataType, 2>& w)
  {
    assert(v.extent(1) == w.extent(1) && v.extent(1) > 0);
    const size_t nst = v.extent(1);

    if (ncurrent_ + nst > max_subspace_) {
      throw std::runtime_error("too many vectors being passed to Davidson. Fix the algorithm");
    }

    // update V and W vectors
    V_->slice(ncurrent_, ncurrent_ + nst) = v;
    W_->slice(ncurrent_, ncurrent_ + nst) = w;
    ncurrent_ += nst;

    // brute force update of subspace overlap and Hamiltonian
    qleve::gemm("t", "n", 1.0, V_->slice(0, ncurrent_), V_->slice(0, ncurrent_), 0.0,
                S_->subtensor({0, 0}, {ncurrent_, ncurrent_}));
    qleve::gemm("t", "n", 1.0, W_->slice(0, ncurrent_), V_->slice(0, ncurrent_), 0.0,
                H_->subtensor({0, 0}, {ncurrent_, ncurrent_}));
  }

  std::tuple<ArrayType, MatType, MatType> estimate() noexcept
  {
    MatType vecs(H_->subtensor({0, 0}, {ncurrent_, ncurrent_}));
    auto eigs = qleve::linalg::diagonalize(vecs);

    MatType fullvecs(size_, neigs_);
    qleve::gemm("n", "n", 1.0, V_->slice(0, ncurrent_), vecs.slice(0, neigs_), 0.0, fullvecs);

    MatType residual(size_, neigs_);
    qleve::gemm("n", "n", 1.0, W_->slice(0, ncurrent_), vecs.slice(0, neigs_), 0.0, residual);
    for (size_t i = 0; i < neigs_; ++i) {
      residual.pluck(i) -= eigs(i) * fullvecs.pluck(i);
    }

    return {eigs, fullvecs, residual};
  }

  ptrdiff_t precondition(MatViewType res, qleve::TensorView_<double, 1> eigs,
                         const std::vector<bool>& converged)
  {
    assert(neigs_ == res.extent(1) && eigs.size() >= neigs_ && neigs_ == converged.size());
    ptrdiff_t nnew = 0;
    for (size_t i = 0; i < neigs_; ++i) {
      if (!converged.at(i)) {
        for (size_t j = 0; j < size_; ++j) {
          double d = (*diag_)(j)-eigs(i);
          if (std::abs(d) < 1.0e-6) {
            d = std::copysign(1.0e-6, d);
          }
          res(j, nnew) = res(j, i) / d;
        }
        // normalize new vector
        const double nrm = qleve::dot_product(size_, &res(0, nnew), &res(0, nnew));
        qleve::scale(size_, 1.0 / std::sqrt(nrm), &res(0, nnew));
        nnew += 1;
      }
    }
    return nnew;
  }

  ptrdiff_t regularize_new_vectors(MatViewType v)
  {
    assert(v.extent(1) <= neigs_);

    // orthogonalize new vectors against old ones
    if (ncurrent_ > 0) {
      MatType tmpV(ncurrent_, v.extent(1));
      qleve::gemm("t", "n", 1.0, V_->slice(0, ncurrent_), v, 0.0, tmpV);
      qleve::gemm("n", "n", -1.0, V_->slice(0, ncurrent_), tmpV, 1.0, v);
    }

    ptrdiff_t nnew = qleve::linalg::orthogonalize_vectors(v);
    return nnew;
  }
};

} // namespace yucca
