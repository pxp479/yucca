// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/algo/bfgs.hpp
///
/// Utility algorithm for BFGS optimization
///
/// Modeled on util/math/bfgs.h in bagel
#pragma once

#include <memory>

#include <qleve/tensor.hpp>
#include <qleve/tensor_dot.hpp>

namespace yucca
{

template <typename DataType>
class BFGS {
 public:
  using MatType = qleve::Tensor_<DataType, 2>;
  using MatViewType = qleve::TensorView_<DataType, 2>;
  using ConstMatViewType = qleve::ConstTensorView_<DataType, 2>;
  using ArrayType = qleve::Tensor_<DataType, 1>;
  using ArrayViewType = qleve::TensorView_<DataType, 1>;
  using ConstArrayViewType = qleve::ConstTensorView_<DataType, 1>;

 protected:
  ptrdiff_t max_size_; ///< maximum size of BFGS extrapolation
  ptrdiff_t head_;     ///< current size

  std::unique_ptr<ArrayType> H0_; ///< initial diagonal Hessian

  std::unique_ptr<MatType> delta_;
  std::unique_ptr<MatType> y_;
  std::unique_ptr<MatType> D_;
  std::unique_ptr<MatType> Y_;

  std::unique_ptr<ArrayType> prev_grad_;
  std::unique_ptr<ArrayType> prev_value_;

 public:
  BFGS(const ConstArrayViewType& h0, const ptrdiff_t max_size) :
      max_size_(max_size),
      head_(0),
      H0_(std::make_unique<ArrayType>(h0)),
      D_(std::make_unique<MatType>(h0.size(), max_size_)),
      delta_(std::make_unique<MatType>(h0.size(), max_size_)),
      y_(std::make_unique<MatType>(h0.size(), max_size_)),
      Y_(std::make_unique<MatType>(h0.size(), max_size_)),
      prev_grad_(std::make_unique<ArrayType>(h0.size())),
      prev_value_(std::make_unique<ArrayType>(h0.size()))
  {}

  ArrayType push_and_extrapolate(const ConstArrayViewType& target, const ConstArrayViewType& grad)
  {
    assert(grad.size() == H0_->size());

    ArrayType out(grad / *H0_);

    if (head_ > 0) {
      const ptrdiff_t k = head_ - 1;

      // yk = dF(k) - dF(k-1)
      auto DD = D_->pluck(k);
      DD = grad - *prev_grad_;

      // Bk^-1 yk
      auto yy = y_->pluck(k);
      yy = DD / *H0_;

      // s_k = x(k) - x(k-1)
      auto vv = delta_->pluck(k);
      vv = target - *prev_value_;

      const ptrdiff_t n = k;

      auto dn = D_->pluck(n);
      for (int i = 0; i < n; ++i) {
        auto di = D_->pluck(i);
        auto deli = delta_->pluck(i);
        auto yi = y_->pluck(i);

        const auto s1 = 1.0 / qleve::dot_product(di, deli);
        const auto s2 = 1.0 / qleve::dot_product(di, yi);
        const auto s3 = qleve::dot_product(deli, grad);
        const auto s4 = qleve::dot_product(yi, grad);
        const auto s5 = qleve::dot_product(deli, dn);
        const auto s6 = qleve::dot_product(yi, dn);

        const auto t1 = (1.0 + s1 / s2) * s1 * s3 - s1 * s4;
        const auto t2 = s1 * s3;
        const auto t3 = (1.0 + s1 / s2) * s1 * s5 - s1 * s6;
        const auto t4 = s1 * s5;

        out += t1 * deli;
        out -= t2 * yi;

        yy += t3 * deli;
        yy -= t4 * yi;
      }

      {
        auto deln = delta_->pluck(n);

        const auto s1 = 1.0 / qleve::dot_product(dn, deln);
        const auto s2 = 1.0 / qleve::dot_product(dn, yy);
        const auto s3 = qleve::dot_product(deln, grad);
        const auto s4 = qleve::dot_product(yy, grad);

        const auto t1 = (1.0 + s1 / s2) * s1 * s3 - s1 * s4;
        const auto t2 = s1 * s3;

        out += t1 * deln;
        out -= t2 * yy;
      }
    }
    head_++;

    *prev_grad_ = grad;
    *prev_value_ = target;

    out *= -1.0;
    return out;
  }

  ArrayType interpolate_hessian(const ConstArrayViewType& val, const bool update = false)
  {
    auto out = val.zeros_like();

    out *= *H0_;

    if (head_ > 0) {
      auto YY = val.zeros_like();
      auto dd = delta_->pluck(head_ - 1);
      YY = dd * *H0_;

      const ptrdiff_t n = head_ - 1;

      auto dn = D_->pluck(n);
      auto deln = delta_->pluck(n);
      for (ptrdiff_t i = 0; i < n; ++i) {
        auto di = D_->pluck(i);
        auto deli = delta_->pluck(i);
        auto Yi = Y_->pluck(i);

        const auto s1 = 1.0 / qleve::dot_product(di, deli);
        const auto s2 = 1.0 / qleve::dot_product(Yi, deli);
        const auto s3 = qleve::dot_product(di, deln);
        const auto s4 = qleve::dot_product(Yi, deln);
        const auto s5 = qleve::dot_product(di, val);
        const auto s6 = qleve::dot_product(Yi, val);

        const auto t1 = s1 * s5; // updating w
        const auto t2 = s2 * s6; // updating w
        const auto t3 = s1 * s3; // updating Y
        const auto t4 = s2 * s4; // updating Y

        out += t1 * di - t2 * Yi;
        YY += t3 * di - t4 * Yi;
      }

      {
        const auto s1 = 1.0 / qleve::dot_product(dn, deln);
        const auto s2 = 1.0 / qleve::dot_product(deln, YY);
        const auto s3 = qleve::dot_product(dn, val);
        const auto s4 = qleve::dot_product(YY, val);

        const auto t1 = s1 * s3;
        const auto t2 = s2 * s4;

        out += t1 * dn - t2 * YY;
      }
      if (update) {
        Y_->pluck(n) = YY;
      }
    }

    return out;
  }

  void clear() { head_ = 0.0; }
};

template <typename T, class Func>
auto bfgs_minimize(Func _eval_and_gradient, qleve::TensorView_<T, 1> x,
                   const qleve::ConstTensorView_<T, 1> h0, const ptrdiff_t maxiter,
                   const double tol = 1e-6)
{
  BFGS<T> bfgs(h0, maxiter);

  auto g = h0.zeros_like();
  bool conv = false;
  T val;
  for (ptrdiff_t i = 0; i < maxiter; ++i) {
    val = _eval_and_gradient(x, g);

    if (g.norm() < tol) {
      conv = true;
      break;
    }
    auto dx = bfgs.push_and_extrapolate(x, g);
    x += dx;
  }

  if (!conv) {
    throw std::runtime_error("bfgs failed to converge");
  }

  return val;
}

} // namespace yucca
