// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/algo/orthogonalize.hpp
///
/// Utility algorithm to orthogonalize based on an overlap matrix
#pragma once

#include <memory>
#include <vector>

#include <fmt/core.h>

#include <qleve/array.hpp>
#include <qleve/diagonalize.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>
#include <qleve/vector_ops.hpp>

namespace yucca
{

/// Given a metric, S, returns a transformation that transforms
/// S into the unit matrix, and hence orthogonalizes the vectors
/// used to construct S
template <typename MatType>
[[deprecated("use qleve::orthogonalize_metric")]] std::enable_if_t<
    qleve::is_const_matrixlike<MatType>::value, qleve::Tensor_<qleve::tensor_datatype<MatType>, 2>>
orthogonalize_metric(const MatType& S, const double thresh = 1.0e-8)
{
  using MatrixT = qleve::Tensor_<qleve::tensor_datatype<MatType>, 2>;

  const auto [ndim, mdim] = S.shape();
  assert(ndim == mdim);

  // simplest thing to start with is symmetric orthogonalization (i.e., Lowdin)
  MatrixT U(S);

  // normalize diagonals to 1 for improved numerical stability
  qleve::Array norms(ndim);
  for (size_t i = 0; i < ndim; ++i) {
    const double nrm = std::abs(S(i, i));
    norms(i) = nrm > thresh ? 1.0 / std::sqrt(nrm) : 1.0;
  }

  for (size_t j = 0; j < mdim; ++j) {
    for (size_t i = 0; i < ndim; ++i) {
      U(i, j) *= norms(i) * norms(j);
    }
  }

  // diagonalize U
  auto eigs = qleve::linalg::diagonalize(U);

  // assume first element is smallest
  if (eigs(0) > thresh) { // do symmetric orthogonalization
    for (size_t i = 0; i < eigs.size(); ++i) {
      assert(eigs(i) > thresh);
      eigs(i) = 1.0 / std::sqrt(std::sqrt(eigs(i)));
      qleve::scale(U.extent(0), eigs(i), &U(0, i));
    }

    MatrixT out(U.extent(0), U.extent(1));
    qleve::gemm("n", "t", 1.0, U, U, 0.0, out);

    for (size_t j = 0; j < mdim; ++j) {
      for (size_t i = 0; i < ndim; ++i) {
        out(i, j) *= norms(i);
      }
    }

    return out;
  } else { // do canonical orthogonalization
    fmt::print("Canonical orthogonalization not yet implemented!\n");
    throw std::runtime_error("Not yet implemented!");
  }
}

/// Given a set of vectors, orthogonalizes the vectors in place.
/// Returns the number of linearly independent vectors.
template <typename MatType>
[[deprecated("use qleve::orthogonalize_vectors")]] std::enable_if_t<
    qleve::is_nonconst_matrixlike<MatType>::value, ptrdiff_t>
orthogonalize_vectors(MatType&& C, const double thresh = 1.0e-12)
{
  using MatrixT = qleve::Tensor_<qleve::tensor_datatype<MatType>, 2>;

  const auto [ndim, mdim] = C.shape();

  // normalize all of the columns
  for (size_t j = 0; j < mdim; ++j) {
    auto col = C.pluck(j);
    col /= col.norm();
  }

  ptrdiff_t j = 0;       // current counter
  ptrdiff_t ntot = mdim; // current known number of linearly independent vectors
  MatrixT inner(1, mdim);

  // start orthogonalizing
  for (ptrdiff_t jvec = 0; jvec < mdim; ++jvec) {
    auto col = C.slice(j, j + 1);

    // normalize column
    const double nrm = col.norm();
    if (nrm < thresh) {
      // slide all the remaining columns down
      for (ptrdiff_t k = j; k < ntot - 1; ++k) {
        C.pluck(k) = C.pluck(k + 1);
      }
      ntot -= 1;
      continue;
    }
    col /= nrm;

    ptrdiff_t nrest = ntot - j - 1;
    if (nrest > 0) { // project this vector out from remaining
      auto rest = C.slice(j + 1, ntot);
      auto tmp = inner.subtensor({0, 0}, {1, ntot - j - 1});
      qleve::gemm("t", "n", 1.0, col, rest, 0.0, tmp);
      qleve::gemm("n", "n", -1.0, col, tmp, 1.0, rest);
    }
    j += 1;
  }

  return ntot;
}

} // namespace yucca
