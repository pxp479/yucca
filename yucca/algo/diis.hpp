// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/algo/diis.hpp
///
/// Utility algorithm for DIIS extrapolation
#pragma once

#include <memory>
#include <vector>

#include <qleve/diagonalize.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/vector_ops.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

namespace yucca
{


template <typename DataType>
class DIIS {
 public:
  using MatType = qleve::Tensor_<DataType, 2>;
  using MatViewType = qleve::TensorView_<DataType, 2>;
  using ConstMatViewType = qleve::ConstTensorView_<DataType, 2>;
  using ArrayType = qleve::Tensor_<DataType, 1>;
  using ArrayViewType = qleve::TensorView_<DataType, 1>;
  using ConstArrayViewType = qleve::ConstTensorView_<DataType, 1>;

 protected:
  ptrdiff_t head_;
  ptrdiff_t max_size_;
  ptrdiff_t min_extrap_; ///< only extrapolate when this minimum is met

  std::unique_ptr<MatType> target_; ///< target vectors to extrapolate
  std::unique_ptr<MatType> error_;  ///< error vectors used in the extrapolation

  std::unique_ptr<MatType> H_; ///< subspace matrix for DIIS

 public:
  DIIS(const ptrdiff_t vectorsize, const ptrdiff_t maxsize, const ptrdiff_t min_extrap) :
      head_(0),
      max_size_(maxsize),
      min_extrap_(min_extrap),
      target_(std::make_unique<MatType>(vectorsize, maxsize)),
      error_(std::make_unique<MatType>(vectorsize, maxsize)),
      H_(std::make_unique<MatType>(maxsize + 1, maxsize + 1))
  {
    H_->subtensor({0, 1}, {1, maxsize + 1}) = 1.0;
    H_->subtensor({1, 0}, {maxsize + 1, 1}) = 1.0;
  }

  void push(const ConstArrayViewType& target, const ConstArrayViewType& err)
  {
    assert(target.size() == target_->extent(0));
    assert(err.size() == error_->extent(0));

    ptrdiff_t head1 = head_ % max_size_;
    ptrdiff_t head0 = (head1 == 0) ? max_size_ - 1 : head1 - 1;

    target_->pluck(head1) = target;
    error_->pluck(head1) = err;

    // update V and W vectors
    ptrdiff_t nvec = std::min(head_ + 1, max_size_);

    auto newerr = error_->pluck(head1);
    for (ptrdiff_t i = 0; i < nvec; ++i) {
      auto olderr = error_->pluck(i);
      const auto tmp = qleve::dot_product(newerr.size(), newerr.data(), olderr.data());

      // head1+1 and i+1 because the first column/row is spoken for
      H_->at(head1 + 1, i + 1) = tmp;
      H_->at(i + 1, head1 + 1) = qleve::detail::conj(tmp);
    }

    head_++;
  }

  void extrapolate(ArrayViewType v) noexcept
  {
    assert(v.size() == target_->extent(0));
    assert(head_ > 0);

    if (head_ < min_extrap_) {
      v = target_->pluck(head_ - 1);
      return;
    }

    ptrdiff_t nvec = std::min(head_, max_size_);

    MatType h(H_->subtensor({0, 0}, {nvec + 1, nvec + 1}));
    MatType g(nvec + 1, 1);
    g(0, 0) = 1.0;

    auto eig = qleve::linalg::diagonalize(h);

    ArrayType eig_inv(eig);
    for (int i = 0; i < nvec + 1; ++i) {
      if (std::abs(eig(i)) < 1e-14) {
        eig_inv(i) = 0.0;
      } else {
        eig_inv(i) = 1.0 / eig(i);
      }
    }

    auto h_inv = h.zeros_like();
    qleve::weighted_gemm("n", "t", 1.0, h, eig_inv, h, 0.0, h_inv);

    auto x = g.zeros_like();
    qleve::gemm("n", "n", 1.0, h_inv, g, 0.0, x);

    ArrayViewType xx(x.data() + 1, nvec);
    qleve::contract(1.0, target_->slice(0, nvec), "pn", xx, "n", 0.0, v, "p");
  }

  void back(ArrayViewType v) const
  {
    const ptrdiff_t head1 = head_ % max_size_;
    const ptrdiff_t head0 = (head1 == 0) ? max_size_ - 1 : head1 - 1;
    v = target_->pluck(head0);
  }

  void clear() { head_ = 0; }
};

} // namespace yucca
