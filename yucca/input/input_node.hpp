// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/input/input_node.hpp
///
/// Recursive data structure containing unstructured input

#pragma once

#include <iostream>
#include <map>
#include <memory>
#include <utility>
#include <variant>
#include <vector>

#include <fmt/core.h>
#include <fmt/ostream.h>

#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>

namespace yucca
{

class InputNode;

template <class T>
class recursive_wrapper {
 private:
  std::unique_ptr<T> data_;

 public:
  recursive_wrapper() : data_(std::make_unique<T>()) {}
  recursive_wrapper(const recursive_wrapper<T>& x) : data_(std::make_unique<T>(x.get())) {}
  recursive_wrapper(recursive_wrapper<T>&& x) { std::swap(data_, x.data_); }
  recursive_wrapper(std::unique_ptr<T>&& x) : data_(std::move(x)) {}

  recursive_wrapper& operator=(const recursive_wrapper<T>& x)
  {
    *data_ = x.get();
    return *this;
  }
  recursive_wrapper& operator=(recursive_wrapper<T>&& x)
  {
    std::swap(data_, x.data_);
    return *this;
  }

  T* operator->() { return data_.get(); }
  const T* const operator->() const { return data_.get(); }

  T& get() { return *data_.get(); }
  const T& get() const { return *data_.get(); }
};

template <typename T, typename... Args>
recursive_wrapper<T> make_recursive(Args&&... args)
{
  return recursive_wrapper<T>(std::make_unique<T>(std::forward<Args>(args)...));
}

template <typename T>
using is_input_value =
    qleve::bool_or<std::is_integral<T>::value, std::is_floating_point<T>::value,
                   std::is_same<T, std::string>::value, std::is_same<T, InputNode>::value>;

template <typename T>
using is_input_scalar = qleve::bool_or<std::is_integral<T>::value, std::is_floating_point<T>::value,
                                       std::is_same<T, std::string>::value>;

template <typename T>
concept input_value = is_input_value<T>::value;

template <typename T>
concept input_scalar = is_input_scalar<T>::value;

static constexpr int vec_elements_per_line = 5;

class InputNode {
 public:
  using recursive_node = recursive_wrapper<InputNode>;
  using node_vector = std::vector<recursive_node>;
  using node_map = std::map<std::string, recursive_node>;
  using data_type = std::variant<ptrdiff_t, double, std::string, bool, node_vector, node_map>;

 public:
  data_type data_;

  InputNode() : data_(node_map{}) {}
  InputNode(const InputNode&) = default;

  /// Constructor for bool, integral, floating point and string types
  template <input_scalar T>
  InputNode(const T& data)
  {
    if constexpr (std::is_same<T, bool>::value) {
      data_ = static_cast<bool>(data);
    } else if constexpr (std::is_integral<T>::value) {
      data_ = static_cast<ptrdiff_t>(data);
    } else if constexpr (std::is_floating_point<T>::value) {
      data_ = static_cast<double>(data);
    }
  }

  /// Constructor for string
  InputNode(const std::string& data) : data_(data) {}

  InputNode& operator=(const InputNode&) = default;

  template <input_value T>
  InputNode(const std::vector<T>& data) : data_(node_vector())
  {
    for (auto& d : data) {
      std::get<node_vector>(data_).emplace_back(make_recursive<InputNode>(d));
    }
  }

  template <input_value T>
  InputNode(const std::map<std::string, T>& data) : data_(node_map())
  {
    for (auto& d : data) {
      std::get<node_map>(data_).emplace(d.first, make_recursive<InputNode>(d.second));
    }
  }

  bool contains(const std::string& key) const
  {
    if (const auto nmap = std::get_if<node_map>(&data_)) {
      const auto it = nmap->find(key);
      return it != nmap->end();
    }
    return false;
  }

  bool contains_any(const std::vector<std::string>& keys) const
  {
    for (const auto& key : keys) {
      if (contains(key)) {
        return true;
      }
    }
    return false;
  }

  template <typename T>
  const T get() const
  {
    try {
      if constexpr (std::is_same<T,
                                 double>::value) { // if requesting a double, also accept a
                                                   // ptrdiff_t
        if (const ptrdiff_t* const pval = std::get_if<ptrdiff_t>(&data_)) {
          const double out = *pval;
          return out;
        }
      }
      return std::get<T>(data_);
    } catch (const std::bad_variant_access& e) {
      throw std::runtime_error(
          fmt::format("Incorrect data type found in input node. Expected {:s}, but found {:s}.",
                      typestring<T>(), has_type()));
    }
  }

  template <typename T>
  void put(const T& x)
  {
    data_ = x;
  }

  template <typename T>
  T get(const std::string& key)
  {
    node_map& nm = std::get<node_map>(data_);
    try {
      return nm.at(key)->get<T>();
    } catch (const std::out_of_range& e) {
      throw std::runtime_error(fmt::format("Key not found: {:s}", key));
    }
  }

  template <typename T>
  const T get(const std::string& key) const
  {
    const node_map& nm = std::get<node_map>(data_);
    try {
      return nm.at(key)->get<T>();
    } catch (const std::out_of_range& e) {
      throw std::runtime_error(fmt::format("Key not found: {:s}", key));
    }
  }

  template <typename T>
  const T get(const std::string& key, const T& def) const
  {
    const node_map& nm = std::get<node_map>(data_);
    if (auto iter = nm.find(key); iter != nm.end()) {
      return iter->second->get<T>();
    } else {
      return def;
    }
  }

  ptrdiff_t size() const
  {
    if (this->is_map()) {
      const node_map& nm = std::get<node_map>(data_);
      return nm.size();
    } else if (this->is_vector()) {
      const node_vector& nv = std::get<node_vector>(data_);
      return nv.size();
    } else {
      return 1;
    }
  }

  template <input_scalar T>
  std::vector<T> as_vector() const
  {
    std::vector<T> out;
    const node_vector& nvec = std::get<node_vector>(data_);
    out.reserve(nvec.size());
    for (const auto& node : nvec) {
      out.push_back(node->get<T>());
    }
    return out;
  }

  template <input_scalar T>
  std::vector<T> get_vector(const std::string& key) const
  {
    std::vector<T> out;
    const node_map& nmap = std::get<node_map>(data_);
    const node_vector& nvec = std::get<node_vector>(nmap.at(key)->data_);
    out.reserve(nvec.size());
    for (const auto& node : nvec) {
      out.push_back(node->get<T>());
    }
    return out;
  }

  template <input_scalar T>
  std::vector<T> get_vector(const std::string& key, std::vector<T> def) const
  {
    std::vector<T> out;
    const node_map& nmap = std::get<node_map>(data_);
    if (auto iter = nmap.find(key); iter != nmap.end() && iter->second->is_vector()) {
      const node_vector& nvec = std::get<node_vector>(iter->second->data_);
      out.reserve(nvec.size());
      for (const auto& node : nvec) {
        out.push_back(node->get<T>());
      }
      return out;
    } else {
      return def;
    }
  }

  template <input_scalar T>
  std::map<std::string, T> get_map(const std::string& key) const
  {
    std::map<std::string, T> out;
    const node_map& nmap = std::get<node_map>(data_);
    out.reserve(nmap.size());
    for (const auto& node : nmap) {
      out.insert({node.first, node.second->get<T>()});
    }
    return out;
  }

  template <qleve::coeffable DataType, int Rank>
  qleve::Tensor_<DataType, Rank> get_tensor(const std::string& key) const
  {
    auto node = this->get_node(key);

    auto shape = node.get_vector<ptrdiff_t>("extent");
    if (shape.size() != Rank) {
      throw std::runtime_error(fmt::format(
          "Tensor {} has wrong rank. Expected {:d}, but found {:d}", key, Rank, shape.size()));
    }
    std::array<ptrdiff_t, Rank> shp;
    std::copy_n(shape.begin(), Rank, shp.begin());

    qleve::Tensor_<DataType, Rank> out(shp);
    auto dat = node.get_vector<DataType>("data");
    if (out.size() != dat.size()) {
      throw std::runtime_error(fmt::format(
          "Tensor {} has wrong number of elements in data. Expected {:d}, but found {:d}", key,
          out.size(), dat.size()));
    }
    for (ptrdiff_t i = 0; i < dat.size(); ++i) {
      out.data(i) = dat[i];
    }

    return out;
  }

  InputNode& get_node(const std::string& key)
  {
    node_map& nmap = std::get<node_map>(data_);
    try {
      return nmap.at(key).get();
    } catch (const std::out_of_range& e) {
      throw std::runtime_error(fmt::format("Key not found: {:s}", key));
    }
  }

  const InputNode& get_node(const std::string& key) const
  {
    const node_map& nmap = std::get<node_map>(data_);
    try {
      return nmap.at(key).get();
    } catch (const std::out_of_range& e) {
      throw std::runtime_error(fmt::format("Key not found: {:s}", key));
    }
  }

  InputNode& get_node(const ptrdiff_t& index)
  {
    node_vector& nvec = std::get<node_vector>(data_);
    return nvec.at(index).get();
  }

  const InputNode& get_node(const ptrdiff_t& index) const
  {
    const node_vector& nvec = std::get<node_vector>(data_);
    return nvec.at(index).get();
  }

  InputNode* get_node_if(const std::string& key)
  {
    if (auto nmap = std::get_if<node_map>(&data_)) {
      if (auto iter = nmap->find(key); iter != nmap->end()) {
        return &iter->second.get();
      }
    }
    return nullptr;
  }

  const InputNode* get_node_if(const std::string& key) const
  {
    if (const node_map* nmap = std::get_if<node_map>(&data_)) {
      if (auto iter = nmap->find(key); iter != nmap->end()) {
        return &iter->second.get();
      }
    }
    return nullptr;
  }

  template <typename T>
  const T get_if_value(const std::string& key, const T& def) const
  {
    const auto node = get_node_if(key);
    if (node && node->is_value()) {
      return this->get<T>(key);
    } else {
      return def;
    }
  }

  template <typename T>
  T get_if_map(const std::string& key, const T& def)
  {
    if (is_map()) {
      return this->get<T>(key, def);
    } else {
      return def;
    }
  }
  template <typename T>
  const T get_if_map(const std::string& key, const T& def) const
  {
    if (is_map()) {
      return this->get<T>(key, def);
    } else {
      return def;
    }
  }

  template <input_value T>
  void put(const std::string& key, const T& x)
  {
    if (auto nmap = std::get_if<node_map>(&data_)) {
      (*nmap)[key] = make_recursive<InputNode>(x);
    } else {
      data_ = node_map();
      node_map& nm = std::get<node_map>(data_);
      nm[key] = make_recursive<InputNode>(x);
    }
  }

  void put(const std::string& key, const std::string& value)
  {
    if (auto nmap = std::get_if<node_map>(&data_); nmap) {
      (*nmap)[key] = make_recursive<InputNode>(value);
    } else {
      data_ = node_map();
      node_map& nm = std::get<node_map>(data_);
      nm[key] = make_recursive<InputNode>(value);
    }
  }

  template <input_value T>
  void put(const std::string& key, const std::vector<T>& x)
  {
    if (auto nmap = std::get_if<node_map>(&data_)) {
      (*nmap)[key] = make_recursive<InputNode>(x);
    } else {
      data_ = node_map();
      node_map& nm = std::get<node_map>(data_);
      nm[key] = make_recursive<InputNode>(x);
    }
  }

  template <input_value T, size_t N>
  void put(const std::string& key, const std::array<T, N>& x)
  {
    std::vector<T> v(x.begin(), x.end());
    this->put(key, v);
  }

  template <input_value T>
  void put(const std::string& key, const std::map<std::string, T>& x)
  {
    if (auto nmap = std::get_if<node_map>(&data_)) {
      (*nmap)[key] = make_recursive<InputNode>(x);
    } else {
      data_ = node_map();
      node_map& nm = std::get<node_map>(data_);
      nm[key] = make_recursive<InputNode>(x);
    }
  }

  template <qleve::const_tensorable T>
  void put(const std::string& key, const T& tens)
  {
    InputNode node;
    std::vector<ptrdiff_t> shape(tens.shape().begin(), tens.shape().end());
    node.put("extent", shape);

    std::vector<double> data(tens.data(), tens.data() + tens.size());
    node.put("data", data);

    this->put(key, node);
  }

  template <typename T>
  void push_back(const T& x)
  {
    // if vector is not currently stored, then make it
    if (auto nvec = std::get_if<node_vector>(&data_)) {
      (*nvec).emplace_back(make_recursive<InputNode>(x));
    } else {
      data_ = node_vector();
      std::get<node_vector>(data_).emplace_back(make_recursive<InputNode>(x));
    }
  }

  bool is_integral() const { return std::holds_alternative<ptrdiff_t>(data_); }
  bool is_double() const { return std::holds_alternative<double>(data_); }
  bool is_string() const { return std::holds_alternative<std::string>(data_); }
  bool is_bool() const { return std::holds_alternative<bool>(data_); }
  bool is_vector() const { return std::holds_alternative<node_vector>(data_); }
  bool is_map() const { return std::holds_alternative<node_map>(data_); }

  bool is_value() const { return is_integral() || is_double() || is_string() || is_bool(); }
  bool is_container() const { return is_vector() || is_map(); }

  template <typename T>
  static std::string typestring()
  {
    if constexpr (std::is_integral<T>::value) {
      return "integer";
    } else if constexpr (std::is_same<T, bool>::value) {
      return "boolean";
    } else if constexpr (std::is_floating_point<T>::value) {
      return "float";
    } else if constexpr (std::is_same<T, std::string>::value) {
      return "string";
    } else if constexpr (std::is_same<T, node_vector>::value) {
      return "vector";
    } else if constexpr (std::is_same<T, node_map>::value) {
      return "map";
    } else {
      return "unknown";
    }
  }

  std::string has_type() const
  {
    if (is_integral()) {
      return "integer";
    } else if (is_double()) {
      return "float";
    } else if (is_string()) {
      return "string";
    } else if (is_bool()) {
      return "boolean";
    } else if (is_vector()) {
      return "vector";
    } else if (is_map()) {
      return "map";
    } else {
      return "unknown";
    }
  }

  void print() const { this->print(std::cout); }
  std::string to_string() const
  {
    std::stringstream ss;
    this->print(ss);
    return ss.str();
  }

  template <class Output>
  void print(Output& ccout, int level = 0, bool first_call = true, bool in_list = false) const
  {
    auto print_value = [&](auto&& arg) {
      using T = std::decay_t<decltype(arg)>;
      if constexpr (std::is_same_v<T, ptrdiff_t> || std::is_same_v<T, double>
                    || std::is_same_v<T, std::string>) {
        fmt::print(ccout, "{}", arg);
      } else if constexpr (std::is_same_v<T, bool>) {
        fmt::print(ccout, "{}", arg ? "true" : "false");
      }
    };
    if (this->is_value()) {
      // print single value
      std::visit(print_value, data_);
      fmt::print(ccout, "\n");
    } else if (this->is_vector()) {
      auto& nvec = std::get<node_vector>(data_);

      const bool all_values =
          std::all_of(nvec.begin(), nvec.end(), [](const auto& i) { return i.get().is_value(); });
      const bool print_inline = all_values;

      if (print_inline) {
        fmt::print(ccout, " [");
        for (ptrdiff_t i = 0; i < nvec.size() - 1; ++i) {
          std::visit(print_value, nvec[i].get().data_);
          if (i != 0 && (i + 1) % vec_elements_per_line == 0) {
            fmt::print(ccout, ",\n{:{}s}", "", 2 * level + 4);
          } else {
            fmt::print(ccout, ", ");
          }
        }
        std::visit(print_value, nvec[nvec.size() - 1].get().data_);
        fmt::print(ccout, "]\n");
      } else {
        fmt::print(ccout, "\n");
        for (auto& i : nvec) {
          fmt::print(ccout, "{:{}s}", "", 2 * level);
          fmt::print(ccout, "- ");
          i.get().print(ccout, level + 1, /*first_call*/ false, /*in_list*/ true);
        }
      }
    } else if (this->is_map()) {
      auto& nmap = std::get<node_map>(data_);

      if (!(in_list || first_call)) {
        fmt::print(ccout, "\n");
      }
      bool first = true;
      for (auto& i : nmap) {
        if (!(in_list && first)) {
          fmt::print(ccout, "{:{}s}", "", 2 * level);
        }
        first = false;
        fmt::print(ccout, "{}: ", i.first);
        i.second.get().print(ccout, level + 1, /*first_call*/ false);
      }
    }
  }
};

} // namespace yucca
