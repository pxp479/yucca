// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/input/read_input.hpp
///
/// Functions to read an input yaml file

#pragma once

#include <string>

#include <yucca/input/input_node.hpp>

namespace yucca
{

InputNode read_input(const std::string& filename);

InputNode read_input_stream(const std::string& yaml_string);

} // namespace yucca
