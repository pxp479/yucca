// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/input/read_input.cpp
///
/// Implementation to read an input yaml file

#include <cassert>
#include <ostream>

#include <yaml-cpp/yaml.h>

#include <yucca/input/read_input.hpp>

using namespace std;
using namespace yucca;

InputNode convert_yaml_to_input_node(const YAML::Node& yaml);

void assign_yaml_scalar_to_input(const YAML::Node& yaml, InputNode& inp)
{
  assert(yaml.IsScalar());

  try {
    const ptrdiff_t try_int = yaml.as<ptrdiff_t>();
    inp = try_int;
  } catch (YAML::TypedBadConversion<ptrdiff_t>& e1) {
    try {
      const double try_float = yaml.as<double>();
      inp = try_float;
    } catch (YAML::TypedBadConversion<double>& e2) {
      try {
        const bool try_bool = yaml.as<bool>();
        inp = try_bool;
      } catch (YAML::TypedBadConversion<bool>& e3) {
        // Fall back to string
        const std::string try_str = yaml.as<std::string>();
        inp = try_str;
      }
    }
  }
}

void assign_yaml_sequence_to_input(const YAML::Node& yaml, InputNode& inp)
{
  assert(yaml.IsSequence());

  for (YAML::const_iterator it = yaml.begin(); it != yaml.end(); ++it) {
    inp.push_back(convert_yaml_to_input_node(*it));
  }
}

void assign_yaml_map_to_input(const YAML::Node& yaml, InputNode& inp)
{
  assert(yaml.IsMap());

  for (YAML::const_iterator it = yaml.begin(); it != yaml.end(); ++it) {
    std::string key = it->first.as<std::string>();
    inp.put(key, convert_yaml_to_input_node(it->second));
  }
}

InputNode convert_yaml_to_input_node(const YAML::Node& yaml)
{
  InputNode out;

  switch (yaml.Type()) {
    case YAML::NodeType::Null:
      break;
    case YAML::NodeType::Scalar:
      assign_yaml_scalar_to_input(yaml, out);
      break;
    case YAML::NodeType::Sequence:
      assign_yaml_sequence_to_input(yaml, out);
      break;
    case YAML::NodeType::Map:
      assign_yaml_map_to_input(yaml, out);
      break;
    case YAML::NodeType::Undefined:
      break;
  }

  return out;
}

InputNode yucca::read_input(const std::string& filename)
{
  YAML::Node yaml = YAML::LoadFile(filename);

  InputNode out;

  // Top layer must be either a sequence or a map (right?)
  assert(yaml.IsSequence() || yaml.IsMap());

  return convert_yaml_to_input_node(yaml);
}

InputNode yucca::read_input_stream(const std::string& yaml_string)
{
  YAML::Node yaml = YAML::Load(yaml_string);

  InputNode out;

  // Top layer must be either a sequence or a map (right?)
  assert(yaml.IsSequence() || yaml.IsMap());

  return convert_yaml_to_input_node(yaml);
}
