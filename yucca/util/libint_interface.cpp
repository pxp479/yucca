// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/libint_interface.cpp
///
/// Interface to deal with libint2

#include <libint2/util/small_vector.h>

#include <yucca/util/libint_interface.hpp>

using namespace std;
using namespace yucca;

template <typename T>
libint2::svector<T> as_svector(const std::vector<T>& x)
{
  return libint2::svector<T>(x.begin(), x.end());
}

vector<libint2::Shell> yucca::libint::get_shells(const yucca::Basis& b)
{
  vector<libint2::Shell> shells;

  const auto& bshells = b.basisshells();
  shells.reserve(bshells.size());

  for (const auto& b : bshells) {
    libint2::Shell::Contraction con = {b.angular_momentum(), b.spherical(), as_svector(b.coeffs())};
    libint2::svector<libint2::Shell::Contraction> convec = {con};
    shells.emplace_back(as_svector(b.exponents()), convec, b.position());
  }

  return shells;
}

vector<pair<double, array<double, 3>>> yucca::libint::get_point_charges(const vector<Atom>& atoms)
{
  vector<pair<double, array<double, 3>>> q;
  for (const auto& atom : atoms) {
    q.push_back({static_cast<double>(atom.charge()), atom.position()});
  }
  return q;
}

libint2::Engine yucca::libint::get_engine(const libint2::Operator op, const int order,
                                          const yucca::Basis& b)
{
  libint2::Engine engine(op, b.max_nprim(), b.max_angular_momentum(), order);

  return engine;
}

namespace
{
template <typename EngineFunc>
void compute_1e_impl(EngineFunc engine_func, const yucca::Basis& basis, qleve::Tensor<2>& integrals,
                     const double fac)
{
  auto shells = libint::get_shells(basis);
  auto& bshells = basis.basisshells();
  const size_t nshells = shells.size();

  auto engine = engine_func();

  const auto& buf = engine.results();

  for (size_t jsh = 0; jsh < nshells; ++jsh) {
    const size_t joff = bshells[jsh].offset();
    const size_t jsize = shells[jsh].size();
    for (size_t ish = 0; ish <= jsh; ++ish) {
      const size_t ioff = bshells[ish].offset();
      const size_t isize = shells[ish].size();

      engine.compute(shells[ish], shells[jsh]);
      const double* vij = buf[0];

      if (vij == nullptr)
        continue;

      // copy onto output
      for (size_t i = ioff, ij = 0; i < ioff + isize; ++i) {
        for (size_t j = joff; j < joff + jsize; ++j, ++ij) {
          integrals(i, j) += fac * vij[ij];
        }
      }

      // transpose if offdiagonal shell pair
      if (ish != jsh) {
        for (size_t i = ioff, ij = 0; i < ioff + isize; ++i) {
          for (size_t j = joff; j < joff + jsize; ++j, ++ij) {
            integrals(j, i) += fac * vij[ij];
          }
        }
      }
    }
  }
}
} // namespace

void yucca::libint::fill_1e(const libint2::Operator op, const yucca::Basis& basis,
                            qleve::Tensor<2>& integrals, const double fac)
{
  compute_1e_impl([&]() { return libint::get_engine(op, 0, basis); }, basis, integrals, fac);
}

void yucca::libint::fill_1e(const libint2::Operator op, const yucca::Basis& basis,
                            qleve::Tensor<2>& integrals, const std::vector<Atom>& atoms,
                            const double fac)
{
  compute_1e_impl(
      [&]() { return libint::get_engine(op, 0, basis, libint::get_point_charges(atoms)); }, basis,
      integrals, fac);
}

void yucca::libint::fill_1e(const libint2::Operator op, const yucca::Basis& basis,
                            qleve::Tensor<2>& integrals, const libint2::BraKet bk, const double fac)
{
  compute_1e_impl([&]() { return libint::get_engine(op, 0, basis, bk); }, basis, integrals, fac);
}

void yucca::libint::compute_1e_stack_impl(const libint2::Operator op, const int& nops,
                                          const yucca::Basis& b, qleve::Tensor<3>& integrals,
                                          const double fac)
{
  auto shells = libint::get_shells(b);
  auto& bshells = b.basisshells();
  const size_t nshells = shells.size();

  auto engine = libint::get_engine(op, 0, b);

  const auto& buf = engine.results();

  for (size_t jsh = 0; jsh < nshells; ++jsh) {
    const size_t joff = bshells[jsh].offset();
    const size_t jsize = shells[jsh].size();
    for (size_t ish = 0; ish <= jsh; ++ish) {
      const size_t ioff = bshells[ish].offset();
      const size_t isize = shells[ish].size();
      const size_t nij = isize * jsize;

      engine.compute(shells[ish], shells[jsh]);
      const double* vij = buf[0];

      if (vij == nullptr)
        continue;

      // copy onto output
      for (int iop = 0; iop < nops; ++iop) {
        for (size_t i = ioff; i < ioff + isize; ++i) {
          for (size_t j = joff; j < joff + jsize; ++j) {
            const ptrdiff_t ij = iop * nij + (i - ioff) * jsize + (j - joff);

            integrals(i, j, iop) += fac * vij[ij];
          }
        }
      }

      // transpose if offdiagonal shell pair
      for (int iop = 0; iop < nops; ++iop) {
        if (ish != jsh) {
          for (size_t i = ioff; i < ioff + isize; ++i) {
            for (size_t j = joff; j < joff + jsize; ++j) {
              const ptrdiff_t ij = iop * nij + (i - ioff) * jsize + (j - joff);
              integrals(j, i, iop) += fac * vij[ij];
            }
          }
        }
      }
    }
  }
}
