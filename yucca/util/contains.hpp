// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/range_contains.hpp
///
/// Helper function to check if a range contains a value.
#pragma once

#include <string>
#include <vector>

namespace yucca
{

bool contains(const std::vector<std::string>& container, const std::string& value);

} // namespace yucca
