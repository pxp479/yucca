// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/default_paths.hpp
///
/// \brief Default locations for data files.
#include <string>
#include <vector>

namespace yucca
{

/// \brief Return a list of default paths for data files.
std::vector<std::string> default_paths();

} // namespace yucca
