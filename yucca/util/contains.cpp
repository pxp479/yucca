// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/contains.hpp
///
/// Helper function to check if a range contains a value.

#include <yucca/util/contains.hpp>

using namespace std;

bool yucca::contains(const vector<string>& container, const string& value)
{
  return find(container.begin(), container.end(), value) != container.end();
}

