// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/mdi_interface.hpp
///
/// Interface for dealing with MDI API calls

#include <external/MDI_Library/MDI_Library/mdi.h>
#include <memory>

#include <yucca/input/input_node.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/util/method.hpp>

namespace yucca::mdi
{

void run_mdi(char* node_name, MDI_Comm mdi_comm, const yucca::InputNode& cs,
             std::shared_ptr<yucca::Method_> method, std::shared_ptr<yucca::Geometry> geometry);

}
