// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/mdi_interface.cpp
///
/// Interface for dealing with MDI API calls

#include <cstring>
#include <stdexcept>

#include <fmt/core.h>

#include <yucca/util/mdi_interface.hpp>

using namespace std;
using namespace yucca;

void mdi::run_mdi(char* node_name, MDI_Comm mdi_comm, const yucca::InputNode& cs,
                  shared_ptr<Method_> method, shared_ptr<Geometry> geometry)
{
  bool exit_flag = false;
  auto command = new char[MDI_COMMAND_LENGTH];

  while (not exit_flag) {
    /* Receive a command from the driver */
    MDI_Recv_command(command, mdi_comm);
    fmt::print("Yucca has received {}\n", command);

    /* Confirm that this command is actually supported at this node */
    int command_supported = 0;
    MDI_Check_command_exists(node_name, command, MDI_COMM_NULL, &command_supported);
    if (command_supported != 1) {
      throw runtime_error("This received command is not recognized by the yucca engine!");
    }

    /* Respond to the received command */
    if (strcmp(command, "EXIT") == 0) {
      exit_flag = true;
    } else if (strcmp(command, "<ENERGY") == 0) {

      method->compute();
      double energy = method->energy();
      MDI_Send(&energy, 1, MDI_DOUBLE, mdi_comm);

    } else if (strcmp(command, "<NATOMS") == 0) {

      std::ptrdiff_t natom = geometry->natoms();
      MDI_Send(&natom, 1, MDI_INT, mdi_comm);

    } else if (strcmp(command, ">COORDS") == 0) {

      std::ptrdiff_t natom = geometry->natoms();
      qleve::Tensor<1> coords_data(natom * 3);
      MDI_Recv(coords_data.data(), natom * 3, MDI_DOUBLE, mdi_comm);
      coords_data.print("Received coordinates (Bohr)");

      // we're assuming the driver will be providing coords in units of bohr!
      *geometry = geometry->update(coords_data.reshape(3, natom));

      // update method with new geometry!
      method = yucca::get_method(cs, geometry);

    } else {
      throw runtime_error("This received command is not recognized by the yucca engine!");
    }
  }
  // Free any memory allocations
  delete[] command;
}
