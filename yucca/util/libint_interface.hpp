// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/libint_interface.hpp
///
/// Interface for dealing with libint2 engines
#pragma once

// Something of a hack to use c++17's any in old version of libint2
#include <any>
using std::any;
using std::any_cast;
using std::bad_any_cast;
#include <libint2.hpp>

#include <yucca/structure/basis.hpp>

namespace yucca::libint
{

std::vector<libint2::Shell> get_shells(const Basis& b);
std::vector<std::pair<double, std::array<double, 3>>> get_point_charges(
    const std::vector<Atom>& atoms);

libint2::Engine get_engine(const libint2::Operator op, const int order, const Basis& basis);

template <typename Param>
libint2::Engine get_engine(const libint2::Operator op, const int order, const Basis& basis,
                           Param&& p)
{
  libint2::Engine engine(op, basis.max_nprim(), basis.max_angular_momentum(), order);
  engine.set_params(p);

  return engine;
}


void fill_1e(const libint2::Operator op, const Basis& basis, qleve::Tensor<2>& integrals,
             const double fac = 1.0);
void fill_1e(const libint2::Operator op, const Basis& basis, qleve::Tensor<2>& integrals,
             const std::vector<Atom>& atoms, const double fac = 1.0);
void fill_1e(const libint2::Operator op, const Basis& basis, qleve::Tensor<2>& integrals,
             const libint2::BraKet bk, const double fac = 1.0);

void compute_1e_stack_impl(const libint2::Operator op, const int& nops, const Basis& basis,
                           qleve::Tensor<3>& integrals, const double fac = 1.0);

} // namespace yucca::libint
