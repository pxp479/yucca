// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/periodictable.hpp
///
/// Periodic Table
#pragma once

#include <map>

namespace yucca
{

struct Element {
  double meanmass;
  double vdwradius;
  double charge;
  std::string symbol;
  std::array<int, 4> configuration;

  Element(const std::string& s, const int ch, const double m, const double vdw,
          const std::array<int, 4>& conf) :
      meanmass(m), vdwradius(vdw), charge(ch), symbol(s), configuration(conf)
  {}
};

class PeriodicTable {
 protected:
  std::map<std::string, yucca::Element> elements_;

 public:
  PeriodicTable();

  const Element& element(std::string s) const { return elements_.at(s); }
};

} // namespace yucca
