// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/factorial.hpp
///
/// Factorial function
#pragma once

namespace yucca
{

inline ptrdiff_t factorial(const int n)
{
  static std::array<ptrdiff_t, 21> table{1ll,
                                         1ll,
                                         2ll,
                                         6ll,
                                         24ll,
                                         120ll,
                                         720ll,
                                         5040ll,
                                         40320ll,
                                         362880ll,
                                         3628800ll,
                                         39916800ll,
                                         479001600ll,
                                         6227020800ll,
                                         87178291200ll,
                                         1307674368000ll,
                                         20922789888000ll,
                                         355687428096000ll,
                                         6402373705728000ll,
                                         121645100408832000ll,
                                         2432902008176640000ll};
  assert(n >= 0 && n < 21);
  return table[n];
}

inline ptrdiff_t double_factorial(const int n)
{
  static std::array<ptrdiff_t, 21> table = {
      1ll,      1ll,       2ll,        3ll,        8ll,         15ll,        48ll,
      105ll,    384ll,     945ll,      3840ll,     10395ll,     46080ll,     135135ll,
      645120ll, 2027025ll, 10321920ll, 34459425ll, 185794560ll, 654729075ll, 3715891200ll};
  assert(n >= 0 && n < 21);
  return table[n];
}

inline double angular_norm(const int n)
{
  static std::array<double, 11> table = {1,
                                         1.0 / 2.0,
                                         3.0 / 4.0,
                                         15.0 / 8.0,
                                         105.0 / 16.0,
                                         945.0 / 32.0,
                                         10395.0 / 64.0,
                                         135135.0 / 128.0,
                                         2027025.0 / 256.0,
                                         34459425.0 / 512.0,
                                         654729075.0 / 1024.0};
  assert(n >= 0 && n < 11);
  return table[n];
}

} // namespace yucca
