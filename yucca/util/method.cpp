// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/method.cpp
///
/// Function to get a method based on an input node
#include <iostream>
#include <string>

#include <qleve/tensor.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/resmf/reshf.hpp>
#include <yucca/resmf/ureshf.hpp>
#include <yucca/response/cis.hpp>
#include <yucca/response/scf_response.hpp>
#include <yucca/scf/scf.hpp>
#include <yucca/scf/uconverger.hpp>
#include <yucca/scf/uscf.hpp>
#include <yucca/util/method.hpp>
#include <yucca/wfn/single_reference.hpp>
#include <yucca/wfn/wfn.hpp>

using namespace std;
using namespace yucca;

yucca::Method_::Method_(const Method_& x) :
    geometry_(x.geometry_),
    gradient_(x.gradient_ ? make_unique<qleve::Tensor<2>>(*x.gradient_) : nullptr)
{}

qleve::Tensor<2> yucca::Method_::gradient()
{
  if (!gradient_) {
    compute_gradient_impl();
  }

  return *gradient_;
}

namespace
{

bool guess_restricted(const yucca::InputNode& input, const shared_ptr<Geometry>& g)
{
  if (input.contains("spin")) {
    return input.get<ptrdiff_t>("spin") == 0;
  } else {
    const ptrdiff_t charge = input.get<ptrdiff_t>("charge", 0);
    const ptrdiff_t nelec = g->total_nuclear_charge() - charge;

    return (nelec % 2 == 0);
  }
}

bool guess_restricted(const yucca::InputNode& input, const shared_ptr<Wfn>& wfn)
{
  shared_ptr<SingleReference> ref_ptr = dynamic_pointer_cast<SingleReference>(wfn);
  if (ref_ptr) {
    if (input.contains_any({"spin", "charge"})) {
      // if charge/spin are specified, reevaluate whether it should be restricted
      if (input.contains("spin")) {
        return input.get<ptrdiff_t>("spin") == 0;
      } else {

        const ptrdiff_t charge = input.get<ptrdiff_t>("charge", ref_ptr->charge());
        const ptrdiff_t nelec = wfn->geometry()->total_nuclear_charge() - charge;

        return nelec % 2 == 0;
      }
    } else {
      return ref_ptr->nspin() == 1;
    }
  } else {
    return guess_restricted(input, wfn->geometry());
  }
}


template <typename... T>
shared_ptr<Method_> get_method_impl(const yucca::InputNode& input, T&&... t)
{
  if (!input.contains("method")) {
    throw runtime_error("No method specified in input file!");
  }

  auto method = input.get<string>("method");

  auto matches_method = [=](const vector<string>& keys) -> bool {
    return std::any_of(keys.begin(), keys.end(), [=](const string& s) { return s == method; });
  };

  if (matches_method({"scf", "uscf", "hf", "uhf", "dft", "ks", "uks"})) {
    if (matches_method({"uscf", "uhf", "uks"})) { // definitely unrestricted
      return make_shared<USCF>(input, forward<T>(t)...);
    } else { // try to guess whether restricted
      if (guess_restricted(input, forward<T>(t)...)) {
        return make_shared<SCF>(input, forward<T>(t)...);
      } else {
        return make_shared<USCF>(input, forward<T>(t)...);
      }
    }
  } else if (matches_method({"cis", "tda", "tddft", "tddft-ris", "ris"})) {
    return make_shared<CIS>(input, forward<T>(t)...);
  } else if (matches_method({"reshf"})) {
    return make_shared<ResHF>(input, forward<T>(t)...);
  } else if (matches_method({"ureshf"})) {
    return make_shared<uResHF>(input, forward<T>(t)...);
  } else if (matches_method({"polarizability"})) {
    return make_shared<SCFResponse>(input, forward<T>(t)...);
  } else {
    throw runtime_error("Unrecognized or unimplemented method!");
  }
}
} // namespace

shared_ptr<Method_> yucca::get_method(const yucca::InputNode& input, const shared_ptr<Geometry>& g)
{
  return get_method_impl(input, g);
}

shared_ptr<Method_> yucca::get_method(const yucca::InputNode& input, const shared_ptr<Wfn>& wfn)
{
  return get_method_impl(input, wfn);
}
