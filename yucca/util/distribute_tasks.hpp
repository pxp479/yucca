// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/distribute_tasks.hpp
///
/// Convenience routine to simplify distributing tasks into batches
#pragma once

#include <array>

namespace yucca
{

class StaticDistributeTasks {
 protected:
  ptrdiff_t ntasks_;
  ptrdiff_t batchsize_;
  ptrdiff_t nbatch_;

 public:
  StaticDistributeTasks(const ptrdiff_t ntasks, const ptrdiff_t batchsize) :
      ntasks_(ntasks), batchsize_(batchsize), nbatch_((ntasks - 1) / batchsize + 1)
  {}

  const std::array<ptrdiff_t, 3> batch(const ptrdiff_t i) const
  {
    assert(i < nbatch_);
    const ptrdiff_t batchstart = i * batchsize_;
    const ptrdiff_t batchend = std::min((i + 1) * batchsize_, ntasks_);
    return std::array<ptrdiff_t, 3>{batchstart, batchend, batchend - batchstart};
  }

  const ptrdiff_t nbatch() const { return nbatch_; }
};

} // namespace yucca
