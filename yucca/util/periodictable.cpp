// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/periodictable.cpp
///
/// Periodic Table
#include <array>
#include <iostream>

#include <yucca/util/periodictable.hpp>

using namespace std;
using namespace yucca;

using confarray = array<int, 4>;

// clang-format off
PeriodicTable::PeriodicTable() {
  elements_.emplace(  "q", Element(  "q",   0,         0.0000,   1.0000, confarray{{ 0, 0, 0, 0}}));
  elements_.emplace(  "h", Element(  "h",   1,        1.00784,   1.2000, confarray{{ 1, 0, 0, 0}}));
  elements_.emplace( "he", Element( "he",   2,      4.0026022,   1.4000, confarray{{ 2, 0, 0, 0}}));
  elements_.emplace( "li", Element( "li",   3,          6.938,   1.8200, confarray{{ 3, 0, 0, 0}}));
  elements_.emplace( "be", Element( "be",   4,     9.01218315,   1.0000, confarray{{ 4, 0, 0, 0}}));
  elements_.emplace(  "b", Element(  "b",   5,         10.806,   1.0000, confarray{{ 4, 1, 0, 0}}));
  elements_.emplace(  "c", Element(  "c",   6,        12.0096,   1.7000, confarray{{ 4, 2, 0, 0}}));
  elements_.emplace(  "n", Element(  "n",   7,       14.00643,   1.5500, confarray{{ 4, 3, 0, 0}}));
  elements_.emplace(  "o", Element(  "o",   8,       15.99903,   1.5200, confarray{{ 4, 4, 0, 0}}));
  elements_.emplace(  "f", Element(  "f",   9,  18.9984031636,   1.4700, confarray{{ 4, 5, 0, 0}}));
  elements_.emplace( "ne", Element( "ne",  10,       20.17976,   1.5400, confarray{{ 4, 6, 0, 0}}));
  elements_.emplace( "na", Element( "na",  11,   22.989769282,   2.2700, confarray{{ 5, 6, 0, 0}}));
  elements_.emplace( "mg", Element( "mg",  12,         24.304,   1.7300, confarray{{ 6, 6, 0, 0}}));
  elements_.emplace( "al", Element( "al",  13,    26.98153857,   1.8400, confarray{{ 6, 7, 0, 0}}));
  elements_.emplace( "si", Element( "si",  14,         28.084,   2.1000, confarray{{ 6, 8, 0, 0}}));
  elements_.emplace(  "p", Element(  "p",  15,  30.9737619985,   1.8000, confarray{{ 6, 9, 0, 0}}));
  elements_.emplace(  "s", Element(  "s",  16,         32.059,   1.8000, confarray{{ 6,10, 0, 0}}));
  elements_.emplace( "cl", Element( "cl",  17,         35.446,   1.7500, confarray{{ 6,11, 0, 0}}));
  elements_.emplace( "ar", Element( "ar",  18,        39.9481,   1.8800, confarray{{ 6,12, 0, 0}}));
  elements_.emplace(  "k", Element(  "k",  19,       39.09831,   2.7500, confarray{{ 7,12, 0, 0}}));
  elements_.emplace( "ca", Element( "ca",  20,        40.0784,   2.3100, confarray{{ 8,12, 0, 0}}));
  elements_.emplace( "sc", Element( "sc",  21,     44.9559085,   1.0000, confarray{{ 8,12, 1, 0}}));
  elements_.emplace( "ti", Element( "ti",  22,        47.8671,   2.2500, confarray{{ 8,12, 2, 0}}));
  elements_.emplace(  "v", Element(  "v",  23,       50.94151,   1.0000, confarray{{ 8,12, 3, 0}}));
  elements_.emplace( "cr", Element( "cr",  24,       51.99616,   1.0000, confarray{{ 8,12, 4, 0}}));
  elements_.emplace( "mn", Element( "mn",  25,     54.9380443,   1.0000, confarray{{ 8,12, 5, 0}}));
  elements_.emplace( "fe", Element( "fe",  26,        55.8452,   1.0000, confarray{{ 8,12, 6, 0}}));
  elements_.emplace( "co", Element( "co",  27,     58.9331944,   1.0000, confarray{{ 8,12, 7, 0}}));
  elements_.emplace( "ni", Element( "ni",  28,       58.69344,   1.6300, confarray{{ 8,12, 8, 0}}));
  elements_.emplace( "cu", Element( "cu",  29,        63.5463,   1.4500, confarray{{ 8,12, 9, 0}}));
  elements_.emplace( "zn", Element( "zn",  30,         65.382,   1.4200, confarray{{ 8,12,10, 0}}));
  elements_.emplace( "ga", Element( "ga",  31,        69.7231,   1.8700, confarray{{ 8,13,10, 0}}));
  elements_.emplace( "ge", Element( "ge",  32,        72.6308,   1.0000, confarray{{ 8,14,10, 0}}));
  elements_.emplace( "as", Element( "as",  33,     74.9215956,   1.8500, confarray{{ 8,15,10, 0}}));
  elements_.emplace( "se", Element( "se",  34,        78.9718,   1.9000, confarray{{ 8,16,10, 0}}));
  elements_.emplace( "br", Element( "br",  35,         79.901,   1.8500, confarray{{ 8,17,10, 0}}));
  elements_.emplace( "kr", Element( "kr",  36,        83.7982,   2.0200, confarray{{ 8,18,10, 0}}));
  elements_.emplace( "rb", Element( "rb",  37,       85.46783,   1.0000, confarray{{ 9,18,10, 0}}));
  elements_.emplace( "sr", Element( "sr",  38,         87.621,   1.0000, confarray{{10,18,10, 0}}));
  elements_.emplace(  "y", Element(  "y",  39,      88.905842,   1.0000, confarray{{10,18,11, 0}}));
  elements_.emplace( "zr", Element( "zr",  40,        91.2242,   1.0000, confarray{{10,18,12, 0}}));
  elements_.emplace( "nb", Element( "nb",  41,      92.906372,   1.0000, confarray{{10,18,13, 0}}));
  elements_.emplace( "mo", Element( "mo",  42,         95.951,   1.0000, confarray{{10,18,14, 0}}));
  elements_.emplace( "tc", Element( "tc",  43,             98,   1.0000, confarray{{10,18,15, 0}}));
  elements_.emplace( "ru", Element( "ru",  44,        101.072,   1.0000, confarray{{10,18,16, 0}}));
  elements_.emplace( "rh", Element( "rh",  45,     102.905502,   1.0000, confarray{{10,18,17, 0}}));
  elements_.emplace( "pd", Element( "pd",  46,        106.421,   1.6300, confarray{{10,18,18, 0}}));
  elements_.emplace( "ag", Element( "ag",  47,      107.86822,   1.7200, confarray{{10,18,19, 0}}));
  elements_.emplace( "cd", Element( "cd",  48,       112.4144,   1.5800, confarray{{10,18,20, 0}}));
  elements_.emplace( "in", Element( "in",  49,       114.8181,   1.9300, confarray{{10,19,20, 0}}));
  elements_.emplace( "sn", Element( "sn",  50,       118.7107,   2.1700, confarray{{10,20,20, 0}}));
  elements_.emplace( "sb", Element( "sb",  51,       121.7601,   1.0000, confarray{{10,21,20, 0}}));
  elements_.emplace( "te", Element( "te",  52,        127.603,   2.0600, confarray{{10,22,20, 0}}));
  elements_.emplace(  "i", Element(  "i",  53,     126.904473,   1.9800, confarray{{10,23,20, 0}}));
  elements_.emplace( "xe", Element( "xe",  54,       131.2936,   2.1600, confarray{{10,24,20, 0}}));
  elements_.emplace( "cs", Element( "cs",  55,  132.905451966,   1.0000, confarray{{11,24,20, 0}}));
  elements_.emplace( "ba", Element( "ba",  56,       137.3277,   2.6800, confarray{{12,24,20, 0}}));
  elements_.emplace( "la", Element( "la",  57,     138.905477,   1.0000, confarray{{12,24,20, 1}}));
  elements_.emplace( "ce", Element( "ce",  58,       140.1161,   1.0000, confarray{{12,24,20, 2}}));
  elements_.emplace( "pr", Element( "pr",  59,     140.907662,   1.0000, confarray{{12,24,20, 3}}));
  elements_.emplace( "nd", Element( "nd",  60,       144.2423,   1.0000, confarray{{12,24,20, 4}}));
  elements_.emplace( "pm", Element( "pm",  61,            145,   1.0000, confarray{{12,24,20, 5}}));
  elements_.emplace( "sm", Element( "sm",  62,        150.362,   1.0000, confarray{{12,24,20, 6}}));
  elements_.emplace( "eu", Element( "eu",  63,       151.9641,   1.0000, confarray{{12,24,20, 7}}));
  elements_.emplace( "gd", Element( "gd",  64,        157.253,   1.0000, confarray{{12,24,20, 8}}));
  elements_.emplace( "tb", Element( "tb",  65,     158.925352,   1.0000, confarray{{12,24,20, 9}}));
  elements_.emplace( "dy", Element( "dy",  66,       162.5001,   1.0000, confarray{{12,24,20,10}}));
  elements_.emplace( "ho", Element( "ho",  67,     164.930332,   1.0000, confarray{{12,24,20,11}}));
  elements_.emplace( "er", Element( "er",  68,       167.2593,   1.0000, confarray{{12,24,20,12}}));
  elements_.emplace( "tm", Element( "tm",  69,     168.934222,   1.0000, confarray{{12,24,20,13}}));
  elements_.emplace( "yb", Element( "yb",  70,       173.0545,   1.0000, confarray{{12,24,20,14}}));
  elements_.emplace( "lu", Element( "lu",  71,      174.96681,   1.0000, confarray{{12,24,21,14}}));
  elements_.emplace( "hf", Element( "hf",  72,        178.492,   1.0000, confarray{{12,24,22,14}}));
  elements_.emplace( "ta", Element( "ta",  73,     180.947882,   1.0000, confarray{{12,24,23,14}}));
  elements_.emplace(  "w", Element(  "w",  74,        183.841,   1.0000, confarray{{12,24,24,14}}));
  elements_.emplace( "re", Element( "re",  75,       186.2071,   1.0000, confarray{{12,24,25,14}}));
  elements_.emplace( "os", Element( "os",  76,        190.233,   1.0000, confarray{{12,24,26,14}}));
  elements_.emplace( "ir", Element( "ir",  77,       192.2173,   1.0000, confarray{{12,24,27,14}}));
  elements_.emplace( "pt", Element( "pt",  78,       195.0849,   1.7500, confarray{{12,24,28,14}}));
  elements_.emplace( "au", Element( "au",  79,    196.9665695,   1.6600, confarray{{12,24,29,14}}));
  elements_.emplace( "hg", Element( "hg",  80,       200.5923,   1.5500, confarray{{12,24,30,14}}));
  elements_.emplace( "tl", Element( "tl",  81,        204.382,   1.9600, confarray{{12,25,30,14}}));
  elements_.emplace( "pb", Element( "pb",  82,         207.21,   2.0200, confarray{{12,26,30,14}}));
  elements_.emplace( "bi", Element( "bi",  83,     208.980401,   1.0000, confarray{{12,27,30,14}}));
  elements_.emplace( "po", Element( "po",  84,            209,   1.0000, confarray{{12,28,30,14}}));
  elements_.emplace( "at", Element( "at",  85,            210,   1.0000, confarray{{12,29,30,14}}));
  elements_.emplace( "rn", Element( "rn",  86,            222,   2.2000, confarray{{12,30,30,14}}));
  elements_.emplace( "fr", Element( "fr",  87,            223,   1.0000, confarray{{13,30,30,14}}));
  elements_.emplace( "ra", Element( "ra",  88,            226,   1.0000, confarray{{14,30,30,14}}));
  elements_.emplace( "ac", Element( "ac",  89,            227,   1.0000, confarray{{14,30,30,15}}));
  elements_.emplace( "th", Element( "th",  90,      232.03774,   1.0000, confarray{{14,30,30,16}}));
  elements_.emplace( "pa", Element( "pa",  91,     231.035882,   1.0000, confarray{{14,30,30,17}}));
  elements_.emplace(  "u", Element(  "u",  92,     238.028913,   1.0000, confarray{{14,30,30,18}}));
  elements_.emplace( "np", Element( "np",  93,            237,   1.0000, confarray{{14,30,30,19}}));
  elements_.emplace( "pu", Element( "pu",  94,            244,   1.0000, confarray{{14,30,30,20}}));
  elements_.emplace( "am", Element( "am",  95,  241.056829319,   1.0000, confarray{{14,30,30,21}}));
  elements_.emplace( "cm", Element( "cm",  96,  243.061389322,   1.0000, confarray{{14,30,30,22}}));
  elements_.emplace( "bk", Element( "bk",  97,  247.070307359,   1.0000, confarray{{14,30,30,23}}));
  elements_.emplace( "cf", Element( "cf",  98,  249.074853923,   1.0000, confarray{{14,30,30,24}}));
  elements_.emplace( "es", Element( "es",  99,   252.08298054,   1.0000, confarray{{14,30,30,25}}));
  elements_.emplace( "fm", Element( "fm", 100,  257.095106169,   1.0000, confarray{{14,30,30,26}}));
  elements_.emplace( "md", Element( "md", 101,  258.098431550,   1.0000, confarray{{14,30,30,27}}));
  elements_.emplace( "no", Element( "no", 102,    259.1010311,   1.0000, confarray{{14,30,30,28}}));
  elements_.emplace( "lr", Element( "lr", 103,    262.1096122,   1.0000, confarray{{14,30,30,28}}));
  elements_.emplace( "rf", Element( "rf", 104,    267.1217962,   1.0000, confarray{{14,30,30,28}}));
  elements_.emplace( "db", Element( "db", 105,    268.1256757,   1.0000, confarray{{14,30,30,28}}));
  elements_.emplace( "sg", Element( "sg", 106,    271.1339363,   1.0000, confarray{{14,30,30,28}}));
  elements_.emplace( "bh", Element( "bh", 107,    272.1382658,   1.0000, confarray{{14,30,30,28}}));
  elements_.emplace( "hs", Element( "hs", 108,    270.1342927,   1.0000, confarray{{14,30,30,28}}));
  elements_.emplace( "mt", Element( "mt", 109,    276.1515959,   1.0000, confarray{{14,30,30,28}}));
}
// clang-format on
