// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/timer.cpp
///
/// Implementation for yucca::Timer class
#include <algorithm>
#include <chrono>
#include <numeric>
#include <vector>

#include <fmt/core.h>

#include <yucca/util/timer.hpp>

using namespace std;

yucca::Timer::Timer(string title) : title_(title) { mark(); }

void yucca::Timer::mark() { now_ = std::chrono::high_resolution_clock::now(); }

double yucca::Timer::tick(const string& section)
{
  auto then = now_;
  now_ = std::chrono::high_resolution_clock::now();
  auto dur = std::chrono::duration_cast<std::chrono::microseconds>(now_ - then);

  if (auto iter = times_.find(section); iter != times_.end()) {
    iter->second += dur;
  } else {
    times_.emplace(section, dur);
  }

  return dur.count() * 1e-6;
}

void yucca::Timer::tick_print(const string& section)
{
  double t = this->tick(section);
  fmt::print("  - {:s}: {:8.4f} s\n", section, t);
}

void yucca::Timer::summarize()
{
  using pt = pair<string, chrono::microseconds>;
  vector<pt> sorted_times(times_.begin(), times_.end());
  sort(sorted_times.begin(), sorted_times.end(),
       [](const pt& x, const pt& y) { return x.second > y.second; });
  const double total_time = accumulate(sorted_times.begin(), sorted_times.end(), 0.0,
                                       [](double x, const pt& y) { return x + y.second.count(); });
  fmt::print("{:>22s} {:>16s} {:>16s}\n", "section", "time", "percentage");
  fmt::print("{:-^56s}\n", "");
  for (auto& x : sorted_times) {
    const double dur = x.second.count();
    fmt::print("{:>22s} {:>16.4f} {:>16.4f}\n", x.first, dur * 1e-6, 100.0 * dur / total_time);
  }
}
