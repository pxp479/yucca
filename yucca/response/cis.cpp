// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/response/cis.cpp
///
/// SCF driver class
#include <iostream>
#include <memory>

#include <fmt/core.h>

#include <qleve/array.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>

#include <yucca/algo/davidson.hpp>
#include <yucca/dft/density_functional.hpp>
#include <yucca/input/input_node.hpp>
#include <yucca/physical/constants.hpp>
#include <yucca/response/cis.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/util/timer.hpp>
#include <yucca/wfn/cis_reference.hpp>
#include <yucca/wfn/single_reference.hpp>

using namespace std;
using namespace yucca;

yucca::CIS::CIS(const InputNode& input, const shared_ptr<Geometry>& g) : Method_(g)
{
  throw std::runtime_error("A reference (SCF) must be computed before a response calculation.");
}

yucca::CIS::CIS(const InputNode& input, const shared_ptr<Wfn>& wfn) : Method_(wfn->geometry())
{
  fmt::print("Setting up CIS calculation\n");

  max_iter_ = input.get<ptrdiff_t>("max_iter", 30);

  // this is an important block: set it up rigorously externally somewhere
  string conv = input.get<string>("convergence", "default");
  if (conv == "loose") {
    conv_residual_ = 1e-4;
  } else if (conv == "tight") {
    conv_residual_ = 1e-7;
  } else if (conv == "ultratight") {
    conv_residual_ = 1e-9;
  } else { // default
    conv_residual_ = 1e-5;
  }

  // allow specific overrides
  conv_residual_ = input.get<double>("conv_residual", conv_residual_);

  nstates_ = input.get<ptrdiff_t>("nstates", 1);
  active_state_ = input.get<ptrdiff_t>("active_state", 0);
  if (active_state_ < 0 || active_state_ > nstates_) {
    throw std::runtime_error("active_state must > 0 and < nstates");
  }

  const shared_ptr<SingleReference> single_ref = dynamic_pointer_cast<SingleReference>(wfn);

  if (single_ref) {
    coeffs_ = make_shared<qleve::Tensor<2>>(*single_ref->coeffs());
    assert(coeffs_);

    nmo_ = coeffs_->extent(1);
    nocc_ = single_ref->nocc();
    nvir_ = nmo_ - nocc_;

    fmt::print("Reference properties:\n");
    fmt::print("  - nmo:  {}\n", nmo_);
    fmt::print("  - nocc: {}\n", nocc_);
    fmt::print("  - nvir: {}\n", nvir_);

    fock_ = make_shared<qleve::Tensor<2>>(*single_ref->fock());
    assert(fock_);

    dft_ = single_ref->dft();
    density_ = single_ref->density();

    energy0_ = single_ref->energy();
  } else {
    throw runtime_error("A single reference (HF) calculation must be run before CIS");
  }

  diag_ = make_shared<qleve::Array>(nocc_ * nvir_);
  qleve::TensorView<2> d_ia = diag_->reshape(nocc_, nvir_);
  for (ptrdiff_t a = 0; a < nvir_; ++a) {
    for (ptrdiff_t i = 0; i < nocc_; ++i) {
      d_ia(i, a) = (*fock_)(a + nocc_, a + nocc_) - (*fock_)(i, i);
    }
  }

  davidson_ = make_unique<Davidson<double>>(nstates_, diag_, nstates_ * max_iter_);
  energies_ = vector<double>(nstates_, 0.0);
  eigenstates_ = make_shared<qleve::Tensor<2>>(diag_->size(), nstates_);

  use_fxc_ = input.get<bool>("fxc", static_cast<bool>(dft_));
  if (auto method = input.get<string>("method"); method == "tddft-ris" || method == "ris") {
    use_fxc_ = false;
  }
}

qleve::Tensor<2> yucca::CIS::initial_guess(const size_t nguess) noexcept
{
  std::vector<size_t> inds(diag_->size());
  std::iota(inds.begin(), inds.end(), 0ull);
  std::partial_sort(inds.begin(), inds.begin() + nguess, inds.end(),
                    [this](int i, int j) { return diag_->operator()(i) < diag_->operator()(j); });

  // V0 and W0 will just hold the memory for the matrix vector products
  qleve::Tensor<2> V0(diag_->size(), nguess);
  for (size_t i = 0; i < nguess; ++i) {
    V0(inds[i], i) = 1.0;
  }

  return V0;
}

void yucca::CIS::compute()
{
  Timer timer("CIS");

  fmt::print("Starting CIS calculation\n");
  if (use_fxc_) {
    throw runtime_error("CIS with XC functional not implemented yet");
  }

  fmt::print("Convergence when residual norm < {:1e}\n", conv_residual_);

  auto cc_occ = make_shared<qleve::Tensor<2>>(coeffs_->slice(0, nocc_));
  auto cc_vir = make_shared<qleve::Tensor<2>>(coeffs_->slice(nocc_, nocc_ + nvir_));
  if (!cis_matrix_) {
    cis_matrix_ = make_unique<RCISMatrix>(*geometry_, diag_, cc_occ, cc_vir, /*jfac*/ 1.0, /*kfac*/
                                          dft_ ? dft_->exchange_factor() : 1.0);
  }

  fmt::print("{:>4s} {:>6s} {:>12s} {:>10s} {:>8s}\n", "iter", "state", "energies", "|res|",
             "time");
  fmt::print("{:-^54s}\n", "");

  vector<double> resnrm(nstates_, 1e-4);
  vector<bool> conv(nstates_, false);

  const ptrdiff_t nph = diag_->size();
  const size_t nguess = std::min(nph, nstates_);

  timer.mark();
  auto V0 = initial_guess(nguess);
  timer.tick_print("initial guess");

  auto W0 = V0.zeros_like();
  ptrdiff_t nnew = nguess;

  for (ptrdiff_t icis = 0; icis < max_iter_; ++icis) {
    qleve::ConstTensorView<2> v = V0.const_slice(0, nnew);
    qleve::TensorView<2> w = W0.slice(0, nnew);
    w = 0.0;

    // matrix vector products
    (*cis_matrix_)(v, w);
    double multtime = timer.tick("matrix vector product");

    davidson_->push_back(v, w);
    auto [eigs, V, residual] = davidson_->estimate();
    *eigenstates_ = V;
    double davidsontime = timer.tick("subspace solve");

    for (ptrdiff_t ist = 0; ist < nstates_; ++ist) {
      resnrm.at(ist) = residual.pluck(ist).norm();
      energies_.at(ist) = eigs(ist);
      conv.at(ist) = resnrm.at(ist) < conv_residual_;
    }

    // print results
    fmt::print("{:4d} {:6d} {:12.6f} {:10.1e} {:8.3f}\n", icis, 0, energies_.at(0), resnrm.at(0),
               multtime);
    if (nstates_ > 1) {
      for (int istate = 1; istate < nstates_; ++istate) {
        fmt::print("{:4s} {:6d} {:12.6f} {:10.1e} {:8.3f}\n", "", istate, energies_.at(istate),
                   resnrm.at(istate), multtime);
      }
      fmt::print("\n");
    }

    if (all_of(conv.begin(), conv.end(), [](const bool x) { return x; })) {
      break;
    }

    timer.mark();

    nnew = davidson_->precondition(residual, eigs, conv);
    timer.tick("precondition");
    nnew = davidson_->regularize_new_vectors(residual.slice(0, nnew));

    timer.tick("regularize");
    V0.slice(0, nnew) = residual.slice(0, nnew);

    timer.mark();
  }

  if (all_of(conv.begin(), conv.end(), [](const bool x) { return x; })) {
    fmt::print("\n  Converged!\n\n");
    fmt::print("Final excitation energies:\n");
    fmt::print("{:>8s} {:>20s} {:>20s} {:>20s}\n", "state", "Hartree", "eV", "nm");
    fmt::print("{:-^72s}\n", "");
    for (int istate = 0; istate < nstates_; ++istate) {
      const double en_H = energies_.at(istate);
      const double en_eV = en_H * physical::Hartree2eV;
      const double en_nm = physical::Hartree2nm / en_H;
      fmt::print("{:8d} {:20.8f} {:20.8f} {:20.8f}\n", istate + 1, en_H, en_eV, en_nm);
    }
  } else {
    throw runtime_error("Failed to converge");
  }

  fmt::print("Now would be a great time to compute some properties\n");
  for (int istate = 0; istate < nstates_; ++istate) {
    const double total_energy = energy0_ + energies_.at(istate);
    fmt::print("State {:d} final energy: {:20.8f}\n", istate + 1, total_energy);

    auto T = eigenstates_->pluck(istate).reshape(nocc_, nvir_);
    const double print_thresh_ = 5e-3;
    for (ptrdiff_t i = nocc_ - 1; i >= 0; --i) {
      for (ptrdiff_t a = 0; a < nvir_; ++a) {
        const double tia = T(i, a);
        if (tia * tia > print_thresh_) {
          fmt::print("  {:3d} -> {:3d} : {:6.4f}\n", i, a + nocc_, tia);
        }
      }
    }
  }

  timer.summarize();
}

double yucca::CIS::energy() { return energies_.at(active_state_); }

vector<double> yucca::CIS::energies() { return energies_; }
void yucca::CIS::compute_gradient_impl()
{
  gradient_ = make_unique<qleve::Tensor<2>>(3, geometry_->natoms());
}

std::shared_ptr<Wfn> yucca::CIS::wavefunction()
{
  auto out = make_shared<CISReference>(geometry_, coeffs_, eigenstates_, nocc_, nvir_);
  return out;
}

