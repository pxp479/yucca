// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/response/cis.hpp
///
/// Response theory-like base class for CIS/TDA and TDHF/RPA
#pragma once

#include <memory>

#include <qleve/tensor.hpp>

#include <yucca/algo/krylov_lse.hpp>
#include <yucca/scf/converger.hpp>
#include <yucca/util/method.hpp>

namespace yucca
{

// use forward declarations here to prevent compilation cascades
class Geometry; // #include <yucca/structure/geometry.hpp>
class Wfn;      // #include <yucca/wfn/wfn.hpp>

class SCFResponse : public Method_ {
 protected:
  ptrdiff_t nmo_;
  ptrdiff_t nao_;
  ptrdiff_t nocc_;
  ptrdiff_t nvir_;
  ptrdiff_t nelec_;
  int charge_;

  double conv_residual_;

  // user parameters
  ptrdiff_t max_multipole_order_;
  ptrdiff_t nperturbations_;
  ptrdiff_t nfrequencies_;
  ptrdiff_t nroots_;

  // input
  std::shared_ptr<qleve::Tensor<2>> coeffs_;
  std::shared_ptr<qleve::Tensor<2>> fock_;
  std::shared_ptr<qleve::Array> diag_;

  // numerical parameters
  ptrdiff_t max_iter_;

  // convergence helper
  std::unique_ptr<KrylovLSE<double>> krylov_solver_;

  // polarizability specific parameters
  std::shared_ptr<qleve::Tensor<1>> frequencies_;     ///< list of frequencies to compute
  std::shared_ptr<qleve::Tensor<1>> root_shifts_;     ///< shift used for each RHS
  std::shared_ptr<qleve::Tensor<2>> PQia_;            ///< property integrals: |P,Q>
  std::shared_ptr<qleve::Tensor<2>> polarization_;    ///< polarization vectors |X,Y>
  std::shared_ptr<qleve::Tensor<3>> linear_response_; ///< linear response tensor: -<P,Q|X,Y>

  qleve::Tensor<2> initial_guess() noexcept;

 public:
  SCFResponse(const InputNode& input, const std::shared_ptr<Geometry>& g);
  SCFResponse(const InputNode& input, const std::shared_ptr<Wfn>& wfn);

  virtual void compute() override;

  virtual double energy() override;
  virtual std::vector<double> energies() override;

  virtual std::shared_ptr<Wfn> wavefunction() override;

  void compute_RHS();

 protected:
  virtual void compute_gradient_impl() override;
};

} // namespace yucca
