// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/response/scf_response.cpp
///
/// SCF Response Theory driver functions
#include <iostream>
#include <memory>

#include <fmt/core.h>

#include <qleve/array.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/trace.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/physical/constants.hpp>
#include <yucca/response/response_matrix.hpp>
#include <yucca/response/scf_response.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/util/timer.hpp>
#include <yucca/wfn/single_reference.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

yucca::SCFResponse::SCFResponse(const InputNode& input, const shared_ptr<Geometry>& g) : Method_(g)
{
  throw std::runtime_error("A reference (SCF) must be computed before a response calculation.");
}

yucca::SCFResponse::SCFResponse(const InputNode& input, const shared_ptr<Wfn>& wfn) :
    Method_(wfn->geometry())
{
  fmt::print("Setting up Linear Response calculation\n");

  max_iter_ = input.get<ptrdiff_t>("max_iter", 30);

  // this is an important block: set it up rigorously externally somewhere
  string conv = input.get<string>("convergence", "default");
  if (conv == "loose") {
    conv_residual_ = 1e-4;
  } else if (conv == "tight") {
    conv_residual_ = 1e-7;
  } else if (conv == "ultratight") {
    conv_residual_ = 1e-9;
  } else { // default
    conv_residual_ = 1e-5;
  }

  // allow specific overrides
  conv_residual_ = input.get<double>("conv_residual", conv_residual_);

  const shared_ptr<SingleReference> single_ref = dynamic_pointer_cast<SingleReference>(wfn);

  if (single_ref) {
    coeffs_ = make_shared<qleve::Tensor<2>>(*single_ref->coeffs());
    assert(coeffs_);

    nmo_ = coeffs_->extent(1);
    nocc_ = single_ref->nocc();
    nvir_ = nmo_ - nocc_;

    fmt::print("Reference properties:\n");
    fmt::print("  - nmo:  {}\n", nmo_);
    fmt::print("  - nocc: {}\n", nocc_);
    fmt::print("  - nvir: {}\n", nvir_);

    fock_ = make_shared<qleve::Tensor<2>>(*single_ref->fock());
    assert(fock_);

  } else {
    throw runtime_error("A single reference (HF) calculation must be run before response");
  }

  diag_ = make_shared<qleve::Array>(nocc_ * nvir_);
  qleve::TensorView<2> d_ia = diag_->reshape(nocc_, nvir_);
  for (ptrdiff_t a = 0; a < nvir_; ++a) {
    for (ptrdiff_t i = 0; i < nocc_; ++i) {
      d_ia(i, a) = (*fock_)(a + nocc_, a + nocc_) - (*fock_)(i, i);
    }
  }

  max_multipole_order_ = input.get<ptrdiff_t>("multipole order", 1);

  nperturbations_ = (2 + max_multipole_order_) * max_multipole_order_;
  vector<double> freqs = input.get_vector<double>("frequencies", {0.0});
  if (any_of(freqs.begin(), freqs.end(), [](const double& x) { return x != 0.0; })) {
    throw runtime_error("dynamic polarizability not yet implemented");
  }
  nfrequencies_ = freqs.size();
  nroots_ = nperturbations_ * nfrequencies_;

  frequencies_ = make_unique<Tensor<1>>(freqs.size());
  copy_n(freqs.data(), nfrequencies_, frequencies_->data());

  root_shifts_ = make_unique<Tensor<1>>(nperturbations_ * freqs.size());
  for (ptrdiff_t i = 0; i < nfrequencies_; ++i) {
    root_shifts_->slice(i * nperturbations_, (i + 1) * nperturbations_) = frequencies_->at(i);
  }
}

void yucca::SCFResponse::compute_RHS()
{
  const ptrdiff_t nao = geometry_->orbital_basis()->nbasis();

  Tensor<3> multipoles = geometry_->multipole(max_multipole_order_, {0.0, 0.0, 0.0});
  // first slice of multipoles is the overlap matrix. the rest are dipole, quadrupole, etc.
  auto ops = multipoles.slice(1, multipoles.extent(2));
  const ptrdiff_t nops = nperturbations_;
  assert(nperturbations_ == ops.extent(2));

  PQia_ = make_unique<Tensor<2>>(nocc_ * nvir_, nroots_);
  auto pqia = PQia_->slice(0, nops).reshape(nocc_, nvir_, nops);

  Tensor<3> pqiv(nocc_, nao, nops);
  qleve::contract(1.0, ops, "uvp", coeffs_->slice(0, nocc_), "ui", 0.0, pqiv, "ivp");
  // factor of -1 accounts for electron charge; factor of 2 accounts for Pia = Qia
  qleve::contract(-2.0, pqiv, "ivp", coeffs_->slice(nocc_, nmo_), "va", 0.0, pqia, "iap");

  for (ptrdiff_t p = 1; p < nfrequencies_; ++p) {
    PQia_->slice(p * nops, (p + 1) * nops) = PQia_->slice(0, nops);
  }

  polarization_ = make_unique<Tensor<2>>(PQia_->zeros_like());
}

qleve::Tensor<2> yucca::SCFResponse::initial_guess() noexcept
{
  assert(PQia_ && frequencies_);

  qleve::Tensor<2> V0(*PQia_);
  auto vv = V0.reshape(nocc_ * nvir_, nperturbations_, nfrequencies_);

  const Tensor<1>& d = *diag_;
  const double e = 1.0e-6;

  for (ptrdiff_t p = 0; p < nroots_; ++p) {
    V0.pluck(p) *= d / (d * d + e * e);
  }

  const ptrdiff_t nvec = krylov_solver_->regularize_new_vectors(V0);

  auto vout = V0.slice(0, nvec);

  return vout;
}

void yucca::SCFResponse::compute()
{
  Timer timer("Response");

  fmt::print("Starting SCF Response Theory calculation\n");

  fmt::print("\n");
  fmt::print("Using frequencies:\n");
  for (ptrdiff_t i = 0; i < nfrequencies_; ++i) {
    const double freq = frequencies_->at(i);
    fmt::print("  - {:12.8f} a.u.  |  {:12.8f} eV\n", freq, freq * physical::Hartree2eV);
  }
  fmt::print("\n");

  fmt::print("Using multipole perturbations up to order {:3d}\n", max_multipole_order_);

  fmt::print("Convergence when residual norm < {:1e}\n", conv_residual_);

  compute_RHS();
  timer.tick_print("build RHS vectors");

  krylov_solver_ =
      make_unique<KrylovLSE<double>>(*PQia_, *root_shifts_, diag_, nroots_ * max_iter_);
  timer.tick_print("krylov solver setup");

  auto cc_occ = make_shared<qleve::Tensor<2>>(coeffs_->slice(0, nocc_));
  auto cc_vir = make_shared<qleve::Tensor<2>>(coeffs_->slice(nocc_, nmo_));
  // (A+B) matrix multiplier
  RAplusB abmatrix(*geometry_, diag_, cc_occ, cc_vir, /*jfac*/ 1.0, /*kfac*/ 1.0);

  fmt::print("{:>8s} {:>8s} {:>20s} {:>12s} {:>12s}\n", "iter", "root", "frequency", "|residual|",
             "time");
  fmt::print("{:-^74s}\n", "");

  const ptrdiff_t nroots = nroots_;

  vector<double> resnrm(nroots, 1.0);
  vector<bool> conv(nroots, false);

  const ptrdiff_t nph = diag_->size();

  // TODO this will fail if there is linear dependence in the initial guess
  auto V0 = initial_guess();
  timer.tick_print("initial guess");

  auto W0 = V0.zeros_like();

  ptrdiff_t nnew = V0.extent(1);

  fmt::print("starting with {} initial vectors\n", nnew);

  for (ptrdiff_t iter = 0; iter < max_iter_; ++iter) {
    qleve::ConstTensorView<2> v = V0.const_slice(0, nnew);
    qleve::TensorView<2> w = W0.slice(0, nnew);
    w = 0.0;

    // matrix vector products
    timer.mark();
    abmatrix(v, w);
    const double focktime = timer.tick("matrix vector product");

    krylov_solver_->push_back(v, w);
    auto [V, residual] = krylov_solver_->estimate();
    // factor of -1 is from definition |x,y> = -(Omega - omega Delta)^{-1}|p,q>
    *polarization_ = -1.0 * V;

    for (ptrdiff_t iroot = 0; iroot < nroots; ++iroot) {
      resnrm.at(iroot) = residual.pluck(iroot).norm();
      conv.at(iroot) = resnrm.at(iroot) < conv_residual_;
    }

    // print results
    for (int istate = 0; istate < nroots; ++istate) {
      fmt::print("{:8s} {:8d} {:20.8f} {:12.3e} {:12.1e}\n", "", istate + 1,
                 root_shifts_->at(istate), resnrm.at(istate), focktime);
    }
    fmt::print("\n");

    if (all_of(conv.begin(), conv.end(), [](const bool x) { return x; })) {
      break;
    }

    nnew = krylov_solver_->precondition(residual, conv);
    nnew = krylov_solver_->regularize_new_vectors(residual.slice(0, nnew));
    V0.slice(0, nnew) = residual.slice(0, nnew);
  }

  if (all_of(conv.begin(), conv.end(), [](const bool x) { return x; })) {
    fmt::print("\n  Converged!\n\n");
  } else {
    throw runtime_error("Failed to converge");
  }

  linear_response_ = make_shared<Tensor<3>>(nperturbations_, nperturbations_, nfrequencies_);
  auto propia = PQia_->slice(0, nperturbations_);
  auto pol = polarization_->reshape(nph, nperturbations_, nfrequencies_);

  // factor of -1 is from definition of linear response as -<x,y|p,q> = -<x+y|p+q>/2
  // note: factor of 1/2 from <x+y|p+q> cancels factor of 2 from alpha + beta
  qleve::contract(-1.0, propia, "xa", pol, "xbf", 0.0, *linear_response_, "abf");

  // print polarizabilities
  for (ptrdiff_t f = 0; f < nfrequencies_; ++f) {
    auto alpha = linear_response_->pluck(f).subtensor({0, 0}, {3, 3});
    alpha.print(fmt::format("Polarizability with frequency {:f}", frequencies_->at(f)));
  }
}

double yucca::SCFResponse::energy()
{
  return qleve::linalg::trace(linear_response_->pluck(0)) / 3.0;
}

vector<double> yucca::SCFResponse::energies()
{
  return vector<double>(linear_response_->data(),
                        linear_response_->data() + linear_response_->size());
}
void yucca::SCFResponse::compute_gradient_impl()
{
  gradient_ = make_unique<qleve::Tensor<2>>(3, geometry_->natoms());
}

std::shared_ptr<Wfn> yucca::SCFResponse::wavefunction() { return nullptr; }
