// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/response/response_matrix.hpp
///
/// Applies response matrix onto vectors
#pragma once

#include <qleve/array.hpp>
#include <qleve/tensor.hpp>

#include <yucca/structure/geometry.hpp>

namespace yucca
{

class SCFResponseMatrix_ {
 protected:
  const Geometry& geometry_;
  std::shared_ptr<qleve::Array> diag_;
  std::shared_ptr<qleve::Tensor<2>> cc_occ_;
  std::shared_ptr<qleve::Tensor<2>> cc_vir_;

  std::shared_ptr<qleve::Tensor<3>> B_iaP_;
  std::shared_ptr<qleve::Tensor<3>> B_ijP_;
  std::shared_ptr<qleve::Tensor<3>> B_abP_;

  double hartree_;
  double exchange_;

  bool use_df_;

 public:
  SCFResponseMatrix_(const Geometry& g, std::shared_ptr<qleve::Array> diag,
                     std::shared_ptr<qleve::Tensor<2>> cc_occ,
                     std::shared_ptr<qleve::Tensor<2>> cc_vir, const double hartree = 1.0,
                     const double exchange = 1.0, const bool use_df = true);
};

class RCISMatrix : public SCFResponseMatrix_ {
 public:
  RCISMatrix(const Geometry& g, std::shared_ptr<qleve::Array> diag,
             std::shared_ptr<qleve::Tensor<2>> cc_occ, std::shared_ptr<qleve::Tensor<2>> cc_vir,
             const double hartree = 1.0, const double exchange = 1.0, const bool use_df = true);

  void operator()(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av);
  qleve::Tensor<2> operator()(const qleve::ConstTensorView<2> v);

  void apply_ao(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av);
  void apply_mo(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av);
};

class RAplusB : public SCFResponseMatrix_ {
 public:
  RAplusB(const Geometry& g, std::shared_ptr<qleve::Array> diag,
          std::shared_ptr<qleve::Tensor<2>> cc_occ, std::shared_ptr<qleve::Tensor<2>> cc_vir,
          const double hartree = 1.0, const double exchange = 1.0, const bool use_df = true);

  void operator()(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av);
  qleve::Tensor<2> operator()(const qleve::ConstTensorView<2> v);

  void apply_ao(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av);
  void apply_mo(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av);
};

#if 0
class RResponseMatrix {
 protected:
  const Geometry& geometry_;

 public:
  RResponseMatrix(const Geometry& g);

  void operator()(const qleve::Tensor<3>& rho, qleve::Tensor<3>& fock, const double jfac = 1.0,
                  const double kfac = 1.0);
  qleve::Tensor<3> operator()(const qleve::Tensor<3>& rho, const double jfac = 1.0,
                                const double kfac = 1.0);
};
#endif

} // namespace yucca
