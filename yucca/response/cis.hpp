// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/response/cis.hpp
///
/// Response theory-like base class for CIS/TDA and TDHF/RPA
#pragma once

#include <memory>

#include <qleve/tensor.hpp>

#include <yucca/algo/davidson.hpp>
#include <yucca/response/response_matrix.hpp>
#include <yucca/scf/converger.hpp>
#include <yucca/util/method.hpp>

namespace yucca
{

// use forward declarations here to prevent compilation cascades
class Geometry;          // #include <yucca/structure/geometry.hpp>
class Wfn;               // #include <yucca/wfn/wfn.hpp>
class DensityFunctional; // #include <yucca/dft/density_functional.hpp>

class CIS : public Method_ {
 protected:
  ptrdiff_t nmo_;
  ptrdiff_t nao_;
  ptrdiff_t nocc_;
  ptrdiff_t nvir_;
  ptrdiff_t nelec_;
  int charge_;

  double conv_residual_;

  // user parameters
  ptrdiff_t nstates_;

  // input
  std::shared_ptr<qleve::Tensor<2>> coeffs_;
  std::shared_ptr<qleve::Tensor<2>> fock_;
  std::shared_ptr<qleve::Array> diag_;

  std::shared_ptr<yucca::DensityFunctional> dft_;
  std::shared_ptr<qleve::Tensor<2>> density_;

  // numerical parameters
  ptrdiff_t max_iter_;

  ptrdiff_t active_state_;

  // response function
  std::unique_ptr<RCISMatrix> cis_matrix_;

  // convergence helper
  std::unique_ptr<Davidson<double>> davidson_;

  // results
  double energy0_;
  std::vector<double> energies_;
  std::shared_ptr<qleve::Tensor<2>> eigenstates_;

  bool use_fxc_;

  qleve::Tensor<2> initial_guess(const size_t nguess) noexcept;

 public:
  CIS(const InputNode& input, const std::shared_ptr<Geometry>& g);
  CIS(const InputNode& input, const std::shared_ptr<Wfn>& wfn);

  virtual void compute() override;

  virtual double energy() override;
  virtual std::vector<double> energies() override;

  virtual std::shared_ptr<Wfn> wavefunction() override;

 protected:
  virtual void compute_gradient_impl() override;
};

} // namespace yucca
