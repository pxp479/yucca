// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/response/response_matrix.cpp
///
/// Applies response matrix to vectors
#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor_contract.hpp>

#include <yucca/response/response_matrix.hpp>
#include <yucca/scf/fock.hpp>
#include <yucca/structure/density_fitting.hpp>
#include <yucca/util/libint_interface.hpp>

using namespace std;
using namespace yucca;

yucca::SCFResponseMatrix_::SCFResponseMatrix_(const Geometry& g, shared_ptr<qleve::Array> diag,
                                              shared_ptr<qleve::Tensor<2>> cc_occ,
                                              shared_ptr<qleve::Tensor<2>> cc_vir,
                                              const double hartree, const double exchange,
                                              const bool use_df) :
    geometry_(g),
    diag_(diag),
    cc_occ_(cc_occ),
    cc_vir_(cc_vir),
    hartree_(hartree),
    exchange_(exchange),
    use_df_((g.density_fitting() || (g.j_density_fitting() && g.k_density_fitting())) && use_df)
{
  if (use_df_) {
    fmt::print("setting up density fitting quantities for CIS\n");

    assert(bool{g.density_fitting()} != (bool{g.j_density_fitting()} && bool{g.k_density_fitting()})
           && "Cannot use both density fitting and J/K density fitting");

    if (g.density_fitting()) {
      const auto& B = g.density_fitting()->B();
      const ptrdiff_t nao = B.extent(0);
      const ptrdiff_t nocc = cc_occ->extent(1);
      const ptrdiff_t nvir = cc_vir->extent(1);
      const ptrdiff_t nfit = B.extent(2);

      B_iaP_ = std::make_shared<qleve::Tensor<3>>(nocc, nvir, nfit);

      qleve::Tensor<1> half(std::max(nocc, nvir) * nao * nfit);

      qleve::TensorView<3> half_ivp(half.data(), nocc, nao, nfit);
      qleve::contract(1.0, B, "uvP", *cc_occ, "ui", 0.0, half_ivp, "ivP");
      qleve::contract(1.0, half_ivp, "ivP", *cc_vir, "va", 0.0, *B_iaP_, "iaP");

      if (exchange_ != 0.0) {
        B_ijP_ = std::make_shared<qleve::Tensor<3>>(nocc, nocc, nfit);
        B_abP_ = std::make_shared<qleve::Tensor<3>>(nvir, nvir, nfit);

        qleve::contract(1.0, half_ivp, "ivP", *cc_occ, "vj", 0.0, *B_ijP_, "ijP");

        qleve::TensorView<3> half_avp(half.data(), nvir, nao, nfit);


        qleve::contract(1.0, B, "uvP", *cc_vir, "ua", 0.0, half_avp, "avP");
        qleve::contract(1.0, half_avp, "avP", *cc_vir, "vb", 0.0, *B_abP_, "abP");
      }
    } else {
      // This whole section only works because Bia is only ever used for coulomb
      // and Bij/Bab are only ever used for exchange in CIS. For RPA, this
      // would be a problem.
      const ptrdiff_t nao = g.orbital_basis()->nbasis();
      const auto nocc = cc_occ->extent(1);
      const auto nvir = cc_vir->extent(1);

      const auto& BJ = g.j_density_fitting()->B();
      const auto njfit = BJ.extent(2);

      const auto& BK = g.k_density_fitting()->B();
      const auto nkfit = BK.extent(2);

      B_iaP_ = std::make_shared<qleve::Tensor<3>>(nocc, nvir, njfit);

      qleve::Tensor<1> half(std::max(nocc, nvir) * nao * std::max(njfit, nkfit));

      qleve::TensorView<3> jhalf_ivp(half.data(), nocc, nao, njfit);
      qleve::contract(1.0, BJ, "uvP", *cc_occ, "ui", 0.0, jhalf_ivp, "ivP");
      qleve::contract(1.0, jhalf_ivp, "ivP", *cc_vir, "va", 0.0, *B_iaP_, "iaP");

      if (exchange_ != 0.0) {
        B_ijP_ = std::make_shared<qleve::Tensor<3>>(nocc, nocc, nkfit);

        qleve::TensorView<3> khalf_ivp(half.data(), nocc, nao, nkfit);
        qleve::contract(1.0, BK, "uvP", *cc_occ, "ui", 0.0, khalf_ivp, "ivP");
        qleve::contract(1.0, khalf_ivp, "ivP", *cc_occ, "vj", 0.0, *B_ijP_, "ijP");

        qleve::TensorView<3> half_avp(half.data(), nvir, nao, nkfit);

        B_abP_ = std::make_shared<qleve::Tensor<3>>(nvir, nvir, nkfit);

        qleve::contract(1.0, BK, "uvP", *cc_vir, "ua", 0.0, half_avp, "avP");
        qleve::contract(1.0, half_avp, "avP", *cc_vir, "vb", 0.0, *B_abP_, "abP");
      }
    }
  }
}

yucca::RCISMatrix::RCISMatrix(const Geometry& g, shared_ptr<qleve::Array> diag,
                              shared_ptr<qleve::Tensor<2>> cc_occ,
                              shared_ptr<qleve::Tensor<2>> cc_vir, const double hartree,
                              const double exchange, const bool use_df) :
    SCFResponseMatrix_(g, diag, cc_occ, cc_vir, hartree, exchange, use_df)
{}

void yucca::RCISMatrix::operator()(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av)
{
  if (use_df_) {
    this->apply_mo(v, Av);
  } else {
    this->apply_ao(v, Av);
  }
}

void yucca::RCISMatrix::apply_ao(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av)
{
  const ptrdiff_t nstate = v.extent(1);
  const auto [nao, nocc] = cc_occ_->shape();
  const ptrdiff_t nvir = cc_vir_->extent(1);
  const ptrdiff_t nph = nocc * nvir;

  // Apply the diagonal term
  for (ptrdiff_t istate = 0; istate < nstate; ++istate) {
    Av.pluck(istate) += (*diag_) * v.const_pluck(istate);
  }

  // Half-transform to AO basis
  qleve::Tensor<3> tmp(nocc, nao, nstate);
  qleve::contract(1.0, v.const_reshape(nocc, nvir, nstate), "iax", *cc_vir_, "va", 0.0, tmp, "ivx");

  // Finish transform to AO basis
  qleve::Tensor<3> vcao(nao, nao, nstate);
  qleve::contract(1.0, tmp, "ivx", *cc_occ_, "ui", 0.0, vcao, "uvx");

  // Collect result in wcao
  qleve::Tensor<3> wcao(nao, nao, nstate);

  const auto shells = libint::get_shells(*geometry_.orbital_basis());
  const auto& bshells = geometry_.orbital_basis()->basisshells();
  auto engine = libint::get_engine(libint2::Operator::coulomb, 0, *geometry_.orbital_basis());

  const size_t nshells = shells.size();

  const auto& buf = engine.results();

  for (ptrdiff_t ish = 0, ijsh = 0; ish < nshells; ++ish) {
    const ptrdiff_t ioff = bshells[ish].offset();
    const ptrdiff_t isize = shells[ish].size();
    for (ptrdiff_t jsh = 0; jsh <= ish; ++jsh, ++ijsh) {
      const ptrdiff_t joff = bshells[jsh].offset();
      const ptrdiff_t jsize = shells[jsh].size();
      for (ptrdiff_t ksh = 0, klsh = 0; ksh < nshells; ++ksh) {
        const ptrdiff_t koff = bshells[ksh].offset();
        const ptrdiff_t ksize = shells[ksh].size();
        for (ptrdiff_t lsh = 0; lsh <= ksh && klsh <= ijsh; ++lsh, ++klsh) {
          const ptrdiff_t loff = bshells[lsh].offset();
          const ptrdiff_t lsize = shells[lsh].size();

          engine.compute(shells[ish], shells[jsh], shells[ksh], shells[lsh]);
          const double* vijkl = buf[0];

          if (vijkl == nullptr)
            continue;

          const double ijfac = ish == jsh ? 0.5 : 1.0;
          const double klfac = lsh == ksh ? 0.5 : 1.0;
          const double ijklfac = ijsh == klsh ? 0.5 : 1.0;

          const double symfac = ijfac * klfac * ijklfac;
          const double jj = 2.0 * hartree_ * symfac;
          const double kk = -exchange_ * symfac;

          for (ptrdiff_t istate = 0; istate < nstate; ++istate) {
            for (size_t i = ioff, ijkl = 0; i < ioff + isize; ++i) {
              for (size_t j = joff; j < joff + jsize; ++j) {
                for (size_t k = koff; k < koff + ksize; ++k) {
                  for (size_t l = loff; l < loff + lsize; ++l, ++ijkl) {
                    wcao(i, j, istate) +=
                        jj * vijkl[ijkl] * (vcao(k, l, istate) + vcao(l, k, istate));
                    wcao(j, i, istate) +=
                        jj * vijkl[ijkl] * (vcao(k, l, istate) + vcao(l, k, istate));
                    wcao(k, l, istate) +=
                        jj * vijkl[ijkl] * (vcao(i, j, istate) + vcao(j, i, istate));
                    wcao(l, k, istate) +=
                        jj * vijkl[ijkl] * (vcao(i, j, istate) + vcao(j, i, istate));

                    wcao(i, k, istate) += kk * vijkl[ijkl] * vcao(j, l, istate);
                    wcao(i, l, istate) += kk * vijkl[ijkl] * vcao(j, k, istate);
                    wcao(j, k, istate) += kk * vijkl[ijkl] * vcao(i, l, istate);
                    wcao(j, l, istate) += kk * vijkl[ijkl] * vcao(i, k, istate);
                    wcao(k, i, istate) += kk * vijkl[ijkl] * vcao(l, j, istate);
                    wcao(k, j, istate) += kk * vijkl[ijkl] * vcao(l, i, istate);
                    wcao(l, i, istate) += kk * vijkl[ijkl] * vcao(k, j, istate);
                    wcao(l, j, istate) += kk * vijkl[ijkl] * vcao(k, i, istate);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  qleve::contract(1.0, wcao, "uvx", *cc_occ_, "ui", 0.0, tmp, "ivx");
  qleve::contract(1.0, tmp, "ivx", *cc_vir_, "va", 1.0, Av.reshape(nocc, nvir, nstate), "iax");
}

void yucca::RCISMatrix::apply_mo(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av)
{
  const ptrdiff_t nstate = v.extent(1);
  const auto [nao, nocc] = cc_occ_->shape();
  const ptrdiff_t nvir = cc_vir_->extent(1);
  const ptrdiff_t nph = nocc * nvir;

  const double jj = 2.0 * hartree_;
  const double kk = -exchange_;

  // Apply the diagonal term
  for (ptrdiff_t istate = 0; istate < nstate; ++istate) {
    Av.pluck(istate) += (*diag_) * v.const_pluck(istate);
  }

  auto vv = v.const_reshape(nocc, nvir, nstate);
  auto av = Av.reshape(nocc, nvir, nstate);

  {
    const ptrdiff_t nfit = B_iaP_->extent(2);
    qleve::Tensor<2> tmp(nfit, nstate);
    qleve::gemm("t", "n", 1.0, B_iaP_->reshape(nph, nfit), v, 0.0, tmp);
    qleve::gemm("n", "n", jj, B_iaP_->reshape(nph, nfit), tmp, 1.0, Av);
  }

  {
    const ptrdiff_t nfit = B_abP_->extent(2);
    assert(B_abP_->extent(2) == B_ijP_->extent(2));
    qleve::Tensor<4> half(nocc, nfit, nvir, nstate); // yikes?
    qleve::contract(1.0, *B_abP_, "abP", vv, "jbm", 0.0, half, "jPam");
    qleve::contract(kk, *B_ijP_, "ijP", half, "jPam", 1.0, av, "iam");
  }
}

qleve::Tensor<2> RCISMatrix::operator()(const qleve::ConstTensorView<2> v)
{
  auto out = v.zeros_like();
  (*this)(v, out);
  return out;
}

// TODO split this file

yucca::RAplusB::RAplusB(const Geometry& g, shared_ptr<qleve::Array> diag,
                        shared_ptr<qleve::Tensor<2>> cc_occ, shared_ptr<qleve::Tensor<2>> cc_vir,
                        const double hartree, const double exchange, const bool use_df) :
    SCFResponseMatrix_(g, diag, cc_occ, cc_vir, hartree, exchange, use_df)
{}

void yucca::RAplusB::operator()(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av)
{
  if (use_df_) {
    this->apply_mo(v, Av);
  } else {
    this->apply_ao(v, Av);
  }
}

void yucca::RAplusB::apply_ao(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av)
{
  const ptrdiff_t nstate = v.extent(1);
  const auto [nao, nocc] = cc_occ_->shape();
  const ptrdiff_t nvir = cc_vir_->extent(1);
  const ptrdiff_t nph = nocc * nvir;

  // Apply the diagonal term
  for (ptrdiff_t istate = 0; istate < nstate; ++istate) {
    Av.pluck(istate) += (*diag_) * v.const_pluck(istate);
  }

  // Half-transform to AO basis
  qleve::Tensor<3> tmp(nocc, nao, nstate);
  qleve::contract(1.0, v.const_reshape(nocc, nvir, nstate), "iax", *cc_vir_, "va", 0.0, tmp, "ivx");

  // Finish transform to AO basis
  qleve::Tensor<3> vcao(nao, nao, nstate);
  qleve::contract(1.0, tmp, "ivx", *cc_occ_, "ui", 0.0, vcao, "uvx");

  // Collect result in wcao
  qleve::Tensor<3> wcao(nao, nao, nstate);

  const auto shells = libint::get_shells(*geometry_.orbital_basis());
  const auto& bshells = geometry_.orbital_basis()->basisshells();
  auto engine = libint::get_engine(libint2::Operator::coulomb, 0, *geometry_.orbital_basis());

  const size_t nshells = shells.size();

  const auto& buf = engine.results();

  for (ptrdiff_t ish = 0, ijsh = 0; ish < nshells; ++ish) {
    const ptrdiff_t ioff = bshells[ish].offset();
    const ptrdiff_t isize = shells[ish].size();
    for (ptrdiff_t jsh = 0; jsh <= ish; ++jsh, ++ijsh) {
      const ptrdiff_t joff = bshells[jsh].offset();
      const ptrdiff_t jsize = shells[jsh].size();
      for (ptrdiff_t ksh = 0, klsh = 0; ksh < nshells; ++ksh) {
        const ptrdiff_t koff = bshells[ksh].offset();
        const ptrdiff_t ksize = shells[ksh].size();
        for (ptrdiff_t lsh = 0; lsh <= ksh && klsh <= ijsh; ++lsh, ++klsh) {
          const ptrdiff_t loff = bshells[lsh].offset();
          const ptrdiff_t lsize = shells[lsh].size();

          engine.compute(shells[ish], shells[jsh], shells[ksh], shells[lsh]);
          const double* vijkl = buf[0];

          if (vijkl == nullptr)
            continue;

          const double ijfac = ish == jsh ? 0.5 : 1.0;
          const double klfac = lsh == ksh ? 0.5 : 1.0;
          const double ijklfac = ijsh == klsh ? 0.5 : 1.0;

          const double symfac = ijfac * klfac * ijklfac;
          const double jj = 4.0 * hartree_ * symfac;
          const double kk = -exchange_ * symfac;

          for (ptrdiff_t istate = 0; istate < nstate; ++istate) {
            for (size_t i = ioff, ijkl = 0; i < ioff + isize; ++i) {
              for (size_t j = joff; j < joff + jsize; ++j) {
                for (size_t k = koff; k < koff + ksize; ++k) {
                  for (size_t l = loff; l < loff + lsize; ++l, ++ijkl) {
                    // (ij|kl) Xkl
                    wcao(i, j, istate) +=
                        jj * vijkl[ijkl] * (vcao(k, l, istate) + vcao(l, k, istate));
                    wcao(j, i, istate) +=
                        jj * vijkl[ijkl] * (vcao(k, l, istate) + vcao(l, k, istate));
                    // (kl|ij) Xij
                    wcao(k, l, istate) +=
                        jj * vijkl[ijkl] * (vcao(i, j, istate) + vcao(j, i, istate));
                    wcao(l, k, istate) +=
                        jj * vijkl[ijkl] * (vcao(i, j, istate) + vcao(j, i, istate));

                    // terms like (ij|ab) Xjb -> Wia
                    wcao(i, k, istate) += kk * vijkl[ijkl] * vcao(j, l, istate);
                    wcao(i, l, istate) += kk * vijkl[ijkl] * vcao(j, k, istate);
                    wcao(j, k, istate) += kk * vijkl[ijkl] * vcao(i, l, istate);
                    wcao(j, l, istate) += kk * vijkl[ijkl] * vcao(i, k, istate);
                    // terms like (ab|ij) Xjb -> Wia
                    wcao(k, i, istate) += kk * vijkl[ijkl] * vcao(l, j, istate);
                    wcao(k, j, istate) += kk * vijkl[ijkl] * vcao(l, i, istate);
                    wcao(l, i, istate) += kk * vijkl[ijkl] * vcao(k, j, istate);
                    wcao(l, j, istate) += kk * vijkl[ijkl] * vcao(k, i, istate);

                    // terms like (ib|ja) Xjb -> Wia
                    wcao(i, l, istate) += kk * vijkl[ijkl] * vcao(k, j, istate);
                    wcao(j, l, istate) += kk * vijkl[ijkl] * vcao(k, i, istate);
                    wcao(i, k, istate) += kk * vijkl[ijkl] * vcao(l, j, istate);
                    wcao(j, k, istate) += kk * vijkl[ijkl] * vcao(l, i, istate);
                    // terms like (ja|ib) Xjb -> Wia
                    wcao(k, j, istate) += kk * vijkl[ijkl] * vcao(i, l, istate);
                    wcao(k, i, istate) += kk * vijkl[ijkl] * vcao(j, l, istate);
                    wcao(l, j, istate) += kk * vijkl[ijkl] * vcao(i, k, istate);
                    wcao(l, i, istate) += kk * vijkl[ijkl] * vcao(j, k, istate);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  qleve::contract(1.0, wcao, "uvx", *cc_occ_, "ui", 0.0, tmp, "ivx");
  qleve::contract(1.0, tmp, "ivx", *cc_vir_, "va", 1.0, Av.reshape(nocc, nvir, nstate), "iax");
}

void yucca::RAplusB::apply_mo(const qleve::ConstTensorView<2> v, qleve::TensorView<2> Av)
{
  const ptrdiff_t nstate = v.extent(1);
  const auto [nao, nocc] = cc_occ_->shape();
  const ptrdiff_t nvir = cc_vir_->extent(1);
  const ptrdiff_t nph = nocc * nvir;
  const ptrdiff_t nfit = B_iaP_->extent(2);

  const double jj = 4.0 * hartree_;
  const double kk = -exchange_;

  // Apply the diagonal term
  for (ptrdiff_t istate = 0; istate < nstate; ++istate) {
    Av.pluck(istate) += (*diag_) * v.const_pluck(istate);
  }

  auto vv = v.const_reshape(nocc, nvir, nstate);
  auto av = Av.reshape(nocc, nvir, nstate);

  { // (ia|jb)
    qleve::Tensor<2> tmp(nfit, nstate);
    qleve::gemm("t", "n", 1.0, B_iaP_->reshape(nph, nfit), v, 0.0, tmp);
    qleve::gemm("n", "n", jj, B_iaP_->reshape(nph, nfit), tmp, 1.0, Av);
  }

  {                                                  // (ij|ab)
    qleve::Tensor<4> half(nocc, nfit, nvir, nstate); // yikes?
    qleve::contract(1.0, *B_abP_, "abP", vv, "jbm", 0.0, half, "jPam");
    qleve::contract(kk, *B_ijP_, "ijP", half, "jPam", 1.0, av, "iam");
  }

  {                                                  // (ib|ja)
    qleve::Tensor<4> half(nocc, nfit, nocc, nstate); // yikes?
    qleve::contract(1.0, *B_iaP_, "ibP", vv, "jbm", 0.0, half, "jPim");
    qleve::contract(kk, *B_iaP_, "jaP", half, "jPim", 1.0, av, "iam");
  }
}

qleve::Tensor<2> RAplusB::operator()(const qleve::ConstTensorView<2> v)
{
  auto out = v.zeros_like();
  (*this)(v, out);
  return out;
}
