#!/usr/bin/env bash

. /opt/intel/oneapi/setvars.sh
git config --global --add safe.directory /workspaces/yucca/external/cxxopts
git config --global --add safe.directory /workspaces/yucca/external/fmt
git config --global --add safe.directory /workspaces/yucca/external/yaml-cpp
git config --global --add safe.directory /workspaces/yucca/external/MDI_Library
git config --global --add safe.directory /workspaces/yucca/qleve
git submodule set-url qleve https://gitlab.com/team-parker/qleve.git
cmake -S . -B build -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_NO_SYSTEM_FROM_IMPORTED=ON
cmake --build build

