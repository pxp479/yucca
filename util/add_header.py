#!/usr/bin/env python
"""Add License

This script replaces the top C-style comments on a page with
the MPL 2.0 header and a Doxygen style name for the page
"""

from __future__ import print_function

import argparse
import re
import os

def get_base_path(filename):
    """Returns the source base path of a file

    Steps backwards up the directory tree until a folder with ".git"
    is found and returns
    """
    head, tail = os.path.split(os.path.abspath(filename))
    while head != "":
        if os.path.isdir(os.path.join(head, ".git")):
            return head
        else:
            head, tail = os.path.split(head)
    raise Exception("Could not find base path because no .git found anywhere!")

def get_rel_path(filename):
    """Returns the path of a file relative to base/src

    Steps backwards up the directory tree until a folder with ".git"
    is found and calls that the base
    """
    basepath = get_base_path(filename)

    relpath = os.path.relpath(filename, basepath)
    return relpath

def print_header(filehandle, relpath, dox=True):
    print('// This Source Code Form is subject to the terms of the Mozilla Public', file=filehandle)
    print('// License, v. 2.0. If a copy of the MPL was not distributed with this', file=filehandle)
    print('// file, You can obtain one at http://mozilla.org/MPL/2.0/.', file=filehandle)
    print('// -------------------------------------------------------------------', file=filehandle)
    if dox:
        print(r'/// \file {0}'.format(relpath), file=filehandle)
        print(r'///', file=filehandle)
        print(r'/// A brief description of the contents of this file', file=filehandle)

def update_header(filename, inplace=False):
    tmpfile = "{0}.tmp".format(filename)
    relpath = get_rel_path(filename)
    with open(filename, "r") as f, open(tmpfile, "w") as fout:
        at_start = True
        has_dox = False
        found_comment_or_blank = False
        for line in f:
            if at_start and re.match(r"^///", line) and not found_comment_or_blank:
                # skip this line
                continue
            elif at_start and re.match(r"^//", line) and (not re.match(r"^///", line)):
                # skip this line
                found_comment_or_blank = True
                continue
            else:
                if re.match(r"^///", line):
                    has_dox = True

                if at_start: # this is first noncomment line
                    print_header(fout, relpath, dox=not has_dox)
                    at_start = False
                print(line, file=fout, end='')

    if inplace:
        os.remove(filename)
        os.rename(tmpfile, filename)

def main():
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("files", nargs='+', type=str, help='files to update')
    parser.add_argument("-i", "--in-place", action='store_true', help='update file in-place')

    args = parser.parse_args()

    for f in args.files:
        print("Updating header of file {0}".format(f))
        update_header(f, args.in_place)

if __name__ == "__main__":
    main()
